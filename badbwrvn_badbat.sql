-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 11 jan. 2023 à 04:59
-- Version du serveur : 10.5.18-MariaDB-0+deb11u1
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `badbwrvn_badbat`
--

-- --------------------------------------------------------

--
-- Structure de la table `table_batteries`
--

CREATE TABLE `table_batteries` (
  `id_batteries` int(11) NOT NULL,
  `references_base` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `etats` int(11) NOT NULL,
  `lieux` int(11) NOT NULL DEFAULT 1,
  `equipements` int(11) NOT NULL,
  `reference` int(11) NOT NULL,
  `technologie` int(11) NOT NULL,
  `date_mes` int(11) NOT NULL,
  `date_fin` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_derniere_operation` int(11) NOT NULL,
  `type_operation` int(11) NOT NULL,
  `dernier_evenement` int(11) NOT NULL,
  `date_dernier_evenement` int(11) NOT NULL,
  `declencheur_dernier_evenement` int(11) NOT NULL,
  `fabricant` int(11) NOT NULL,
  `fournisseur` int(11) NOT NULL,
  `longueur` decimal(10,0) NOT NULL,
  `largeur` decimal(10,0) NOT NULL,
  `hauteur` decimal(10,0) NOT NULL,
  `cosses` int(11) NOT NULL,
  `raccordement` int(11) NOT NULL,
  `tension_reseau` int(11) NOT NULL,
  `tension_batterie` int(11) NOT NULL COMMENT 'tension de référence',
  `nombre_batterie` int(11) NOT NULL,
  `tension_element` decimal(10,0) NOT NULL,
  `tension_floating_element` decimal(10,0) NOT NULL,
  `nombre_element` int(11) NOT NULL,
  `capacite_batterie` int(11) NOT NULL,
  `autonomie_mesuree` int(11) NOT NULL,
  `autonomie_requise` int(11) NOT NULL,
  `date_mesure_autonomie` int(11) NOT NULL,
  `largeur_chantier` decimal(10,0) NOT NULL,
  `longueur_chantier` decimal(10,0) NOT NULL,
  `hauteur_chantier` decimal(10,0) NOT NULL,
  `courbe` int(11) NOT NULL COMMENT 'id_courbe',
  `divers_batterie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `table_batteries`
--

INSERT INTO `table_batteries` (`id_batteries`, `references_base`, `etats`, `lieux`, `equipements`, `reference`, `technologie`, `date_mes`, `date_fin`, `date_derniere_operation`, `type_operation`, `dernier_evenement`, `date_dernier_evenement`, `declencheur_dernier_evenement`, `fabricant`, `fournisseur`, `longueur`, `largeur`, `hauteur`, `cosses`, `raccordement`, `tension_reseau`, `tension_batterie`, `nombre_batterie`, `tension_element`, `tension_floating_element`, `nombre_element`, `capacite_batterie`, `autonomie_mesuree`, `autonomie_requise`, `date_mesure_autonomie`, `largeur_chantier`, `longueur_chantier`, `hauteur_chantier`, `courbe`, `divers_batterie`) VALUES
(1, '', 6, 2, 8, 0, 0, 0, '0000-00-00 00:00:00', 1620604800, 4, 0, 1621330026, 0, 0, 0, '0', '0', '0', 0, 0, 2, 0, 0, '0', '0', 0, 0, 0, 4, 0, '0', '0', '0', 0, ''),
(2, '', 4, 3, 3, 0, 0, 0, '0000-00-00 00:00:00', 1619913600, 4, 0, 1621345734, 0, 0, 0, '0', '0', '0', 0, 0, 2, 0, 0, '0', '0', 0, 0, 0, 4, 0, '0', '0', '0', 0, ''),
(3, '', -1, 2, 7, 0, 0, 0, '0000-00-00 00:00:00', 1620604800, 4, 0, 1621330026, 0, 0, 0, '0', '0', '0', 0, 0, 2, 0, 0, '0', '0', 0, 0, 0, 3, 0, '0', '0', '0', 0, ''),
(4, '', 4, 4, 3, 0, 0, 0, '0000-00-00 00:00:00', 1619913600, 4, 0, 1621345734, 0, 0, 0, '0', '0', '0', 0, 0, 4, 0, 0, '0', '0', 0, 0, 372, 300, 1620604800, '0', '0', '0', 0, ''),
(5, '', 4, 5, 4, 0, 0, 0, '0000-00-00 00:00:00', 1620604800, 4, 0, 1621345734, 0, 0, 0, '0', '0', '0', 0, 0, 4, 0, 0, '0', '0', 0, 0, 305, 6, 1620432000, '0', '0', '0', 0, ''),
(6, '', 4, 7, 7, 0, 0, 0, '0000-00-00 00:00:00', 1620691200, 2, 0, 1621345734, 0, 0, 0, '0', '0', '0', 0, 0, 5, 0, 0, '0', '0', 0, 0, 364, 604, 1620691200, '0', '0', '0', 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `table_declencheurs_temporels`
--

CREATE TABLE `table_declencheurs_temporels` (
  `id_declencheur` int(11) NOT NULL,
  `nom_declencheur` varchar(255) NOT NULL,
  `delai_declencheur` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `delai_prevenance_declencheur` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation_declencheur` tinyint(1) NOT NULL DEFAULT 0,
  `divers_declencheur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_declencheurs_temporels`
--

INSERT INTO `table_declencheurs_temporels` (`id_declencheur`, `nom_declencheur`, `delai_declencheur`, `delai_prevenance_declencheur`, `activation_declencheur`, `divers_declencheur`) VALUES
(1, 'non défini', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'non défini'),
(2, 'déclencheur 1', '2020-06-08 08:14:45', '2020-06-08 08:14:45', 1, 'une échéance est arrivée'),
(3, 'déclencheur 2', '2020-06-08 08:14:45', '2020-06-08 08:14:45', 1, 'une échéance 2 est arrivée');

-- --------------------------------------------------------

--
-- Structure de la table `table_equipements`
--

CREATE TABLE `table_equipements` (
  `id_equipement` int(11) NOT NULL,
  `nom_equipement` varchar(255) NOT NULL,
  `divers_equipement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `table_equipements`
--

INSERT INTO `table_equipements` (`id_equipement`, `nom_equipement`, `divers_equipement`) VALUES
(1, 'non défini', 'non défini'),
(3, 'EQUIPEMENT 2', 'DME seul'),
(4, 'EQUIPEMENT 3', 'equipement 3'),
(5, 'EQUIPEMENT 4', 'equipement 4'),
(7, 'EQUIPEMENT 5', 'equipement 5'),
(8, 'ESSAI', '');

-- --------------------------------------------------------

--
-- Structure de la table `table_etats`
--

CREATE TABLE `table_etats` (
  `id_etat` int(11) NOT NULL,
  `nom_etat` varchar(255) NOT NULL,
  `valeur_etat` int(11) NOT NULL,
  `divers_etat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_etats`
--

INSERT INTO `table_etats` (`id_etat`, `nom_etat`, `valeur_etat`, `divers_etat`) VALUES
(1, 'non défini', 0, 'non défini'),
(2, 'ETAT INITIAL', 100, 'à la réception de la batterie'),
(3, 'OPERATIONELLE', 100, 'batterie vérifiées et disponible'),
(4, 'À VÉRIFIER', 100, 'Une décharge doit être faite'),
(6, 'DÉCLASSE', 100, 'mise à la décharge');

-- --------------------------------------------------------

--
-- Structure de la table `table_evenements`
--

CREATE TABLE `table_evenements` (
  `id_evenement` int(11) NOT NULL,
  `nom_evenement` varchar(255) NOT NULL,
  `id_etat_precedent_evenement` int(11) NOT NULL,
  `id_etat_suivant_evenement` int(11) NOT NULL,
  `presence_temps_maintien_etat` int(11) NOT NULL DEFAULT 0,
  `temps_maintien_etat` int(11) NOT NULL,
  `presence_utilisateur_fin_maintien` int(11) NOT NULL DEFAULT 0,
  `id_utilisateur_fin_maintien` int(11) NOT NULL DEFAULT 1,
  `presence_temps_prevenance_etat` int(11) NOT NULL DEFAULT 0,
  `temps_prevenance_changement_etat` int(11) NOT NULL COMMENT 'c''est le temps avant le changement d''état',
  `presence_utilisateur_fin_prevenance` int(11) NOT NULL DEFAULT 0,
  `id_utilisateur_fin_prevenance` int(11) NOT NULL DEFAULT 1,
  `divers_evenement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_evenements`
--

INSERT INTO `table_evenements` (`id_evenement`, `nom_evenement`, `id_etat_precedent_evenement`, `id_etat_suivant_evenement`, `presence_temps_maintien_etat`, `temps_maintien_etat`, `presence_utilisateur_fin_maintien`, `id_utilisateur_fin_maintien`, `presence_temps_prevenance_etat`, `temps_prevenance_changement_etat`, `presence_utilisateur_fin_prevenance`, `id_utilisateur_fin_prevenance`, `divers_evenement`) VALUES
(1, 'non défini', 1, 1, 0, -1, 0, 1, 0, -1, 0, 1, 'non défini'),
(6, 'JOUR A', 4, 3, 1, 86400, 0, 0, 0, 0, 1, 2, 'passage au jour A'),
(9, 'JOUR B', 3, 4, 1, 86400, 1, 2, 1, 0, 1, 2, '');

-- --------------------------------------------------------

--
-- Structure de la table `table_historique`
--

CREATE TABLE `table_historique` (
  `id_historique` int(11) NOT NULL,
  `id_batterie_historique` int(11) NOT NULL,
  `date_historique` int(11) NOT NULL,
  `id_etat_actuel_historique` int(11) NOT NULL,
  `id_etat_precedent_historique` int(11) NOT NULL,
  `id_evenement_historique` int(11) NOT NULL,
  `id_operation` int(11) NOT NULL,
  `divers_historique` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `table_lieux`
--

CREATE TABLE `table_lieux` (
  `id_lieux` int(11) NOT NULL,
  `nom_lieux` varchar(20) NOT NULL,
  `divers_lieux` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_lieux`
--

INSERT INTO `table_lieux` (`id_lieux`, `nom_lieux`, `divers_lieux`) VALUES
(1, 'non défini', 'non défini'),
(2, 'LIEUX 4', 'VOR de quimper'),
(3, 'LIEUX 2', ''),
(4, 'LIEUX 1', 'C\'est l\'antenne avancée de Hanvec\nadresse: lieu dit labou dirimeur , 29800 Hanvec'),
(5, 'LIEUX 3', ''),
(6, 'ESSAI', ''),
(7, 'ASASASASASAS', '');

-- --------------------------------------------------------

--
-- Structure de la table `table_operations`
--

CREATE TABLE `table_operations` (
  `id_operation` int(11) NOT NULL,
  `nom_operation` varchar(255) NOT NULL,
  `mesure_autonomie_check` int(11) NOT NULL DEFAULT 0,
  `divers_operation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_operations`
--

INSERT INTO `table_operations` (`id_operation`, `nom_operation`, `mesure_autonomie_check`, `divers_operation`) VALUES
(1, 'non défini', 0, 'non défini'),
(2, 'DECHARGE BATTERIE', 1, 'réalisation d\'une décharge batterie\n'),
(3, 'TEST BATTERIE', 1, 'on vérifie la tension\n'),
(4, 'TEST TEMPÉRATURE', 0, 'mesure de la température');

-- --------------------------------------------------------

--
-- Structure de la table `table_tension_batteries`
--

CREATE TABLE `table_tension_batteries` (
  `id_tension_batterie` int(11) NOT NULL,
  `nom_tension_batterie` varchar(255) NOT NULL,
  `valeur_tension_batterie` float NOT NULL,
  `divers_tension_batterie` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_tension_batteries`
--

INSERT INTO `table_tension_batteries` (`id_tension_batterie`, `nom_tension_batterie`, `valeur_tension_batterie`, `divers_tension_batterie`) VALUES
(1, 'non défini', 0, 'non défini'),
(2, '12V', 100, '');

-- --------------------------------------------------------

--
-- Structure de la table `table_tension_reseaux`
--

CREATE TABLE `table_tension_reseaux` (
  `id_tension_reseau` int(11) NOT NULL,
  `nom_tension_reseau` varchar(255) NOT NULL,
  `valeur_tension_reseau` decimal(5,2) DEFAULT NULL,
  `divers_tension_reseau` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_tension_reseaux`
--

INSERT INTO `table_tension_reseaux` (`id_tension_reseau`, `nom_tension_reseau`, `valeur_tension_reseau`, `divers_tension_reseau`) VALUES
(1, 'non défini', NULL, 'non défini'),
(2, 'RADIO RÉSEAU A', '24.00', 'réseau A\nC est le réseau principal de la distribution électrique des antennes avancées car les automates sont branchés sur ce réseau'),
(3, 'RADIO RÉSEAU B', '24.00', 'radio réseau B'),
(4, 'TELECOM', '-48.00', 'créé depuis le 24V et le 230V\n'),
(5, 'AASAA', '12.00', 'sa');

-- --------------------------------------------------------

--
-- Structure de la table `table_utilisateurs`
--

CREATE TABLE `table_utilisateurs` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(255) NOT NULL,
  `prenom_utilisateur` varchar(255) NOT NULL,
  `mail_utilisateur` varchar(255) NOT NULL,
  `tel_utilisateur` varchar(255) NOT NULL,
  `divers_utilisateur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `table_utilisateurs`
--

INSERT INTO `table_utilisateurs` (`id_utilisateur`, `nom_utilisateur`, `prenom_utilisateur`, `mail_utilisateur`, `tel_utilisateur`, `divers_utilisateur`) VALUES
(1, 'non défini', 'non défini', '', '', 'non défini'),
(2, 'AAAA', 'aaaa', 'aaaa@aaa.com', '', 'c\'est le premier utilisateur'),
(3, 'BBBB', 'bbbb', 'bbb@bbb.com', '', 'c\'est le second utilisateur'),
(5, 'CCCC', 'cccc', 'cccc@cc.com', '', 'c\'est le dernier utilisateur'),
(6, 'VIENNOT', 'Nicolas', 'nicolas.viennot@monmail.com', '', 'essai');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `table_batteries`
--
ALTER TABLE `table_batteries`
  ADD PRIMARY KEY (`id_batteries`);

--
-- Index pour la table `table_declencheurs_temporels`
--
ALTER TABLE `table_declencheurs_temporels`
  ADD PRIMARY KEY (`id_declencheur`);

--
-- Index pour la table `table_equipements`
--
ALTER TABLE `table_equipements`
  ADD PRIMARY KEY (`id_equipement`);

--
-- Index pour la table `table_etats`
--
ALTER TABLE `table_etats`
  ADD PRIMARY KEY (`id_etat`);

--
-- Index pour la table `table_evenements`
--
ALTER TABLE `table_evenements`
  ADD PRIMARY KEY (`id_evenement`);

--
-- Index pour la table `table_historique`
--
ALTER TABLE `table_historique`
  ADD PRIMARY KEY (`id_historique`);

--
-- Index pour la table `table_lieux`
--
ALTER TABLE `table_lieux`
  ADD PRIMARY KEY (`id_lieux`);

--
-- Index pour la table `table_operations`
--
ALTER TABLE `table_operations`
  ADD PRIMARY KEY (`id_operation`);

--
-- Index pour la table `table_tension_batteries`
--
ALTER TABLE `table_tension_batteries`
  ADD PRIMARY KEY (`id_tension_batterie`);

--
-- Index pour la table `table_tension_reseaux`
--
ALTER TABLE `table_tension_reseaux`
  ADD PRIMARY KEY (`id_tension_reseau`);

--
-- Index pour la table `table_utilisateurs`
--
ALTER TABLE `table_utilisateurs`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `table_batteries`
--
ALTER TABLE `table_batteries`
  MODIFY `id_batteries` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `table_declencheurs_temporels`
--
ALTER TABLE `table_declencheurs_temporels`
  MODIFY `id_declencheur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `table_equipements`
--
ALTER TABLE `table_equipements`
  MODIFY `id_equipement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `table_etats`
--
ALTER TABLE `table_etats`
  MODIFY `id_etat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `table_evenements`
--
ALTER TABLE `table_evenements`
  MODIFY `id_evenement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `table_historique`
--
ALTER TABLE `table_historique`
  MODIFY `id_historique` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `table_lieux`
--
ALTER TABLE `table_lieux`
  MODIFY `id_lieux` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `table_operations`
--
ALTER TABLE `table_operations`
  MODIFY `id_operation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `table_tension_batteries`
--
ALTER TABLE `table_tension_batteries`
  MODIFY `id_tension_batterie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `table_tension_reseaux`
--
ALTER TABLE `table_tension_reseaux`
  MODIFY `id_tension_reseau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `table_utilisateurs`
--
ALTER TABLE `table_utilisateurs`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
