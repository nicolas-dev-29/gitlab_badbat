<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs = "SELECT `id_batteries`,`references_base`,`reference`,
	`id_tension_reseau`,`nom_tension_reseau`,`valeur_tension_reseau`,`divers_tension_reseau`,
	`nom_equipement`,`divers_equipement`,
	`nom_lieux`,`date_mes`,
	`divers_lieux`,`equipements`,`lieux`,`id_etat`,`nom_etat`,`divers_etat`,`nom_evenement`,
	`date_dernier_evenement`,`nom_operation`,`date_derniere_operation`,
	`autonomie_mesuree`,`autonomie_requise`,`date_mesure_autonomie`,
	`longueur`,`largeur`,`hauteur`,`longueur_chantier`,`largeur_chantier`,`hauteur_chantier`,
	`id_fabricant`, `nom_fabricant`, `adresse_fabricant`, `mail_fabricant`, `telephone_fabricant`, `divers_fabricant` 
	FROM table_batteries T
	LEFT JOIN table_lieux	L
		ON T.lieux = L.id_lieux
	LEFT JOIN table_equipements E
		ON T.equipements = E.id_equipement
	LEFT JOIN table_tension_reseaux	TR
		ON T.tension_reseau = TR.id_tension_reseau
	LEFT JOIN table_evenements TE
		ON T.dernier_evenement = TE.id_evenement
	LEFT JOIN table_etats TET
		ON T.etats = TET.id_etat
	LEFT JOIN table_operations TOP
		ON T.type_operation = TOP.id_operation
	LEFT JOIN table_fabricants TFAB
		ON T.fabricant = TFAB.id_fabricant
	WHERE `id_batteries` = ?
	";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation
	$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
	if($stmt_valeurs)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	
			{
				//  les données sont valides
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{
				//execution
				if(mysqli_stmt_execute($stmt_valeurs))	
				{
					mysqli_stmt_store_result($stmt_valeurs);
					$nbre = mysqli_stmt_num_rows($stmt_valeurs);
						if($nbre == 1)
						{
						    mysqli_stmt_bind_result($stmt_valeurs,$ligne['id_batteries'],$ligne['reference_base'],$ligne['reference'],						    
							$ligne['id_tension_reseau'],$ligne['nom_tension_reseau'],$ligne['valeur_tension_reseau'],$ligne['divers_tension_reseau'],
							$ligne['nom_equipement'],$ligne['divers_equipement'],
							$ligne['nom_lieux'],$ligne['date_mes'],$ligne['divers_lieux'],$ligne['id_equipements'],$ligne['id_lieux'],
							$ligne['id_etat'],$ligne['nom_etat'],$ligne['divers_etat'],$ligne['nom_evenement'],
							$ligne['date_dernier_evenement'],$ligne['nom_operation'],$ligne['date_derniere_operation'],
							$ligne['autonomie_mesuree'],$ligne['autonomie_requise'],$ligne['autonomie_date_derniere_mesure'],
							$ligne['dimension_longueur'],$ligne['dimension_largeur'],$ligne['dimension_hauteur'],
							$ligne['dimension_longueur_chantier'],$ligne['dimension_largeur_chantier'],$ligne['dimension_hauteur_chantier'],
							$ligne['id_fabricant'],$ligne['nom_fabricant'],$ligne['adresse_fabricant'],$ligne['mail_fabricant'],$ligne['telephone_fabricant'],$ligne['divers_fabricant']
							);
							//$index=0;
							while(mysqli_stmt_fetch($stmt_valeurs))
							{
							$data['resultat'] = $msg['code_ok']['id'];
							$data['id'] = 							htmlentities($ligne['id_batteries'],ENT_QUOTES,'UTF-8');
							$data['reference_base'] = 				htmlentities($ligne['reference_base'],ENT_QUOTES,'UTF-8');
							$data['reference_industrielle'] = 	    htmlspecialchars($ligne['reference'],ENT_QUOTES,'UTF-8');
							//tension réseau
							$data['id_tension'] = 					htmlentities($ligne['id_tension_reseau'],ENT_QUOTES,'UTF-8');
							$data['nom_tension_reseau'] = 			htmlspecialchars($ligne['nom_tension_reseau'],ENT_NOQUOTES,'UTF-8');
							$data['valeur_tension_reseau'] = 		htmlentities($ligne['valeur_tension_reseau'],ENT_QUOTES,'UTF-8');
							$data['divers_tension_reseau'] = 		htmlspecialchars($ligne['divers_tension_reseau'],ENT_NOQUOTES,'UTF-8');
							// equipements
							$data['id_equipement'] = 				htmlentities($ligne['id_equipements'],ENT_QUOTES,'UTF-8');
							$data['nom_equipement'] = 				htmlspecialchars($ligne['nom_equipement'],ENT_NOQUOTES,'UTF-8');
							$data['divers_equipement'] = 			htmlspecialchars($ligne['divers_equipement'],ENT_NOQUOTES,'UTF-8');
							//lieux
							$data['id_lieux'] = 					htmlentities($ligne['id_lieux'],ENT_QUOTES,'UTF-8');
							$data['nom_lieux'] = 					htmlspecialchars($ligne['nom_lieux'],ENT_NOQUOTES,'UTF-8');
							$data['divers_lieux'] = 				$ligne['divers_lieux'];//htmlspecialchars($ligne['divers_lieux'],ENT_NOQUOTES,'UTF-8');
							

							
							//etats
							$data['id_etat'] = 						htmlentities($ligne['id_etat'],ENT_QUOTES,'UTF-8');
							$data['nom_etat'] = 					htmlspecialchars($ligne['nom_etat'],ENT_NOQUOTES,'UTF-8');
							$data['divers_etat'] = 					htmlspecialchars($ligne['divers_etat'],ENT_QUOTES,'UTF-8');
							//évenements
							$data['nom_evenement'] = 				htmlspecialchars($ligne['nom_evenement'],ENT_NOQUOTES,'UTF-8');
							$data['date_dernier_evenement'] = 		htmlentities($ligne['date_dernier_evenement'],ENT_QUOTES,'UTF-8');
							
							//opérations
							$data['nom_operation'] = 				htmlspecialchars($ligne['nom_operation'],ENT_NOQUOTES,'UTF-8');
							$data['date_derniere_operation'] = 		htmlentities($ligne['date_derniere_operation'],ENT_QUOTES,'UTF-8');
							
							//autonomie
							$data['autonomie_mesuree'] = 					htmlentities($ligne['autonomie_mesuree'],ENT_QUOTES,'UTF-8');
							$data['autonomie_requise'] = 					htmlentities($ligne['autonomie_requise'],ENT_QUOTES,'UTF-8');
							$data['autonomie_date_derniere_mesure'] = 		htmlentities($ligne['autonomie_date_derniere_mesure'],ENT_QUOTES,'UTF-8');
							
							//dimensions
							$data['dimension_longueur'] = 					htmlentities($ligne['dimension_longueur'], ENT_QUOTES, 'UTF-8');
							$data['dimension_largeur'] = 					htmlentities($ligne['dimension_largeur'], ENT_QUOTES, 'UTF-8');
							$data['dimension_hauteur'] = 					htmlentities($ligne['dimension_hauteur'], ENT_QUOTES, 'UTF-8');
							
							//dimensions du chantier
							$data['dimension_longueur_chantier'] = 					htmlentities($ligne['dimension_longueur_chantier'], ENT_QUOTES, 'UTF-8');
							$data['dimension_largeur_chantier'] = 					htmlentities($ligne['dimension_largeur_chantier'], ENT_QUOTES, 'UTF-8');
							$data['dimension_hauteur_chantier'] = 					htmlentities($ligne['dimension_hauteur_chantier'], ENT_QUOTES, 'UTF-8');
							//fabricant
							$data['id_fabricant'] = 								htmlentities($ligne['id_fabricant'], ENT_QUOTES, 'UTF-8');
							$data['nom_fabricant'] = 								htmlentities($ligne['nom_fabricant'], ENT_QUOTES, 'UTF-8');
							$data['adresse_fabricant'] = 							htmlentities($ligne['adresse_fabricant'], ENT_QUOTES, 'UTF-8');
							$data['mail_fabricant'] = 								htmlentities($ligne['mail_fabricant'], ENT_QUOTES, 'UTF-8');
							$data['telephone_fabricant'] = 							htmlentities($ligne['telephone_fabricant'], ENT_QUOTES, 'UTF-8');
							$data['divers_fabricant'] = 							htmlentities($ligne['divers_fabricant'], ENT_QUOTES, 'UTF-8');

							//message
							$data['date_mes'] = 					htmlentities($ligne['date_mes'],ENT_QUOTES,'UTF-8');
							}
					}
					else
					{
					// id non trouvé ou non unique
					$data['resultat'] = $msg['code_echec_07']['id'];
					}
				}
				else 	
				{	//échec de l'exécution
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}
		else
		{
			//erreur de POST
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_valeurs);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>