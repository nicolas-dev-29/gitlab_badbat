<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_fabricant`,`nom_fabricant`,
	`adresse_fabricant`,`mail_fabricant`,`telephone_fabricant`,`divers_fabricant` FROM table_fabricants ORDER BY `nom_fabricant` ASC";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_fabricant'],$ligne['nom_fabricant'],
			$ligne['adresse_fabricant'],$ligne['mail_fabricant'],$ligne['telephone_fabricant'],$ligne['divers_fabricant']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['resultat'] = 		$msg['code_ok']['id'];
				$data[$index]['id'] = 				htmlentities($ligne['id_fabricant'],ENT_QUOTES,'UTF-8');
				$data[$index]['nom'] = 				htmlspecialchars($ligne['nom_fabricant'],ENT_QUOTES,'UTF-8');
				$data[$index]['adresse'] = 			htmlspecialchars($ligne['adresse_fabricant'],ENT_QUOTES,'UTF-8');
				$data[$index]['mail'] = 			htmlspecialchars($ligne['mail_fabricant'],ENT_QUOTES,'UTF-8');
				$data[$index]['telephone'] = 		htmlspecialchars($ligne['telephone_fabricant'],ENT_QUOTES,'UTF-8');
				$data[$index]['divers'] = 			htmlentities($ligne['divers_fabricant'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>