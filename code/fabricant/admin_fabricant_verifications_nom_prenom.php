<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requête
	$requete_verification = "SELECT `nom_fabricant`,`adresse_fabricant` FROM `table_fabricants` 
	WHERE (";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$fin_requete="";
	$data=array();
	$nbre=0;
	if((isset($_POST['nom_fabricant']) && preg_match($pattern_admin_fabricant_nom_verification,$_POST['nom_fabricant'])))
	{ 
		//test du nom initialement
		if((isset($_POST['adresse_fabricant'])) && preg_match($pattern_admin_fabricant_nom_verification,$_POST['adresse_fabricant']))
		{ //test sur le nom et prénom
			$nom_fabricant_temp=filter_input(INPUT_POST,'nom_fabricant',FILTER_SANITIZE_STRING);
			$nom_fabricant_base = mb_strtoupper($nom_fabricant_temp,'UTF-8');		
			//
			$adresse_fabricant_temp=filter_input(INPUT_POST,'adresse_fabricant',FILTER_SANITIZE_STRING);
			$adresse_fabricant_base = mb_strtoupper($adresse_fabricant_temp,'UTF-8');
			//
			$fin_requete="(`nom_fabricant`=?)AND(`adresse_fabricant`=?))";
			$requete_verification=$requete_verification.$fin_requete;
			//
			$stmt = mysqli_prepare($db,$requete_verification);
			if($stmt)
			{
				if(mysqli_stmt_bind_param($stmt,'ss',$nom_fabricant_base,$adresse_fabricant_base))
				{
					if(mysqli_stmt_execute($stmt))
					{
						mysqli_stmt_store_result($stmt);
						//$nbre = mysqli_stmt_num_rows($stmt);
						$nbre = mysqli_stmt_affected_rows($stmt);
						mysqli_stmt_bind_result($stmt,$ligne['nom_fabricant'],$ligne['adresse_fabricant']);
						$index=0;
						$data['resultat'] = 		$msg['code_ok']['id'];
						$data['nombre'] = 			$nbre;
						while(mysqli_stmt_fetch($stmt))
						{
							$data[$index]['nom'] = 				htmlspecialchars($ligne['nom_fabricant'],ENT_QUOTES,'UTF-8');
							$data[$index]['adresse'] = 			htmlspecialchars($ligne['adresse_fabricant'],ENT_QUOTES,'UTF-8');
							$index++;
						}
					}
					else
					{
					//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
			else
			{
			//code erreur de prepare
			$data['resultat'] = $msg['code_echec_05']['id'];
			}
		}
		else
		{	//le prénom est vide, on ne teste que le nom
			$fin_requete="(`nom_fabricant`=?))";
			$requete_verification=$requete_verification.$fin_requete;
			$nom_fabricant_temp=filter_input(INPUT_POST,'nom_fabricant',FILTER_SANITIZE_STRING);
			$nom_fabricant_base = mb_strtoupper($nom_fabricant_temp,'UTF-8');		
			$stmt = mysqli_prepare($db,$requete_verification);
			if($stmt)
			{
				if(mysqli_stmt_bind_param($stmt,'s',$nom_fabricant_base))
				{
					if(mysqli_stmt_execute($stmt))
					{
					mysqli_stmt_store_result($stmt);
					//$nbre = mysqli_stmt_num_rows($stmt);
					$nbre = mysqli_stmt_affected_rows($stmt);
					mysqli_stmt_bind_result($stmt,$ligne['nom_fabricant'],$ligne['adresse_fabricant']);
					$index=0;
					$data['resultat'] = 		$msg['code_ok']['id'];
					$data['nombre'] = 			$nbre;
					while(mysqli_stmt_fetch($stmt))
					{
						$data[$index]['nom'] = 				htmlspecialchars($ligne['nom_fabricant'],ENT_QUOTES,'UTF-8');
						$data[$index]['adresse'] = 			htmlspecialchars($ligne['adresse_fabricant'],ENT_QUOTES,'UTF-8');
						$index++;
					}
					}
					else
					{
					//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
			else
			{
			//code erreur de prepare
			$data['resultat'] = $msg['code_echec_05']['id'];
			}
		}
	}
	else
	{
	//le champs est vide ou le $_POST n'est pas "set"
	$data['resultat'] = $msg['code_echec_03']['id'];
	}
mysqli_stmt_close($stmt);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>