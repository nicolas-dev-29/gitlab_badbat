	<!-- Modal ajout fabricant-->
	<div class="modal fade"  id="modal_ajout_fabricant" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_fabricant" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'un fabricant</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel fabricant</p>	
					<!--<form class="needs-validation" novalidate >-->
					<div class="row">
						<div class="col-6">
							<!-- ajout du nom du fabricant -->
							<label id="modal_ajout_fabricant_nom_label" class="control-label "  for="modal_ajout_fabricant_nom">nom:</label> 
							<input id="modal_ajout_fabricant_nom" type="text" class="form-control modal_ajout_fabricant_nom_adresse" placeholder="nom du fabricant" autocomplete="off"   aria-label="modal_ajout_fabricant_nom_aide"     >
							<span id="modal_ajout_fabricant_nom_aide" class="help-block small">entrer le nom du fabricant</span>
							<div class="valid-feedback"><span id="modal_ajout_fabricant_nom_ok"></span></div>
							<div class="invalid-feedback"><span id="modal_ajout_fabricant_nom_erreur"> erreur!</span></div>
						</div>
						<div class="col-6">
							<!--ajout de l'adresse du fabricant -->
							<label id="modal_ajout_fabricant_adresse_label" class="control-label "  for="modal_ajout_fabricant_adresse">adresse:</label> 
							<input id="modal_ajout_fabricant_adresse" type="text" class="form-control modal_ajout_fabricant_nom_adresse" placeholder="adresse du fabricant" autocomplete="off"   aria-label="modal_ajout_fabricant_adresse_aide"    >
							<span id="modal_ajout_fabricant_adresse_aide" class="help-block small">entrer l'adresse du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_fabricant_adresse_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-12">
							<!--ajout de le téléphone du fabricant -->
							<label id="modal_ajout_fabricant_telephone_label" class="control-label "  for="modal_ajout_fabricant_telephone">telephone:</label> 
							<input id="modal_ajout_fabricant_telephone" type="text" class="form-control modal_ajout_fabricant_nom_telephone" placeholder="telephone du fabricant" autocomplete="off"   aria-label="modal_ajout_fabricant_telephone_aide"    >
							<span id="modal_ajout_fabricant_telephone_aide" class="help-block small">entrer l'telephone du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_fabricant_telephone_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-12">
							<!-- ajout du mail du fabricant -->
							<label id="modal_ajout_fabricant_mail_label" class="control-label"  for="modal_ajout_fabricant_mail">mail:</label> 
							<input id="modal_ajout_fabricant_mail" type="email" class="form-control" placeholder="mail du fabricant" autocomplete="off"   aria-label="modal_ajout_fabricant_mail_aide"     >
							<span id="modal_ajout_fabricant_mail_aide" class="help-block small">entrer le mail du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_fabricant_erreur"> erreur!</span></div>
						</div>
					</div>
					<hr/>		
							<div >
								<label id="modal_ajout_fabricant_divers_label" for="modal_ajout_fabricant_divers">divers:</label>
								<textarea  id="modal_ajout_fabricant_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_ajout_fabricant_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_fabricant_divers_erreur"> erreur!</span></div>
							</div>
						
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_fabricant_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	
	<!-- modification du fabricant -->
<div class="modal fade"  id="modal_modification_fabricant" tabindex="-1" role="dialog" aria-labelledby="modal_modification_fabricant" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">modification d'un fabricant</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel fabricant</p>	
					<span id="modal_modification_fabricant_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du fabricant pour faire la requete AJAX directement avec l'id au fabricant du nom-->
					
					<div class="row">
						<div class="col-6">
							<!-- modification du nom du fabricant -->
							<label id="modal_modification_fabricant_nom_label" class="control-label"  for="modal_modification_fabricant_nom">nom:</label> 
							<input id="modal_modification_fabricant_nom" type="text" class="form-control modal_modification_fabricant_nom_adresse" placeholder="nom du fabricant" autocomplete="off"   aria-label="modal_modification_fabricant_nom_aide"   required  >
							<span id="modal_modification_fabricant_nom_aide" class="help-block small">entrer le nom du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_nom_fabricant_erreur"> erreur!</span></div>
						</div>
						<div class="col-6">
							<!--modification du adresse du fabricant -->
						<label id="modal_modification_fabricant_adresse_label" class="control-label"  for="modal_modification_fabricant_adresse">adresse:</label> 
							<input id="modal_modification_fabricant_adresse" type="text" class="form-control modal_modification_fabricant_nom_adresse" placeholder="adresse du fabricant" autocomplete="off"   aria-label="modal_modification_fabricant_adresse_aide"   required  >
							<span id="modal_modification_fabricant_adresse_aide" class="help-block small">entrer l'adresse du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_adresse_fabricant_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>

					<div class="row">
						<div class="col-12">
							<!-- modification du mail du fabricant -->
							<label id="modal_modification_fabricant_mail_label" class="control-label"  for="modal_modification_fabricant_mail">mail:</label> 
							<input id="modal_modification_fabricant_mail" type="email" class="form-control" placeholder="mail du fabricant" autocomplete="off"   aria-label="modal_modification_fabricant_mail_aide"   required  >
							<span id="modal_modification_fabricant_mail_aide" class="help-block small">entrer le mail du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_fabricant_erreur"> erreur!</span></div>
						</div>
					</div>
					<hr/>		
					<div class="row">
						<div class="col-12">
							<!--modification de le téléphone du fabricant -->
							<label id="modal_modification_fabricant_telephone_label" class="control-label "  for="modal_modification_fabricant_telephone">telephone:</label> 
							<input id="modal_modification_fabricant_telephone" type="text" class="form-control modal_modification_fabricant_nom_telephone" placeholder="telephone du fabricant" autocomplete="off"   aria-label="modal_modification_fabricant_telephone_aide"    >
							<span id="modal_modification_fabricant_telephone_aide" class="help-block small">entrer l'telephone du fabricant</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_fabricant_telephone_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>							
					<div >
						<label id="modal_modification_fabricant_divers_label" for="modal_modification_fabricant_divers">divers:</label>
						<textarea  id="modal_modification_fabricant_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_modification_fabricant_divers_aide"></textarea>
						<div class="invalid-feedback"><span id="modal_modification_fabricant_divers_erreur"> erreur!</span></div>
					</div>
						
				
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_modification_fabricant_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>	

	
	
	<!-- Modal suppression d'un fabricant-->
	<div class="modal fade" id="modal_suppression_fabricant" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_fabricant" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un fabricant</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer l'fabricant suivant:</p>	
					<span id="modal_suppression_fabricant_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du fabricant pour faire la requete AJAX directement avec l'id au fabricant du nom-->
					<div class="row">
						<div class="col-lg-6">
						<div class="texte_important">nom: </div><span id="modal_suppression_fabricant_nom"  >nom</span>  <br>  
					
						</div>
						<div class="col-lg-6">
						<div class="texte_important">nom: </div><span id="modal_suppression_fabricant_adresse"  >adresse</span>  <br>  
					
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="texte_important">courriel: </div>	<div class="modal_span"> <span id="modal_suppression_fabricant_mail" ></span></div>
				
						</div>
						<div class="col-lg-6">
						<div class="texte_important">divers: </div>	<div class="modal_span"> <span id="modal_suppression_fabricant_divers" ></span></div>
				
						</div>
					</div>
</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_fabricant_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>
	
	
		<!-- Modal suppression de tous les fabricant -->
		
	<div class="modal fade" id="modal_suppression_fabricant_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_fabricant_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les fabricants</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les fabricants de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_fabricant_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>