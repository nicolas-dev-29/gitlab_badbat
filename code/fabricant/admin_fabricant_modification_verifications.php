<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation de la requête
	$requete_verification_presence = "SELECT * FROM `table_fabricants` WHERE ((nom_fabricant=?) AND(adresse_fabricant=?))";
	
	//on verifie que ce n'est pas le même non que l'on rentre pour éviter les doublons dans la base
	$requete_verification_identique = "SELECT `nom_fabricant`,`adresse_fabricant` FROM `table_fabricants` WHERE id_fabricant=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	
	//preparation de la requete_verification
	$stmt_verification_presence = mysqli_prepare($db,$requete_verification_presence);
	$stmt_verification_identique = mysqli_prepare($db,$requete_verification_identique);
	$verification_nom="";
	
	$data=array();
	if($stmt_verification_presence)
	{
		if(isset($_POST['nom_fabricant']) && preg_match($pattern_admin_nom_verification,$_POST['nom_fabricant']))
		{	
			$nom_temp=filter_input(INPUT_POST,'nom_fabricant',FILTER_SANITIZE_STRING);
			$adresse_temp=filter_input(INPUT_POST,'adresse_fabricant',FILTER_SANITIZE_STRING);
			$nom_base = mb_strtoupper($nom_temp,'UTF-8');
			//liaison parametres
			if(mysqli_stmt_bind_param($stmt_verification_presence,'ss',$nom_base,$adresse_temp))
			{
				if(mysqli_stmt_execute($stmt_verification_presence))
				{
					mysqli_stmt_store_result($stmt_verification_presence);
					$nbre = mysqli_stmt_num_rows($stmt_verification_presence);
					if($nbre == 0)
					{	// ce nom n'existe pas , on peut donc l'utiliser
						$data['resultat'] = $msg['code_ok']['id'];
					}
					else
					{	//le nom existe déjà, on vérifie que c'est bien le même
						//on vérifie maintenant que ce n'est pas le même nom_lieux
						if($stmt_verification_identique)
						{
						//nettoyage des informations provenant de POST
			
							if(isset($_POST['id_user']) && $_POST['id_user']!="")
							{
								//nettoyage des informations provenant de POST
								if(filter_input(INPUT_POST,'id_user',FILTER_SANITIZE_NUMBER_INT)==FALSE)
								{	//erreur de typage
									$data['resultat']=$msg['code_echec_04']['id']; 
								}
								else	//  les données sont valides
								{
									$id_base=filter_input(INPUT_POST,'id_user',FILTER_SANITIZE_NUMBER_INT);
									if(mysqli_stmt_bind_param($stmt_verification_identique,'i',$id_base))
									{
											if(mysqli_stmt_execute($stmt_verification_identique))
											{
												mysqli_stmt_bind_result($stmt_verification_identique,$ligne['nom'],$ligne['adresse']);
												while(mysqli_stmt_fetch($stmt_verification_identique))
												{
													$verification_nom = $ligne['nom'];
													$verification_adresse = $ligne['adresse'];
												}
												if(($nom_base == $verification_nom)&&($adresse_temp == $verification_adresse))
												{
													$data['resultat'] = $msg['code_ok']['id'];
												}
												else
												{
													//le nom est dajà utilisé
													$data['resultat'] = $msg['code_echec_02'];
												}
											}
											else
											{
												$data['resultat'] = $msg['code_echec_01'];
											}
									}
									else
									{
									//erreur de bind
									$data['resultat'] = $msg['code_echec_06']['id'];
									}
								}
							}
							else
							{
								//le champs est vide ou le $_POST n'est pas "set"
								$data['resultat'] = $msg['code_echec_03']['id'];
							}
						}
						else
						{
							//code erreur de prepare
							$data['resultat'] = $msg['code_echec_05']['id'];
						}
						
						//$data['resultat'] = $msg['code_ok']['id'];
					}
				}
				else
				{	//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
			}
			else
			{	//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
			}
		}
		else
		{	//le champs est vide ou le $_POST n'est pas "set"
			$data['resultat'] = $msg['code_echec_03']['id'];	
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];
	}
	
								

mysqli_stmt_close($stmt_verification_identique);
mysqli_stmt_close($stmt_verification_presence);
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>