	<!-- Modal ajout tension_reseau-->
	<div class="modal fade"  id="modal_ajout_tension_reseau" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_tension_reseau" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'une nouvelle tension de réseau</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations de tension de réseau</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div>
						<!-- ajout du nom de la tension -->
							<label id="modal_ajout_tension_reseau_nom_label" class="control-label" for="modal_ajout_tension_reseau_nom">nom de la tension:</label> 
							<input id="modal_ajout_tension_reseau_nom" type="text" class="form-control" placeholder="nom de la tension" autocomplete="off"   aria-label="modal_ajout_tension_reseau_nom_aide"   required  >
							<span id="modal_ajout_tension_reseau_nom_aide" class="help-block small">entrer le nom de la tension</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_tension_reseau_nom_erreur"> erreur!</span></div>
							
						</div>
						<!-- ajout de la valeur de la tension -->
						<div class="row">
						<div class="col-6">
							<label id="modal_ajout_tension_reseau_valeur_label" class="control-label" for="modal_ajout_tension_reseau_valeur">valeur:</label> 
							<input id="modal_ajout_tension_reseau_valeur" type="number" step="0.1" class="form-control" placeholder="valeur de la tension" autocomplete="off"   aria-label="modal_ajout_tension_reseau_valeur_aide"   required  >
							<span id="modal_ajout_tension_reseau_valeur_aide" class="help-block small">entrer la valeur de la tension</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_tension_reseau_valeur_erreur"> erreur!</span></div>
						</div>
						<div class="col-4">
							<span> V </span>
						</div>
						</div>
							<div class="form-group">
								<label id="modal_ajout_tension_reseau_divers_label" for="modal_ajout_tension_reseau_divers">divers:</label>
								<textarea  id="modal_ajout_tension_reseau_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_ajout_tension_reseau_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_tension_reseau_divers_erreur"> erreur!</span></div>
							</div>
						
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_tension_reseau_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	<!-- Modal modification tension_reseau-->
	<div class="modal fade"  id="modal_modification_tension_reseau" tabindex="-1" role="dialog" aria-labelledby="modal_modification_tension_reseau" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">modification d'une nouvelle tension de réseau</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations de tension de réseau</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div>
						<!-- modification du nom de la tension -->
							<span id="modal_modification_tension_reseau_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du équipement pour faire la requete AJAX directement avec l'id au équipement du nom-->
							<label id="modal_modification_tension_reseau_nom_label" class="control-label" for="modal_modification_tension_reseau_nom">nom de la tension:</label> 
							<input id="modal_modification_tension_reseau_nom" type="text" class="form-control" placeholder="nom de la tension" autocomplete="off"   aria-label="modal_modification_tension_reseau_nom_aide"   required  >
							<span id="modal_modification_tension_reseau_nom_aide" class="help-block small">entrer le nom de la tension</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_tension_reseau_nom_erreur"> erreur!</span></div>
						<!-- modification de la valeur de la tension -->	
						</div>
						<div class="row">
						<div class="col-6">
							<label id="modal_modification_tension_reseau_valeur_label" class="control-label" for="modal_modification_tension_reseau_valeur">valeur:</label> 
							<input id="modal_modification_tension_reseau_valeur" type="number" step="0.1" class="form-control" placeholder="valeur de la tension" autocomplete="off"   aria-label="modal_modification_tension_reseau_valeur_aide"   required  >
							<span id="modal_modification_tension_reseau_valeur_aide" class="help-block small">entrer la valeur de la tension</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_tension_reseau_valeur_erreur"> erreur!</span></div>
						</div>
						<div class="col-4">
							<span> V </span>
						</div>
						</div>
							<div class="form-group">
								<label id="modal_modification_tension_reseau_divers_label" for="modal_modification_tension_reseau_divers">divers:</label>
								<textarea  id="modal_modification_tension_reseau_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_modification_tension_reseau_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_tension_reseau_divers_erreur"> erreur!</span></div>
							</div>
						
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_modification_tension_reseau_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	<!-- Modal suppression d'un état-->
	<div class="modal fade" id="modal_suppression_tension_reseau" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_tension_reseau" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un état</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer l'état suivant:</p>	
					<span id="modal_suppression_tension_reseau_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du état pour faire la requete AJAX directement avec l'id au état du nom-->
					<div class="texte_important">nom: </div><span id="modal_suppression_tension_reseau_nom"  >nom</span> <br> 
					<div class="texte_important">diverss: </div>	
					<div class="modal_span">
						<span id="modal_suppression_tension_reseau_divers" ></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_tension_reseau_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>