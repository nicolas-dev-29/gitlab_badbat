<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_suppression_tous = "TRUNCATE TABLE table_tension_reseaux ";
	$requete_ajout_defaut = "INSERT INTO table_tension_reseaux (`nom_tension_reseau`, `divers_tension_reseau`)	VALUES (?,?)";
	$requete_update_table_principale = " UPDATE `table_batteries` SET tension_reseau = ? ";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	
	$stmt_suppression_tous = mysqli_prepare($db,$requete_suppression_tous);
	$stmt_ajout_defaut = mysqli_prepare($db,$requete_ajout_defaut);
	$stmt_update_table_principale = mysqli_prepare($db,$requete_update_table_principale);
	if($stmt_suppression_tous)
	{
		if(mysqli_stmt_execute($stmt_suppression_tous))
		{
			if(mysqli_stmt_bind_param($stmt_ajout_defaut,'ss',$defaut_nom,$defaut_divers))
			{
				if(mysqli_execute($stmt_ajout_defaut))
				{
					if(mysqli_stmt_bind_param($stmt_update_table_principale,'i',$defaut_id))
					{
						if(mysqli_execute($stmt_update_table_principale))
						{
						$data['resultat'] = $msg['code_ok']['id'];
						}
						else
						{
						//erreur d'execute
						$data['resultat'] = $msg['code_echec_01']['id'];
						}
					}
					else
					{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
					}
				}
				else
				{
				//erreur d'execute
				$data['resultat'] = $msg['code_echec_01']['id'];					
				}
			}
			else
			{
			//erreur de bind
			$data['resultat'] = $msg['code_echec_06']['id'];
			}
		}
		else
		{	
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];
	}
	mysqli_stmt_close($stmt_suppression_tous);
	mysqli_stmt_close($stmt_ajout_defaut);
	mysqli_stmt_close($stmt_update_table_principale);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>