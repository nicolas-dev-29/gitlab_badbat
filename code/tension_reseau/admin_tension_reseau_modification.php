<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation des requêtes
	$requete_verification="SELECT * FROM table_tension_reseaux WHERE id_tension_reseau = ?";
	$requete_update = "UPDATE table_tension_reseaux SET `nom_tension_reseau`=?,`valeur_tension_reseau`=?,`divers_tension_reseau`=? WHERE `table_tension_reseaux`.`id_tension_reseau`=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=01;
	$data=array();
	
	//preparation des requetes
	$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update = mysqli_prepare($db, $requete_update);
	
	if($stmt_verification)
	{
		if(isset($_POST['id']) && $_POST['id']!=""&& isset($_POST['nom_tension']) && preg_match($pattern_admin_nom_verification,$_POST['nom_tension'])
		   && preg_match($pattern_admin_divers_verification,$_POST['divers']))
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				$nom_temp=filter_input(INPUT_POST,'nom_tension',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
				$nom_base = mb_strtoupper($nom_temp,'UTF-8');
				$valeur_base=$_POST['valeur_tension'];//filter_input(INPUT_POST,'tension_reseau_valeur',FILTER_SANITIZE_NUMBER_FLOAT);
				$divers_base=filter_input(INPUT_POST,'divers',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
				
				
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							if(mysqli_stmt_bind_param($stmt_update,'sdsi',$nom_base,$valeur_base,$divers_base,$id_base))
							{
								if(mysqli_execute($stmt_update))
								{		
									$data['resultat'] = $msg['code_ok']['id'];;	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}


	
	
								

	
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>