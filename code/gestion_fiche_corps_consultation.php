	<div class="row align-item-center">
		<div class="col-lg-12">
			<div class="nav nav-tabs justify-content-center" role="tablist">
					<a class="nav-item nav-link my_onglets active " 	id="onglet_1_fiche_consultation"	data-toggle="tab" 	role="tab" 	aria-selected="true"	href="#nav_consultation-1">	paramètres généraux 	</a>
					<a class="nav-item nav-link my_onglets"  			id="onglet_2_fiche_consultation"	data-toggle="tab"	role="tab"	aria-selected="true"	href="#nav_consultation-2">	paramètres mécaniques 	</a>
					<a class="nav-item nav-link my_onglets" 			id="onglet_3_fiche_consultation"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_consultation-3">	paramètres électriques	</a>
					<a class="nav-item nav-link my_onglets" 			id="onglet_4_fiche_consultation"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_consultation-4">	paramètres d'achat		</a>
					<a class="nav-item nav-link my_onglets" 			id="onglet_5_fiche_consultation"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_consultation-5">	photos					</a>				
					<a class="nav-item nav-link my_onglets" 			id="onglet_6_fiche_consultation"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_consultation-6">	divers					</a>					
			</div>
			<!-- contenu associé aux onglets -->
			<div class="tab-content">
				<!--onglet 1: paramètres généraux -->
				<div class="tab-pane  active fiche_contenu"	id="nav_consultation-1" 	role="tabpanel" aria-labelledby="onglet_1_fiche_consultation">
					<div class="row">						
						<div class="col-lg-12 offset-lg-1 card-columns my-3">
							<div class="card bg-light border-dark  " >
								<div class="card-header ">	
									<h5 class="card_titre_entete">Paramètres généraux 	</h5>
								</div>
								
								<div class="card-body " id="gestion_fiche_contenu_parametres_generaux">
									<div class="row ">
										<div class="col-lg-12">
											<h5 class="card_titre">Référence </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label"> Référence constructeur:</span> <span id="gestion_fiche_reference_industrielle">X</span>			</div>
									</div>
									<div class="row ">
										<div class="col-lg-12">
											<h5 class="card_titre"> Lieu </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label"> nom:</span> <span id="gestion_fiche_lieux_nom">X</span>			</div>
										<div class="col-lg-12"> <span class="card_label"> divers: </span>		</div>
										<div class="col-lg-12"> <span class="card_divers" id="gestion_fiche_lieux_divers">X</span>	</div>
									</div>
									<div class="row my-3">
										<div class="col-lg-12">
											<h5 class="card_titre"> Equipement </h5>
										</div>
											<div class="col-12"> <span class="card_label"> nom:</span> <span id="gestion_fiche_equipement_nom">X</span>			</div>
											<div class="col-12"> <span class="card_label"> divers: </span> 				</div>
											<div class="col-12"> <span class="card_divers" id="gestion_fiche_equipement_divers">X</span> 	</div>
									</div>
									<div class="row my-3">
										<div class="col-lg-12">
											<h5 class="card_titre"> Tension du réseau </h5>
										</div>
										<div class="col-lg-6"> <span class="card_label"> nom: </span><span id="gestion_fiche_tension_reseau_nom">X</span>			</div>
										<div class="col-lg-6"> <span class="card_label"> valeur: </span><span id="gestion_fiche_tension_reseau_valeur"> X</span> V 	</div>
										<div class="col-lg-12"> <span class="card_label"> divers: </span></div>
										<div class="col-lg-12"> <span class="card_divers" id="gestion_fiche_tension_reseau_divers">X</span> </div>
									</div>
								
								</div>
							</div>	
							<div class="card bg-light border-dark  " >
								<div class="card-header">	
									<h5 class="card_titre_entete">Etats - Evenements 	</h5>
								</div>
								<div class="card-body ">
									<div class="row ">
										<div class="col-lg-12">
											<h5 class="card_titre"> Etats </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label"> état actuel: </span> <span id="gestion_fiche_consultation_etat_nom">X</span>		</div>
										<div class="col-lg-12"> <span class="card_label">divers: </span><span id="gestion_fiche_consultation_etat_divers">X</span>	</div>
									</div>
									<div class="row my-3">
										<div class="col-lg-12">
											<h5 class="card_titre"> Evenements </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label">dernier événement:</span> <span id="gestion_fiche_consultation_nom_evenement">X</span> 	</div>
										<div class="col-lg-12"> <span class="card_label">le:</span> <span id="gestion_fiche_consultation_etat_date_dernier_evenement">X</span> 	</div>
									</div>
									<div class="row my-3">
										<div class="col-lg-12">
											<h5 class="card_titre"> Opérations </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label">dernière opération:</span> <span id="gestion_fiche_consultation_nom_operation">X</span> 	</div>
										<div class="col-lg-12"> <span class="card_label">le:</span> <span id="gestion_fiche_consultation_etat_date_derniere_operation">X</span> 	</div>
									</div>
									<div class="row my-3">
										<div class="col-lg-12">
											<h5 class="card_titre"> Autonomie </h5>
										</div>
										<div class="col-lg-12"> <span class="card_label">dernière autonomie mesurée:</span> <span id="gestion_fiche_consultation_autonomie_mesuree">X</span> 	</div>
										<div class="col-lg-12"> <span class="card_label">le:</span> <span id="gestion_fiche_consultation_date_derniere_autonomie_mesuree">X</span> 	</div>
										<div class="col-lg-12"> <span class="card_label">autonomie requise:</span> <span id="gestion_fiche_consultation_autonomie_requise">X</span> 	</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fiche_contenu" 			id="nav_consultation-2" 	role="tabpanel" aria-labelledby="onglet_2_fiche_consultation">
					<div class="row">					
						<div class="col-10 offset-lg-1 card-columns my-3">
							<div class="card bg-light border-dark  " >
								<div class="card-header ">	
									<h5 class="card_titre_entete">Dimensions de la batterie	</h5>
									Ce sont les dimensions de la batterie
								</div>
								<div class="card-body">
									<div class="col-lg-6"> <span class="card_label">longueur:</span> <span id="gestion_fiche_consultation_dimension_longueur"> - </span> cm </div>
									<div class="col-lg-6"> <span class="card_label">largeur:</span> <span id="gestion_fiche_consultation_dimension_largeur"> - </span> cm	</div>
									<div class="col-lg-6"> <span class="card_label">hauteur:</span> <span id="gestion_fiche_consultation_dimension_hauteur"> - </span> cm 	</div>
								</div>
							</div>
							<div class="card bg-light border-dark  " >
								<div class="card-header ">	
									<h5 class="card_titre_entete">Dimensions du chantier</h5>
									Ce sont les dimensions du bati support
								</div>
								<div class="card-body">
									<div class="col-lg-6"> <span class="card_label">longueur:</span> <span id="gestion_fiche_consultation_dimension_longueur_chantier"> - </span> cm </div>
									<div class="col-lg-6"> <span class="card_label">largeur:</span> <span id="gestion_fiche_consultation_dimension_largeur_chantier"> - </span> cm	</div>
									<div class="col-lg-6"> <span class="card_label">hauteur:</span> <span id="gestion_fiche_consultation_dimension_hauteur_chantier"> - </span> cm 	</div>
								</div>
							</div>
						</div>
					</div>
				</div>		
				<div class="tab-pane" 			id="nav_consultation-3" 	role="tabpanel" aria-labelledby="onglet_3_fiche_consultation">ccc</div>
				<!-- parametres d'achatsd es batteries -->
				<div class="tab-pane fiche_contenu" 			id="nav_consultation-4" 	role="tabpanel" aria-labelledby="onglet_4_fiche_consultation">
					<div class="row">					
							<div class="col-10 offset-lg-1 card-columns my-3">
								<div class="card bg-light border-dark  " >
									<div class="card-header ">	
										<h5 class="card_titre_entete">fournisseur</h5>
									</div>
									<div class="card-body">
										<div class="col-lg-6"> <span class="card_label">nom:</span> <span id="gestion_fiche_consultation_fournisseur_nom"> - </span>  </div>
										<div class="col-lg-6"> <span class="card_label">adresse:</span> <span id="gestion_fiche_consultation_fournisseur_adresse"> - </span> 	</div>
										<div class="col-lg-6"> <span class="card_label">mail:</span> <span id="gestion_fiche_consultation_fournisseur_mail"> - </span>  	</div>
										<div class="col-lg-6"> <span class="card_label">téléphone:</span> <span id="gestion_fiche_consultation_fournisseur_téléphone"> - </span> 	</div>
										<div class="col-lg-6"> <span class="card_label">divers:</span> <span id="gestion_fiche_consultation_fournisseur_divers"> - </span> 	</div>
									
									</div>
								</div>
								<div class="card bg-light border-dark  " >
									<div class="card-header ">	
										<h5 class="card_titre_entete">Fabricant</h5>
										
									</div>
									<div class="card-body">
										<div class="col-lg-12"> <span class="card_label">nom:</span> <span id="gestion_fiche_consultation_fabricant_nom"> - </span>  </div>
										<div class="col-lg-12"> <span class="card_label">adresse:</span> <span id="gestion_fiche_consultation_fabricant_adresse"> - </span> 	</div>
										<div class="col-lg-12"> <span class="card_label">mail:</span> <span id="gestion_fiche_consultation_fabricant_mail"> - </span>  	</div>
										<div class="col-lg-12"> <span class="card_label">téléphone:</span> <span id="gestion_fiche_consultation_fabricant_telephone"> - </span> 	</div>
										<div class="col-lg-12"> <span class="card_label">divers:</span> <span id="gestion_fiche_consultation_fabricant_divers"> - </span> 	</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="tab-pane" 			id="nav_consultation-5" 	role="tabpanel" aria-labelledby="onglet_5_fiche_consultation">eee</div>
				<div class="tab-pane" 			id="nav_consultation-6" 	role="tabpanel" aria-labelledby="onglet_6_fiche_consultation">ff</div>
			</div>
		</div>
	</div>