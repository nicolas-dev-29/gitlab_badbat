<?php
header( 'content-type: text/html; charset=utf-8' );
//chargement des constantes
include ("../constantes/badbat_constante.inc");
include ("../constantes/dictionnaire.inc");
//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
//extraction du contenu du ficheir JSON
$msg=array();
$msg=json_decode($contenu_fichier_json,true);
//préparation de la requete
$requete_valeurs = "SELECT `id_batterie_historique`,`references_base`,
    TET1.`nom_etat` AS nom_etat_suivant, TET2.`nom_etat`AS nom_etat_precedent,
    `nom_evenement`,
	`date_dernier_evenement`,`nom_operation`,`date_derniere_operation`,
	`autonomie_mesuree`,`autonomie_requise`,`date_mesure_autonomie`,`date_historique`
	FROM table_historique AS TH
	LEFT JOIN table_etats AS TET1
		ON (TH.id_etat_suivant_historique = TET1.id_etat)
	LEFT JOIN table_etats AS TET2
		ON TH.id_etat_precedent_historique = TET2.id_etat
	LEFT JOIN table_evenements TE
		ON TH.id_evenement_historique = TE.id_evenement
	LEFT JOIN table_operations TOP
		ON TH.id_operation = TOP.id_operation
    LEFT JOIN table_batteries TB
		ON TH.id_batterie_historique = TB.id_batteries 
	WHERE ((`id_batterie_historique` = ?) )
	";
//valeurs recherhcées
$requete_valeurs_recherchees = "SELECT `id_batterie_historique`,`references_base`,
    TET1.`nom_etat` AS nom_etat_suivant, TET2.`nom_etat`AS nom_etat_precedent,
    `nom_evenement`,
	`date_dernier_evenement`,`nom_operation`,`date_derniere_operation`,
	`autonomie_mesuree`,`autonomie_requise`,`date_mesure_autonomie`,`date_historique`
	FROM table_historique AS TH
	LEFT JOIN table_etats AS TET1
		ON (TH.id_etat_suivant_historique = TET1.id_etat)
	LEFT JOIN table_etats AS TET2
		ON TH.id_etat_precedent_historique = TET2.id_etat
	LEFT JOIN table_evenements TE
		ON TH.id_evenement_historique = TE.id_evenement
	LEFT JOIN table_operations TOP
		ON TH.id_operation = TOP.id_operation
    LEFT JOIN table_batteries TB
		ON TH.id_batterie_historique = TB.id_batteries 
	WHERE ((`id_batterie_historique` = ?) and 
			(`date_historique` LIKE ? or 
			 TET1.`nom_etat` LIKE ? or
			 `nom_operation` LIKE ? ))
	";	
$requete_valeurs_nombre_total = "SELECT COUNT(*) 
								FROM `table_historique`";

								


//ouverture de la base de données
$db = new mysqli($host_db, $login_db, $passwd_db, $database);
// Check connection
if (!$db) {
    die("Echec connexion: " . mysqli_connect_error());
}
mysqli_set_charset( $db,"utf8" );

$nbre_filtre=0;								
$nbre_total=0;
// récupération de la valeur de recherche
$searchValue ='';
$searchValue = mysqli_real_escape_string($db,$_POST['search']['value']); // Search value
$valeurs_recherchees= "%".$searchValue."%";

$reponse=array();
$data=array();
//preparation
$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
$stmt_valeurs_recherchees = mysqli_prepare($db,$requete_valeurs_recherchees);
$stmt_valeurs_nombre_total = mysqli_prepare($db,$requete_valeurs_nombre_total);
if(($stmt_valeurs)&&($stmt_valeurs_recherchees)&&($stmt_valeurs_nombre_total))
{
    if(isset($_POST['id']) && $_POST['id']!="")
    {
        //nettoyage des informations provenant de POST
        if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
        {
            //erreur de typage
            $data['resultat']=$msg['code_echec_04']['id'];
        }
        else
        {
            //  les données sont valides
            
			//comptage du nombre total d'enregistrement
			if(mysqli_stmt_execute($stmt_valeurs_nombre_total))
            {
				mysqli_stmt_store_result($stmt_valeurs_nombre_total);
				mysqli_stmt_bind_result($stmt_valeurs_nombre_total,$temp);
				while(mysqli_stmt_fetch($stmt_valeurs_nombre_total))
				{
					$nbre_total = $temp;
				}
				
                
			}
			else
			{
				//échec de l'exécution
				$data['resultat'] = $msg['code_echec_01']['id'];
			}
			$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
			//aiguillage de la requete suivant la recherche
			if($searchValue!='') //filtrage
			{
			if(mysqli_stmt_bind_param($stmt_valeurs_recherchees,'iiss',$id_base,$valeurs_recherchees,$valeurs_recherchees,$valeurs_recherchees))
				{
					if(mysqli_stmt_execute($stmt_valeurs_recherchees))
					{
						mysqli_stmt_store_result($stmt_valeurs_recherchees);
						$nbre_filtre = mysqli_stmt_num_rows($stmt_valeurs_recherchees);
						if($nbre_filtre > 0)
						{
							mysqli_stmt_bind_result($stmt_valeurs_recherchees,$ligne['id_batteries_historique'],$ligne['reference_base'],                   
								$ligne['nom_etat_suivant'],$ligne['nom_etat_precedent'],
								$ligne['nom_evenement'],$ligne['date_dernier_evenement'],
								$ligne['nom_operation'],$ligne['date_derniere_operation'],
								$ligne['autonomie_mesuree'],$ligne['autonomie_requise'],$ligne['date_mesure_autonomie'],$ligne['date_historique']);
							$index=0;
							while(mysqli_stmt_fetch($stmt_valeurs_recherchees))
							{
								//$data[$index]['resultat'] = $msg['code_ok']['id'];
								//$data[$index]['reference_base'] = 				htmlentities($ligne['reference_base'],ENT_QUOTES,'UTF-8');
								//etats
								$data[$index]['nom_etat_suivant'] = 			htmlspecialchars($ligne['nom_etat_suivant'],ENT_NOQUOTES,'UTF-8');
								$data[$index]['nom_etat_precedent'] = 			htmlspecialchars($ligne['nom_etat_precedent'],ENT_NOQUOTES,'UTF-8');
								//évenements
								//$data[$index]['nom_evenement'] = 				htmlspecialchars($ligne['nom_evenement'],ENT_NOQUOTES,'UTF-8');
								//$data[$index]['date_dernier_evenement'] = 		htmlentities($ligne['date_dernier_evenement'],ENT_QUOTES,'UTF-8');
								$data[$index]['date_historique'] = 		       htmlentities($ligne['date_historique'],ENT_QUOTES,'UTF-8');
								//opérations
								$data[$index]['nom_operation'] = 				htmlspecialchars($ligne['nom_operation'],ENT_NOQUOTES,'UTF-8');
								//$data[$index]['date_derniere_operation'] = 		htmlentities($ligne['date_derniere_operation'],ENT_QUOTES,'UTF-8');
								//autonomie
								//$data[$index]['autonomie_mesuree'] = 					htmlentities($ligne['autonomie_mesuree'],ENT_QUOTES,'UTF-8');
								//$data[$index]['autonomie_requise'] = 					htmlentities($ligne['autonomie_requise'],ENT_QUOTES,'UTF-8');
							   // $data['autonomie_date_derniere_mesure'] = 		htmlentities($ligne['autonomie_date_derniere_mesure'],ENT_QUOTES,'UTF-8');
								$index++;
							}
						}
						else
						{
							// problème d'Id
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
						//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}				
			else //pas de filtrage 
			{
			if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_valeurs))
					{
						mysqli_stmt_store_result($stmt_valeurs);
						if($nbre_total > 0)
						{
							mysqli_stmt_bind_result($stmt_valeurs,$ligne['id_batteries_historique'],$ligne['reference_base'],                   
								$ligne['nom_etat_suivant'],$ligne['nom_etat_precedent'],
								$ligne['nom_evenement'],$ligne['date_dernier_evenement'],
								$ligne['nom_operation'],$ligne['date_derniere_operation'],
								$ligne['autonomie_mesuree'],$ligne['autonomie_requise'],$ligne['date_mesure_autonomie'],$ligne['date_historique']);
							$index=0;
							while(mysqli_stmt_fetch($stmt_valeurs))
							{
								//$data[$index]['resultat'] = $msg['code_ok']['id'];
								//$data[$index]['reference_base'] = 				htmlentities($ligne['reference_base'],ENT_QUOTES,'UTF-8');
								//etats
								$data[$index]['nom_etat_suivant'] = 			htmlspecialchars($ligne['nom_etat_suivant'],ENT_NOQUOTES,'UTF-8');
								$data[$index]['nom_etat_precedent'] = 			htmlspecialchars($ligne['nom_etat_precedent'],ENT_NOQUOTES,'UTF-8');
								//évenements
								//$data[$index]['nom_evenement'] = 				htmlspecialchars($ligne['nom_evenement'],ENT_NOQUOTES,'UTF-8');
								//$data[$index]['date_dernier_evenement'] = 		htmlentities($ligne['date_dernier_evenement'],ENT_QUOTES,'UTF-8');
								$data[$index]['date_historique'] = 		       htmlentities($ligne['date_historique'],ENT_QUOTES,'UTF-8');
								//opérations
								$data[$index]['nom_operation'] = 				htmlspecialchars($ligne['nom_operation'],ENT_NOQUOTES,'UTF-8');
								//$data[$index]['date_derniere_operation'] = 		htmlentities($ligne['date_derniere_operation'],ENT_QUOTES,'UTF-8');
								//autonomie
								//$data[$index]['autonomie_mesuree'] = 					htmlentities($ligne['autonomie_mesuree'],ENT_QUOTES,'UTF-8');
								//$data[$index]['autonomie_requise'] = 					htmlentities($ligne['autonomie_requise'],ENT_QUOTES,'UTF-8');
							   // $data['autonomie_date_derniere_mesure'] = 		htmlentities($ligne['autonomie_date_derniere_mesure'],ENT_QUOTES,'UTF-8');
								$index++;
							}
						}
						else
						{
							// problème d'Id
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
						//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
        }
    }
    else
    {
        //erreur de POST
        $data['resultat'] = $msg['code_echec_01']['id'];
    }
}
else
{
    //code erreur de prepare
    $data['resultat'] = $msg['code_echec_05']['id'];
}
mysqli_stmt_close($stmt_valeurs);
//encodage JSON
/*header('Content-Type: application/json');
echo json_encode($data);*/
$reponse["draw"] = 1;
$reponse["recordsTotal"] = $nbre_total;
$reponse["recordsFiltered"] = $nbre_filtre;
$reponse["data"]=$data;
echo json_encode($reponse);
mysqli_close($db);
?>