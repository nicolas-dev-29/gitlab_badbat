<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	include ("../constantes/badbat_pattern.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation des requêtes
	$requete_verification="SELECT * FROM table_lieux WHERE id_lieux = ?";
	$requete_update = "UPDATE table_lieux SET `nom_lieux`=?,`divers_lieux`=? WHERE `table_lieux`.`id_lieux`=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=01;
	$data=array();
	
	//preparation des requetes
	$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update = mysqli_prepare($db, $requete_update);
	
	if($stmt_verification)
	{
		if(isset($_POST['id']) && $_POST['id']!=""&& isset($_POST['lieu']) && preg_match($pattern_admin_lieux_nom_verification,$_POST['lieu'])
		   && preg_match($pattern_admin_lieux_divers_verification,$_POST['divers']))
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				$lieu_temp=filter_input(INPUT_POST,'lieu',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
				$divers_base=filter_input(INPUT_POST,'divers',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
				$lieu_base = mb_strtoupper($lieu_temp,'UTF-8');
				
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							if(mysqli_stmt_bind_param($stmt_update,'ssi',$lieu_base,$divers_base,$id_base))
							{
								if(mysqli_execute($stmt_update))
								{		
									$data['resultat'] = $msg['code_ok']['id'];;	
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}


	

	mysqli_stmt_close($stmt_update);
	mysqli_stmt_close($stmt_verification);						

	
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>