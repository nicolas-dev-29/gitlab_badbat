<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation des requêtes
	$requete_verification="SELECT * FROM table_batteries WHERE (id_batteries = ?)";
	$requete_update_avec_autonomie = "UPDATE table_batteries SET `autonomie_mesuree`=?,`date_mesure_autonomie`=?,`date_derniere_operation`=?,`type_operation`=? 
										WHERE `table_batteries`.`id_batteries`=?";
	$requete_update_sans_autonomie = "UPDATE table_batteries SET `date_derniere_operation`=?,`type_operation`=? WHERE `table_batteries`.`id_batteries`=?";
	
	
	$requete_update_sans_autonomie_sans_modification_etat = "UPDATE table_batteries SET `date_derniere_operation`=?,`type_operation`=? WHERE `table_batteries`.`id_batteries`=?";
	$requete_update_sans_autonomie_avec_modification_etat = "UPDATE table_batteries SET `etats`=?,`date_mesure_autonomie`=?,`date_derniere_operation`=?,`type_operation`=? 
										WHERE `table_batteries`.`id_batteries`=?";
	
	
	$requete_update_avec_autonomie_sans_modification_etat = "UPDATE table_batteries SET `autonomie_mesuree`=?,`date_mesure_autonomie`=?,`date_derniere_operation`=?,`type_operation`=? 
										WHERE `table_batteries`.`id_batteries`=?";
	$requete_update_avec_autonomie_avec_modification_etat = "UPDATE table_batteries SET `etats`=?,`autonomie_mesuree`=?,`date_mesure_autonomie`=?,`date_derniere_operation`=?,`type_operation`=? 
										WHERE `table_batteries`.`id_batteries`=?";
	
	
	
	
	$requete_insertion_table_historique = "INSERT INTO `table_historique`
	(`id_batterie_historique`, `date_historique`, `id_etat_suivant_historique`, 
	`id_etat_precedent_historique`, `switch_ope_evt`, `id_ope_evt`, `divers_historique`) 
	VALUES (?,?,?,?,?,?,?)";


	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=01;
	$data=array();
	//preparation des requetes
	$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_update_avec_autonomie = mysqli_prepare($db, $requete_update_avec_autonomie);
	$stmt_update_sans_autonomie = mysqli_prepare($db, $requete_update_sans_autonomie);
	$stmt_update_sans_autonomie_sans_modification_etat = mysqli_prepare($db, $requete_update_sans_autonomie_sans_modification_etat);
	$stmt_update_sans_autonomie_avec_modification_etat = mysqli_prepare($db, $requete_update_sans_autonomie_avec_modification_etat);
	$stmt_update_avec_autonomie_sans_modification_etat = mysqli_prepare($db, $requete_update_avec_autonomie_sans_modification_etat);
	$stmt_update_avec_autonomie_avec_modification_etat = mysqli_prepare($db, $requete_update_avec_autonomie_avec_modification_etat);
	
	
	
	$stmt_insertion_table_historique = mysqli_prepare($db,$requete_insertion_table_historique);
	//
	if($stmt_verification)
	{
		if(isset($_POST['id_batterie']) && $_POST['id_batterie']!="" 
			&& isset($_POST['id_operation']) && $_POST['id_operation']!=""
			&& isset($_POST['date']) && $_POST['date']!=""
			&& isset($_POST['modif_autonomie']) && $_POST['modif_autonomie']!=""
			&& isset($_POST['modif_etat']) && $_POST['modif_etat']!=""
		    
		    )
		{
			//nettoyage des informations provenant de POST
			if((filter_input(INPUT_POST,'id_batterie',FILTER_SANITIZE_NUMBER_INT)==FALSE) ||
			(filter_input(INPUT_POST,'id_operation',FILTER_SANITIZE_NUMBER_INT)==FALSE) ||
			(filter_input(INPUT_POST,'modif_autonomie',FILTER_SANITIZE_NUMBER_INT)==FALSE) ||
			(filter_input(INPUT_POST,'modif_etat',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_batterie_base=filter_input(INPUT_POST,'id_batterie',FILTER_SANITIZE_NUMBER_INT);
				$id_operation_base=filter_input(INPUT_POST,'id_operation',FILTER_SANITIZE_NUMBER_INT);
				$modif_autonomie_base=filter_input(INPUT_POST,'modif_autonomie',FILTER_SANITIZE_NUMBER_INT);
				
				//
				$modif_etat_base=filter_input(INPUT_POST,'modif_etat',FILTER_SANITIZE_NUMBER_INT);
				$nouvel_etat_base=filter_input(INPUT_POST,'nouvel_etat',FILTER_SANITIZE_NUMBER_INT);
				$ancien_etat_base=filter_input(INPUT_POST,'ancien_etat',FILTER_SANITIZE_NUMBER_INT);
				
				
				//conversion de la date en timestamp
				date_default_timezone_set('Europe/Paris');
				$date_base = strtotime($_POST['date'])+7200; //7200 correspond au décalage horaire (+2 heurses)
				//
				//
				//$nouvel_etat_base=filter_input(INPUT_POST,'modif_autonomie',FILTER_SANITIZE_NUMBER_INT);
				//
				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_batterie_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							//avec modification de l'autonomie
							if($modif_autonomie_base == 2)
							{
								if(isset($_POST['autonomie']) && $_POST['autonomie']!="")
								{
								//nettoyage des informations provenant de POST
									if((filter_input(INPUT_POST,'autonomie',FILTER_SANITIZE_NUMBER_INT)==FALSE))
									{
										//erreur de typage
										$data['resultat']=$msg['code_echec_04']['id']; 
									}
									else	//  les données sont valides
									{
										$autonomie_base=filter_input(INPUT_POST,'autonomie',FILTER_SANITIZE_NUMBER_INT);
										//avec modification de l'état
										if($modif_etat_base == 2)
										{
										    if(isset($_POST['nouvel_etat']) && $_POST['nouvel_etat']!="")
										    {
										        if((filter_input(INPUT_POST,'nouvel_etat',FILTER_SANITIZE_NUMBER_INT)==FALSE))
										        {
										            //erreur de typage
										            $data['resultat']=$msg['code_echec_04']['id'];
										        }
										        else	//  les données sont valides
										        {
										            $nouvel_etat_base=filter_input(INPUT_POST,'nouvel_etat',FILTER_SANITIZE_NUMBER_INT);
													//mise à jour de l'historique
													if($stmt_insertion_table_historique)
													{
														
														//insertion dans la table historique
														if($stmt_insertion_table_historique)
														{
															//insertion dans la table historique
															//nouvel état, n autonomie inchangée
															if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiiis',
															$id_batterie_base, $date_base,
															$nouvel_etat_base,$ancien_etat_base, 
															$historique_ope,
															$id_operation_base, $defaut_divers_historique))
															{
																mysqli_execute($stmt_insertion_table_historique);
																$data['resultat_insertion']= "insertion_ok";

															}
															else
															{
																$data['resultat_insertion'] = $msg['code_echec_01']['id'];
															}
														}
													}


										            if(mysqli_stmt_bind_param($stmt_update_avec_autonomie_avec_modification_etat,'iiiiii',
										                      $nouvel_etat_base,$autonomie_base,$date_base,$date_base,$id_operation_base,$id_batterie_base))
										            {
										                if(mysqli_execute($stmt_update_avec_autonomie_avec_modification_etat))
										                {
										                    $data['resultat'] = $msg['code_ok']['id'];;
										                }
										                else
										                {
										                    $data['resultat'] = $msg['code_echec_01']['id'];
										                }
										            }
										            else
										            {
										                //erreur de bind
										                $data['resultat'] = $msg['code_echec_06']['id'];
										            }
										        }
										    }
										    else
										    {
										        $data['resultat'] = $msg['code_echec_01']['id'];
										    }
										}
										else //sans modification de l'état
										{
											if($stmt_insertion_table_historique)
											{
												
												//insertion dans la table historique
												if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiiis',
												$id_batterie_base, $date_base,$id_operation_base,
												$ancien_etat_base, $ancien_etat_base, $defaut_evenement_historique,$defaut_divers_historique))
												{
													mysqli_execute($stmt_insertion_table_historique);
													$data['resultat_insertion']= "insertion_ok";
												}
												if($stmt_insertion_table_historique)
												{
													//insertion dans la table historique
													//ancien  état, ancien etat nouvelle autonomie
													if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiiis',
													$id_batterie_base, $date_base,
													$ancien_etat_base,$ancien_etat_base, 
													$historique_ope,
													$id_operation_base, $defaut_divers_historique))
													{
														mysqli_execute($stmt_insertion_table_historique);
														$data['resultat_insertion']= "insertion_ok";

													}
													else
													{
														$data['resultat_insertion'] = $msg['code_echec_01']['id'];
													}
												}





											}
										    if(mysqli_stmt_bind_param($stmt_update_avec_autonomie_sans_modification_etat,'iiiii',$autonomie_base,$date_base,$date_base,$id_operation_base,$id_batterie_base))
										    {
										        if(mysqli_execute($stmt_update_avec_autonomie_sans_modification_etat))
										        {
										            $data['resultat'] = $msg['code_ok']['id'];;
										        }
										        else
										        {
										            $data['resultat'] = $msg['code_echec_01']['id'];
										        }
										    }
										    else
										    {
										        //erreur de bind
										        $data['resultat'] = $msg['code_echec_06']['id'];
										    }

										}

									}
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];
								}
							}
							else	//sans modification de l'autonomie
							{
							    if($modif_etat_base == 2) //avec mofication de l'état
							    {
							        if(isset($_POST['nouvel_etat']) && $_POST['nouvel_etat']!="")
							        {
							            if((filter_input(INPUT_POST,'nouvel_etat',FILTER_SANITIZE_NUMBER_INT)==FALSE))
							            {
							                //erreur de typage
							                $data['resultat']=$msg['code_echec_04']['id'];
							            }
							            else	//  les données sont valides
							            {
							                $nouvel_etat_base=filter_input(INPUT_POST,'nouvel_etat',FILTER_SANITIZE_NUMBER_INT);
											if($stmt_insertion_table_historique)
											{
												//insertion dans la table historique
												//nouvel état, n autonomie inchangée
												if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiiis',
												$id_batterie_base, $date_base,
												$nouvel_etat_base,$ancien_etat_base, 
												$historique_ope,
												$id_operation_base, $defaut_divers_historique))
												{
													mysqli_execute($stmt_insertion_table_historique);
													$data['resultat_insertion']= "insertion_ok";

												}
												else
												{
													$data['resultat_insertion'] = $msg['code_echec_01']['id'];
												}
											}
											if(mysqli_stmt_bind_param($stmt_update_sans_autonomie_avec_modification_etat,'iiiii',$nouvel_etat_base,$date_base,$date_base,$id_operation_base,$id_batterie_base))
							                {
							                    if(mysqli_execute($stmt_update_sans_autonomie_avec_modification_etat))
							                    {
							                        $data['resultat'] = $msg['code_ok']['id'];;
							                    }
							                    else
							                    {
							                        $data['resultat'] = $msg['code_echec_01']['id'];
							                    }
							                }
							                else
							                {
							                    //erreur de bind
							                    $data['resultat'] = $msg['code_echec_06']['id'];
							                }
							            }
							        }
							        else
							        {
							            $data['resultat'] = $msg['code_echec_01']['id'];
							        }
							    }
							    else //sans autonomie et sans modification état
							    {
									if($stmt_insertion_table_historique)
									{
										
										//insertion dans la table historique
										//nouvel état = ancien état
										if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiiis',
										$id_batterie_base, $date_base,
										$ancien_etat_base,$ancien_etat_base, 
										$historique_ope,
										$id_operation_base, $defaut_divers_historique))
										{
											mysqli_execute($stmt_insertion_table_historique);
											$data['resultat_insertion']= "insertion_ok";

										}
										else
										{
											$data['resultat_insertion'] = $msg['code_echec_01']['id'];
										}
									}
									else
									{
										$data['resultat_insertion'] = $msg['code_echec_01']['id'];
									}
									if(mysqli_stmt_bind_param($stmt_update_sans_autonomie_sans_modification_etat,'iii',$date_base,$id_operation_base,$id_batterie_base))
							        {
							            if(mysqli_execute($stmt_update_sans_autonomie_sans_modification_etat))
							            {
							                $data['resultat'] = $msg['code_ok']['id'];;
							            }
							            else
							            {
							                $data['resultat'] = $msg['code_echec_01']['id'];
							            }
							        }
							        else
							        {
							            //erreur de bind
							            $data['resultat'] = $msg['code_echec_06']['id'];
							        }
							    }
							    
							    
								
							}

						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}

	mysqli_stmt_close($stmt_verification);
	mysqli_stmt_close($stmt_update_avec_autonomie);
	mysqli_stmt_close($stmt_update_sans_autonomie);
	//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>