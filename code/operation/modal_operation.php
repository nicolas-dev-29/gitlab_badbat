	<!-- Modal ajout operation-->
	<div class="modal fade"  id="modal_ajout_operation" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_operation" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'une nouvelle opération</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations d'une nouvelle opération</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<label id="modal_ajout_operation_nom_label" class="control-label"  for="modal_ajout_operation_nom">nom:</label> 
							<input id="modal_ajout_operation_nom" type="text" class="form-control" placeholder="nom de l'opération" autocomplete="off"   aria-label="modal_ajout_operation_nom_aide"   required  >
							<span id="modal_ajout_operation_nom_aide" class="help-block small">entrer le nom de l'opération</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_operation_erreur"> erreur!</span></div>
						</div>		
						<div class="form-group">
							<div class="custom-control custom-switch">
							  <input type="checkbox" class="custom-control-input" id="modal_ajout_operation_mesure_autonomie_choix">	  
							  <label class="custom-control-label" id="modal_ajout_operation_mesure_autonomie_choix_label" for="modal_ajout_operation_mesure_autonomie_choix">  modification autonomie</label>
							</div>
						</div>
						<div class="form-group">
							<label id="modal_ajout_operation_divers_label" for="modal_ajout_operation_divers">divers:</label>
							<textarea  id="modal_ajout_operation_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_ajout_operation_divers_aide"></textarea>
							<div class="invalid-feedback"><span id="modal_ajout_operation_divers_erreur"> erreur!</span></div>
						</div>
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_operation_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	<!-- Modal modification operation-->
	<div class="modal fade" id="modal_modification_operation" tabindex="-1" role="dialog" aria-labelledby="modal_modification_operation" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">Modification de l'opération</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations de l'opération</p>	
				<!--	<form class="needs-validation" novalidate > -->
						<div class="form-group">
							<span id="modal_modification_operation_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du opération pour faire la requete AJAX directement avec l'id au opération du nom-->
							<label id="modal_modification_operation_nom_label" class="control-label"  for="modal_modification_operation_nom">nom:</label> 
							<input id="modal_modification_operation_nom" type="text" class="form-control"  autocomplete="off"   aria-label="modal_ajout_operation_nom_aide"  title=" entrez entre 3 et 20 caractères"   required   >
							<span id="modal_modification_operation_nom_aide" class="help-block small">entrer le nouveau nom de l'opération</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_operation_erreur"> erreur!</span></div>
						</div>
							<div class="form-group">
								<div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" id="modal_modification_operation_mesure_autonomie_choix">	  
								<label class="custom-control-label" id="modal_modification_operation_mesure_autonomie_choix_label" for="modal_modification_operation_mesure_autonomie_choix">  modification autonomie</label>
								</div>
							</div>
							<div class="form-group">
								<label  for="modal_modification_operation_divers">divers:</label>
								<textarea  id="modal_modification_operation_divers" class="form-control" rows="3" aria-label="modal_ajout_operation_divers_aide" title=" entrez moins de 255 caractères"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_operation_divers_erreur"> erreur!</span></div>
							</div>
						
				<!--	</form> -->
				</div>										
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-warning" id="modal_modification_operation_button">modifier</button>
				</div>	
			</div>
		</div>
	</div>
	<!-- Modal suppression d'un opération-->
	<div class="modal fade" id="modal_suppression_operation" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_operation" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'une opération</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer l'opération suivante:</p>	
					<span id="modal_suppression_operation_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du opération pour faire la requete AJAX directement avec l'id au opération du nom-->
					<div class="texte_important">nom: </div><span id="modal_suppression_operation_nom"  >nom</span>  <br>  
					<div class="texte_important">divers: </div>	
					<div class="modal_span">
						<span id="modal_suppression_operation_divers" ></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_operation_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>