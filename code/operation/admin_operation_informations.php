<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation des requêtes
/*	$requete_information="SELECT `autonomie_mesuree`,`autonomie_requise`,`id_operation`,`nom_operation`,`mesure_autonomie_check`,`divers_operation`
	FROM `table_batteries` 
	INNER JOIN `table_operations`
	ON `table_operations`.`id_operation` = `table_batteries`.`type_operation`
	WHERE `id_operation` = ?";
*/	
	// récupere les informations de l'opération
	$requete_information_operation="SELECT `id_operation`,`nom_operation`,`mesure_autonomie_check`
	FROM `table_operations` 
	WHERE `id_operation` = ?";
	
	// va permettre de récupérer les informations des dernières autonomie
	$requete_information_batterie="SELECT `id_batteries`,`autonomie_mesuree`, `autonomie_requise`,`nom_etat`,`id_etat`
	FROM `table_batteries` AS TB
     JOIN `table_etats` AS TE
        ON (TB.etats = TE.id_etat)
	WHERE `id_batteries` = ?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=01;
	$data=array();
	
	//preparation des requetes
	$stmt_information_operation = mysqli_prepare($db,$requete_information_operation);
	$stmt_information_batterie = mysqli_prepare($db,$requete_information_batterie);
	
	
	if($stmt_information_operation)
	{
		if(isset($_POST['id_operation']) && $_POST['id_operation']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id_operation',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_operation_base=filter_input(INPUT_POST,'id_operation',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_information_operation,'i',$id_operation_base))
				{
					if(mysqli_stmt_execute($stmt_information_operation))
					{
						mysqli_stmt_store_result($stmt_information_operation);
						$nbre = mysqli_stmt_num_rows($stmt_information_operation);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							mysqli_stmt_bind_result($stmt_information_operation,
							$ligne['id_operation'],$ligne['nom_operation'],
							$ligne['mesure_autonomie_choix']);
							
							mysqli_stmt_fetch($stmt_information_operation);
							$data['resultat'] = $msg['code_ok']['id'];
							//$data['autonomie_derniere_valeur'] = htmlentities($ligne['autonomie_mesuree'],ENT_QUOTES,'UTF-8');
							//$data['autonomie_requise'] = htmlentities($ligne['autonomie_requise'],ENT_QUOTES,'UTF-8');
							$data['id_operation'] = 			htmlentities($ligne['id_operation'],ENT_QUOTES,'UTF-8');
							$data['nom'] = 						htmlspecialchars($ligne['nom_operation'],ENT_QUOTES,'UTF-8');
							$data['mesure_autonomie_choix'] = 	htmlentities($ligne['mesure_autonomie_choix'],ENT_QUOTES,'UTF-8');
							
							
							
						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}

if($stmt_information_batterie)
	{
		if(isset($_POST['id_batterie']) && $_POST['id_batterie']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id_batterie',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_batterie_base=filter_input(INPUT_POST,'id_batterie',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_information_batterie,'i',$id_batterie_base))
				{
					if(mysqli_stmt_execute($stmt_information_batterie))
					{
						mysqli_stmt_store_result($stmt_information_batterie);
						$nbre = mysqli_stmt_num_rows($stmt_information_batterie);
						if($nbre == 1)	// l'id est unique et est trouvé
						{
							mysqli_stmt_bind_result($stmt_information_batterie,
							    $ligne['id_batterie'],$ligne['autonomie_mesuree'],
							    $ligne['autonomie_requise'],$ligne['nom_etat'],$ligne['id_etat']);
							
							mysqli_stmt_fetch($stmt_information_batterie);
							$data['resultat'] = $msg['code_ok']['id'];
							$data['autonomie_derniere_valeur'] = 	htmlentities($ligne['autonomie_mesuree'],ENT_QUOTES,'UTF-8');
							$data['autonomie_requise'] = 			htmlentities($ligne['autonomie_requise'],ENT_QUOTES,'UTF-8');
							$data['nom_etat'] = 					htmlspecialchars($ligne['nom_etat'],ENT_QUOTES,'UTF-8');
							$data['id_etat'] = 						htmlentities($ligne['id_etat'],ENT_QUOTES,'UTF-8');
							
						}
						else	//pas de résultat trouvé dans la requete->modification impossible
						{
							$data['resultat'] = $msg['code_echec_07']['id'];
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];			
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}

	
	
								

	mysqli_stmt_close($stmt_information_batterie);
	mysqli_stmt_close($stmt_information_operation);
	
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>