<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//preparation des requêtes
	$requete_verification = "SELECT * FROM table_equipements WHERE nom_equipement = ?";
	$requete_insertion = "INSERT INTO table_equipements (`nom_equipement` ,`divers_equipement`)	VALUES (?,?)";
	
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	
	//preparation des requêtes
	$stmt_verification = mysqli_prepare($db,$requete_verification);
	$stmt_insertion = mysqli_prepare($db,$requete_insertion);
	
	$valeur_base=100;
	
	if($stmt_verification)
	{
		if(isset($_POST['equipement']) && preg_match($pattern_admin_nom_verification,$_POST['equipement'])
		   && preg_match($pattern_admin_divers_verification,$_POST['divers']))
		{
			$nom_temp=filter_input(INPUT_POST,'equipement',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
			$nom_base = mb_strtoupper($nom_temp,'UTF-8');
			$divers_base=filter_input(INPUT_POST,'divers',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
			
			if(mysqli_stmt_bind_param($stmt_verification,'s',$nom_base))
			{
				if(mysqli_stmt_execute($stmt_verification))
				{
					mysqli_stmt_store_result($stmt_verification);
					$nbre = mysqli_stmt_num_rows($stmt_verification);
					if($nbre>0)
					{
						//ce n'est pas un code d'erreur de la fonction mais du résultat (tout s'est bien passé mais l'enregistrement est déjà présent)
						$data['resultat'] = $msg['code_echec_02']['id'];
					}
					else
					{
						if(mysqli_stmt_bind_param($stmt_insertion,'ss',$nom_base,$divers_base))
						{
							if(mysqli_stmt_execute($stmt_insertion))
							{
								$data['resultat'] = $msg['code_ok']['id'];		
							}
							else
							{
								//erreur d'execute
								$data['resultat'] = $msg['code_echec_01']['id'];	
							}
						}
						else
						{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
						}
					}
				}
				else
				{
					//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];	
				}
			}
			else
			{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];	
			}
		}	
		else
		{	//le champs est vide ou le $_POST n'est pas "set"
			$data['resultat'] = $msg['code_echec_03']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];	
	}

	mysqli_stmt_close($stmt_insertion);
	mysqli_stmt_close($stmt_verification);
								
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>