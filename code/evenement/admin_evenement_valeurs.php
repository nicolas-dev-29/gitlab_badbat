<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_valeurs = "SELECT `id_evenement`,`nom_evenement`,
	`id_etat_precedent_evenement`,`id_etat_suivant_evenement`,
	`presence_temps_maintien_etat`,`temps_maintien_etat`,`presence_utilisateur_fin_maintien`,`id_utilisateur_fin_maintien`,
	`presence_temps_prevenance_etat`,`temps_prevenance_changement_etat`,`presence_utilisateur_fin_prevenance`,`id_utilisateur_fin_prevenance`,
	`divers_evenement`
	FROM table_evenements 
	WHERE `id_evenement` = ?";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//preparation
	$stmt_valeurs = mysqli_prepare($db,$requete_valeurs);
	if($stmt_valeurs)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_valeurs,'i',$id_base))
				{								
					if(mysqli_stmt_execute($stmt_valeurs))	
					{
						//$nbre = mysqli_stmt_affected_rows($stmt_valeurs);
						mysqli_stmt_bind_result($stmt_valeurs,
						$ligne['id_evenement'],$ligne['nom_evenement'],
						$ligne['id_etat_prec'],$ligne['id_etat_suiv'],
						$ligne['pres_temps_mnt_etat'],$ligne['temps_mnt_etat'],$ligne['pres_utilisateur_fin_mnt'],$ligne['id_utilisateur_fin_mnt'],
						$ligne['pres_temps_prev_etat'],$ligne['temps_prev_changement_etat'],$ligne['pres_utilisateur_fin_prev'],$ligne['id_utilisateur_fin_prev'],
						$ligne['divers_evenement']);
						$index=0;
						while(mysqli_stmt_fetch($stmt_valeurs))
						{
							$data['resultat'] = $msg['code_ok']['id'];
							$data['id'] = htmlentities($ligne['id_evenement'],ENT_QUOTES,'UTF-8');
							$data['nom'] = htmlspecialchars($ligne['nom_evenement'],ENT_QUOTES,'UTF-8');
							$data['id_etat_precedent'] = htmlentities($ligne['id_etat_prec'],ENT_QUOTES,'UTF-8');
							$data['id_etat_suivant'] = htmlentities($ligne['id_etat_suiv'],ENT_QUOTES,'UTF-8');
							$data['presence_temps_maintien_etat'] = htmlentities($ligne['pres_temps_mnt_etat'],ENT_QUOTES,'UTF-8');
							$data['temps_maintien_etat'] = htmlentities($ligne['temps_mnt_etat'],ENT_QUOTES,'UTF-8');
							$data['presence_utilisateur_fin_maintien'] = htmlentities($ligne['pres_utilisateur_fin_mnt'],ENT_QUOTES,'UTF-8');
							$data['id_utilisateur_fin_maintien'] = htmlentities($ligne['id_utilisateur_fin_mnt'],ENT_QUOTES,'UTF-8');
							$data['presence_temps_prevenance_etat'] = htmlentities($ligne['pres_temps_prev_etat'],ENT_QUOTES,'UTF-8');
							$data['temps_prevenance_changement_etat'] = htmlentities($ligne['temps_prev_changement_etat'],ENT_QUOTES,'UTF-8');
							$data['presence_utilisateur_fin_prevenance'] = htmlentities($ligne['pres_utilisateur_fin_prev'],ENT_QUOTES,'UTF-8');
							$data['id_utilisateur_fin_prevenance'] = htmlentities($ligne['id_utilisateur_fin_prev'],ENT_QUOTES,'UTF-8');
							$data['divers'] = htmlspecialchars($ligne['divers_evenement'],ENT_QUOTES,'UTF-8');
						}
					}
					else 	
					{	//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
	mysqli_stmt_close($stmt_valeurs);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>