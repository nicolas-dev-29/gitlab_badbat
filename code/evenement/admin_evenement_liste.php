<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_evenement`,`nom_evenement`,T1.`nom_etat`AS nom_prec, T2.`nom_etat`AS nom_suiv ,
		`presence_temps_maintien_etat`,`temps_maintien_etat`,`presence_utilisateur_fin_maintien`,`id_utilisateur_fin_maintien`,T3.`nom_utilisateur` AS `nom_utilisateur_maintien`, T3.`prenom_utilisateur` AS `prenom_utilisateur_maintien`,
		`presence_temps_prevenance_etat`,`temps_prevenance_changement_etat`, `presence_utilisateur_fin_prevenance`, `id_utilisateur_fin_prevenance`,T4.`nom_utilisateur` AS `nom_utilisateur_prevenance`, T4.`prenom_utilisateur` AS `prenom_utilisateur_prevenance`,`divers_evenement` 
	FROM table_evenements AS TE
	LEFT JOIN table_etats AS T1
		ON ( TE.id_etat_precedent_evenement = T1.id_etat)
    LEFT JOIN table_etats AS T2
		ON ( TE.id_etat_suivant_evenement = T2.id_etat)
	LEFT JOIN table_utilisateurs AS T3
		ON (TE.id_utilisateur_fin_maintien = T3.id_utilisateur)	
	LEFT JOIN table_utilisateurs AS T4
		ON (TE.id_utilisateur_fin_prevenance = T4.id_utilisateur)
	GROUP BY `id_evenement`";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_evenement'],$ligne['nom_evenement'],
			$ligne['nom_prec'],$ligne['nom_suiv'],
			$ligne['pres_temps_maintien_etat'],$ligne['temps_maintien_etat'],$ligne['pres_utilisateur_maintien_etat'],$ligne['id_utilisateur_fin_maintien'],$ligne['utilisateur_fin_maintien_nom'],$ligne['utilisateur_fin_maintien_prenom'],
			$ligne['pres_temps_prevenance_etat'],$ligne['temps_prevenance_etat'],$ligne['pres_utilisateur_prevenance_etat'],$ligne['id_utilisateur_fin_prevenance'],$ligne['utilisateur_fin_prevenance_nom'],$ligne['utilisateur_fin_prevenance_prenom'],
			$ligne['divers_evenement']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['nombre'] = $nbre;
				$data[$index]['resultat'] = $msg['code_ok']['id'];
				$data[$index]['id'] = htmlentities($ligne['id_evenement'],ENT_QUOTES,'UTF-8');
				$data[$index]['nom'] = htmlspecialchars($ligne['nom_evenement'],ENT_QUOTES,'UTF-8');
				$data[$index]['etat_precedent'] = htmlentities($ligne['nom_prec'],ENT_QUOTES,'UTF-8');
				$data[$index]['etat_suivant'] = htmlentities($ligne['nom_suiv'],ENT_QUOTES,'UTF-8');
				
				$data[$index]['pres_temps_maintien_etat'] = htmlentities($ligne['pres_temps_maintien_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['temps_maintien_etat'] = htmlentities($ligne['temps_maintien_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['pres_utilisateur_maintien_etat'] = htmlentities($ligne['pres_utilisateur_maintien_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['id_utilisateur_fin_maintien'] = htmlentities(($ligne['id_utilisateur_fin_maintien']),ENT_QUOTES,'UTF-8');
				$data[$index]['utilisateur_fin_maintien_nom'] = htmlentities(($ligne['utilisateur_fin_maintien_nom']),ENT_QUOTES,'UTF-8');
				$data[$index]['utilisateur_fin_maintien_prenom'] = htmlentities($ligne['utilisateur_fin_maintien_prenom'],ENT_QUOTES,'UTF-8');
				
				$data[$index]['pres_temps_prevenance_etat'] = htmlentities($ligne['pres_temps_prevenance_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['temps_prevenance_changement_etat'] = htmlentities($ligne['temps_prevenance_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['pres_utilisateur_prevenance_etat'] = htmlentities($ligne['pres_utilisateur_prevenance_etat'],ENT_QUOTES,'UTF-8');
				$data[$index]['id_utilisateur_fin_prevenance'] = htmlentities(($ligne['id_utilisateur_fin_prevenance']),ENT_QUOTES,'UTF-8');
				$data[$index]['utilisateur_fin_prevenance_nom'] = htmlentities(($ligne['utilisateur_fin_prevenance_nom']),ENT_QUOTES,'UTF-8');
				$data[$index]['utilisateur_fin_prevenance_prenom'] = htmlentities($ligne['utilisateur_fin_prevenance_prenom'],ENT_QUOTES,'UTF-8');
				
				$data[$index]['divers'] = htmlspecialchars($ligne['divers_evenement'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>