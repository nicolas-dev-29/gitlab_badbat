	<!-- Modal ajout evenement-->
	<div class="modal fade"  id="modal_ajout_evenement" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_evenement" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'un evenement</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel evenement</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<span id="modal_modification_evenement_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du état pour faire la requete AJAX directement avec l'id au état du nom-->
							<label id="modal_ajout_evenement_nom_label" class="control-label" for="modal_ajout_evenement_nom">nom:</label> 
							<input id="modal_ajout_evenement_nom" type="text" class="form-control" placeholder="nom de l'evenement" autocomplete="off"   aria-label="modal_ajout_evenement_nom_aide"   required  >
							<span id="modal_ajout_evenement_nom_aide" class="help-block small">entrer l'evenement de la batterie</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_evenement_erreur"> erreur!</span></div>
							<div class="form-group">
								<label id="modal_ajout_evenement_divers_label"for="modal_ajout_evenement_divers">divers:</label>
								<textarea  id="modal_ajout_evenement_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_ajout_evenement_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_evenement_divers_erreur"> erreur!</span></div>
							</div>
						</div>	
							<div class="row">
								<div class="col-5 text-center">
									état initial
								</div>
								<div class="col-2 text-center">
								<span class="fa fa-arrow-right fa-1x "></span>
								</div>
								<div class="col-5 text-center">
									état final
								</div>
							</div>
							<div class="row">
								<div class="col-5">
									<select id="modal_ajout_evenement_etat_initial" class="custom-select">
										<!-- rempli par jquerry -->
									</select>
								</div>
								<div class="col-2 text-center">
								<span class="fa fa-arrow-right fa-1x "></span>
								</div>
								<div class="col-5">
									<select id="modal_ajout_evenement_etat_final" class="custom-select">
										<!-- rempli par jquerry -->
									</select>
								</div>
							</div>
							<div class="row d-flex align-items-start">
								<div class="col-6 my-3">
									<div class="custom-control custom-switch">
									  <input type="checkbox" class="custom-control-input" id="modal_ajout_evenement_temps_maintien" value="FALSE">
									  <label class="custom-control-label text-center" for="modal_ajout_evenement_temps_maintien">ajouter une durée avant de basculer</label>
									</div>
									<div id="modal_ajout_evenement_temps_maintien_choix" class="element_cache_debut ">
											 <label class="d-inline-block" for="modal_ajout_evenement_temps_maintien_annee"><small><strong>nombre d'année</strong></small></label>
											  <select id="modal_ajout_evenement_temps_maintien_annee" class="custom-select">
												<option selected value="0">0</option>
												<option  value="1">1</option>
												<option  value="2">2</option>
												<option  value="3">3</option>
												<option  value="4">4</option>
												<option  value="5">5</option>
											</select>
											 <label class="d-inline-block" for="modal_ajout_evenement_temps_maintien_mois"><small><strong>nombre de mois</strong></small></label>
										<select id="modal_ajout_evenement_temps_maintien_mois" class="custom-select">
											<option selected value="0">0</option>
											<option  value="1">1</option>
											<option  value="2">2</option>
											<option  value="3">3</option>
											<option  value="4">4</option>
											<option  value="5">5</option>
											<option  value="6">6</option>
											<option  value="7">7</option>
											<option  value="8">8</option>
											<option  value="9">9</option>
											<option  value="10">10</option>
											<option  value="11">11</option>
										</select>
										<label class="d-inline-block" for="modal_ajout_evenement_temps_maintien_jours"><small><strong>nombre de jours</strong></small></label>
										<select id="modal_ajout_evenement_temps_maintien_jours" class="custom-select">
											<option selected value="0">0</option>
											<option  value="1">1</option>
											<option  value="2">2</option>
											<option  value="3">3</option>
											<option  value="4">4</option>
											<option  value="5">5</option>
											<option  value="6">6</option>
											<option  value="7">7</option>
											<option  value="8">8</option>
											<option  value="9">9</option>
											<option  value="10">10</option>
											<option  value="11">11</option>
											<option  value="12">12</option>
											<option  value="13">13</option>
											<option  value="14">14</option>
											<option  value="15">15</option>
											<option  value="16">16</option>
											<option  value="17">17</option>
											<option  value="18">18</option>
											<option  value="19">19</option>
											<option  value="20">20</option>
											<option  value="21">21</option>
											<option  value="22">22</option>
											<option  value="23">23</option>
											<option  value="24">24</option>
											<option  value="25">25</option>
											<option  value="26">26</option>
											<option  value="27">27</option>
											<option  value="28">28</option>
											<option  value="29">29</option>
											<option  value="30">30</option>
											
										</select>
										<div class="custom-control custom-switch my-3">
										  <input type="checkbox" class="custom-control-input" id="modal_ajout_evenement_temps_maintien_utilisateur" value="FALSE">
										  <label class="custom-control-label text-center" for="modal_ajout_evenement_temps_maintien_utilisateur">prévenir un utilisateur</label>
										</div>
										<select id="modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom" class="custom-select element_cache_debut">
											<!-- rempli par jquery -->
										</select>
									</div>	
								</div>
								<div class="col-6 my-3 ">
									<div class="custom-control   custom-switch">
									  <input type="checkbox" class="custom-control-input " id="modal_ajout_evenement_temps_prevenance" value="FALSE">
									  <label class="custom-control-label text-center" for="modal_ajout_evenement_temps_prevenance">ajouter un delai de prevenance</label>
									</div>
								<div id="modal_ajout_evenement_temps_prevenance_choix" class="element_cache_debut ">
									 <label class="d-inline-block" for="modal_ajout_evenement_temps_prevenance_annee"><small><strong>nombre d'année</strong></small></label>
									  <select id="modal_ajout_evenement_temps_prevenance_annee" class="custom-select">
										<option selected value="0">0</option>
										<option  value="1">1</option>
										<option  value="2">2</option>
										<option  value="3">3</option>
										<option  value="4">4</option>
										<option  value="5">5</option>
									</select>
									 <label class="d-inline-block" for="modal_ajout_evenement_temps_prevenance_mois"><small><strong>nombre de mois</strong></small></label>
								<select id="modal_ajout_evenement_temps_prevenance_mois" class="custom-select">
									<option selected value="0">0</option>
									<option  value="1">1</option>
									<option  value="2">2</option>
									<option  value="3">3</option>
									<option  value="4">4</option>
									<option  value="5">5</option>
									<option  value="6">6</option>
									<option  value="7">7</option>
									<option  value="8">8</option>
									<option  value="9">9</option>
									<option  value="10">10</option>
									<option  value="11">11</option>
								</select>
								<label class="d-inline-block" for="modal_ajout_evenement_temps_prevenance_jours"><small><strong>nombre de jours</strong></small></label>
								<select id="modal_ajout_evenement_temps_prevenance_jours" class="custom-select">
									<option selected value="0">0</option>
									<option  value="1">1</option>
									<option  value="2">2</option>
									<option  value="3">3</option>
									<option  value="4">4</option>
									<option  value="5">5</option>
									<option  value="6">6</option>
									<option  value="7">7</option>
									<option  value="8">8</option>
									<option  value="9">9</option>
									<option  value="10">10</option>
									<option  value="11">11</option>
									<option  value="12">12</option>
									<option  value="13">13</option>
									<option  value="14">14</option>
									<option  value="15">15</option>
									<option  value="16">16</option>
									<option  value="17">17</option>
									<option  value="18">18</option>
									<option  value="19">19</option>
									<option  value="20">20</option>
									<option  value="21">21</option>
									<option  value="22">22</option>
									<option  value="23">23</option>
									<option  value="24">24</option>
									<option  value="25">25</option>
									<option  value="26">26</option>
									<option  value="27">27</option>
									<option  value="28">28</option>
									<option  value="29">29</option>
									<option  value="30">30</option>
								</select>
								<div class="custom-control custom-switch my-3">
									<input type="checkbox" class="custom-control-input" id="modal_ajout_evenement_temps_prevenance_utilisateur" value="FALSE">
									<label class="custom-control-label text-center" for="modal_ajout_evenement_temps_prevenance_utilisateur">prévenir un utilisateur</label>
								<label class="d-inline-block" for="modal_ajout_evenement_temps_prevenance_utilisateur">
								</div>
								<select id="modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom" class="custom-select element_cache_debut">
									<!-- rempli par jquery -->
								</select>
							</div>	
						</div>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
				<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_evenement_button">sauvegarder</button> 
			</div>
			</div>
		</div>
	</div>
	</div>
	<!-- Modal modification evenement-->
	<div class="modal fade"  id="modal_modification_evenement" tabindex="-1" role="dialog" aria-labelledby="modal_modification_evenement" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">modification d'un evenement</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel evenement</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<label id="modal_modification_evenement_nom_label" class="control-label" for="modal_modification_evenement_nom">nom:</label> 
							<input id="modal_modification_evenement_nom" type="text" class="form-control" placeholder="nom de l'evenement" autocomplete="off"   aria-label="modal_modification_evenement_nom_aide"   required  >
							<span id="modal_modification_evenement_nom_aide" class="help-block small">entrer l'evenement de la batterie</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_evenement_erreur"> erreur!</span></div>
							<div class="form-group">
								<label id="modal_modification_evenement_divers_label"for="modal_modification_evenement_divers">divers:</label>
								<textarea  id="modal_modification_evenement_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_modification_evenement_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_evenement_divers_erreur"> erreur!</span></div>
							</div>
						</div>	
							<div class="row">
								<div class="col-5 text-center">
									état initial
								</div>
								<div class="col-2 text-center">
								<span class="fa fa-arrow-right fa-1x "></span>
								</div>
								<div class="col-5 text-center">
									état final
								</div>
							</div>
							<div class="row">
								<div class="col-5">
									<select id="modal_modification_evenement_etat_initial" class="custom-select">
										<!-- rempli par jquerry -->
									</select>
								</div>
								<div class="col-2 text-center">
								<span class="fa fa-arrow-right fa-1x "></span>
								</div>
								<div class="col-5">
									<select id="modal_modification_evenement_etat_final" class="custom-select">
										<!-- rempli par jquerry -->
									</select>
								</div>
							</div>
							<div class="row d-flex align-items-start">
								<div class="col-6 my-3">
									<div class="custom-control custom-switch">
									  <input type="checkbox" class="custom-control-input" id="modal_modification_evenement_temps_maintien" value="FALSE">
									  <label class="custom-control-label text-center" for="modal_modification_evenement_temps_maintien">notifier une durée avant de basculer</label>
									</div>
									<div id="modal_modification_evenement_temps_maintien_choix" class="element_cache_debut ">
											 <label class="d-inline-block" for="modal_modification_evenement_temps_maintien_annee"><small><strong>nombre d'année</strong></small></label>
											  <select id="modal_modification_evenement_temps_maintien_annee" class="custom-select">
												<option selected value="0">0</option>
												<option  value="1">1</option>
												<option  value="2">2</option>
												<option  value="3">3</option>
												<option  value="4">4</option>
												<option  value="5">5</option>
											</select>
											 <label class="d-inline-block" for="modal_modification_evenement_temps_maintien_mois"><small><strong>nombre de mois</strong></small></label>
										<select id="modal_modification_evenement_temps_maintien_mois" class="custom-select">
											<option selected value="0">0</option>
											<option  value="1">1</option>
											<option  value="2">2</option>
											<option  value="3">3</option>
											<option  value="4">4</option>
											<option  value="5">5</option>
											<option  value="6">6</option>
											<option  value="7">7</option>
											<option  value="8">8</option>
											<option  value="9">9</option>
											<option  value="10">10</option>
											<option  value="11">11</option>
										</select>
										<label class="d-inline-block" for="modal_modification_evenement_temps_maintien_jours"><small><strong>nombre de jours</strong></small></label>
										<select id="modal_modification_evenement_temps_maintien_jours" class="custom-select">
											<option selected value="0">0</option>
											<option  value="1">1</option>
											<option  value="2">2</option>
											<option  value="3">3</option>
											<option  value="4">4</option>
											<option  value="5">5</option>
											<option  value="6">6</option>
											<option  value="7">7</option>
											<option  value="8">8</option>
											<option  value="9">9</option>
											<option  value="10">10</option>
											<option  value="11">11</option>
											<option  value="12">12</option>
											<option  value="13">13</option>
											<option  value="14">14</option>
											<option  value="15">15</option>
											<option  value="16">16</option>
											<option  value="17">17</option>
											<option  value="18">18</option>
											<option  value="19">19</option>
											<option  value="20">20</option>
											<option  value="21">21</option>
											<option  value="22">22</option>
											<option  value="23">23</option>
											<option  value="24">24</option>
											<option  value="25">25</option>
											<option  value="26">26</option>
											<option  value="27">27</option>
											<option  value="28">28</option>
											<option  value="29">29</option>
											<option  value="30">30</option>
										</select>
										<div class="custom-control custom-switch my-3">
										  <input type="checkbox" class="custom-control-input" id="modal_modification_evenement_temps_maintien_utilisateur" value="FALSE">
										  <label class="custom-control-label text-center" for="modal_modification_evenement_temps_maintien_utilisateur">prévenir un utilisateur</label>
										</div>
										<select id="modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom" class="custom-select element_cache_debut">
											<!-- rempli par jquery -->
										</select>
									</div>								
								</div>
								<div class="col-6 my-3 ">
									<div class="custom-control   custom-switch">
									  <input type="checkbox" class="custom-control-input " id="modal_modification_evenement_temps_prevenance" value="FALSE">
									  <label class="custom-control-label text-center" for="modal_modification_evenement_temps_prevenance">notifier un delai de prevenance</label>
									</div>
								<div id="modal_modification_evenement_temps_prevenance_choix" class="element_cache_debut ">
									 <label class="d-inline-block" for="modal_modification_evenement_temps_prevenance_annee"><small><strong>nombre d'année</strong></small></label>
									  <select id="modal_modification_evenement_temps_prevenance_annee" class="custom-select">
										<option selected value="0">0</option>
										<option  value="1">1</option>
										<option  value="2">2</option>
										<option  value="3">3</option>
										<option  value="4">4</option>
										<option  value="5">5</option>
									</select>
									 <label class="d-inline-block" for="modal_modification_evenement_temps_prevenance_mois"><small><strong>nombre de mois</strong></small></label>
								<select id="modal_modification_evenement_temps_prevenance_mois" class="custom-select">
									<option selected value="0">0</option>
									<option  value="1">1</option>
									<option  value="2">2</option>
									<option  value="3">3</option>
									<option  value="4">4</option>
									<option  value="5">5</option>
									<option  value="6">6</option>
									<option  value="7">7</option>
									<option  value="8">8</option>
									<option  value="9">9</option>
									<option  value="10">10</option>
									<option  value="11">11</option>
								</select>
								<label class="d-inline-block" for="modal_modification_evenement_temps_prevenance_jours"><small><strong>nombre de jours</strong></small></label>
								<select id="modal_modification_evenement_temps_prevenance_jours" class="custom-select">
									<option selected value="0">0</option>
									<option  value="1">1</option>
									<option  value="2">2</option>
									<option  value="3">3</option>
									<option  value="4">4</option>
									<option  value="5">5</option>
									<option  value="6">6</option>
									<option  value="7">7</option>
									<option  value="8">8</option>
									<option  value="9">9</option>
									<option  value="10">10</option>
									<option  value="11">11</option>
									<option  value="12">12</option>
									<option  value="13">13</option>
									<option  value="14">14</option>
									<option  value="15">15</option>
									<option  value="16">16</option>
									<option  value="17">17</option>
									<option  value="18">18</option>
									<option  value="19">19</option>
									<option  value="20">20</option>
									<option  value="21">21</option>
									<option  value="22">22</option>
									<option  value="23">23</option>
									<option  value="24">24</option>
									<option  value="25">25</option>
									<option  value="26">26</option>
									<option  value="27">27</option>
									<option  value="28">28</option>
									<option  value="29">29</option>
									<option  value="30">30</option>

								</select>
								<div class="custom-control custom-switch my-3">
									<input type="checkbox" class="custom-control-input" id="modal_modification_evenement_temps_prevenance_utilisateur" value="FALSE">
									<label class="custom-control-label text-center" for="modal_modification_evenement_temps_prevenance_utilisateur">prévenir un utilisateur</label>
								<label class="d-inline-block" for="modal_modification_evenement_temps_prevenance_utilisateur">
								</div>
								<select id="modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom" class="custom-select element_cache_debut">
									<!-- rempli par jquery -->
								</select>
							</div>
						</div>
			</div>						
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-warning" id="modal_modification_evenement_button">modifier</button>
				</div>	
			</div>
		</div>
	</div>
	</div>
	<!-- Modal suppression d'un evenement-->
	<div class="modal fade" id="modal_suppression_evenement" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_evenement" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un evenement</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer l'evenement suivant:</p>	
					<span id="modal_suppression_evenement_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du evenement pour faire la requete AJAX directement avec l'id au evenement du nom-->
					<div class="texte_important">nom: </div><span id="modal_suppression_evenement_nom"  >nom</span> <br> 
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_evenement_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>