<!--		administration de la table des fabricants des batteries		
				date:26/05/2020

-->

	

<?php 

	
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
	
?>

<div class="container-fluid" >

	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des fabricants </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'fabricants définis:</div>
		<div class="col-lg-1"><span id="nombre_fabricants">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			
			

			
			<button class="btn btn-primary"  id="ajout_fabricant" name="ajout_fabricant" data-toggle="tooltip" data-placement="top"
                title="ajout d'un fabricant" 	value="ajout_fabricant">	
				<span id="ajout_fabricant_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un fabricant
			</button>
			<button class="btn btn-danger"  id="suppression_fabricant_tous" name="suppression_fabricant_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les fabricants"  	value="suppression_fabricant_tous">	
				<span id="suppression_fabricant_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les fabricants
			</button>
					
		</div>
	</div>

	<!-- affichage du tableau de la liste des fabricant -->

	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des fabricants présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom					</th>
							<th scope="col">	adresse				</th>
							<th scope="col">	mail				</th>
							<th scope="col">	telephone			</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_fabricant">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	


	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/fabricant/modal_fabricant.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
	
<script src="js/badbat/common_admin_fabricant.js"></script> 
<script src="js/badbat/admin_fabricant.js"></script> 