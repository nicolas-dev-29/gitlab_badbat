<!--		administration de la table des utilisateurs des batteries		
				date:26/05/2020

-->

	

<?php 

	
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
	
?>

<div class="container-fluid" >

	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des utilisateurs </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'utilisateurs définis:</div>
		<div class="col-lg-1"><span id="nombre_utilisateurs">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			
			

			
			<button class="btn btn-primary"  id="ajout_utilisateur" name="ajout_utilisateur" data-toggle="tooltip" data-placement="top"
                title="ajout d'un utilisateur" 	value="ajout_utilisateur">	
				<span id="ajout_utilisateur_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un utilisateur
			</button>
			<button class="btn btn-danger"  id="suppression_utilisateur_tous" name="suppression_utilisateur_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les utilisateurs"  	value="suppression_utilisateur_tous">	
				<span id="suppression_utilisateur_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les utilisateurs
			</button>
					
		</div>
	</div>

	<!-- affichage du tableau de la liste des utilisateur -->

	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des utilisateurs présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom					</th>
							<th scope="col">	prenom				</th>
							<th scope="col">	mail				</th>
							
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_utilisateur">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	


	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/utilisateur/modal_utilisateur.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
	
<script src="js/badbat/common_admin_utilisateur.js"></script> 
<script src="js/badbat/admin_utilisateur.js"></script> 