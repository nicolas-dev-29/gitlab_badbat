	 
	<!-- Modal échéances changement-->
	<div class="modal fade"  id="modal_echeances_changement" tabindex="-1" role="dialog" aria-labelledby="modal_echeances_changement" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_echeances_changement">
					<h5 class="modal-title">liste des changements réalisés</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<span id="modal_echeances_changement_texte" > - </span>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					<!--<button aria-disabled="true"  class="btn btn-primary" id="modal_echeances_changement_button">ok</button> -->
				</div>
			</div>
		</div>
	</div>
	 
	<!-- Modal échéances prévenance-->
	<div class="modal fade"  id="modal_echeances_prevenance" tabindex="-1" role="dialog" aria-labelledby="modal_echeances_prevenance" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_echeances_prevenance">
					<h5 class="modal-title">liste des prevenances en cours</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> 
					<span id="modal_echeances_prevenance_texte" > - </span>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					<!--<button aria-disabled="true"  class="btn btn-primary" id="modal_echeances_prevenance_button">ok</button> -->
				</div>
			</div>
		</div>
	</div>	
