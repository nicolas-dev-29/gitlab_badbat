<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	
	include_once ("./public_html/constantes/badbat_constante.inc");
	include_once ("./public_html/constantes/dictionnaire.inc");
	include_once ("./public_html/fonctions/fonction_mail.inc"); 
	


	//fonction durée
	include ("./public_html/fonctions/fonction_duree.php");
	setlocale(LC_TIME, 'fr_FR.utf8','fra'); 
	$data = array();
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("./public_html/constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$max_temps_reference = 0;
	$msg=json_decode($contenu_fichier_json,true);
	$requete_liste_batteries = "SELECT `references_base`,`id_batteries` , `etats` ,
	`date_derniere_operation`,`temps_maintien_etat`,`id_etat_suivant_evenement`,`id_evenement`,
	`temps_prevenance_changement_etat`,T1.`nom_etat` AS nom_etat_precedent, T2.`nom_etat` AS nom_etat_suivant,
	`nom_equipement`,`nom_lieux`,`valeur_tension_reseau`,`nom_tension_reseau`,
	`presence_temps_maintien_etat`,`presence_utilisateur_fin_maintien`,TU_fin_maintien.`nom_utilisateur` AS nom_utilisateur_fin_maintien,TU_fin_maintien.`prenom_utilisateur` AS prenom_utilisateur_fin_maintien,TU_fin_maintien.`mail_utilisateur` AS mail_utilisateur_fin_maintien,
	`presence_temps_prevenance_etat`,`presence_utilisateur_fin_prevenance`,TU_fin_prevenance.`nom_utilisateur` AS nom_utilisateur_fin_prevenance,TU_fin_prevenance.`prenom_utilisateur` AS prenom_utilisateur_fin_prevenance,TU_fin_prevenance.`mail_utilisateur` AS mail_utilisateur_fin_prevenance,
	`date_dernier_evenement`
	FROM `table_batteries` B
	LEFT  JOIN `table_evenements` TEV
		ON B.etats = TEV.id_etat_precedent_evenement
	JOIN table_etats AS T1	
		ON ( TEV.id_etat_precedent_evenement = T1.id_etat)
	JOIN table_etats AS T2
		ON ( TEV.id_etat_suivant_evenement = T2.id_etat)
	LEFT JOIN table_equipements AS TE
		ON B.equipements = TE.id_equipement
	LEFT JOIN table_lieux
		ON B.lieux = table_lieux.id_lieux
	LEFT JOIN table_tension_reseaux
		ON B.tension_reseau = table_tension_reseaux.id_tension_reseau
	LEFT JOIN table_utilisateurs AS TU_fin_maintien
		ON TEV.id_utilisateur_fin_maintien = TU_fin_maintien.id_utilisateur	
	LEFT JOIN table_utilisateurs AS TU_fin_prevenance
		ON TEV.id_utilisateur_fin_prevenance = TU_fin_prevenance.id_utilisateur
		";
	$requete_liste_declencheurs = "SELECT id_declencheur,`delai_declencheur`,`delai_prevenance_declencheur`,`activation_declencheur` FROM table_declencheurs_temporels";
	//
	//
	//preparation des requetes d'actions
	$requete_action = "UPDATE table_batteries SET etats = ? ,`date_dernier_evenement`  = ?, `declencheur_dernier_evenement` = ? , `dernier_evenement` = ?
	WHERE `id_batteries`=?";
	//
	//
	//insertion dans la table historique
	$requete_insertion_table_historique = "INSERT INTO `table_historique`( 
		`id_batterie_historique`, `date_historique`, 
		`id_etat_suivant_historique`, `id_etat_precedent_historique`, 
		`switch_ope_evt`, `id_ope_evt`) 
	VALUES (?,?,?,?,?,?)";
	//
	//
	//
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$index_batteries=0;
	$nbre_batt_prevenance=0;
	$nbre_batt_changement=0;
	//déclaration des variables pour la création du message
	$heure = date('H:i:s',time());
	//$destinataire = 'site.badbat@gmail.com';
	$expediteur = 'alertE.site.badbat@gmail.com';
	$alias_expediteur ='test mail badbat';
	//
	$sujet = 'mon sujet - '.$heure;
	$corps = 'corps de texte';
	$corps_alt ='alternate corps';
	$data_mail='';
	$data_mail_corps=array();
	$data_mail_entete='';
	$data_mail_pied = 'Merci de vous connecter sur le site <a href="http://badbat.go.yo.fr/"> site Badbat </a> <br>
	Ce mail a été généré automatiquement le '.$heure;
	$data_mail_adresse_destinataire=array();
	$data_mail_sujet = array();
	$data_mail_res='';
	//récupération du timestamp actuel
	$timestamp_actuel = time();
	//preparation des requetes de rechreches
	$stmt_liste_batteries = mysqli_prepare($db,$requete_liste_batteries);
	$stmt_liste_declencheurs = mysqli_prepare($db,$requete_liste_declencheurs);
	//
	$batteries = array();
	//preparation des requtes d'actions
	$stmt_action_changement_etat = mysqli_prepare($db,$requete_action);
	//preparation de l'insertion
	$stmt_insertion_table_historique = mysqli_prepare($db,$requete_insertion_table_historique);
	
	/****************************************************************/
	//essai
	/****************************************************************/
	/*$data_mail_adresse_destinataire= 'site.badbat@gmail.com';
	$data_mail_sujet = 'le sujet du test';
	$data_mail = 'les données du mail';
									
	file_put_contents('../../sauvegarde/sauvegarde_1.txt', "\r\n".$data_mail, FILE_APPEND);
	*/
	
		//execution
		if(mysqli_stmt_execute($stmt_liste_batteries))	
		{	
			mysqli_stmt_store_result($stmt_liste_batteries);
			$nbre_ligne = mysqli_stmt_num_rows($stmt_liste_batteries);
			mysqli_stmt_bind_result($stmt_liste_batteries,$ligne['references_base'],$ligne['id_batteries'],$ligne['etats'],
			$ligne['date_derniere_operation'],$ligne['temps_maintien_etat'],
			$ligne['id_etat_suivant_evenement'],$ligne['id_evenement'],
			$ligne['temps_prevenance_changement_etat'],$ligne['nom_etat_precedent'],$ligne['nom_etat_suivant'],
			$ligne['nom_equipement'],$ligne['nom_lieux'],$ligne['valeur_tension_reseau'],$ligne['nom_tension_reseau'],
			$ligne['presence_temps_maintien_etat'],$ligne['presence_utilisateur_fin_maintien'],$ligne['nom_utilisateur_fin_maintien'],$ligne['prenom_utilisateur_fin_maintien'],$ligne['mail_utilisateur_fin_maintien'],
			$ligne['presence_temps_prevenance_etat'],$ligne['presence_utilisateur_fin_prevenance'],$ligne['nom_utilisateur_fin_prevenance'],$ligne['prenom_utilisateur_fin_prevenance'],$ligne['mail_utilisateur_fin_prevenance'],
			$ligne['date_dernier_evenement']);
			if($nbre_ligne!=0)
			{
				while(mysqli_stmt_fetch($stmt_liste_batteries))
				{
					$batteries[$index_batteries]['id_batteries'] = "teste";
					$batteries[$index_batteries]['ref_base'] = $ligne['references_base'];
					$batteries[$index_batteries]['id_batteries'] = $ligne['id_batteries'];
					$batteries[$index_batteries]['etats'] = $ligne['etats'];
					$batteries[$index_batteries]['date_derniere_operation'] = $ligne['date_derniere_operation'];
					$batteries[$index_batteries]['temps_maintien_etat'] = $ligne['temps_maintien_etat'];
					$batteries[$index_batteries]['id_etat_suivant_evenement'] = $ligne['id_etat_suivant_evenement'];
					$batteries[$index_batteries]['id_evenement'] = $ligne['id_evenement'];
					$batteries[$index_batteries]['temps_prevenance_changement_etat'] = $ligne['temps_prevenance_changement_etat'];
					$batteries[$index_batteries]['nom_etat_precedent'] = $ligne['nom_etat_precedent'];
					$batteries[$index_batteries]['nom_etat_suivant'] = $ligne['nom_etat_suivant'];
					$batteries[$index_batteries]['nom_equipement'] = $ligne['nom_equipement'];
					$batteries[$index_batteries]['nom_lieux'] = $ligne['nom_lieux'];
					$batteries[$index_batteries]['valeur_tension_reseau'] = $ligne['valeur_tension_reseau'];
					$batteries[$index_batteries]['nom_tension_reseau'] = $ligne['nom_tension_reseau'];
					//informations utilisateurs maintien
					$batteries[$index_batteries]['presence_temps_maintien_etat'] = $ligne['presence_temps_maintien_etat'];
					$batteries[$index_batteries]['presence_utilisateur_fin_maintien'] = $ligne['presence_utilisateur_fin_maintien'];
					$batteries[$index_batteries]['nom_utilisateur_fin_maintien'] = $ligne['nom_utilisateur_fin_maintien'];
					$batteries[$index_batteries]['prenom_utilisateur_fin_maintien'] = $ligne['prenom_utilisateur_fin_maintien'];
					$batteries[$index_batteries]['mail_utilisateur_fin_maintien'] = $ligne['mail_utilisateur_fin_maintien'];
					//informations utilisateurs prevenance
					
					$batteries[$index_batteries]['presence_temps_prevenance_etat'] = $ligne['presence_temps_prevenance_etat'];
					$batteries[$index_batteries]['presence_utilisateur_fin_prevenance'] = $ligne['presence_utilisateur_fin_prevenance'];
					$batteries[$index_batteries]['nom_utilisateur_fin_prevenance'] = $ligne['nom_utilisateur_fin_prevenance'];
					$batteries[$index_batteries]['prenom_utilisateur_fin_prevenance'] = $ligne['prenom_utilisateur_fin_prevenance'];
					$batteries[$index_batteries]['mail_utilisateur_fin_prevenance'] = $ligne['mail_utilisateur_fin_prevenance'];

					$batteries[$index_batteries]['date_dernier_evenement'] = $ligne['date_dernier_evenement'];
					$index_batteries++;
				}
				//compare date actuelle avec la date de la dernière opération + son delai de maintien en balayant toutes les batteries
				$index_boucle=0;
				foreach($batteries as $batt)
				{
					//copie des résultats dans le tableau data
					$index_boucle++;
					$data[$index_boucle]['nbre']= $nbre_ligne;
					$data[$index_boucle]['timestamp_utilisé'] = $timestamp_actuel;
					$data[$index_boucle]['date_derniere_operation'] = $batt['date_derniere_operation'];
					$data[$index_boucle]['date_derniere_operation_string'] = date('d-m-Y',$batt['date_derniere_operation']);
					$data[$index_boucle]['temps_maintien_etat'] = $batt['temps_maintien_etat'];
					$data[$index_boucle]['id']=$batt['id_batteries'];
					$data[$index_boucle]['id_etat_actuel'] = $batt['etats'];
					$data[$index_boucle]['ref_base']=$batt['ref_base'];
					$data[$index_boucle]['nom_equipement']=$batt['nom_equipement'];
					$data[$index_boucle]['nom_lieux']=$batt['nom_lieux'];
					$data[$index_boucle]['valeur_tension_reseau']=$batt['valeur_tension_reseau'];
					$data[$index_boucle]['nom_tension_reseau']=$batt['nom_tension_reseau'];
					//informations utilisateurs maintien
					$data[$index_boucle]['presence_temps_maintien_etat'] = $batt['presence_temps_maintien_etat'];
					$data[$index_boucle]['presence_utilisateur_fin_maintien'] = $batt['presence_utilisateur_fin_maintien'];
					$data[$index_boucle]['nom_utilisateur_fin_maintien'] = $batt['nom_utilisateur_fin_maintien'];
					$data[$index_boucle]['prenom_utilisateur_fin_maintien'] = $batt['prenom_utilisateur_fin_maintien'];
					$data[$index_boucle]['mail_utilisateur_fin_maintien'] = $batt['mail_utilisateur_fin_maintien'];
					//informations utilisateurs prevenance
					$data[$index_boucle]['presence_temps_prevenance_etat'] = $batt['presence_temps_prevenance_etat'];
					$data[$index_boucle]['presence_utilisateur_fin_prevenance'] = $batt['presence_utilisateur_fin_prevenance'];
					$data[$index_boucle]['nom_utilisateur_fin_prevenance'] = $batt['nom_utilisateur_fin_prevenance'];
					$data[$index_boucle]['prenom_utilisateur_fin_prevenance'] = $batt['prenom_utilisateur_fin_prevenance'];
					$data[$index_boucle]['mail_utilisateur_fin_prevenance'] = $batt['mail_utilisateur_fin_prevenance'];

					$data[$index_boucle]['date_dernier_evenement'] = $batt['date_dernier_evenement'];

					$data[$index_boucle]['etat_tempo']= "RAS";
					/*
					//
					// tenir compte du changement d'état après une opération et un évenement
					//
					*/				
					//prevenance du changement d'état
					$max_temps_reference = max($batt['date_derniere_operation'],$batt['date_dernier_evenement']);
					if((($timestamp_actuel > ($max_temps_reference + $batt['temps_maintien_etat']-$batt['temps_prevenance_changement_etat']))
					)&&($data[$index_boucle]['presence_temps_prevenance_etat'] == 1))
					{
						// on est dans le temps de prevenance
						$nbre_batt_prevenance++;
						$data[$index_boucle]['etat_tempo']= "Prevenance en cours / pas de changement";
						$data[$index_boucle]['temps_prevenance_restant_timestamp'] = $max_temps_reference + $batt['temps_maintien_etat'] - $timestamp_actuel;
						$data[$index_boucle]['temps_prevenance_restant_string'] = duree($max_temps_reference + $batt['temps_maintien_etat'] - $timestamp_actuel);
						//
						//prépartion du message à l'utilisateur pour le prévenir du changelent arrivant
						//
						//
						// récupération des informations pour la fabrication du mail
						if(isset($data[$index_boucle]['mail_utilisateur_fin_prevenance']))
						{
							$data_mail_adresse_destinataire = $data[$index_boucle]['mail_utilisateur_fin_prevenance'];
							$data_mail_sujet = '[BadBat] -[PREVENANCE] - '.$heure.' - '.$data[$index_boucle]['valeur_tension_reseau'].'V '
												.'('.$data[$index_boucle]['nom_tension_reseau'].') / '
												.$data[$index_boucle]['nom_equipement'].' / '
												.$data[$index_boucle]['nom_lieux'];
								$data_mail_sujet =utf8_decode($data_mail_sujet);
							//création du corps de texte
							//
							//conversion du timestamp en date
							//setlocale(LC_TIME, 'fr_FR.utf8','fra'); 
							//$data_mail_date_changement = strftime("%A %d %B %G",$timestamp_actuel);
							//création du corps de mail en html
							$data_mail_corps = ' [PREVENANCE] <h1>Modification prochaine de l\'état d\'une batterie </h1>
												Bonjour, <br>
												Nous vous informons que la batterie <br>'
												.$data[$index_boucle]['valeur_tension_reseau'].'V '
												.' ( '.$data[$index_boucle]['nom_tension_reseau'].' ) / '
												.$data[$index_boucle]['nom_equipement'].' / '
												.$data[$index_boucle]['nom_lieux']
												.' va bientôt changer d\'état	<br>';
								
							// création du corps de mail sans html + sauvegarde dans le fichier
							$data_mail_corps_alt = "[PREVENANCE] modification prochaine de l'état d'une batterie:\r\n - "
												.$data[$index_boucle]['valeur_tension_reseau']." V \r\n - "
												.$data[$index_boucle]['nom_tension_reseau']."\r\n - "
												.$data[$index_boucle]['nom_equipement']."\r\n - "
												.$data[$index_boucle]['nom_lieux']."\r\n va changé d'état \r\n";
							$data_mail = $data_mail_corps.$data_mail_pied;
							//
							//	envoi du mail de changement d'état
							//
							envoi_mail_simple($expediteur,$alias_expediteur,$data_mail_adresse_destinataire,$data_mail_sujet,$data_mail,$data_mail);
							//$data[$index_boucle]['mail_contenu'] = " sujet: ".$data_mail_sujet;
							file_put_contents('../../sauvegarde/sauvegarde_1.txt', "\r\n".$data_mail_corps_alt, FILE_APPEND);
						}
						else
						{
							//échec de l'exécution
						$data['resultat'] = $msg['code_echec_01']['id'];
						}

					}
					else		
					{
						$data[$index_boucle]['etat_tempo']= "RAS";
					} 
					//
					//le temps est écoulé
					//
					if(($timestamp_actuel >( $max_temps_reference+ $batt['temps_maintien_etat']))
					&&($data[$index_boucle]['presence_temps_maintien_etat'] == 1))
					{
						//on compte le nombre de batterie en modification
						$nbre_batt_changement++;
						//on quitte le temps de prévenance
						$data[$index_boucle]['etat_tempo']= "Temps ecoule / chgt fait";
						//actions de changement d'état
						$id_etat_base = $batt['id_etat_suivant_evenement'];
						$id_etat_prec_base = $data[$index_boucle]['id_etat_actuel'];
						$date_dernier_eve_base = $timestamp_actuel;
						//
						$dernier_eve_base = $batt['id_evenement'];
						$id_batteries_base = $batt['id_batteries'];
						////////////////////////////////////////////////////////////////////////////////
						//temporaire!!
						$declencheur_dernier_ev_base = 01;
						$data[$index_boucle]['debug']['id']= $id_etat_base;
						$data[$index_boucle]['debug']['date']= $date_dernier_eve_base;
						$data[$index_boucle]['debug']['declencheur']= $declencheur_dernier_ev_base;
						$data[$index_boucle]['debug']['event']= $dernier_eve_base ;
						$data[$index_boucle]['debug']['id_batterie']= $id_batteries_base;
						

						/////////////////////////////////////////////////////////////////////////////////
						if(mysqli_stmt_bind_param($stmt_action_changement_etat,'iiiii',
													$id_etat_base,
													$date_dernier_eve_base,
													$declencheur_dernier_ev_base,
													$dernier_eve_base ,
													$id_batteries_base))
						{
							if(mysqli_stmt_execute($stmt_action_changement_etat))	
							{
								$data[$index_boucle]['resultat'] = $msg['code_ok']['id'];
								$data[$index_boucle]['changement_etat'] = "fait";
								$data[$index_boucle]['nom_etat_prec']=$batt['nom_etat_precedent'];
								$data[$index_boucle]['nom_etat_suiv']=$batt['nom_etat_suivant'];
								$data[$index_boucle]['etat final'] = $id_etat_base;
								//
								// récupération des informations pour la fabrication du mail
								if(isset($data[$index_boucle]['mail_utilisateur_fin_maintien']))
								{
									$data_mail_adresse_destinataire = $data[$index_boucle]['mail_utilisateur_fin_maintien'];
									$data_mail_sujet = '[BadBat]- [MODIFICATION] -'.$heure.' - '.$data[$index_boucle]['valeur_tension_reseau'].'V '
														.'('.$data[$index_boucle]['nom_tension_reseau'].') / '
														.$data[$index_boucle]['nom_equipement'].' / '
														.$data[$index_boucle]['nom_lieux'];
										$data_mail_sujet =utf8_decode($data_mail_sujet);
									//création du corps de texte
									//
									//conversion du timestamp en date
									setlocale(LC_TIME, 'fr_FR.utf8','fra'); 
									$data_mail_date_changement = strftime("%A %d %B %G",$timestamp_actuel);
									//création du corps de mail en html
									$data_mail_corps = '[MODIFICATION] <h1>  Modification de l\'état d\'une batterie </h1>
														Bonjour, <br>
														Nous vous informons que la batterie <br>'
														.$data[$index_boucle]['valeur_tension_reseau'].'V '
														.' ( '.$data[$index_boucle]['nom_tension_reseau'].' ) / '
														.$data[$index_boucle]['nom_equipement'].' / '
														.$data[$index_boucle]['nom_lieux']
														.' a changé d\'état	<br>
														Elle est passée de l\'état '.$data[$index_boucle]['nom_etat_prec']
														.' à l\'état '.$data[$index_boucle]['nom_etat_suiv'].' . <br>
														Le changement a été effectué le '.$data_mail_date_changement.'. <br>';
										
									// création du corps de mail sans html + sauvegarde dans le fichier
									$data_mail_corps_alt = "[MODIFICATION] modification de l'état d'une batterie:\r\n - "
														.$data[$index_boucle]['valeur_tension_reseau']." V \r\n - "
														.$data[$index_boucle]['nom_tension_reseau']."\r\n - "
														.$data[$index_boucle]['nom_equipement']."\r\n - "
														.$data[$index_boucle]['nom_lieux']."\r\n a changé d'état \r\n";
									$data_mail = $data_mail_corps.$data_mail_pied;
									//
									//	envoi du mail de changement d'état
									//
									envoi_mail_simple($expediteur,$alias_expediteur,$data_mail_adresse_destinataire,$data_mail_sujet,$data_mail,$data_mail);
									//$data[$index_boucle]['mail_contenu'] = " sujet: ".$data_mail_sujet;
									file_put_contents('../../sauvegarde/sauvegarde_1.txt', "\r\n".$data_mail_corps_alt, FILE_APPEND);
								}
								else
								{
									//échec de l'exécution
								$data['resultat'] = $msg['code_echec_01']['id'];
								}
								
									//insertion dans la table historique
									if($stmt_insertion_table_historique)
									{
										$un = 1;
										if(mysqli_stmt_bind_param($stmt_insertion_table_historique,'iiiiii',
														$data[$index_boucle]['id'],
														$data[$index_boucle]['timestamp_utilisé'],
														$data[$index_boucle]['etat final'],
														$id_etat_prec_base,
														$historique_evt,
														$dernier_eve_base,
														))
										{
											if(mysqli_execute($stmt_insertion_table_historique))
											{
												$data['resultat_insertion'] = $msg['code_ok']['id'];;
											}
											else
											{
												//échec de l'exécution
												$data['resultat_insertion'] = $msg['code_echec_01']['id'];
											}
										
										
										}
									}
									else
									{
										//échec de l'exécution
										$data['resultat'] = $msg['code_echec_03']['id'];
									}


								
							}
							else
							{
								//échec de l'exécution
								$data['resultat'] = $msg['code_echec_02']['id'];
							}
						}
						else
						{
						$data['resultat'] = $msg['code_echec_06']['id'];
						}
					}
					else
					{
						$data[$index_boucle]['resultat_test_declénchement'] = "declenchement non réalisé";
					}
				}
				//on inscrit les données générales (nombre de batteries,...)
				$data['nbre_batt_prev']=$nbre_batt_prevenance;
				$data['nbre_batt_chgt']=$nbre_batt_changement;
			}
			else
			{
				$data['resultat'] = "erreur de corrélation entre les états et les événements";
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_03']['id'];
		}
	mysqli_stmt_close($stmt_liste_batteries);
	mysqli_stmt_close($stmt_liste_declencheurs);
	mysqli_stmt_close($stmt_action_changement_etat);
//envoi_mail_simple($expediteur,$alias_expediteur,$destinataire,$sujet,$corps,$corps_alt);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);
?>
