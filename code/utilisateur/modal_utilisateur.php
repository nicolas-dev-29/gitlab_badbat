	<!-- Modal ajout utilisateur-->
	<div class="modal fade"  id="modal_ajout_utilisateur" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_utilisateur" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'un utilisateur</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel utilisateur</p>	
					<!--<form class="needs-validation" novalidate >-->
					<div class="row">
						<div class="col-6">
							<!-- ajout du nom de l'utilisateur -->
							<label id="modal_ajout_utilisateur_nom_label" class="control-label "  for="modal_ajout_utilisateur_nom">nom:</label> 
							<input id="modal_ajout_utilisateur_nom" type="text" class="form-control modal_ajout_utilisateur_nom_prenom" placeholder="nom de l'utilisateur" autocomplete="off"   aria-label="modal_ajout_utilisateur_nom_aide"     >
							<span id="modal_ajout_utilisateur_nom_aide" class="help-block small">entrer le nom de l'utilisateur</span>
							<div class="valid-feedback"><span id="modal_ajout_utilisateur_nom_ok"></span></div>
							<div class="invalid-feedback"><span id="modal_ajout_utilisateur_nom_erreur"> erreur!</span></div>
						</div>
						<div class="col-6">
							<!--ajout du prénom de l'utilisateur -->
						<label id="modal_ajout_utilisateur_prenom_label" class="control-label "  for="modal_ajout_utilisateur_prenom">prénom:</label> 
							<input id="modal_ajout_utilisateur_prenom" type="text" class="form-control modal_ajout_utilisateur_nom_prenom" placeholder="prénom de l'utilisateur" autocomplete="off"   aria-label="modal_ajout_utilisateur_prenom_aide"    >
							<span id="modal_ajout_utilisateur_prenom_aide" class="help-block small">entrer le prénom de l'utilisateur</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_utilisateur_prenom_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-12">
							<!-- ajout du mail de l'utilisateur -->
							<label id="modal_ajout_utilisateur_mail_label" class="control-label"  for="modal_ajout_utilisateur_mail">mail:</label> 
							<input id="modal_ajout_utilisateur_mail" type="email" class="form-control" placeholder="mail de l'utilisateur" autocomplete="off"   aria-label="modal_ajout_utilisateur_mail_aide"     >
							<span id="modal_ajout_utilisateur_mail_aide" class="help-block small">entrer le mail de l'utilisateur</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_utilisateur_erreur"> erreur!</span></div>
						</div>
					</div>
					<hr/>		
							<div >
								<label id="modal_ajout_utilisateur_divers_label" for="modal_ajout_utilisateur_divers">divers:</label>
								<textarea  id="modal_ajout_utilisateur_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_ajout_utilisateur_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_utilisateur_divers_erreur"> erreur!</span></div>
							</div>
						
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_utilisateur_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	
	<!-- modification de l'utilisateur -->
<div class="modal fade"  id="modal_modification_utilisateur" tabindex="-1" role="dialog" aria-labelledby="modal_modification_utilisateur" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">modification d'un utilisateur</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouvel utilisateur</p>	
					<span id="modal_modification_utilisateur_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du utilisateur pour faire la requete AJAX directement avec l'id au utilisateur du nom-->
					
					<div class="row">
						<div class="col-6">
							<!-- modification du nom de l'utilisateur -->
							<label id="modal_modification_utilisateur_nom_label" class="control-label"  for="modal_modification_utilisateur_nom">nom:</label> 
							<input id="modal_modification_utilisateur_nom" type="text" class="form-control modal_modification_utilisateur_nom_prenom" placeholder="nom de l'utilisateur" autocomplete="off"   aria-label="modal_modification_utilisateur_nom_aide"   required  >
							<span id="modal_modification_utilisateur_nom_aide" class="help-block small">entrer le nom de l'utilisateur</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_nom_utilisateur_erreur"> erreur!</span></div>
						</div>
						<div class="col-6">
							<!--modification du prénom de l'utilisateur -->
						<label id="modal_modification_utilisateur_prenom_label" class="control-label"  for="modal_modification_utilisateur_prenom">prénom:</label> 
							<input id="modal_modification_utilisateur_prenom" type="text" class="form-control modal_modification_utilisateur_nom_prenom" placeholder="prénom de l'utilisateur" autocomplete="off"   aria-label="modal_modification_utilisateur_prenom_aide"   required  >
							<span id="modal_modification_utilisateur_prenom_aide" class="help-block small">entrer le prénom de l'utilisateur</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_prenom_utilisateur_erreur"> erreur!</span></div>	
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-12">
							<!-- modification du mail de l'utilisateur -->
							<label id="modal_modification_utilisateur_mail_label" class="control-label"  for="modal_modification_utilisateur_mail">mail:</label> 
							<input id="modal_modification_utilisateur_mail" type="email" class="form-control" placeholder="mail de l'utilisateur" autocomplete="off"   aria-label="modal_modification_utilisateur_mail_aide"   required  >
							<span id="modal_modification_utilisateur_mail_aide" class="help-block small">entrer le mail de l'utilisateur</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_utilisateur_erreur"> erreur!</span></div>
						</div>
					</div>
					<hr/>		
							<div >
								<label id="modal_modification_utilisateur_divers_label" for="modal_modification_utilisateur_divers">divers:</label>
								<textarea  id="modal_modification_utilisateur_divers" class="form-control" placeholder="renseignements divers " rows="3" aria-label="modal_modification_utilisateur_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_utilisateur_divers_erreur"> erreur!</span></div>
							</div>
						
				
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_modification_utilisateur_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>	

	
	
	<!-- Modal suppression d'un utilisateur-->
	<div class="modal fade" id="modal_suppression_utilisateur" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_utilisateur" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un utilisateur</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer l'utilisateur suivant:</p>	
					<span id="modal_suppression_utilisateur_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du utilisateur pour faire la requete AJAX directement avec l'id au utilisateur du nom-->
					<div class="row">
						<div class="col-lg-6">
						<div class="texte_important">nom: </div><span id="modal_suppression_utilisateur_nom"  >nom</span>  <br>  
					
						</div>
						<div class="col-lg-6">
						<div class="texte_important">nom: </div><span id="modal_suppression_utilisateur_prenom"  >prenom</span>  <br>  
					
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="texte_important">courriel: </div>	<div class="modal_span"> <span id="modal_suppression_utilisateur_mail" ></span></div>
				
						</div>
						<div class="col-lg-6">
						<div class="texte_important">divers: </div>	<div class="modal_span"> <span id="modal_suppression_utilisateur_divers" ></span></div>
				
						</div>
					</div>
</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_utilisateur_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>
	
	
		<!-- Modal suppression de tous les utilisateur -->
		
	<div class="modal fade" id="modal_suppression_utilisateur_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_utilisateur_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les utilisateurs</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les utilisateurs de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_utilisateur_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>