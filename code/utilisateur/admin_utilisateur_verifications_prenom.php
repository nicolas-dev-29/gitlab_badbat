<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	


	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation de la requête
	$requete_verification = "SELECT `nom_utilisateur`,`prenom_utilisateur` FROM `table_utilisateurs` 
	WHERE ((`nom_utilisateur`=?) and (`prenom_utilisateur`=?))";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	
	//preparation de la requete_verification
	$stmt = mysqli_prepare($db,$requete_verification);
	
	$data=array();
	if($stmt)
	{
		if((isset($_POST['nom_utilisateur']) && preg_match($pattern_admin_nom_verification,$_POST['nom_utilisateur']))
			&&((isset($_POST['prenom_utilisateur'])) && preg_match($pattern_admin_nom_verification,$_POST['prenom_utilisateur'])))
		{	
			$nom_utilisateur_temp=filter_input(INPUT_POST,'nom_utilisateur',FILTER_SANITIZE_STRING);
			$nom_utilisateur_base = mb_strtoupper($nom_utilisateur_temp,'UTF-8');
			
			$prenom_utilisateur_temp=filter_input(INPUT_POST,'prenom_utilisateur',FILTER_SANITIZE_STRING);
			$prenom_utilisateur_base = mb_strtoupper($prenom_utilisateur_temp,'UTF-8');
			
			
			
			//liaison parametres
			if(mysqli_stmt_bind_param($stmt,'ss',$nom_utilisateur_base,$prenom_utilisateur_base))
			{
				if(mysqli_stmt_execute($stmt))
				{
					mysqli_stmt_store_result($stmt);
					//$nbre = mysqli_stmt_num_rows($stmt);
					$nbre = mysqli_stmt_affected_rows($stmt);
					$data['nombre'] = $nbre;
					$data['resultat'] = 		$msg['code_ok']['id'];
					/*mysqli_stmt_bind_result($stmt,$ligne['nom_utilisateur'],$ligne['prenom_utilisateur']);
					$index=0;
					while(mysqli_stmt_fetch($stmt))
					{
						$data[$index]['resultat'] = 		$msg['code_ok']['id'];
						$data[$index]['nombre'] = 			$nbre;
						$data[$index]['nom'] = 				htmlspecialchars($ligne['nom_utilisateur'],ENT_QUOTES,'UTF-8');
						$data[$index]['prenom'] = 			htmlspecialchars($ligne['prenom_utilisateur'],ENT_QUOTES,'UTF-8');
						$index++;
					}*/
					
				}
				else
				{	//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
			}
			else
			{	//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
			}
		}
		else
		{	//le champs est vide ou le $_POST n'est pas "set"
			$data['resultat'] = $msg['code_echec_03']['id'];	
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];
	}
	
								

mysqli_stmt_close($stmt);
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>