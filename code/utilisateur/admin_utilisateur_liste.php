<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_utilisateur`,`nom_utilisateur`,`prenom_utilisateur`,`mail_utilisateur`,`divers_utilisateur` FROM table_utilisateurs ORDER BY `nom_utilisateur` ASC";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_utilisateur'],$ligne['nom_utilisateur'],$ligne['prenom_utilisateur'],$ligne['mail_utilisateur'],$ligne['divers_utilisateur']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['resultat'] = 		$msg['code_ok']['id'];
				$data[$index]['id'] = 				htmlentities($ligne['id_utilisateur'],ENT_QUOTES,'UTF-8');
				$data[$index]['nom'] = 				htmlspecialchars($ligne['nom_utilisateur'],ENT_QUOTES,'UTF-8');
				$data[$index]['prenom'] = 			htmlspecialchars($ligne['prenom_utilisateur'],ENT_QUOTES,'UTF-8');
				$data[$index]['mail'] = 			htmlspecialchars($ligne['mail_utilisateur'],ENT_QUOTES,'UTF-8');
				$data[$index]['divers'] = 			htmlentities($ligne['divers_utilisateur'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>