	<!-- Modal operation courante -->
	<div class="modal fade"  id="modal_operation_courante" tabindex="-1" role="dialog" aria-labelledby="modal_operation_courante" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Opération courante</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p><span id="modal_operation_courante_titre">Opération courante</span></p>	
						<!-- récupération et stockage des informations (id batterie et id opération dans deux span non visibles -->
							<span id="modal_operation_courante_id_batterie" class="element_cache_debut"></span>
							<span id="modal_operation_courante_id_operation" class="element_cache_debut"></span>
							<!-- ***  -->
						<div class="form-group">
							<label id="modal_operation_courante_date_label" class="control-label my_modal_operation_courante_titre"  for="modal_operation_courante_date">Date:</label> 
							<input id="modal_operation_courante_date" type="date" class="form-control text-center" >
						</div>
	
						<div class="element_cache_debut" id="modal_operation_courante_autonomie">
						<span class="my_modal_operation_courante_titre"> Autonomie mesurée</span>
							<div class="form-group ">
								<div class="row">
									<div class="col-3">
										<input id="modal_operation_courante_mesure_autonomie_heure" value="0" min="0" max="23" type="number" class="form-control text-center" >
									</div>
									<div class="col-3">
										<label id="modal_operation_courante_mesure_autonomie_heure_label" class="control-label text-right"  for="modal_operation_courante_autonomie">heure</label>
									</div>
									<div class="col-3">
											<input id="modal_operation_courante_mesure_autonomie_minute" value="0" min="0" max="59" type="number" class="form-control text-center" >
									</div>
									<div class="col-3">
										<label id="modal_operation_courante_mesure_autonomie_minute_label" class="control-label text-right"  for="modal_operation_courante_autonomie">minute</label> 
									</div>
								</div>
								<div class="row">
									<div class="col-3">
										<span class="element_cache_debut entree_erreur" id="modal_operation_courante_mesure_autonomie_heure_erreur"> erreur!</span>
									</div>
									<div class="col-3">
									</div>
									<div class="col-3">
											<span class="element_cache_debut entree_erreur" id="modal_operation_courante_mesure_autonomie_minute_erreur"> erreur!</span>
									</div>
									<div class="col-3">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<span id="modal_operation_courante_autonomie_requise_label"> autonomie annoncée: </span>
								</div>
								<div class="col-6">
									<span id="modal_operation_courante_autonomie_requise_valeur"> 0 minutes </span>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<span id="modal_operation_courante_autonomie_derniere_label"> dernière autonomie mesurée: </span>
								</div>
								<div class="col-6">
									<span id="modal_operation_courante_autonomie_derniere_valeur"> 0 minutes </span>
								</div>
							</div>
						</div>
						<div class="my-3" id="modal_operation_courante_etat">
							<div class="row">
								
								<div class="col-lg-12">
                                  <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="modal_operation_courante_etat_switch" value="false">
                                    <label class="custom-control-label my_modal_operation_courante_titre" for="modal_operation_courante_etat_switch">Modification de l'état</label>
                                  </div>
								</div>
							</div>
						
							<div id="modal_operation_courante_etat_changement" class="form_group ">
								<div class="row">
									<div class="col-lg-5 ">
										<span hidden id="modal_operation_courante_etat_actuel_id"></span>
										<span id="modal_operation_courante_etat_actuel">X</span>
									</div>
									<div class="col-2 text-center">
										<span class="fa fa-arrow-right fa-1x "></span>
									</div>
									<div class="col-lg-5">
										
										<select class="custom-select text-center" id="modal_operation_courante_etat_suivant">
											<!-- remplit automatiquementpar ajax/jquery -->
										</select>
									</div>
								</div>
							</div>
						
						</div>
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_operation_courante_button">Valider</button> 
				</div>
			</div>
		</div>
	</div>