<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../../constantes/badbat_constante.inc");
	include ("../../constantes/dictionnaire.inc");
	include ("../../constantes/badbat_pattern.inc");
	


	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//préparation de la requête
	$requete_verification = "SELECT * FROM `table_etats` WHERE nom_etat=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	
	//preparation de la requete_verification
	$stmt = mysqli_prepare($db,$requete_verification);
	
	$data=array();
	if($stmt)
	{
		if(isset($_POST['etat']) && preg_match($pattern_admin_nom_verification,$_POST['etat']))
		{	
			$nom_temp=filter_input(INPUT_POST,'etat',FILTER_SANITIZE_STRING);
			$nom_base = mb_strtoupper($nom_temp,'UTF-8');
			//liaison parametres
			if(mysqli_stmt_bind_param($stmt,'s',$nom_base))
			{
				if(mysqli_stmt_execute($stmt))
				{
					mysqli_stmt_store_result($stmt);
					$nbre = mysqli_stmt_num_rows($stmt);
					if($nbre>0)
					{	//ce n'est pas un code d'erreur de la fonction mais du résultat (tout s'est bien passé mais l'enregistrement est déjà présent)
						$data['resultat'] = $msg['code_echec_02']['id'];
					}
					else
					{
						$data['resultat'] = $msg['code_ok']['id'];
					}
				}
				else
				{	//erreur d'execute
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
			}
			else
			{	//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
			}
		}
		else
		{	//le champs est vide ou le $_POST n'est pas "set"
			$data['resultat'] = $msg['code_echec_03']['id'];	
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];
	}
	
								

mysqli_stmt_close($stmt);
	
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>