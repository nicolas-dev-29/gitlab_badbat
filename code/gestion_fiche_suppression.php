<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	
	//prepapration des requêtes
	$requete_verification = "SELECT * FROM table_batteries WHERE id_batteries = ?";
	$requete_suppression = "DELETE FROM table_batteries WHERE `table_batteries`.`id_batteries`=?";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$nbre=0;
	$data=array();
	
	//prepapration des requêtes
	$stmt_verification=mysqli_prepare($db,$requete_verification);
	$stmt_suppression=mysqli_prepare($db,$requete_suppression);
	
	if($stmt_verification)
	{
		if(isset($_POST['id']) && $_POST['id']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{	//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
				$id_base=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);

				if(mysqli_stmt_bind_param($stmt_verification,'i',$id_base))
				{
					if(mysqli_stmt_execute($stmt_verification))
					{
						mysqli_stmt_store_result($stmt_verification);
						$nbre = mysqli_stmt_num_rows($stmt_verification);
						if($nbre == 1)	//l'id est unique es est trouvé
						{
							if(mysqli_stmt_bind_param($stmt_suppression,'i',$id_base))
							{
								if(mysqli_execute($stmt_suppression))
								{
									//prévoir la mise à une valeur par défaut dans la table principale
									//$requete_suppression_valeur_defaut;
									$data['resultat'] = $msg['code_ok']['id'];
								}
								else
								{
									$data['resultat'] = $msg['code_echec_01']['id'];	
								}
								
							}
							else
							{
							//erreur de bind
							$data['resultat'] = $msg['code_echec_06']['id'];
							}
						}
					}
					else
					{
					//erreur d'execute de verification
					$data['resultat'] = $msg['code_echec_01']['id'];
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
		//code erreur de prepare
		$data['resultat'] = $msg['code_echec_05']['id'];
	}
	
	mysqli_stmt_close($stmt_suppression);
	mysqli_stmt_close($stmt_verification);
		
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>