	 
	<!-- Modal ajout lieux-->
	<div class="modal fade"  id="modal_ajout_lieux" tabindex="-1" role="dialog" aria-labelledby="modal_ajout_lieux" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_ajout">
					<h5 class="modal-title">Ajout d'un lieu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					 
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouveau lieu</p>	
					<!--<form class="needs-validation" novalidate >-->
						<div class="form-group">
							<label id="modal_ajout_lieux_nom_label" class="control-label" for="modal_ajout_lieux_nom">nom:</label> 
							<input id="modal_ajout_lieux_nom" type="text" class="form-control" placeholder="nom de la station" autocomplete="off"   aria-label="modal_ajout_lieux_nom_aide"   required  >
							<span id="modal_ajout_lieux_nom_aide" class="help-block small">entrer le nom de la station / bloc technique</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_ajout_lieux_erreur"> erreur!</span></div>
							<div class="form-group">
								<label id="modal_ajout_lieux_divers_label" for="modal_ajout_lieux_divers">divers:</label>
								<textarea  id="modal_ajout_lieux_divers" class="form-control" placeholder="renseignements divers de la station" rows="3" aria-label="modal_ajout_lieux_divers_aide"></textarea>
								<div class="invalid-feedback"><span id="modal_ajout_lieux_divers_erreur"> erreur!</span></div>
							</div>
						</div>
				<!--	</form> -->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true"  class="btn btn-primary" id="modal_ajout_lieux_button">sauvegarder</button> 
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal modification lieux-->
	<div class="modal fade" id="modal_modification_lieux" tabindex="-1" role="dialog" aria-labelledby="modal_modification_lieux" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_modification">
					<h5 class="modal-title">Modification du lieu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>entrer les informations du nouveau lieu</p>	
				<!--	<form class="needs-validation" novalidate > -->
						<div class="form-group">
							<span id="modal_modification_lieux_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
							<label id="modal_modification_lieux_nom_label" class="control-label"  for="modal_modification_lieux_nom">nom:</label> 
							<input id="modal_modification_lieux_nom" type="text" class="form-control"  autocomplete="off"   aria-label="modal_ajout_lieux_nom_aide"  title=" entrez entre 4 et 20 caractères"   required   >
							<span id="modal_modification_lieux_nom_aide" class="help-block small">entrer le nom de la station / bloc technique</span>
							<div class="valid-feedback">Ok</div>
							<div class="invalid-feedback"><span id="modal_modification_lieux_erreur"> erreur!</span></div>
							<div class="form-group">
								<label  for="modal_modification_lieux_divers">divers:</label>
								<textarea  id="modal_modification_lieux_divers" class="form-control " rows="3" aria-label="modal_ajout_lieux_divers_aide" title=" entrez moins de 255 caractères"></textarea>
								<div class="invalid-feedback"><span id="modal_modification_lieux_divers_erreur"> erreur!</span></div>
							</div>
						</div>
				<!--	</form> -->
				</div>										
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-warning" id="modal_modification_lieux_button">modifier</button>
				</div>	
			</div>
		</div>
	</div>
	
	<!-- Modal suppression d'un lieu-->
	<div class="modal fade" id="modal_suppression_lieux" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_lieux" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
			
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression d'un lieu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer le lieu suivant:</p>	
					<span id="modal_suppression_lieux_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
					<div class="texte_important">nom: </div><span id="modal_suppression_lieux_nom"  >nom</span>  <br>  
					<div class="texte_important">diverss: </div>	
					<div class="modal_span">
						<span id="modal_suppression_lieux_divers" ></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_lieux_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>

