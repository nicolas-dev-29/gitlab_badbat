<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	$requete_liste = "SELECT `id_batteries`,`references_base`,`valeur_tension_reseau`,`nom_equipement`,`nom_lieux`,`date_mes`	 
	FROM table_batteries T
	LEFT JOIN table_lieux	L
		ON T.lieux = L.id_lieux
	LEFT JOIN table_equipements E
		ON T.equipements = E.id_equipement
	LEFT JOIN table_tension_reseaux	TR
		ON T.tension_reseau = TR.id_tension_reseau
	WHERE (`nom_lieux` LIKE ? 	AND `equipements` = ?) 
    ORDER BY `nom_lieux` ASC
	";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		$equipement_base=filter_input(INPUT_POST,'equipement',FILTER_SANITIZE_NUMBER_INT);		
		
		$lieu_temp=filter_input(INPUT_POST,'lieu',FILTER_SANITIZE_STRING);		
		$lieu_tempp = mb_strtoupper($lieu_temp,'UTF-8');
		$lieu_base = "%".$lieu_tempp."%";
		if(mysqli_stmt_bind_param($stmt_liste,'si',$lieu_base,$equipement_base))
		{
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_batteries'],$ligne['references_base'],$ligne['valeur_tension_reseau'],$ligne['nom_equipement'],$ligne['lieux'],$ligne['date_mes']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['resultat'] = $msg['code_ok']['id'];
				$data[$index]['id'] = htmlentities($ligne['id_batteries'],ENT_QUOTES,'UTF-8');
				$data[$index]['references'] = htmlentities($ligne['references_base'],ENT_QUOTES,'UTF-8');
				$data[$index]['tension'] = htmlentities($ligne['valeur_tension_reseau'],ENT_QUOTES,'UTF-8');
				$data[$index]['equipements'] = htmlentities($ligne['nom_equipement'],ENT_QUOTES,'UTF-8');
				$data[$index]['lieux'] = htmlentities($ligne['lieux'],ENT_QUOTES,'UTF-8');
				$data[$index]['date_mes'] = htmlentities($ligne['date_mes'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
		}
		else
		{
			//erreur de bind
			$data['resultat'] = $msg['code_echec_06']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>