<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);

	//préparation de la requete
	//$requete_liste = "SELECT * FROM table_lieux WHERE `nom_lieux` LIKE ? ORDER BY `nom_lieux` ASC";
	$requete_liste = "SELECT `id_lieux`,`nom_lieux`,`divers_lieux` 
	FROM table_batteries T
	LEFT JOIN table_lieux	L
		ON T.lieux = L.id_lieux
	WHERE `nom_lieux` LIKE ? 
	GROUP BY `id_lieux`
	ORDER BY `nom_lieux` ASC";
	
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );

	$data=array();
	
	//preparation
	$stmt_liste = mysqli_prepare($db,$requete_liste);
	if($stmt_liste)
	{
		if(isset($_POST['lieu']))// && $_POST['lieu']!="")
		{
		$lieu_base = "%".$_POST['lieu']."%";
			if(mysqli_stmt_bind_param($stmt_liste,'s',$lieu_base))
			{
				//execution
				if(mysqli_stmt_execute($stmt_liste))	
				{
					$nbre = mysqli_stmt_affected_rows($stmt_liste);
					mysqli_stmt_bind_result($stmt_liste,$ligne['id_lieux'],$ligne['nom_lieux'],$ligne['divers_lieux']);
					$index=0;
					while(mysqli_stmt_fetch($stmt_liste))
					{
						array_push($data,$ligne['nom_lieux']);
					}
				}
				else 	
				{	//échec de l'exécution
					$data['resultat'] = $msg['code_echec_01']['id'];
				}
			}
			else
			{
				//erreur de bind
				$data['resultat'] = $msg['code_echec_06']['id'];
			}
		}	
		else
		{
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
	}
	else
	{
	//code erreur de prepare
	$data['resultat'] = $msg['code_echec_05']['id'];	
	}
			
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>