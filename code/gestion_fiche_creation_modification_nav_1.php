	<div class="row align-item-center">
			<div class="col-lg-12">
				<div class="nav nav-tabs justify-content-center" role="tablist">
						<a class="nav-item nav-link my_onglets  active" 	id="onglet_1_fiche_modification"	data-toggle="tab" 	role="tab" 	aria-selected="true"	href="#nav_modification-1">	paramètres généraux 	</a>
						<a class="nav-item nav-link my_onglets "  			id="onglet_2_fiche_modification"	data-toggle="tab"	role="tab"	aria-selected="true"	href="#nav_modification-2">	paramètres mécaniques 	</a>
						<a class="nav-item nav-link my_onglets " 			id="onglet_3_fiche_modification"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_modification-3">	paramètres électriques	</a>
						<a class="nav-item nav-link my_onglets " 			id="onglet_4_fiche_modification"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_modification-4">	paramètres d'achat	</a>
						<a class="nav-item nav-link my_onglets " 			id="onglet_5_fiche_modification"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_modification-5">	photos					</a>				
						<a class="nav-item nav-link my_onglets " 			id="onglet_6_fiche_modification"	data-toggle="tab"	role="tab"	aria-selected="true" 	href="#nav_modification-6">	divers					</a>					
				</div>
				<!-- contenu associé aux onglets -->
				<div class="tab-content">
					<!--onglet 1: paramètres généraux -->
					<div class="tab-pane active fiche_contenu"	id="nav_modification-1" 	role="tabpanel" aria-labelledby="onglet_1_fiche_modification">
						<div class="row">	
							<div class="col-lg-10 card-column offset-lg-1  my-3">
								<div class="card-columns">
									<!-- paramètres de base :tension, équipement, lieux-->
									<div class="card  bg-light border-dark  " >
										<div class="card-header">	
											<h5>paramètres généraux	</h5>
										</div>
										<div class="card-body ">
											<!-- formulaire de la tension -->
											<div class="card-title mb-3">
											veuillez entrer la tension du réseau:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
													<select id="creation_fiche_tension_liste_select" class="custom-select creation_fiche_select">
													</select>
												</div>
												<div class="col-lg-6">
													<button class="btn btn-dark" id="creation_fiche_ajout_tension" name="ajout_tension" title="ajout d'une nouvelle tension" data-toggle="tooltip" data-placement="top"   value="ajout_tension">
														<span class="fa fa-plus fa-1x "></span> 
													</button>
													<button class="btn btn-warning "  id="raz_fiche_tension" name="raz_fiche_lieu" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_tension">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div>
											</div>
											<!-- formualire de l'équipement -->
											<div class="card-title my-3">
											veuillez entrer l'équipement associé:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
													<select id="creation_fiche_equipement_liste_select" class="custom-select creation_fiche_select">
													</select>
												</div>
												<div class="col-lg-6">
													<button class="btn btn-dark" id="creation_fiche_ajout_equipement" name="ajout_equipement" title="ajout d'un nouvel equipement" data-toggle="tooltip" data-placement="top"   value="ajout_equipement">
														<span class="fa fa-plus fa-1x "></span> 
													</button>
													<button class="btn btn-warning "  id="raz_fiche_equipement" name="raz_fiche_lieu" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_equipement">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div>
											</div>
											<!-- formulaire du lieu -->
											<div class="card-title mt-3"> 
												veuillez entrer le lieu:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
													<select id="creation_fiche_lieux_liste_select" class="custom-select creation_fiche_select">
														<!--<option selected value="-1"> entrer le lieu </option> -->
													</select>
												</div>
												<div class="col-lg-6">
													<button class="btn btn-dark" id="creation_fiche_ajout_lieux" name="ajout_lieux" title="ajout d'un nouveau lieu" data-toggle="tooltip" data-placement="top"   value="ajout_lieux">
														<span class="fa fa-plus fa-1x "></span> 
													</button>
													<button class="btn btn-warning "  id="raz_fiche_lieux" name="raz_fiche_lieu" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_lieu">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div>
											</div>
										</div>
									</div>	
									<div class="card bg-light border-dark  " >
										<div class="card-header">	
											<h5>états	</h5>
										</div>
										<div class="card-body ">
											<div class="card-title"> veuillez entrer l'état:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
													<select id="creation_fiche_etat_liste_select" class="custom-select creation_fiche_select">
														<!--<option selected value="-1"> entrer l'etat </option>-->
													</select>
												</div>
												<div class="col-lg-6">
													<button class="btn btn-dark" id="creation_fiche_ajout_etat" name="ajout_etat" title="ajout d'un nouvel etat" data-toggle="tooltip" data-placement="top"   value="ajout_etat">
														<span class="fa fa-plus fa-1x "></span> 
													</button>
													<button class="btn btn-warning "  id="raz_fiche_etat" name="raz_fiche_etat" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_etat">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div>
											</div>
										</div>
									</div>
									<div class="card bg-light border-dark  " >
										<div class="card-header">	
											<h5>Référence batterie	</h5>
										</div>
										<div class="card-body ">
											<div class="card-title"> veuillez entrer la référence de la batterie:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
												  <input type="text" class="form-control" id="creation_fiche_reference" value="" >
												</div>
												<div class="col-lg-6">
													<button class="btn btn-warning "  id="raz_fiche_reference" name="reference" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_reference">	
														<span class="fa fa-times fa-1x "></span>
														RAZ
													</button>
												</div>
											</div>
										</div>
									</div>
									<div class="card bg-light border-dark  " >
										<div class="card-header">	
											<h5>autonomie requise</h5>
										</div>
										<div class="card-body ">
											<div class="card-title"> veuillez entrer l'autonomie requise (Cf manex CA):
											</div>
											<div class="row">
												<div class="col-8">
													<div class="row text-left">
														<div class="row my-1">
															<div class="col-6">
																<input id="creation_fiche_autonomie_requise_heure" value="0" min="0" max="23" type="number" class="form-control text-center" >
															</div>
															<div class="col-3">
																<label id="creation_fiche_autonomie_requise_heure_label" class="control-label text-left"  for="creation_fiche_autonomie_requise_heure">heure</label>
															</div>
														</div>
														<div class="row">
															<div class="col-12">
																<span class="element_cache_debut entree_erreur" id="creation_fiche_autonomie_requise_heure_erreur"> erreur!</span>
															</div>
														</div>
														<div class="row my-1">
															<div class="col-6">
																	<input id="creation_fiche_autonomie_requise_minute" value="0" min="0" max="59" type="number" class="form-control text-center" >
															</div>
															<div class="col-3">
																<label id="creation_fiche_autonomie_requise_minute_label" class="control-label text-left"  for="creation_fiche_autonomie_requise_minute">minute</label> 
															</div>
														</div>
														<div class="row">
															<div class="col-12">
																<span class=" element_cache_debut entree_erreur" id="creation_fiche_autonomie_requise_minute_erreur"> erreur!</span>
															</div>
														</div>
													</div>
												</div>
												<div class="col-3 mx-1 ">
													<button class="btn btn-warning "  id="raz_fiche_autonomie_requise" name="raz_fiche_autonomie_requise" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie de l'autonomie" 	value="raz_fiche_autonomie_requise">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div> 
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane	fiche_contenu	"	id="nav_modification-2" 	role="tabpanel" aria-labelledby="onglet_2_fiche_modification">
						<div class="row">	
							<div class="col-lg-12 card-column offset-lg-1  my-3">
								<div class="card-columns">
									<!-- paramètres des dimensions-->
									<div class="card  bg-light border-dark  " >
										<div class="card-header">	
												<h5>dimensions de la batterie</h5>
												Ce sont les dimensions de la batterie.
										</div>	
										<div class="card-body">											
											<!--formulaire des dimensions -->
												<div class="card-title mb-3">
												veuillez entrer les dimensions (en mm):
												</div>
												<div class="row">
													<div class="col-lg-8">
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_longueur_label" class="control-label text-left"  for="creation_fiche_dimension_longueur">longueur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_longueur" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_largeur_label" class="control-label text-left"  for="creation_fiche_dimension_largeur">largeur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_largeur" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_hauteur_label" class="control-label text-left"  for="creation_fiche_dimension_hauteur">hauteur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_hauteur" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>									
													</div> 
													<div class="col-3 mx-1">
																<button class="btn btn-warning "  id="raz_fiche_dimensions" name="raz_fiche_dimensions" data-toggle="tooltip" data-placement="top"
																	title="annulation de la saisie" 	value="raz_fiche_dimensions">	
																	<span class="fa fa-times fa-1x "></span>
																	<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
																	RAZ
																</button>
													</div>											
											</div>	
										</div>	
									</div>
									<div class="card  bg-light border-dark  " >
										<div class="card-header">	
												<h5>dimensions du chantier</h5>
												Ce sont les dimensions du bati support
										</div>	
										<div class="card-body">											
											<!--formulaire des dimensions du chantier-->
												<div class="card-title mb-3">
												veuillez entrer les dimensions (en mm):
												</div>
												<div class="row">
													<div class="col-lg-8">
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_longueur_chantier_label" class="control-label text-left"  for="creation_fiche_dimension_longueur_chantier">longueur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_longueur_chantier" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_largeur_chantier_label" class="control-label text-left"  for="creation_fiche_dimension_largeur_chantier">largeur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_largeur_chantier" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>
															<div class="row my-1">
																<div class="col-4">
																	<label id="creation_fiche_dimension_hauteur_chantier_label" class="control-label text-left"  for="creation_fiche_dimension_hauteur_chantier">hauteur:</label> 															
																</div>
																<div class="col-5 align-items-right" >														
																		<input type="number" id="creation_fiche_dimension_hauteur_chantier" value="0" min="0" max="9999"  class="form-control text-center" >		
																</div>
																<div class="col-3">mm</div>
															</div>									
													</div> 
													<div class="col-3 mx-1">
																<button class="btn btn-warning "  id="raz_fiche_dimensions_chantier" name="raz_fiche_dimensions_chantier" data-toggle="tooltip" data-placement="top"
																	title="annulation de la saisie" 	value="raz_fiche_dimensions_chantier">	
																	<span class="fa fa-times fa-1x "></span>
																	<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
																	RAZ
																</button>
													</div>											
											</div>	
										</div>	
									</div>
								</div>
							</div>	
						</div>
					</div> 
					<div class="tab-pane" 			id="nav_modification-3" 	role="tabpanel" aria-labelledby="onglet_3_fiche_modification">ccc</div>
					<div class="tab-pane fiche_contenu" 			id="nav_modification-4" 	role="tabpanel" aria-labelledby="onglet_4_fiche_modification">
					<div class="row">					
							<div class="col-10 offset-lg-1 card-columns my-3">
								<div class="card bg-light border-dark  " >
									<div class="card-header ">	
										<h5 class="card_titre_entete">fournisseur</h5>
									</div>
									<div class="card-body">
										<div class="col-lg-6"> <span class="card_label">nom:</span> <span id="gestion_fiche_modification_fournisseur_nom"> - </span>  </div>
										<div class="col-lg-6"> <span class="card_label">adresse:</span> <span id="gestion_fiche_modification_fournisseur_adresse"> - </span> 	</div>
										<div class="col-lg-6"> <span class="card_label">mail:</span> <span id="gestion_fiche_modification_fournisseur_mail"> - </span>  	</div>
										<div class="col-lg-6"> <span class="card_label">téléphone:</span> <span id="gestion_fiche_modification_fournisseur_téléphone"> - </span> 	</div>
										<div class="col-lg-6"> <span class="card_label">divers:</span> <span id="gestion_fiche_modification_fournisseur_divers"> - </span> 	</div>
									
									</div>
								</div>
								<div class="card bg-light border-dark  " >
									<div class="card-header ">	
										<h5 class="card_titre_entete">Fabricant</h5>
										
									</div>  
									<div class="card-body">
												<!-- formulaire de la tension -->
												<div class="card-title mb-3">
											veuillez entrer le nom du fabricant:
											</div>
											<div class="form-row">
												<div class="col-lg-6">
													<select id="creation_fiche_fabricant_liste_select" class="custom-select creation_fiche_select">
													</select>
												</div>
												<div class="col-lg-6">
													<button class="btn btn-dark" id="creation_fiche_ajout_fabricant" name="ajout_fabricant" title="ajout d'un nouveau fabricant" data-toggle="tooltip" data-placement="top"   value="ajout_fabricant">
														<span class="fa fa-plus fa-1x "></span> 
													</button>
													<button class="btn btn-warning "  id="raz_fiche_fabricant" name="raz_fiche_lieu" data-toggle="tooltip" data-placement="top"
														title="annulation de la saisie" 	value="raz_fiche_fabricant">	
														<span class="fa fa-times fa-1x "></span>
														<!--<span id="creation_fiche_validation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
														RAZ
													</button>
												</div>
											</div>										
									</div>
								</div>
							</div>
						</div>
					
					</div>
					<div class="tab-pane" 			id="nav_modification-5" 	role="tabpanel" aria-labelledby="onglet_5_fiche_modification">eee</div>
					<div class="tab-pane" 			id="nav_modification-6" 	role="tabpanel" aria-labelledby="onglet_6_fiche_modification">ff</div>
				</div>
			</div>
		</div>