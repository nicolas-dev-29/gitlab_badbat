<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	include ("../constantes/badbat_pattern.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//preparation des requêtes
	$requete_insertion = "INSERT INTO table_batteries (`tension_reseau`,
	`equipements`,`lieux`,`etats`,`reference`,`autonomie_requise`) VALUES (?,?,?,?,?,?)";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	//décodage de la table provenant du lient en JSON
	$tab_batteries= array();
	//faire un isset sur tab
	$tab_batteries = json_decode($_POST['tab'],true);
	//preparation des requêtes
	$stmt_insertion = mysqli_prepare($db,$requete_insertion);
		if($tab_batteries['id_lieux']!="")
		{
			//nettoyage des informations provenant de POST
			if(filter_var($tab_batteries['id_lieux'],FILTER_SANITIZE_NUMBER_INT)==FALSE)
			{
				//erreur de typage
				$data['resultat']=$msg['code_echec_04']['id']; 
			}
			else	//  les données sont valides
			{
			$id_tension_base=filter_var($tab_batteries['id_tension'],FILTER_SANITIZE_NUMBER_INT);
			$id_equipements_base=filter_var($tab_batteries['id_equipement'],FILTER_SANITIZE_NUMBER_INT);
			$id_lieux_base=filter_var($tab_batteries['id_lieux'],FILTER_SANITIZE_NUMBER_INT);
			$id_etats_base=filter_var($tab_batteries['id_etat'],FILTER_SANITIZE_NUMBER_INT);
			$reference_industrielle_base = filter_var($tab_batteries['reference_industrielle'],FILTER_SANITIZE_STRING);
			$autonomie_requise_base= filter_var($tab_batteries['autonomie_requise'],FILTER_SANITIZE_NUMBER_INT);
				if(mysqli_stmt_bind_param($stmt_insertion,'iiiisi',
										$id_tension_base,
										$id_equipements_base,
										$id_lieux_base,$id_etats_base,
										$reference_industrielle_base,
										$autonomie_requise_base))
				{
					if(mysqli_stmt_execute($stmt_insertion))
					{
						$data['resultat'] = $msg['code_ok']['id'];		  
					}
					else
					{
						//erreur d'execute
						$data['resultat'] = $msg['code_echec_01']['id'];	
					}
				}
				else
				{
					//erreur de bind
					$data['resultat'] = $msg['code_echec_06']['id'];
				}
			}
		}
		else
		{	//le champs est vide ou le $_POST n'est pas "set"
			$data['resultat'] = $msg['code_echec_03']['id'];
		}
	mysqli_stmt_close($stmt_insertion);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>