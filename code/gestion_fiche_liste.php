<?php
	header( 'content-type: text/html; charset=utf-8' );	
	//chargement des constantes 
	include ("../constantes/badbat_constante.inc");
	include ("../constantes/dictionnaire.inc");
	//récupération des mémoniques des tests ("échec,ok,..) et codes d'erreurs
	$contenu_fichier_json=file_get_contents("../constantes/code_message.json");
	//extraction du contenu du ficheir JSON
	$msg=array();
	$msg=json_decode($contenu_fichier_json,true);
	//préparation de la requete
	$requete_liste = "SELECT `id_batteries`,`references_base`,
	`valeur_tension_reseau`,`nom_equipement`,`nom_lieux`,`date_mes`,`date_derniere_operation`,`nom_etat`	 
	FROM table_batteries T
	LEFT JOIN table_lieux	L
		ON T.lieux = L.id_lieux
	LEFT JOIN table_equipements E
		ON T.equipements = E.id_equipement	
	LEFT JOIN table_etats ETAT
		ON T.etats = ETAT.id_etat
	LEFT JOIN table_tension_reseaux	TR
		ON T.tension_reseau = TR.id_tension_reseau";
	//ouverture de la base de données
	$db = new mysqli($host_db, $login_db, $passwd_db, $database);
	// Check connection
	if (!$db) {
		die("Echec connexion: " . mysqli_connect_error());
	}
	mysqli_set_charset( $db,"utf8" );
	$data=array();
	$equipement_base=filter_input(INPUT_POST,'equipement',FILTER_SANITIZE_NUMBER_INT);		
	$tension_base = filter_input(INPUT_POST,'tension', FILTER_SANITIZE_NUMBER_INT);
	
	$lieu_temp=filter_input(INPUT_POST,'lieu',FILTER_SANITIZE_STRING);		
	$lieu_tempp = strtoupper($lieu_temp);
	$lieu_base = "%".$lieu_tempp."%";

			if($equipement_base == 1) // pas d'équipement désigné
			{
				if($tension_base == 1) 		// pas de tension désigné
				{
				$requete_liste.=" WHERE (`nom_lieux` LIKE ?)";
				$requete_liste.="ORDER BY `nom_lieux` ASC";
				$stmt_liste = mysqli_prepare($db,$requete_liste);
				if($stmt_liste)
					{
						$res_bind = mysqli_stmt_bind_param($stmt_liste,'s',$lieu_base);
						//$res_bind = mysqli_stmt_bind_param($stmt_liste);//,'s',$lieu_base);
					}		
				}
				else	// une tension choisie
				{
				$requete_liste.=" WHERE (`nom_lieux` LIKE ? AND `tension_reseau`=?)";
				$requete_liste.="ORDER BY `nom_lieux` ASC";
				$stmt_liste = mysqli_prepare($db,$requete_liste);
				if($stmt_liste)
					{
						$res_bind = mysqli_stmt_bind_param($stmt_liste,'si',$lieu_base,$tension_base);
					}		
				}
				

			}
			else
			{
				if($tension_base == 1)
				{
					$requete_liste.=" WHERE (`nom_lieux` LIKE ? 	AND `equipements` = ?) ";
					$requete_liste.="ORDER BY `nom_lieux` ASC";
					$stmt_liste = mysqli_prepare($db,$requete_liste);
					if($stmt_liste)
					{
						$res_bind = mysqli_stmt_bind_param($stmt_liste,'si',$lieu_base,$equipement_base);
					}
				}
				else
				{
				$requete_liste.=" WHERE (`nom_lieux` LIKE ? AND `tension_reseau` = ? AND `equipements` = ?) ";
					$requete_liste.="ORDER BY `nom_lieux` ASC";
					$stmt_liste = mysqli_prepare($db,$requete_liste);
					if($stmt_liste)
					{
						$res_bind = mysqli_stmt_bind_param($stmt_liste,'sii',$lieu_base,$tension_base,$equipement_base);
					}
				}
			}
		

	
		//execution
		if(mysqli_stmt_execute($stmt_liste))	
		{
			$nbre = mysqli_stmt_affected_rows($stmt_liste);
			mysqli_stmt_bind_result($stmt_liste,$ligne['id_batteries'],$ligne['references_base'],$ligne['valeur_tension_reseau'],$ligne['nom_equipement'],$ligne['lieux'],$ligne['date_mes'],
			$ligne['date_dop'],$ligne['etat_actuel']);
			$index=0;
			while(mysqli_stmt_fetch($stmt_liste))
			{
				$data[$index]['resultat'] = $msg['code_ok']['id'];
				$data[$index]['id'] = 			htmlentities($ligne['id_batteries'],ENT_QUOTES,'UTF-8');
				$data[$index]['references'] = 	htmlentities($ligne['references_base'],ENT_QUOTES,'UTF-8');
				$data[$index]['tension'] = 		htmlentities($ligne['valeur_tension_reseau'],ENT_QUOTES,'UTF-8');
				$data[$index]['equipements'] = 	htmlentities($ligne['nom_equipement'],ENT_QUOTES,'UTF-8');
				$data[$index]['lieux'] = 		htmlentities($ligne['lieux'],ENT_QUOTES,'UTF-8');
				$data[$index]['date_mes'] = 	htmlentities(date('j/m/Y',($ligne['date_mes'])),ENT_QUOTES,'UTF-8');
				$data[$index]['date_dop'] = 	htmlentities(date('j/m/Y',($ligne['date_dop'])),ENT_QUOTES,'UTF-8');
				$data[$index]['etat_actuel'] = 	htmlentities($ligne['etat_actuel'],ENT_QUOTES,'UTF-8');
				$index++;
			}
		}
		else 	
		{	//échec de l'exécution
			$data['resultat'] = $msg['code_echec_01']['id'];
		}
		
	mysqli_stmt_close($stmt_liste);
//encodage JSON
header('Content-Type: application/json');
echo json_encode($data);	
mysqli_close($db);	
?>