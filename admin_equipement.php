<!--		administration de la table des equipements des batteries		
				date:26/05/2020

-->

	

<?php 

	
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
	
?>

<div class="container-fluid" >

	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des équipements </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'équipements définis:</div>
		<div class="col-lg-1"><span id="nombre_equipements">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			
			

			
			<button class="btn btn-primary"  id="ajout_equipement" name="ajout_equipement" data-toggle="tooltip" data-placement="top"
                title="ajout d'un équipement" 	value="ajout_equipement">	
				<span id="ajout_equipement_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un équipement
			</button>
			<button class="btn btn-danger"  id="suppression_equipement_tous" name="suppression_equipement_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les equipements"  	value="suppression_equipement_tous">	
				<span id="suppression_equipement_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les équipements
			</button>
					
		</div>
	</div>

	<!-- affichage du tableau de la liste des equipement -->

	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des équipements présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom des équipements	</th>
							<th scope="col">	divers			</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_equipement">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	

	<!-- Modal suppression de tous les equipement -->
		
	<div class="modal fade" id="modal_suppression_equipement_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_equipement_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les équipements</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les équipements de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_equipement_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/equipement/modal_equipement.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
	
<script src="js/badbat/common_admin_equipement.js"></script> 
<script src="js/badbat/admin_equipement.js"></script> 