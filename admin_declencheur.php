<!--		administration de la table evenements des batteries		
				date:26/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
?>
<div class="container-fluid" >
	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des déclencheurs </h1>
		</div>
	</div>
	<div class="row">
		<div  class="row align-items-center my-3" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des déclencheurs temporels présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  text-center align-middle text-left table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	réf du déclencheur 				</th>
							<th scope="col">	nom du déclencheur 				</th>
							<th scope="col">	délai 							</th>
							<th scope="col">	activé/desactivé				</th>
							<th scope="col">	divers							</th>
							<th scope="col">	modifier						</th>
							<th scope="col">	supprimer						</th>
						</tr>
					</thead>
					<tbody  id="table_declencheur_temporel">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</div>
</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		//include ("./code/etat/modal_etat.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	//include ("./code/toast_perso.php");
?>	
<!--<script src="js/badbat/common_admin_etat.js"></script> -->
<script src="js/badbat/admin_declencheur.js"></script> 