<!--		administration de la table etat des batteries		
				date:26/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
?>
<div class="container-fluid" >
	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des états </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'états définis:</div>
		<div class="col-lg-1"><span id="nombre_etats">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			<button class="btn btn-primary"  id="ajout_etat" name="ajout_etat" data-toggle="tooltip" data-placement="top"
                title="ajout d'un état" 	value="ajout_etat">	
				<span id="ajout_etat_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un état
			</button>
			<button class="btn btn-danger"  id="suppression_etat_tous" name="suppression_etat_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les etats"  	value="suppression_etat_tous">	
				<span id="suppression_etat_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les etats
			</button>
		</div>
	</div>
	<!-- affichage du tableau de la liste des etat -->
	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des etats présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom des etats	</th>
							<th scope="col">	divers			</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_etat">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	
	<!-- Modal suppression de tous les etat -->
	<div class="modal fade" id="modal_suppression_etat_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_etat_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les etats</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les etats de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_etat_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/etat/modal_etat.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
<script src="js/badbat/common_admin_etat.js"></script> 
<script src="js/badbat/admin_etat.js"></script> 