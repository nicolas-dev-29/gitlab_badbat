<!--		création d'un fiche batterie		
				date:06/05/2020

-->

<?php 

	
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
	include ("./code/modal_lieux.php");
	include ("./code/etat/modal_etat.php");
	include ("./code/equipement/modal_equipement.php");
	include ("./code/tension_reseau/modal_tension_reseau.php");
	
?>


<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1> Création d'un nouvelle fiche batterie </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-4">
			référence de la fiche:			<span  class="reference_batterie_css" id="creation_fiche_reference">XX-XX-XX</span> 
		</div>	
		<div class="  col-8 text-right my-3 ">
			<button class="btn btn-primary" disabled id="validation_fiche" name="validation_fiche" data-toggle="tooltip" data-placement="top"
				title="validation de la fiche" 	value="validation_fiche">	
				<span id="validation_fiche_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				valider la fiche
			</button>
			<button class="btn btn-warning"  id="creation_fiche_raz" name="raz_fiche" data-toggle="tooltip" data-placement="top"
                title="remise à zéro des champs"  	value="raz_fiche">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				vider tous les champs
			</button>
			<button class="btn btn-danger"  id="creation_fiche_quitter" name="creation_fiche_quitter" data-toggle="tooltip" data-placement="top"
                title="quitter"  	value="creation_fiche_quitter">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				quitter
			</button>
		</div>
		
	</div>
		<?php
		require("./code/gestion_fiche_creation_modification_nav_1.php");	
	?>

</div>
<?php
include ("./code/toast_perso.php");
?>
<!-- tension réseaux 
<script src="js/badbat/common_admin_tension_reseau.js"></script> 
<script src="js/badbat/admin_tension_reseau.js"></script> 

<!--  équipement 
<script src="js/badbat/admin_equipement.js"></script> 
<script src="js/badbat/common_admin_equipement.js"></script> 
<!-- états
<script src="js/badbat/admin_etat.js"></script> 
<script src="js/badbat/common_admin_etat.js"></script> 
<!-- lieux -->
<script src="js/badbat/badbat_admin_lieux.js"></script>
<script src="js/badbat/common_admin_lieux.js"></script> 
<!-- script spécifique à la page -->
<script src="js/badbat/badbat_creation_fiche.js"></script> 