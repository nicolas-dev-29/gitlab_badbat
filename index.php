<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/png" href="images/favicon.png">
		<!-- bibliotheques css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/themes/cupertino/jquery-ui.css">
		<link  href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/badbat.css" rel="stylesheet" >
		<!-- chargement des bibliotheques js -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
		<script src="js/jquery-3.5.1.min.js"></script> 
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/popper.min.js"></script>		<!--inclusion de la bibliotheques poppover -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/moment/moment-with-locales-2.26.0.js"></script>
		<script src="js/badbat/echeances.js"></script>
		<script src="js/badbat/admin_test.js"></script>
		<!-- chargement des constantes du site -->
		<?php
		include ("./constantes/badbat_constante.inc");
		include ("./constantes/dictionnaire.inc");
		?>
		<title>Base de données  des Batteries</title>
	</head>
	<body>
		<noscript>Your browser does not support JavaScript!</noscript>
		<header>
		Badbat- Base de Données des Batteries
		</header>
		<nav class="navbar navbar-expand-lg bg-light navbar-light">
			<div class="container-fluid">		
					<div class="nav-header">
						<a class="navbar-brand" href="index.php" 	> Badbat </a>
					</div>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_restreinte">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbar_restreinte">
						<ul class="nav navbar-nav ">
							<!--<li class="nav-item-active">	<a class="nav-link" href="index.php?page=echeances.php">		échéances</a></li> -->
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> fiches batteries </a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="index.php?page=creation_fiche.php"> création d'une fiche</a>
								<!--	<a class="dropdown-item" href="index.php?page=consultation_fiche.php">consultation d'une fiche </a>
									<a class="dropdown-item" href="#">modification d'une fiche </a>
									<a class="dropdown-item" href="#">suppression d'une fiche </a>
								-->	<a class="dropdown-item" href="index.php?page=gestion_fiche.php">gestion d'une fiche </a>
								</div>
							</li>
							<!--<li class="nav-item">			<a class="nav-link" href="index.php?page=recherche.php">		recherche</a></li>  -->
							<!--<li class="nav-item">			<a class="nav-link" href="index.php?page=maj.php">				mise à jour</a></li> -->
							<!--<li class="nav-item-active">	<a class="nav-link  disabled" href="index.php?page=courbes.php">			courbes</a> </li> -->
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown"> administration de la base  </a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="index.php?page=admin_etat.php">												états</a>
									<a class="dropdown-item" href="index.php?page=admin_equipement.php">										équipements</a> 
									<a class="dropdown-item" href="index.php?page=admin_lieux.php">												lieux</a>
									<a class="dropdown-item" href="index.php?page=admin_tension_reseau.php">									tension reseau</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_tension_batterie.php" >	tension batterie</a> 									
									<a class="dropdown-item " href="index.php?page=admin_evenement.php">										évenements</a>
									<a class="dropdown-item " href="index.php?page=admin_operation.php">										opérations</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_declencheur.php">			déclencheurs</a>
									<a class="dropdown-item " href="index.php?page=admin_utilisateur.php">										utilisateurs</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_cosse.php">				cosses</a>
									<a class="dropdown-item " href="index.php?page=admin_fabricant.php">												fabricants</a> 
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_fournisseur.php">			fournisseurs</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_raccordement.php">			raccordements</a> 
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_reseau_electrique.php">	réseaux électriques</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_technologie.php">			technologies</a> 
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_reference.php">			tensions références</a>
									<a class="dropdown-item element_cache_debut disabled" href="index.php?page=admin_test.php">					test- no touch</a> 
								</div>
							</li>
						</ul>
					</div>
			</div>
		</nav>
		<div id= "banniere_echeances" class="container-fluid">
			<div class="row d-flex align-items-stretch    justify-content-center my-2" id="index_echeances_affichage">
				<div class=" col-auto   d-flex justify-content-center align-items-center banniere_echeances_off ">
					<span class="mx-2 fa fa-bell fa-1x"> </span>
					<span id="banniere_echeances_changement_badge" class="badge-pill badge-info">0</span>
					<button id="banniere_echeances_changement_button" class=" btn   btn-danger text-right element_cache_debut" type="button"  >
							<span class="fa fa-angle-double-down fa-1x "> </span>
					</button>
				</div>
				<div class=" col-auto mx-1 banniere_echeances_off ">
					<span class="fa fa-exclamation-triangle fa-1x"> </span>
					<span id="banniere_echeances_prevenance_badge" class="badge-pill badge-info">0</span>
					<button id="banniere_echeances_prevenance_button" class=" btn   btn-warning text-right element_cache_debut" type="button"  >
							<span class="fa fa-angle-double-down fa-1x "> </span>
					</button>
				</div>
			</div>
		</div>
		
		<div class="container-fluid">
			<?php
			//test de la variable page et affichage du contenu de la page
			if (isset($_GET['page']) && $_GET['page']!="" ) 
			{
				$page = $_GET['page'];
				if(in_array($page,$pages_autorisees))
				{
					include ($page);
				}
				else
				{?>
					<h1> Page introuvable </h1>
				<?php
				}
			}
			else
			{?>
				<h1>accueil </h1>
			<?php
			}
			?>				
		</div>
		<footer>
		Base de Données des batteries - SNA/O - maintenance de Brest-Guipavas 
		publié le 30/11/2022 V0.15
		</footer>
		<?php
		//chargement des toasts de validation/echec enregistrements
		include ("./code/banniere_echeance/modal_echeances.php");
		?>	
	</body>

</html>	