<!--		consultation d'un fiche batterie		
				date:11/05/2020
-->
<?php 
	//chargement des constantes 
	
	include ("./constantes/badbat_constante.inc");
	include ("./code/modal_lieux.php");
	include ("./code/etat/modal_etat.php");
	include ("./code/equipement/modal_equipement.php");
	include ("./code/tension_reseau/modal_tension_reseau.php");

	
?>


<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 id="gestion_fiche_titre"> Liste des fiches </h1>
		</div>
	</div>
	<!--                                         -->
	<!--    champs de la recherche               -->
	<!--                                         -->
	<div class=" gestion_fiche_recherche">	

		
		<div class="row my-3">
			<div class="col  gestion_fiche_liste">				
				<!-- choix de la tesnion-->
				tension:<select id="recherche_fiche_tension_select" class="custom-select">
				</select> 
			</div>
			<div class="col gestion_fiche_liste">				
				<!-- choix de l'équipement-->
				équipement:
				<select id="recherche_fiche_equipement_select" class="custom-select">
				</select>
			</div>
			<div class="col gestion_fiche_liste">
				<!-- recherche du lieu-->
				lieu:
				<input type="text" class="form-control" id="recherche_fiche_lieux"  title="entrez le lieux recherché"
					placeholder="entrez le lieu recherché" >
			</div>
			<div class="col gestion_fiche_liste">
				<br>
				<button class="btn btn-warning "  id="raz_recherche_fiche" name="raz_fiche" data-toggle="tooltip" 
					title="remise à zéro des champs"  	value="raz_recherche_fiche">	effacer la recherche
				</button>
			</div>
		
			
			
			<div class="col-lg-9 gestion_fiche_consultation element_cache_debut">
			</div>
			<div class="col-lg-6 gestion_fiche_modification element_cache_debut" >
			</div>
			<div class="  col text-right gestion_fiche_modification element_cache_debut">
				<button class="btn btn-primary "  id="gestion_fiche_validation_fiche" name="validation_fiche" data-toggle="tooltip" data-placement="top"
					title="enregistrer les modifications" 	value="validation_fiche">	
					<span id="validation_fiche_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
					enregistrer les modifications
				</button>
				<button class="btn btn-warning "  id="gestion_fiche_raz_fiche" name="raz_fiche" data-toggle="tooltip" data-placement="top"
					title="remise à zéro des champs"  	value="raz_fiche">	
					<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
					RAZ
				</button>
			</div>
			<div class="col text-right">
				<br>
				<button class="btn btn-danger gestion_fiche_historique element_cache_debut"  id="gestion_fiche_historique_suppression_tous" name="gestion_fiche_historique_suppression_tous" data-toggle="tooltip" data-placement="top"
					title="suppression de toutes les entrées de l'historique" 	value="gestion_fiche_historique_suppression_tous">	
					<span class="fa fa-trash	 fa-1x "></span>
					<!--<span id="validation_fiche_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> -->
				</button>
				<button class="btn btn-primary  gestion_fiche_liste"  id="gestion_fiche_creation_fiche" name="creation_fiche" data-toggle="tooltip" data-placement="top"
				title="création d'une nouvelle fiche" 	value="creation_fiche">	
					<span class="fa fa-plus fa-1x "></span>
				</button>
				<button class="btn btn-info gestion_fiche_consultation gestion_fiche_modification element_cache_debut"  id="gestion_fiche_retour_liste" name="gestion_fiche_retour" data-toggle="tooltip" data-placement="top"
					title="retour à la liste des fiches" 	value="gestion_fiche_retour">	
					<span class="fa fa-arrow-left fa-1x "></span>
					<!--<span id="validation_fiche_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> -->
				</button>


				
			</div>
		
		</div>
	<!--                                               -->
	<!--   champs du tableau (corps de l'écran)        -->
	<!--                                               -->
	<div class="gestion_fiche_liste">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h3> Liste des fiches disponibles: <span id="consultation_fiche_nombre">X</span> </h3>
			</div>
			<div class="col-lg-12">
				<div class="table-responsive">
					<table class="table align-middle text-center table-condensed table-stripped ">
						<thead>
							<tr class="align-middle">
								<!--<th scope="col">	Référence 				</th> -->
								<th scope="col">	tension						</th>
								<th scope="col">	équipement					</th>
								<th scope="col">	lieux						</th>
								<th scope="col">	état actuel					</th>
								<th scope="col">	date dernière opération		</th>
								<th scope="col">	opération courante			</th>
								<th scope="col">	consulter					</th>
								<th scope="col">	historique					</th>
								<th scope="col">	modifier					</th>
								<th scope="col">	supprimer					</th>
							</tr>
						</thead>
						<tbody id="gestion_fiche_tableau">
							<!-- insertion des données par jquery depuis une requête AJAX -->
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--                                               -->
	<!-- champs de la consultation(corps de l'écran)   -->
	<!--                                               -->
	<div class="gestion_fiche_consultation element_cache_debut">
		<?php
			include ("./code/gestion_fiche_corps_consultation.php");	
		?>
	</div>
	<!--                                                    -->
	<!--    champs de la modification(corps de l'écran)     -->
	<!--                                                    -->
	<div class="gestion_fiche_modification element_cache_debut">
	<?php
		require("./code/gestion_fiche_creation_modification_nav_1.php");	
	?>
	</div>
	<div class="gestion_fiche_historique element_cache_debut">
	<?php 
	       include ("./code/gestion_fiche_corps_historique.php");
	?>
		
	</div>

	
	<!-- Modal suppression d'une fiche-->
	<div class="modal fade" id="modal_suppression_gestion_fiche" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_lieux" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de la fiche</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <!-- insertion du formulaire de remplissage-->
					<p>êtes vous sur de vouloir supprimer la fiche suivante:</p>	
					<span id="modal_suppression_gestion_fiche_id"   ></span> <!-- le span n'est pas visible, il sert à stocker l'id du lieu pour faire la requete AJAX directement avec l'id au lieu du nom-->
					<div class="texte_important">reference: 	</div>				<span id="modal_suppression_gestion_fiche_reference" 		 ></span>  <br>  
					<div class="row">
						<div class="col-lg-6">
							<div class="texte_important">tension:  		</div>				<span id="modal_suppression_gestion_fiche_tension_reseau" 	 ></span>  <br>  
						</div>
						<div class="col-lg-6">
							<div class="texte_important">equipement: 	</div>				<span id="modal_suppression_gestion_fiche_equipement" 		 ></span>  <br>  
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="texte_important">lieu: 			</div>				<span id="modal_suppression_gestion_fiche_lieu" 			 ></span>  <br>  
						</div>
						<div class="col-lg-6">
							<div class="texte_important">date de mise en service	</div>	<span id="modal_suppression_gestion_fiche_date_mes" 		 ></span>  <br>  	
						</div>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary " data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_gestion_fiche_validation_button">supprimer</button>
				</div> 
			</div>
		</div>
	</div>
</div>


<?php
	include ("./code/modal_fiche_operations_courantes.php");
	include ("./code/equipement/modal_equipement.php");
	include ("./code/toast_perso.php");
	
?>

<!-- tension réseaux -->
<script src="js/badbat/echeances_all_pages.js"></script> 


<!-- tension réseau -->
<script src="js/badbat/admin_tension_reseau.js"></script> 
<script src="js/badbat/common_admin_tension_reseau.js"></script> 
<!--  équipement  -->
<script src="js/badbat/admin_equipement.js"></script> 
<script src="js/badbat/common_admin_equipement.js"></script> 
<!-- états -->
<script src="js/badbat/admin_etat.js"></script> 
<script src="js/badbat/common_admin_etat.js"></script> 
<!-- lieux  -->
<script src="js/badbat/badbat_admin_lieux.js"></script>
<!--<script src="js/badbat/common_admin_lieux.js"></script> 
 <!--fabricant -->
<script src="js/badbat/admin_fabricant.js"></script>
<script src="js/badbat/common_admin_fabricant.js"></script> 

<script src="js/badbat/gestion_fiche.js"></script> 


<!-- insertion des datatables -->

<!--<script src="vendor/datatables/datatables/media/js/jquery.dataTables.js"></script> -->
