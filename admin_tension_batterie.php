<!--		administration de la table tension_batterie des batteries		
				date:26/05/2020

-->

	

<?php 

	
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
	
?>

<div class="container-fluid" >

	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des tensions batteries </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre de tension batterieS définies:</div>
		<div class="col-lg-1"><span id="nombre_tension_batteries">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			
			

			
			<button class="btn btn-primary"  id="ajout_tension_batterie" name="ajout_tension_batterie" data-toggle="tooltip" data-placement="top"
                title="ajout d'un tension batterie" 	value="ajout_tension_batterie">	
				<span id="ajout_tension_batterie_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter une tension batterie
			</button>
			<button class="btn btn-danger"  id="suppression_tension_batterie_tous" name="suppression_tension_batterie_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les tension_batteries"  	value="suppression_tension_batterie_tous">	
				<span id="suppression_tension_batterie_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tout
			</button>
					
		</div>
	</div>

	<!-- affichage du tableau de la liste des tension_batterie -->

	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des tension_batteries présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	tension batteries	</th>
							<th scope="col">	divers			</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_tension_batterie">
						
						<!-- insertion des données par jquery depuis une requête AJAX -->
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	

	<!-- Modal suppression de tous les tension_batterie -->
		
	<div class="modal fade" id="modal_suppression_tension_batterie_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_tension_batterie_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les tension_batteries</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les tension_batteries de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_tension_batterie_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/tension_batterie/modal_tension_batterie.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
	
<script src="js/badbat/common_admin_tension_batterie.js"></script> 
<script src="js/badbat/admin_tension_batterie.js"></script> 