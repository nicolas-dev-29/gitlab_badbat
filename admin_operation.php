<!--		administration de la table des operations des batteries		
				date:26/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
?>
<div class="container-fluid" >
	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des opérations </h1>
		</div>
	</div>
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'opérations définies:</div>
		<div class="col-lg-1"><span id="nombre_operations">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			<button class="btn btn-primary"  id="ajout_operation" name="ajout_operation" data-toggle="tooltip" data-placement="top"
                title="ajout d'une opération" 	value="ajout_operation">	
				<span id="ajout_operation_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter une opération
			</button>
			<button class="btn btn-danger"  id="suppression_operation_tous" name="suppression_operation_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de toutes les operations"  	value="suppression_operation_tous">	
				<span id="suppression_operation_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer toutes les opérations
			</button>
		</div>
	</div>
	<!-- affichage du tableau de la liste des operation -->
	<div  class="row align-items-center" >
		<div class="offset-lg-1 col-lg-10">
			<h3> liste des opérations présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  align-middle text-center table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	nom des opérations	</th>
							<th scope="col">	divers			</th>
							<th scope="col">	modification autonomie			</th>
							<th scope="col">	modification	</th>
							<th scope="col">	suppression		</th>
						</tr>
					</thead>
					<tbody id="table_operation">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	
	<!-- Modal suppression de tous les operation -->
	<div class="modal fade" id="modal_suppression_operation_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_operation_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les opérations</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les opérations de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_operation_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>
	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/operation/modal_operation.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	

<script src="js/badbat/admin_operation.js"></script> 