<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- bootstrapcss -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link href="css/badbat.css" rel="stylesheet" >
		<link  href="css/bootstrap.min.css" rel="stylesheet">
		<link src="vendor/datatables/datatables/media/css/jquery.dataTables.css"></link> 
		


		<!-- chargement des bibliotheques js -->	
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/badbat/admin_test.js"> </script>
		<script src="js/badbat/badbat.js"></script> 
		
		<!-- chargement des constantes du site -->
		<?php
		include ("./constantes/badbat_constante.inc");
		include ("./constantes/dictionnaire.inc");
		?>
		
		<title>Base de données  des batterie</title>
	</head>

	<body>
		<header>
		BaDBat- Base de Données des Batteries
		</header>
		<nav class="navbar navbar-expand-sm bg-light navbar-light">
			<div class="container-fluid">
				<div class="nav-header">
					<a class="navbar-brand" href="index.php" > BaDBat </a>
				</div>
				<ul class="navbar-nav">
					<li class="nav-item-active">	<a class="nav-link" href="index.php?page=echeances.php">	échéances</a></li>
					<li class="nav-item">			<a class="nav-link" href="index.php?page=recherche.php">	recherche</a></li>
					<li class="nav-item">			<a class="nav-link" href="index.php?page=maj.php">		mise à jour</a></li>
					<li class="nav-item-active">	<a class="nav-link" href="index.php?page=courbes.php">	courbes</a> </li>
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"> administration base  </a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="index.php?page=admin_lieux.php">				lieux</a>
							<a class="dropdown-item" href="index.php?page=admin_equipements.php">		equipements</a> 
							<a class="dropdown-item" href="index.php?page=admin_cosses.php">				cosses</a>
							<a class="dropdown-item" href="index.php?page=admin_fabricants.php">			fabricants</a> 
							<a class="dropdown-item" href="index.php?page=admin_fournisseurs.php">		fournisseurs</a>
							<a class="dropdown-item" href="index.php?page=admin_raccordements.php">		raccordements</a> 
							<a class="dropdown-item" href="index.php?page=admin_reseau_electriques.php">	réseaux électriques</a>
							<a class="dropdown-item" href="index.php?page=admin_technologies.php">		technologies</a> 
							<a class="dropdown-item" href="index.php?page=admin_references.php">			tensions références</a>
						</div>
					</li>
				</ul>
			</div>
		</nav>
		
		<div class="container-fluid">
			<div class="col-lg-12">
				<div class="row">
					<h3> echéances à venir </h3>
				</div>
				<div class="row">
					<span id="banniere_echeances"> pas DDDD'échéances </span>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<?php
			//test de la variable page et affichage du contenu de la page
			
			if (isset($_GET['page']) && $_GET['page']!="" ) 
			{
				$page = $_GET['page'];
				if(in_array($page,$pages_autorisees))
				{
					include ($page);
				}
				else
				{?>
					<h1> Page introuvable </h1>
				<?php
				}
			}
			else
			{?>
				<h1>accueil </h1>
			<?php
			}
			?>				
		</div>
		
		<footer>
		Base de Données des batteries - SNA/O - maintenance de Brest-Guipavas 
		01/05/2020 V0.2
		</footer>

	
	</body>
</html>