//
//
//const options_date = {day:'numeric',weekday:'long',year:'numeric',month:'long',hour:'numeric',minute:'numeric', second:'numeric'};
const options_date = {day:'numeric',weekday:'long',year:'numeric',month:'long' 	};
const options_date_tableau = {day:'numeric',year:'numeric',month:'numeric' 	};
//
//
//
//
var button_operation_courante='<div class="dropdown show">\
									<a class="btn btn-info gestion_fiche_opération_courante dropdown-toggle" name="operation_courante_fiche_batterie" \
									href="#" role="button" title="opération courante sur la batterie"	value="operation_courante_fiche_batterie"\
									data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
										<span class="fa fa-wrench fa-1x "></span>\
									</a>\
								<div class="dropdown-menu" >';
//
var button_historique='<button class="btn btn-info gestion_fiche_historique"  name="historique_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="historique de la fiche de la batterie"	value="historique_fiche_batterie">	<span class="fa  fa-history fa-1x "></span></button>';
//
var button_consulter='<button class="btn btn-primary gestion_fiche_consulter" name="consultation_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="consultation de la fiche de la batterie"	value="consultation_fiche_batterie">	<span class="fa fa-eye fa-1x "></span></button>';
//
var button_modifier='<button class="btn btn-warning gestion_fiche_modifier" name="modifier_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="modification de la fiche de la batterie"	value="modification_fiche_batterie">	<span class="fa fa-edit fa-1x "></span></button>';
//
var button_supprimer='<button class="btn btn-danger gestion_fiche_supprimer" name="suppression_fiche_batterie" title="suppression de la fiche batterie" data-toggle="tooltip" data-placement="top"   value="suppression_fiche_batterie"><span class="fa fa-trash fa-1x "></span></button>';

$(function(){

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // newly activated tab
  e.relatedTarget // previous active tab
  console.log(e.currentTarget.id);
})

var tab_msg={};
$('[data-toggle="tooltip"]').tooltip();

$.getScript("./js/badbat/gestion_fiche_creation_modification_nav_1.js");
function liste_fiche(){
	if($('#recherche_fiche_tension_select').val())
	{
	var tension_recherche=$('#recherche_fiche_tension_select').val();
	}
	else
	{
	var tension_recherche = 01;
	}
	if($('#recherche_fiche_equipement_select').val())
	{
	var equipement_recherche=$('#recherche_fiche_equipement_select').val();
	}
	else
	{
	var equipement_recherche = 01;
	}
	var lieu_recherche = $('#recherche_fiche_lieux').val();
	//var tension_recherche=$('#recherche_fiche_tension_select').val();
	//var equipement_recherche = 01;//$('#recherche_fiche_equipement_select').val();
	console.log("liste_fiche:tension: "+tension_recherche+" equipement: "+equipement_recherche+" lieu:"+lieu_recherche);
	$.ajax({
					url      	: "code/operation/admin_operation_liste.php",
					type   		: "POST",		
					cache    	: false,
					async		: true,
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								   alert("Erreur : responseText: "+request.responseText);
								 },
					success  	:function(reponse) 
								{  	console.log(reponse);
								var button_operation_courante='\
								<div class="dropdown show">\
									<a class="btn btn-info gestion_fiche_opération_courante dropdown-toggle" name="operation_courante_fiche_batterie" \
										href="#" role="button" title="opération courante sur la batterie"	value="operation_courante_fiche_batterie"\
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
										<span class="fa fa-wrench fa-1x "></span>\
									</a>\
								<div class="dropdown-menu" >';
									var ligne_operations='';
									$.each(reponse,function(i,item){
										if(item.resultat ==  tab_msg['code_ok']['id'])
										{
											if(item.id != 1)
											{
											ligne_operations += '<a class="dropdown-item operations_courantes_liste" id='+item.id+' href="#">'+item.nom+'</a>'
											}
											else
											{
												// c'est le cas de l'opération "non défini"
											}
										}
										else
										{
										var res = item.resultat;
										var message = tab_msg[res]['texte'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
										}
									});
									ligne_operations +=('</div> </div>');
									button_operation_courante +=ligne_operations;
									$.ajax({
														url      	: "code/gestion_fiche_liste.php",
														type   		: "POST",	
														data     	: {lieu: lieu_recherche, equipement: equipement_recherche, tension:tension_recherche},		
														cache    	: false,
														async		: true,
														dataType 	: "json",
														error    	: function(request, error) { // Info Debuggage si erreur         
																	   alert("Erreur : responseText: "+request.responseText);
																	 },
														success  	:function(reponse) 
																	{  	//console.log(reponse);
																		$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
																		$('#gestion_fiche_tableau tr').each(function(){
																			$(this).remove();
																		});
																		var ligne_table='';
																		$.each(reponse,function(i,item){
																			if(item.resultat ==  tab_msg['code_ok']['id'])
																			{
																				ligne_table +='<tr class="consultation_fiche" id='+item.id
																							+'><td>' + item.tension+ 'V</td><td>'+ item.equipements+ '</td><td>'+ item.lieux
																							+'</td><td>'+item.etat_actuel
																							+'</td><td>'+ item.date_dop
																							+'</td><td>'+button_operation_courante
																							+'</td><td>'+button_consulter
																							+'</td><td>'+button_historique
																							+'</td><td>'+button_modifier
																							+'</td><td>'+button_supprimer+'</td></tr>';
																			}
																			else
																			{
																			var res = item.resultat;
																			var message = tab_msg[res]['texte'];
																			$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																			$('#toast_enregistrement_echec').toast('show');
																			}
																		});
																		$('#gestion_fiche_tableau').append(ligne_table);
																	}						
										});
								}						
						});
}
$(document).ready(function(){
			//mise à jour des échéances
			$('[data-toggle="tooltip"]').tooltip();
			//mise à jour de la banniere échéances
			$.getScript("./js/badbat/echeances_all_pages.js",function(){
			maj_echeance();});
				
	
	
			//$('#validation_fiche_spinner').hide();
			//on cache les toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			$('.gestion_fiche_consultation').hide();
			$('.gestion_fiche_modification').hide();
			$('.gestion_fiche_historique').hide();
			//$('#gestion_fiche_retour_liste').hide();
			$('#recherche_fiche_lieux').show();		
						$.ajax({
				url			:"./constantes/code_message.json",
				type		:"get",
				cache		:"true",
				dataType	:"json",
				success		:function(data){
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
				});
				}
			});
			//insertions de la liste des tenions diposnibles	
			//insertion de la liste des équipements disponibles
			$('#recherche_fiche_tension_select').empty();
			$.ajax({
				url			:"code/tension_reseau/admin_tension_reseau_liste_2.php", //on utilise la page admin_equipement
				type		:"GET",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							//ligne_select+='<option  value="-1"> entrer l\'équipement </option>'
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if(item.id == 1)
									{
										//ligne_select +='<option value=1 selected="selected" >'+item.valeur+'</option>';
										ligne_select +='<option value=1 selected="selected" >'+'tous'+'</option>';
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.valeur+' V'+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
							//ajout de la valeur -1 pour les remises à zéro
							$('#recherche_fiche_tension_select').append(ligne_select);
							//mise_a_jour_liste();
				//	});
			$.ajax({
				url			:"code/equipement/admin_equipement_liste.php", //on utilise la page admin_equipement
				type		:"GET",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							//ligne_select+='<option  value="-1"> entrer l\'équipement </option>'
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if(item.id == 1)
									{
										ligne_select +='<option value=1 selected="selected" >'+'tous'+'</option>';
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
							//ajout de la valeur -1 pour les remises à zéro
							$('#recherche_fiche_equipement_select').append(ligne_select);
							liste_fiche();
							}
					});
				}
				});
});
/*************************************************/
//recherche de la fiche
/************************************************/
/************************ recherche avec la tension ************************/
$('#recherche_fiche_tension_select').on('change',function(){
	liste_fiche();
});
/************************ recherche avec l'equipement ************************/
$('#recherche_fiche_equipement_select').on('change',function(){
	liste_fiche();
});
/************************ recherche avec le lieu ************************/
$('#recherche_fiche_lieux').on('change',function(){
	liste_fiche();
});
$('#recherche_fiche_lieux').autocomplete({
	minLength:		0,
	select:			function(event,ui){
					$('#recherche_fiche_lieux').val(ui.item.value);
					liste_fiche();
					//mise_a_jour_liste();
					}, 	//action de selection
	response: 		function( event, ui ) {
					//console.log($(this).val());
					var lieu_recherche =$(this).val();
					var tension_recherche=$('#recherche_fiche_tension_select').val();
					var equipement_recherche = $('#recherche_fiche_equipement_select').val();
					console.log("response:"+lieu_recherche);
					$.ajax({
						url      	: "code/gestion_fiche_liste.php",
						type   		: "POST",	
						data     	: {lieu: lieu_recherche, equipement: equipement_recherche},		
						cache    	: false,
						async		: true,
						dataType 	: "json",
						error    	: function(request, error) { // Info Debuggage si erreur         
									   alert("Erreur : responseText: "+request.responseText);
									 },
						success  	:function(reponse) 
									{  	//console.log(reponse);
										$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
										$('#gestion_fiche_tableau tr').each(function(){
											$(this).remove();
										});
										var ligne_table='';
										$.each(reponse,function(i,item){
											if(item.resultat ==  tab_msg['code_ok']['id'])
											{
												ligne_table +='<tr class="consultation_fiche" id='+item.id+'><td>'+ item.references + '</td><td>' + item.tension+ 'V</td><td>'
												+ item.equipements+ '</td><td>'+ item.lieux+ '</td><td>'+ item.date_dop+ '</td><td>'+button_consulter+'</td><td>'+button_historique+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
											}
											else
											{
											var res = item.resultat;
											var message = tab_msg[res]['texte'];
											$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
											$('#toast_enregistrement_echec').toast('show');
											}
										});
										$('#gestion_fiche_tableau').append(ligne_table);
									}						
							});	
					//mise_a_jour_liste();
					liste_fiche();
					},
	source:			function(requete,reponse){
						//récupération de la liste des lieux
					$.ajax({
								url			:"code/recherche_lieux_liste.php", 
								type		:"POST",
								cache		:true,
								data		:{lieu: requete.term},
								dataType	:"json",
								error		: function(request,error){
											alert("Erreur : responseText: "+request.responseText);
											},
								success		: function(donne){
													reponse(donne);
													//console.log(donne);
											}
							});
					}
});
/*********************************************************************************************************************************************************************/
// 
//							bandeau principal
//
//
//boutons généraux - validation, raz et annulation , création d'une nouvelle fiche
/*********************************************************************************************************************************************************************/
/************************************************/
// bouton: validation
/************************************************/
// bouton: effacer la recherche
$('#raz_recherche_fiche').on('click',function(e){
	$('#recherche_fiche_lieux').val("");
	$('#recherche_fiche_equipement_select').val(1);
	$('#recherche_fiche_tension_select').val(1);
	//$('#gestion_fiche_tableau tr').each(function(){	$(this).remove();});
	liste_fiche();
	$(this).blur();
});
/************************************************/
// bouton: annulation recherche
$('#annulation_recherche_fiche').on('click',function(e){
	$('.gestion_fiche_liste').hide();
	$('.gestion_fiche_consultation').hide();
	$('.gestion_fiche_modification').hide();
	//$('#gestion_fiche_retour_liste').hide();
	$(this).blur();
	$(location).attr('href','index.php');
});
	//bouton retour à la liste
	$('#gestion_fiche_retour_liste').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		$('.gestion_fiche_historique').hide();
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabeld',false);
		$('.gestion_fiche_liste').show();
		$('#recherche_fiche_lieux').show();
		$('#gestion_fiche_titre').text("Liste des fiches");
		liste_fiche();
		$(this).blur();
	});	
	//bouton création d'une nouvelle fiche
	$('#gestion_fiche_creation_fiche').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		//$('#gestion_fiche_retour_liste').hide();
		$('.gestion_fiche_liste').hide();
		$('#gestion_fiche_titre').text("Liste des fiches");
		$(location).attr('href','./index.php?page=creation_fiche.php');
	});	
/*********************************************************************************************************************************************************************/
// 
//							tableau
//
//boutons du tableau - consultation, modification et suppression
//
/*********************************************************************************************************************************************************************/
//
//
//
/*****************************************************************/
//
//
// //operations courantes - modal 
//
//
//
/*****************************************************************/
//
//			ouverture du modal
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.operations_courantes_liste',function(e){
		$(this).blur();
		//mise à jour des valeurs
		var id_batterie_recup  = $(e.currentTarget).closest($('.consultation_fiche')).attr("id");		
		var id_operation_recup = $(e.currentTarget).attr("id");
		var texte_operation_courante = $(e.currentTarget).text();
		$('#modal_operation_courante_etat_switch').prop('checked',false);
		$('#modal_operation_courante_etat_changement').hide();

		console.log("id_battee"+id_batterie_recup+" id_operation:"+id_operation_recup);
//		
//		
		var today = moment().format('YYYY-MM-DD');	
		$('#modal_operation_courante_date').val(today);
//		
		$('#modal_operation_courante_mesure_autonomie_heure').val('');
		$('#modal_operation_courante_mesure_autonomie_heure_label').text("heure");
		//
		$('#modal_operation_courante_mesure_autonomie_minute').val('');
		$('#modal_operation_courante_mesure_autonomie_minute_label').text("minute");
		$('#modal_operation_courante_autonomie').hide();
		//
		//requete AJAX pour récupérer les informations de l'opération
		$.ajax({
			url			:"code/etat/admin_etat_liste.php", //on utilise la page admin_operation
			type   		: "POST",			
			cache    	: false,
			async		: true,
			dataType 	: "json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse){
						console.log(reponse);
						var ligne_select='';
							$.each(reponse,function(i,item)
							{
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
								ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
								}
								else
								{
									var res = retour_json['resultat'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_modification_operation').modal('toggle');		
								}
							});
							$('#modal_operation_courante_etat_suivant').append(ligne_select);
							}
				});
		//
		//requete AJAX pour récupérer les informations de l'opération
		$.ajax({
			url			:"code/operation/admin_operation_informations.php", //on utilise la page admin_operation
			type   		: "POST",	
			data     	: {id_operation: id_operation_recup, id_batterie:id_batterie_recup},		
			cache    	: false,
			async		: true,
			dataType 	: "json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(reponse){
						console.log(reponse);
							$('#modal_operation_courante_titre').text((reponse.nom).toLowerCase());
							$('#modal_operation_courante_etat_actuel').text(reponse.nom_etat);
							$('#modal_operation_courante_etat_actuel_id').text(reponse.id_etat);
							if(reponse.mesure_autonomie_choix == "1")
							{
								$('#modal_operation_courante_autonomie').show();
								$('#modal_operation_courante_button').attr('disabled',true);
								$('#modal_operation_courante_button').addClass('disabled');
								var ligne_autonomie_requise ="";
								var autonomie_requise_heure=Math.floor(reponse.autonomie_requise / 60);
								if(autonomie_requise_heure>1)
								{
									ligne_autonomie_requise = autonomie_requise_heure+" heures ";
								}
								else
								{
									ligne_autonomie_requise = autonomie_requise_heure+" heure ";
								}
								var autonomie_requise_minute=reponse.autonomie_requise - (autonomie_requise_heure * 60);
								if(autonomie_requise_minute>1)
								{
									ligne_autonomie_requise += autonomie_requise_minute+" minutes ";
								}
								else
								{
									ligne_autonomie_requise += autonomie_requise_minute+" minute ";
								}
								var ligne_autonomie_derniere_valeur ="";
								var autonomie_derniere_valeur_heure=Math.floor(reponse.autonomie_derniere_valeur / 60);
								if(autonomie_derniere_valeur_heure>1)
								{
									ligne_autonomie_derniere_valeur = autonomie_derniere_valeur_heure+" heures ";
								}
								else
								{
									ligne_autonomie_derniere_valeur = autonomie_derniere_valeur_heure+" heure ";
								}
								var autonomie_derniere_valeur_minute=reponse.autonomie_derniere_valeur - (autonomie_derniere_valeur_heure * 60);
								if(autonomie_derniere_valeur_minute>1)
								{
									ligne_autonomie_derniere_valeur += autonomie_derniere_valeur_minute+" minutes ";
								}
								else
								{
									ligne_autonomie_derniere_valeur += autonomie_derniere_valeur_minute+" minute ";
								}
								$('#modal_operation_courante_autonomie').removeClass("element_cache_debut");
								$('#modal_operation_courante_autonomie_requise_valeur').text(ligne_autonomie_requise);
								$('#modal_operation_courante_autonomie_derniere_valeur').text(ligne_autonomie_derniere_valeur);
							}
							else
							{
								//rien
							}
						}
				});
		// installation de l'id batterie et de l'id opération dans le modal
		$('#modal_operation_courante_id_batterie').text(id_batterie_recup);
		$('#modal_operation_courante_id_operation').text(id_operation_recup);
		//initialisation des messages d'erreurs
		//heure
		$('#modal_operation_courante_button').attr('disabled',false);
		$('#modal_operation_courante_button').removeClass('disabled');
		$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("");
		$('#modal_operation_courante_mesure_autonomie_heure_erreur').hide();
		
		//minute
		$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("");
		$('#modal_operation_courante_mesure_autonomie_minute_erreur').hide();
		//		
		$('#modal_operation_courante_date').focus();
		$('#modal_operation_courante').modal('show');
	});
/*****************************************************************/
// animation des labels en fonction du nombre précédent
/*****************************************************************/	
	$('#modal_operation_courante_mesure_autonomie_heure').on("change",function(e,data){
		var heures = $('#modal_operation_courante_mesure_autonomie_heure').val();
		var minutes = $('#modal_operation_courante_mesure_autonomie_minute').val();
		
		if(((heures>0) &&(heures<23))||((heures+minutes)>0))
		{
			
			$('#modal_operation_courante_button').attr('disabled',false);
			$('#modal_operation_courante_button').removeClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("");
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').hide();
		}
		else
		{		
			$('#modal_operation_courante_button').attr('disabled',true);
			$('#modal_operation_courante_button').addClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("");
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').hide();
		}
		if(heures>1)
		{
		$('#modal_operation_courante_mesure_autonomie_heure_label').text("heures");
		}
		else
		{
		$('#modal_operation_courante_mesure_autonomie_heure_label').text("heure");
		}
	});
	$('#modal_operation_courante_mesure_autonomie_heure').on("blur",function(e,data){
	//interdiction de sortir  si valeur incorrecte
		if(($('#modal_operation_courante_mesure_autonomie_heure').val())!='')
		{
			if(($('#modal_operation_courante_mesure_autonomie_heure').val())>23)
				{
					$('#modal_operation_courante_button').attr('disabled',true);
					$('#modal_operation_courante_button').addClass('disabled');
					$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("erreur! (Max:23)");
					$('#modal_operation_courante_mesure_autonomie_heure_erreur').show();
					$('#modal_operation_courante_mesure_autonomie_heure').val("23");
				}
				else
				{
					$('#modal_operation_courante_button').attr('disabled',false);
					$('#modal_operation_courante_button').removeClass('disabled');
					$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("");
					$('#modal_operation_courante_mesure_autonomie_heure_erreur').hide();
				}
		}
		else //données incorrectes
		{
			$('#modal_operation_courante_button').attr('disabled',true);
			$('#modal_operation_courante_button').addClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').text("erreur!");
			$('#modal_operation_courante_mesure_autonomie_heure_erreur').show();
		}
	});
	
	//minutes
	
	
	$('#modal_operation_courante_mesure_autonomie_minute').on("change",function(e,data){
		var heures = $('#modal_operation_courante_mesure_autonomie_heure').val();
		var minutes = $('#modal_operation_courante_mesure_autonomie_minute').val();
		if(((minutes>0) &&(minutes<60))||((heures+minutes)>0))
		{
			
			$('#modal_operation_courante_button').attr('disabled',false);
			$('#modal_operation_courante_button').removeClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("");
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').hide();
		}
		else
		{		
			$('#modal_operation_courante_button').attr('disabled',true);
			$('#modal_operation_courante_button').addClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("");
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').hide();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		if($('#modal_operation_courante_mesure_autonomie_minute').val()>1)
		{
			$('#modal_operation_courante_mesure_autonomie_minute_label').text("minutes");
		}
		else
		{
			$('#modal_operation_courante_mesure_autonomie_minute_label').text("minute");
		}
	});
	
	
	
	
	$('#modal_operation_courante_mesure_autonomie_minute').on("blur",function(e,data){
		if(($('#modal_operation_courante_mesure_autonomie_minute').val())!='')
		{
			if(($('#modal_operation_courante_mesure_autonomie_minute').val())>59)
				{
					$('#modal_operation_courante_button').attr('disabled',true);
					$('#modal_operation_courante_button').addClass('disabled');
					$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("erreur! (Max:59)");
					$('#modal_operation_courante_mesure_autonomie_minute_erreur').show();
					$('#modal_operation_courante_mesure_autonomie_minute').val("59");
				}
				else
				{
					$('#modal_operation_courante_button').attr('disabled',false);
					$('#modal_operation_courante_button').removeClass('disabled');
					$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("");
					$('#modal_operation_courante_mesure_autonomie_minute_erreur').hide();
				}
		}
		else //données incorrectes
		{
			$('#modal_operation_courante_button').attr('disabled',true);
			$('#modal_operation_courante_button').addClass('disabled');
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').text("erreur!");
			$('#modal_operation_courante_mesure_autonomie_minute_erreur').show();
		}
	});
//
//
/*****************************************************************/
//mise à jour de l'état'
/*****************************************************************/	
// 
//
$('#modal_operation_courante_etat_switch').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_operation_courante_etat_changement').is(":visible"))
	{
	$('#modal_operation_courante_etat_changement').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_operation_courante_etat_changement').removeClass('element_cache_debut');
	$('#modal_operation_courante_etat_changement').show();
	}
});
//
//
//
/*****************************************************************/
//mise à jour de la base
/*****************************************************************/	
// 
//
//
	$('#modal_operation_courante').on('hidden.bs.modal',function(){
	});
	$('#modal_operation_courante_button').on('click', function(e){
		/* récupération des id batteries et id operations dans les spans cachés de modal body.*/
		var id_batterie_recup  = $('#modal_operation_courante_id_batterie').text();
		var id_operation_recup = $('#modal_operation_courante_id_operation').text();		
		var date_recup = $('#modal_operation_courante_date').val();
		var modification_autonomie = 1;
		var modification_etat = 1; //si 1, pas de moficiation d'état'
		if($('#modal_operation_courante_autonomie').is(':visible'))
		{
				modification_autonomie = 2;	//modification de l'autonomie
		}
		else
		{
				modification_autonomie = 1;	//pas de modification de l'autonomie
		}
		if($('#modal_operation_courante_etat_changement').is(':visible'))
		{
				modification_etat = 2;	//modification de l'autonomie
		}
		else
		{
				modification_etat = 1;	//pas de modification de l'autonomie
		}
		//console.log("modif etat "+modification_etat);
		//récupération des données
		var autonomie_mesuree_heure = Number($('#modal_operation_courante_mesure_autonomie_heure').val());
		var autonomie_mesuree_minute = Number($('#modal_operation_courante_mesure_autonomie_minute').val());
		var autonomie_mesuree = 0;
		autonomie_mesuree = (autonomie_mesuree_heure*60)+(autonomie_mesuree_minute);
		var old_etat = $('#modal_operation_courante_etat_actuel_id').text();
		var new_etat = $('#modal_operation_courante_etat_suivant option:selected').val();
		//console.log("h: "+autonomie_mesuree_heure+" minutes: "+autonomie_mesuree_minute);
		//console.log("autonomie_mesure: "+autonomie_mesuree+" modif_autonomie: "+modification_autonomie);
		console.log($("#gestion_fiche_tableau").find('[id^="'+id_batterie_recup+'"]').find("td").eq(4).text());
		console.log("ancien etat:"+old_etat+" nouvel_etat"+new_etat);
		console.log("date: "+date_recup);
		$.ajax({
			url			:"code/operation/FE_operation_mise_a_jour.php", 
			type   		: "POST",	
			data     	: {id_operation: id_operation_recup,
							id_batterie:id_batterie_recup,
								autonomie: autonomie_mesuree, 
								date: date_recup, 
								modif_autonomie: modification_autonomie,
								modif_etat:modification_etat,
								ancien_etat:old_etat,
								nouvel_etat: new_etat},		
			cache    	: false,
			async		: true,
			dataType 	: "json",
			error		: function(request,error){
						alert("Erreur : responseText: "+request.responseText);
						},
			success		: function(retour_json){
												//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_operation_courante').modal('toggle');
							//mise à jour de la liste des batteries
							date_mise_en_forme = new Date(date_recup)
							console.log(date_mise_en_forme.toLocaleDateString('fr-FR', options_date));
							$("#gestion_fiche_tableau").find('[id^="'+id_batterie_recup+'"]').find("td").eq(4).text(date_mise_en_forme.toLocaleDateString('fr-FR', options_date_tableau));
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_operation').modal('toggle');
							}
						}	
					});
	});
//
//
//
//
//
/*****************************************************************/
//
//
// //consultation
//
//
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_consulter',function(e){
		$(this).blur();		
		$('.gestion_fiche_liste').hide();
		$('.gestion_fiche_modification').hide();
		$('#recherche_fiche_lieux').hide();
		$('.gestion_fiche_historique').hide();
		$('.gestion_fiche_consultation').show();
		$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_reference').show();
		$('#gestion_fiche_titre').text("Consultation d'une fiche");
		 //mise à jour des valeurs
		 var id_recup  = 	$(e.currentTarget).parent().parent().attr("id");
		 //console.log("valeur id envoyé:"+id_recup);
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							console.log(retour_json);
							$('#gestion_fiche_reference').text(retour_json['valeur_tension_reseau']+'/'+retour_json['nom_equipement']+'/'+retour_json['nom_lieux']);
							//
							//réfence constructeur
							$('#gestion_fiche_reference_industrielle').text(retour_json['reference_industrielle']);
							//tension
							$('#gestion_fiche_tension_reseau_nom').text(retour_json['nom_tension_reseau']);
							$('#gestion_fiche_tension_reseau_valeur').text(retour_json['valeur_tension_reseau']);
							$('#gestion_fiche_tension_reseau_divers').html((retour_json['divers_tension_reseau']).replace(/\n/g,'<br/>'));
							//equipement
							$('#gestion_fiche_equipement_nom').text(retour_json['nom_equipement']);
							$('#gestion_fiche_equipement_divers').html((retour_json['divers_equipement']).replace(/\n/g,'<br/>'));
							//lieux
							$('#gestion_fiche_lieux_nom').text(retour_json['nom_lieux']);
							$('#gestion_fiche_lieux_divers').html((retour_json['divers_lieux']).replace(/\n/g,'<br/>'));
							//
							//inserion données états et événement
							$('#gestion_fiche_consultation_etat_nom').text(retour_json['nom_etat'].toLowerCase());
							$('#gestion_fiche_consultation_etat_divers').text(retour_json['divers_etat'].toLowerCase());
							$('#gestion_fiche_consultation_nom_evenement').text(retour_json['nom_evenement'].toLowerCase());
							$('#gestion_fiche_consultation_etat_date_dernier_evenement').text(retour_json['date_dernier_evenement']);
							var date_dernier_evenement = new Date(retour_json['date_dernier_evenement']*1000);
							$('#gestion_fiche_consultation_etat_date_dernier_evenement').text(date_dernier_evenement.toLocaleDateString('fr-FR', options_date));
							//
							//insertion des operations
							//
							$('#gestion_fiche_consultation_nom_operation').text(retour_json['nom_operation']);
							var date_derniere_operation = new Date(retour_json['date_derniere_operation']*1000);
							$('#gestion_fiche_consultation_etat_date_derniere_operation').text(date_derniere_operation.toLocaleDateString('fr-FR', options_date));
							//
							//insertion des autonomies
							//
							var autonomie_mesuree_heure =  Math.floor(retour_json['autonomie_mesuree']/60);
							var autonomie_mesuree_minute =  retour_json['autonomie_mesuree'] - (autonomie_mesuree_heure*60);
							$('#gestion_fiche_consultation_autonomie_mesuree').text(autonomie_mesuree_heure + " h "+ autonomie_mesuree_minute + " min");
							//
							var autonomie_requise_heure =  Math.floor(retour_json['autonomie_requise']/60);
							var autonomie_requise_minute =  retour_json['autonomie_requise'] - (autonomie_requise_heure*60);
							$('#gestion_fiche_consultation_autonomie_requise').text(autonomie_requise_heure + " h "+ autonomie_requise_minute + " min");
							//inserer la date de la dernière mesure autonomie
							//autonomie_date_derniere_mesure
							var autonomie_date_derniere_mesure = new Date(retour_json['autonomie_date_derniere_mesure']*1000);
							$('#gestion_fiche_consultation_date_derniere_autonomie_mesuree').text(autonomie_date_derniere_mesure.toLocaleDateString('fr-FR', options_date));
							//insertion des dimensions
							$('#gestion_fiche_consultation_dimension_longueur').text(retour_json['dimension_longueur']);
							$('#gestion_fiche_consultation_dimension_largeur').text(retour_json['dimension_largeur']);
							$('#gestion_fiche_consultation_dimension_hauteur').text(retour_json['dimension_hauteur']);
							//insertion des dimensions du chantier
							$('#gestion_fiche_consultation_dimension_longueur_chantier').text(retour_json['dimension_longueur_chantier']);
							$('#gestion_fiche_consultation_dimension_largeur_chantier').text(retour_json['dimension_largeur_chantier']);
							$('#gestion_fiche_consultation_dimension_hauteur_chantier').text(retour_json['dimension_hauteur_chantier']);
							//insertion des données du fabricant
							$('#gestion_fiche_consultation_fabricant_nom').text(retour_json['nom_fabricant']);
							$('#gestion_fiche_consultation_fabricant_adresse').text(retour_json['adresse_fabricant']);
							$('#gestion_fiche_consultation_fabricant_mail').text(retour_json['mail_fabricant']);
							$('#gestion_fiche_consultation_fabricant_telephone').text(retour_json['telephone_fabricant']);
							$('#gestion_fiche_consultation_fabricant_divers').text(retour_json['divers_fabricant']);

							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							mise_a_jour_liste();
							}
						}													
				});	
});
//
//
//
/*****************************************************************/
//
//
// //historique
//
//
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_historique',function(e){
		$(this).blur();		
		$('.gestion_fiche_liste').hide();
		$('.gestion_fiche_modification').hide();
		$('.gestion_fiche_consultation').hide();

		$('#recherche_fiche_lieux').hide();
		$('.gestion_fiche_historique').show();
		$('#gestion_fiche_retour_liste').show();
		
		$('#gestion_fiche_reference').show();
		$('#gestion_fiche_titre').text("Historique d'une fiche");
		 //mise à jour des valeurs
		 var id_recup  = 	$(e.currentTarget).parent().parent().attr("id");
		 console.log("valeur id envoyé:"+id_recup);
		$.ajax({
			url      	: "code/gestion_fiche_consultation_historique.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						console.log(retour_json);
						$('#table_historique').empty();
						var ligne_tableau='';
								$.each(retour_json,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if(item.switch_ope_evt == 2) //c'est une opéraion
									{
										ligne_tableau+='<tr>\
										<td>'+moment.unix(item.date_historique).format("DD/MM/YYYY")+'</td>\
										<td>'+item.nom_etat_precedent+'</td>\
										<td>'+item.nom_etat_suivant+'</td>\
										<td>'+item.nom_operation+'</td>\
										</tr>';
									}
									else	//c'est un évenment
									{
										ligne_tableau+='<tr>\
										<td>'+moment.unix(item.date_historique).format("DD/MM/YYYY")+'</td>\
										<td>'+item.nom_etat_precedent+'</td>\
										<td>'+item.nom_etat_suivant+'</td>\
										<td>'+item.nom_evenement+'</td>\
										</tr>';
									}
									
								}
							});
							console.log(ligne_tableau);
							$('#table_historique').append(ligne_tableau);
							var historique_nombre_ligne_entete="";
							switch(retour_json['nombre'])
							{
								case 0: historique_nombre_ligne_entete = "pas d'historique";
													break;
								case 1: historique_nombre_ligne_entete = "une ligne trouvée";
													break;
								default: historique_nombre_ligne_entete = "nombre de lignes trouvées: "+retour_json['nombre'];
							}
							$('#gestion_fiche_message').text(historique_nombre_ligne_entete);
							}
				});	
			});
		/****************************************************************/
		// suppression tous
		/************************************************************* */
		$('#gestion_fiche_historique_suppression_tous').on('click',function(e){
		$.ajax({
			url      	: "code/historique/admin_journal_historique_suppression_tous.php",
			type   		: "POST",
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
							//alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							},
			success  	: function(retour_json) 
						{  
						console.log(retour_json);
						if(retour_json['resultat']==  tab_msg['code_ok']['id'])
						{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							
							$('.gestion_fiche_consultation').hide();
							$('.gestion_fiche_modification').hide();
							$('.gestion_fiche_historique').hide();
							$('#gestion_fiche_raz_fiche').removeClass('disabled');
							$('#gestion_fiche_raz_fiche').attr('disabled',false);
							$('#gestion_fiche_validation_fiche').removeClass('disabled');
							$('#gestion_fiche_validation_fiche').attr('disabeld',false);
							$('.gestion_fiche_liste').show();
							$('#recherche_fiche_lieux').show();
							$('#gestion_fiche_titre').text("Liste des fiches");
							liste_fiche();
							$(this).blur();
						}
						else
						{
							var res = item.resultat;
							var message = tab_msg[res]['texte'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
						}
					}
				});	
			});



//
//
//
/*****************************************************************/
//
//
//
//
 //modification
//
//
//
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_modifier',function(e){
		$('.gestion_fiche_liste').hide();
		$('#recherche_fiche_lieux').hide();	
		$('#gestion_fiche_reference').show();
		$('.gestion_fiche_modification').show();

		//$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_titre').text("Modification d'une fiche");
		//$('#gestion_fiche_retour_liste').text("quitter sans enregistrer");
		//$.each(".my_onglets",function(){removeClass('active');});
		//$('#nav-1').addClass('active');
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#creation_fiche_autonomie_requise_heure_erreur').hide();		
		$('#creation_fiche_autonomie_requise_minute_erreur').hide();		
		$(this).blur();
		//
		//
		//mise en place des valeurs
		//
		//
		var id_recup = 	$(e.currentTarget).parent().parent().attr("id");
		tab_provisoire['id_batteries']= id_recup;
		console.log("valeur id envoyé:"+id_recup);
		//$('#creation_fiche_lieux_liste_select').empty();
		$('#validation_fiche_spinner').hide();
			$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
		//création du tab_provisoire et du tab_provisoire_initiale (pour le retour arrière)
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						if(retour_json['resultat']==  tab_msg['code_ok']['id'])
						{
						//chargement des données de la base dans le tableau provisoire
						$.each(retour_json,function(i,item){
						//
						tab_provisoire[i] = retour_json[i];
						});
						//tab_provsoire = JSON.parse(JSON.stringify(retour_json));
							//console.log(retour_json);
							console.log(tab_provisoire);
							$('#gestion_fiche_reference').text(retour_json['valeur_tension_reseau']+'/'+retour_json['nom_equipement']+'/'+retour_json['nom_lieux']);
							$('#gestion_fiche_reference').val(retour_json['reference']);
							$('#creation_fiche_reference').val(tab_provisoire['reference_industrielle']);
							$('#creation_fiche_dimension_longueur').val(tab_provisoire['dimension_longueur']);
							$('#creation_fiche_dimension_largeur').val(tab_provisoire['dimension_largeur']);
							$('#creation_fiche_dimension_hauteur').val(tab_provisoire['dimension_hauteur']);
							//dimension du chantier
							$('#creation_fiche_dimension_longueur_chantier').val(tab_provisoire['dimension_longueur_chantier']);
							$('#creation_fiche_dimension_largeur_chantier').val(tab_provisoire['dimension_largeur_chantier']);
							$('#creation_fiche_dimension_hauteur_chantier').val(tab_provisoire['dimension_hauteur_chantier']);
							//insertion de la liste des lieux après le success précédent pour être sur d'avoir les bonnes valeurs dans le tab_provisoire
							$.ajax({
									url			:"code/admin_lieux_liste.php", //on utilise la page admin_lieux
									type		:"POST",
									cache		:true,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer le lieu </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	//console.log("item.id:"+item.id+" tab_provisoire:"+tab_provisoire['id_lieux']);
															if((item.id)==(tab_provisoire['id_lieux']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_lieux_liste_select').append(ligne_select);
												}
									});
							//creation de la liste des tensions
							$('#creation_fiche_tension_liste_select').empty();
							$.ajax({
									url			:"code/tension_reseau/admin_tension_reseau_liste_2.php", //on utilise le code de listing des tensions réseaux
									type		:"POST",
									cache		:true,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer la tension </option>'
													//$('#creation_fiche_tension_liste_select').
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
																if((item.id)==(tab_provisoire['id_tension']))
																{
																	ligne_select +='<option selected value='+item.id+'>'+item.valeur+' V </option>';
																}
																else
																{
																	ligne_select +='<option value='+item.id+'>'+item.valeur+' V </option>';
																}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_tension_liste_select').append(ligne_select);
												}
									});
							//creation de la liste des équipements
							$('#creation_fiche_equipement_liste_select').empty();
							$.ajax({
									url			:"code/equipement/admin_equipement_liste.php", //on utilise le code de listing des tensions réseaux
									type		:"POST",
									cache		:true,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer l\'état </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
															if((item.id)==(tab_provisoire['id_equipement']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_equipement_liste_select').append(ligne_select);
												}
									});
							//chargement des états possibles
							$('#creation_fiche_etat_liste_select').empty();
							$.ajax({
									url			:"code/etat/admin_etat_liste.php", //on utilise la page admin_lieux
									type		:"POST",
									cache		:true,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur : responseText: "+request.responseText);
												},
									success		: function(reponse){
													var ligne_select='';
													ligne_select+='<option value="-1"> entrer l\'état </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
															if((item.id)==(tab_provisoire['id_etat']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_etat_liste_select').append(ligne_select);
												}
									});
								//chargement des fabricants possibles
								$('#creation_fiche_fabricant_liste_select').empty();
								$.ajax({
										url			:"code/fabricant/admin_fabricant_liste.php", //on utilise la page admin_fabricant
										type		:"POST",
										cache		:true,
										dataType	:"json",
										error		: function(request,error){
													alert("Erreur : responseText: "+request.responseText);
													},
										success		: function(reponse){
														var ligne_select='';
														ligne_select+='<option value="-1"> entrer le fabricant</option>'
														$.each(reponse,function(i,item){
															if(item.resultat ==  tab_msg['code_ok']['id'])
															{	
																if((item.id)==(tab_provisoire['id_fabricant']))
																{
																	ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
																}
																else
																{
																	ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
																}
															}
															else
															{
															var res = item.resultat;
															var message = tab_msg[res]['texte'];
															$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
															$('#toast_enregistrement_echec').toast('show');
															}
														});
														$('#creation_fiche_fabricant_liste_select').append(ligne_select);
													}
										});

							
							//insertion de l'autonomie requise
							var autonomie_requise_heure = Math.floor(retour_json['autonomie_requise']/60);
							$('#creation_fiche_autonomie_requise_heure').val(autonomie_requise_heure);
							if(autonomie_requise_heure>1)
							{
								$('#creation_fiche_autonomie_requise_heure_label').text("heures");
							}
							else
							{
							$('#creation_fiche_autonomie_requise_heure_label').text("heure");
							}
							if(autonomie_requise_minute>1)
							{
								$('#creation_fiche_autonomie_requise_minute_label').text("minutes");
							}
							else
							{
							$('#creation_fiche_autonomie_requise_minute_label').text("minute");
							}
							var autonomie_requise_minute = retour_json['autonomie_requise']-(autonomie_requise_heure*60)
							$('#creation_fiche_autonomie_requise_minute').val(autonomie_requise_minute);
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}													
				});			
			});
		}); //fin de modifier
	//
	/*************************************************/
// boutons généraux - validation, raz et annulation
/************************************************/
/************************************************/
// bouton: validation
/************************************************/
$('#gestion_fiche_validation_fiche').on('click',function(){
	console.log(tab_provisoire);
	var json_tab = JSON.stringify(tab_provisoire);
	//
	//insertion des éléments dans la base à partir des index
	$.ajax({
				url			:"code/gestion_fiche_validation_fiche.php", 
				type		:"POST",
				data		:{tab: json_tab},
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(retour_json){
							$('#validation_fiche_spinner').hide();
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$(this).blur();
							$(this).addClass('disabled');
							$(this).attr('disabled',true);
							$('#gestion_fiche_raz_fiche').attr('disabled',true);
							$('#gestion_fiche_raz_fiche').addClass('disabled');
							$('.gestion_fiche_consultation').hide();
							$('.gestion_fiche_modification').hide();
							$('#gestion_fiche_raz_fiche').removeClass('disabled');
							$('#gestion_fiche_raz_fiche').attr('disabled',false);
							$('#gestion_fiche_validation_fiche').removeClass('disabled');
							$('#gestion_fiche_validation_fiche').attr('disabeld',false);
							$('.gestion_fiche_liste').show();
							$('#recherche_fiche_lieux').show();
							$('#gestion_fiche_titre').text("Liste des fiches");
							liste_fiche();
							$(this).blur();							
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}	
	});
});
	//************************************************************/
	//RAZ - retour des valeurs initiales
	/******************************************************************/
	$('#gestion_fiche_raz_fiche').on('click',function(){
		/*$.each(tab_provisoire,function(index,d){
			tab_provisoire[index]="";
		});	*/
		console.log(tab_provisoire);
		//retour aux valeurs initiales de toutes les valeurs
		tab_provisoire['id_lieux'] = 01;//tab_provisoire_initiale['id_lieux'];
		tab_provisoire['id_equipement'] =  01;//tab_provisoire_initiale['id_equipement'];
		tab_provisoire['id_tension'] =  01;//tab_provisoire_initiale['id_tension'];
		tab_provisoire['id_etat'] = 01;
		//tab_provisoire['id_batteries']= 01;//tab_provisoire_initiale['id_batteries'];
		//tab_provisoire['lieux']=tab_provisoire_initiale['lieux'];
		//tab_provisoire['equipement']=tab_provisoire_initiale['equipements'];
		//tab_provisoire['tension']=tab_provisoire_initiale['tension'];
		$('#creation_fiche_tension_liste_select').val(tab_provisoire['id_tension']);
		$('#creation_fiche_equipement_liste_select').val(tab_provisoire['id_equipement']);
		$('#creation_fiche_lieux_liste_select').val(tab_provisoire['id_lieux']);
		$('#creation_fiche_etat_liste_select').val(tab_provisoire['id_etat']);
		mise_a_jour_reference();
	$(this).blur();
	});
	//************************************************************/
	//suppression
	/******************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_supprimer',function(e){
	//récupération des informations
		var id_recup = 			$(e.currentTarget).parent().parent().attr("id");
		var reference_recup = 	$(e.currentTarget).parent().parent().find("td").eq(0).html();
		var tension_recup = 	$(e.currentTarget).parent().parent().find("td").eq(01).html();
		var equipement_recup = 	$(e.currentTarget).parent().parent().find("td").eq(02).html();
		var lieu_recup = 		$(e.currentTarget).parent().parent().find("td").eq(03).html();
		var date_recup = 		$(e.currentTarget).parent().parent().find("td").eq(04).html();
		$('#gestion_fiche_titre').text("Suppression d'une fiche");
		//initialisation du modal
		$('#modal_suppression_gestion_fiche_id').attr("value",id_recup);
		$('#modal_suppression_gestion_fiche_reference').text(reference_recup);
		$('#modal_suppression_gestion_fiche_tension_reseau').text(tension_recup);
		$('#modal_suppression_gestion_fiche_equipement').text(equipement_recup);
		$('#modal_suppression_gestion_fiche_lieu').text(lieu_recup);
		$('#modal_suppression_gestion_fiche_date_mes').text(date_recup);
	//lancement de la modal	
		$('#modal_suppression_gestion_fiche').modal('show');
		$(this).blur();
	});
			$('#modal_suppression_gestion_fiche').on('hide.bs.modal',function(){
				$('#gestion_fiche_titre').text("Liste des fiches");
				liste_fiche();
			});
	// suppression de la fiche
		$('#modal_suppression_gestion_fiche_validation_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var id_supprime = $('#modal_suppression_gestion_fiche_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/gestion_fiche_suppression.php",
			type   		: "POST",
			data     	: {id: id_supprime},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							liste_fiche();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							liste_fiche();
							}
						}												
				});		
	});
/************************************************/
// bouton: interdiction pendant le chargement
/************************************************/
	$('#toast_enregistrement_ok').on('hidden.bs.toast',function(){
		/*
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');*/
		//$(location).attr('href','index.php?page=gestion_fiche.php');
	});
	$('#toast_enregistrement_echec').on('hidden.bs.toast',function(){
		/*
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		//$(location).attr('href','index.php?page=gestion_fiche.php');*/
	});
});