	$(document).ready(function(){
var pattern_equipement_nom = /.{3,}/i; 	//tous les caractères nbre:3->20 sans saut ligne
var pattern_equipement_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_divers = 255;
	
var tab_msg={};



/********************************************************/
// fonctions admin equipement
/********************************************************/
/**variagles globales                                  **/

function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des equipement
			$.ajax({
				url      	: "code/equipement/admin_equipement_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								////console.log(reponse);
								$('#nombre_equipements').html("<b>"+reponse.length+"</b>");
								$('#table_equipement tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_equipement" name="modification_equipement" 	data-toogle="tooltip" data-placement="top" title="modification du equipement"	value="modification_equipement">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_equipement" name="suppression_equipement" title="suppression du equipement" data-toggle="tooltip" data-placement="top"   value="suppression_equipement"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								//console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défut
										{
										ligne_table +='<tr class="admin_equipement" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.divers+ '</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_equipement').append(ligne_table);
							}						
					});	
			});
}
	

		//mise à jour de la banniere échéances
		$.getScript("./js/badbat/echeances_all_pages.js",function(){
		maj_echeance();
		});
		
		$('[data-toggle="tooltip"]').tooltip();
		//alert("mise à jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		
		$('#suppression_equipement_tous_spinner').hide();
		$('#ajout_equipement_spinner').hide();
		
		mise_a_jour_liste();  


		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		
		
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
		
	});
	
	/*************************************************/
	// modal suppression de tous les equipement en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_equipement_tous').click(function(event){
		$('#modal_suppression_equipement_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_equipement_tous_button').click(function(event){
		$('#modal_suppression_equipement_tous').hide();
		$('#suppression_equipement_tous_spinner').show();
		
		//suppression en cours
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/equipement/admin_equipement_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_equipement_tous').modal('toggle');
							$('#suppression_equipement_tous_spinner').hide();
							mise_a_jour_liste();
							
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_equipement_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_equipement_tous').modal('toggle');
							mise_a_jour_liste();
							
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_equipement_tous').modal('toggle');
							
							mise_a_jour_liste();
							}
						}													
				});		
		
		
	});
	
	
	/*************************************************/
	// modal modification des equipement
	/************************************************/
	
	//sélection des boutons "modifier" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_equipement').on("click",".modification_equipement",function(e){
		
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		////console.log("id: "+id_recup +" / nom: "+nom_recup+" / valeur divers:"+divers_recup );
		
		//ouverture modal de modification
		$('#modal_modification_equipement_nom').val("");
		$('#modal_modification_equipement_divers').val("");
		$('#modal_modification_equipement_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_equipement_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_equipement_nom').focus();
		
		$('#modal_modification_equipement').modal('show');
		$('#modal_modification_equipement').on('shown.bs.modal', function() {
			$('#modal_modification_equipement_nom').focus();
			$('#modal_modification_equipement_nom').val(nom_recup);	
			$('#modal_modification_equipement_divers').val(divers_recup);
			$('#modal_modification_equipement_id').attr("value",id_recup);
			
			});	
		}); //fin du On click	
			
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_modification_equipement_nom" ).focus(function() {
					$('#modal_modification_equipement_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_equipement_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_equipement_nom_aide').show();
					
					$('#modal_modification_equipement_button').attr('disabled',false);
					$('#modal_modification_equipement_button').removeClass('disabled');
		});
		
		//test de la validité des données
		$( "#modal_modification_equipement_nom" ).blur(function() {
			
			//récupération de l'id de la ligne concernée
			var id_equipement = $('#modal_modification_equipement_id').attr("value");
			
		
			//recherche d'un élément déjà présent
			var equipement_modifie = $('#modal_modification_equipement_nom').val();
			var longueur_equipement_modifie = equipement_modifie.length;
			if($('#modal_modification_equipement_nom').val()!="")
				{
				if(pattern_equipement_nom.test(equipement_modifie)&&(longueur_equipement_modifie<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/equipement/admin_equipement_modification_verifications.php",
					type   		: "POST",
					data     	: {equipement: equipement_modifie, id:id_equipement},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_equipement').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas présent ->ok
								//			- le nom est présent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, échec
									////console.log("retour"+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										
										$('#modal_modification_equipement_nom').addClass('is-valid');
										$('#modal_modification_equipement_nom').removeClass('is-invalid');
										$('#modal_modification_equipement_nom_label').addClass('text-success');
										$('#modal_modification_equipement_erreur').text("l'état est valide");
										$('#modal_modification_equipement_button').attr('disabled',false);
										$('#modal_modification_equipement_button').removeClass('disabled');
										$('#modal_modification_equipement_nom_aide').hide();
									}
									else
									{
										$('#modal_modification_equipement_nom').addClass('is-invalid');
										$('#modal_modification_equipement_nom').removeClass('is-valid');
										$('#modal_modification_equipement_nom_label').addClass('text-danger');
										$('#modal_modification_equipement_nom_aide').hide();
										
										$('#modal_modification_equipement_erreur').text("échec");					
										$('#modal_modification_equipement_button').attr('disabled',true);
										$('#modal_modification_equipement_button').addClass('disabled');
									}
								}
					
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_equipement_nom').addClass('is-invalid');
				$('#modal_modification_equipement_nom_label').addClass('text-danger');
				$('#modal_modification_equipement_nom_aide').hide();
				
				$('#modal_modification_equipement_erreur').text("le nom doit comporter entre 4 et 20 caractères");					
				$('#modal_modification_equipement_button').attr('disabled',true);
				$('#modal_modification_equipement_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_equipement_nom').addClass('is-invalid');
		$('#modal_modification_equipement_nom_label').addClass('text-danger');
		$('#modal_modification_equipement_nom_aide').hide();
		
		$('#modal_modification_equipement_erreur').text("le  champs est vide");					
		$('#modal_modification_equipement_button').attr('disabled',true);
		$('#modal_modification_equipement_button').addClass('disabled');
		}
	});
		
	$('#modal_modification_equipement_divers').blur(function() {
		
		var divers_modifie = $('#modal_modification_equipement_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_equipement_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_equipement_divers').removeClass('is-invalid');
			$('#modal_modification_equipement_divers').addClass('is-valid');
			$('#modal_modification_equipement_divers_label').removeClass('text-danger');
			$('#modal_modification_equipement_divers_label').addClass('text-success');
			
			$('#modal_modification_equipement_divers_erreur').text("le champs est valide!");
			$('#modal_modification_equipement_button').attr('disabled',false);
			$('#modal_modification_equipement_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_equipement_divers').removeClass('is-valid');
			$('#modal_modification_equipement_divers').addClass('is-invalid');
			$('#modal_modification_equipement_divers_label').addClass('text-danger');
			$('#modal_modification_equipement_divers_label').removeClass('text-success');
		
			$('#modal_modification_equipement_nom_aide').hide();
			$('#modal_modification_equipement_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_equipement_button').attr('disabled',true);
			$('#modal_modification_equipement_button').addClass('disabled');
		}
	
	});
	
	//mise à jour des données depuis la fenêtre modal
	$('#modal_modification_equipement_button').click(function(event){
		
		//récupération des informations de la fenêtre moddal
		var equipement_modifie = $('#modal_modification_equipement_nom').val();
		var divers_modifie = $('#modal_modification_equipement_divers').val();
		var id_modifie = $('#modal_modification_equipement_id').attr("value");
			//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/equipement/admin_equipement_modification.php",
			type   		: "POST",
			data     	: {equipement: equipement_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_equipement').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_equipement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_equipement').modal('toggle');
							mise_a_jour_liste();
							
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un état
	/************************************************/
	function initialisation_modal_suppresion_equipement(fonction_callback)
	{
	$('#modal_suppression_equipement_nom').val("");
	$('#modal_suppression_equipement_divers').val("");
	$('#modal_suppression_equipement_button').attr('disabled',false);
	$('#modal_suppression_equipement_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	
	
	
	//sélection des boutons "supprimer" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_equipement').on("click",".suppression_equipement",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		
	
		//ouverture modal de modification			
		initialisation_modal_ajout_equipement(function(){
			$('#modal_suppression_equipement_id').attr("value",id_recup);
			$('#modal_suppression_equipement_nom ').text(nom_recup);	
			$('#modal_suppression_equipement_divers').text(divers_recup);
			
			$('#modal_suppression_equipement').modal('show'); 
			
			});
		}); //fin du On click	
		
		
			$('#modal_suppression_equipement').on('shown.bs.modal', function() {		
			
			});			
		
		
	//mise à jour des données depuis la fenêtre modal
	$('#modal_suppression_equipement_button').click(function(event){
		
		//récupération des informations de la fenêtre moddal
		var equipement_modifie = $('#modal_suppression_equipement_nom').text();
		var divers_modifie = $('#modal_suppression_equipement_divers').text();
		var id_modifie = $('#modal_suppression_equipement_id').attr("value");
		
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/equipement/admin_equipement_suppression.php",
			type   		: "POST",
			data     	: {equipement: equipement_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_equipement').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_equipement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_equipement').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	
	/*************************************************/
	// modal ajout des equipement
	/************************************************/
	
	function initialisation_modal_ajout_equipement(fonction_callback)
	{
	$('#modal_ajout_equipement_nom').val("");
	$('#modal_ajout_equipement_divers').val("");
	$('#modal_ajout_equipement_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_equipement_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_equipement_nom_aide').show();
	$('#modal_ajout_equipement_button').attr('disabled',false);
	$('#modal_ajout_equipement_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	 
	
	
	$('#ajout_equipement').click(function() {
		initialisation_modal_ajout_equipement(function(){
			$('#modal_ajout_equipement').modal('show');
			$('#modal_ajout_equipement_nom').focus()	
			});
		});
		
		$('#modal_ajout_equipement').on('shown.bs.modal', function() {
			$('#modal_ajout_equipement_nom').focus();
						});
		
		$('#modal_ajout_equipement').on('hidden.bs.modal', function (e) {
//en réserve		
		});
			
		
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_ajout_equipement_nom" ).focus(function() {
					$('#modal_ajout_equipement_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_equipement_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_equipement_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_equipement_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_equipement_nom_aide').show();
					$('#modal_ajout_equipement_button').attr('disabled',false);
					$('#modal_ajout_equipement_button').removeClass('disabled');
				
			});
		
		//test de la validité des données
		$( "#modal_ajout_equipement_nom" ).blur(function() {
			
			//recherche d'un élément déjà présent
			var equipement_ajoute = $('#modal_ajout_equipement_nom').val();
			var longueur_equipement_ajoute = equipement_ajoute.length;
			
			
			if(($('#modal_ajout_equipement_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_equipement_nom.test(equipement_ajoute)&&(longueur_equipement_ajoute<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/equipement/admin_equipement_verifications.php",
					type   		: "POST",
					data     	: {equipement: equipement_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_equipement').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  
									
									if(retour_json['resultat'] == tab_msg['code_ok']['id']) 
									{
									//champs valide
									$('#modal_ajout_equipement_nom').removeClass('is-invalid');
									$('#modal_ajout_equipement_nom').addClass('is-valid');
									$('#modal_ajout_equipement_nom_label').addClass('text-success');
									$('#modal_ajout_equipement_nom_aide').hide();
									
									$('#modal_ajout_equipement_erreur').text("l'état est valide");
									$('#modal_ajout_equipement_button').attr('disabled',false);
									$('#modal_ajout_equipement_button').removeClass('disabled');
									}
									else	
									{
										//le champs est déjà utilisé
									$('#modal_ajout_equipement_nom').addClass('is-invalid');
									$('#modal_ajout_equipement_nom').removeClass('is-valid');
									$('#modal_ajout_equipement_nom_label').addClass('text-danger');
									$('#modal_ajout_equipement_nom_aide').hide();
									
									$('#modal_ajout_equipement_erreur').text("l'état est déjà présent");
									$('#modal_ajout_equipement_button').attr('disabled',true);
									$('#modal_ajout_equipement_button').addClass('disabled');
									}
									
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_equipement_nom').addClass('is-invalid');
				$('#modal_ajout_equipement_nom_label').addClass('text-danger');
				$('#modal_ajout_equipement_nom_aide').hide();
				
				$('#modal_ajout_equipement_erreur').text("le nom doit comporter entre 4 et 20 lettres");					
				$('#modal_ajout_equipement_button').attr('disabled',true);
				$('#modal_ajout_equipement_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_equipement_nom').addClass('is-invalid');
				$('#modal_ajout_equipement_nom_label').addClass('text-danger');
				$('#modal_ajout_equipement_nom_aide').hide();
				
				$('#modal_ajout_equipement_erreur').text("le  champs est vide");					
				$('#modal_ajout_equipement_button').attr('disabled',true);
				$('#modal_ajout_equipement_button').addClass('disabled');
			}					
		});
					
	
	$('#modal_ajout_equipement_divers').blur(function() {
		
		var divers_ajoute = $('#modal_ajout_equipement_divers').val();
		var longueur_divers_ajoute = divers_ajoute.length;
		//test du pattern
		if(pattern_equipement_divers.test(divers_ajoute)&&(longueur_divers_ajoute<longueur_max_divers))
		{
		//champs valide
			$('#modal_ajout_equipement_divers').removeClass('is-invalid');
			$('#modal_ajout_equipement_divers').addClass('is-valid');
			$('#modal_ajout_equipement_divers_label').removeClass('text-danger');
			$('#modal_ajout_equipement_divers_label').addClass('text-success');
			
			$('#modal_ajout_equipement_divers_erreur').text("le champs est valide!");
			$('#modal_ajout_equipement_button').attr('disabled',false);
			$('#modal_ajout_equipement_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_equipement_divers').removeClass('is-valid');
			$('#modal_ajout_equipement_divers').addClass('is-invalid');
			$('#modal_ajout_equipement_divers_label').addClass('text-danger');
			$('#modal_ajout_equipement_divers_label').removeClass('text-success');
			
			$('#modal_ajout_equipement_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_ajout_equipement_button').attr('disabled',true);
			$('#modal_ajout_equipement_button').addClass('disabled');
		}
	
	
	});
	
	
	$('#modal_ajout_equipement_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var equipement_ajoute = $('#modal_ajout_equipement_nom').val();
		var divers_ajoute = $('#modal_ajout_equipement_divers').val();
		$('#ajout_equipement_spinner').show();
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/equipement/admin_equipement_ajout.php",
			type   		: "POST",
			data     	: {equipement: equipement_ajoute,divers:divers_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_equipement').modal('toggle');
							$('#ajout_equipement_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_equipement_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_equipement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_equipement').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
