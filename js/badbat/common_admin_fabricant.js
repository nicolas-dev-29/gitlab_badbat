	function initialisation_modal_ajout_fabricant(fonction_callback)
	{
	$('#modal_ajout_fabricant_nom').val("");
	$('#modal_ajout_fabricant_divers').val("");
	$('#modal_ajout_fabricant_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_fabricant_nom_aide').show();
	$('#modal_ajout_fabricant_button').attr('disabled',false);
	$('#modal_ajout_fabricant_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}