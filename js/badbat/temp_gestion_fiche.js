const options_date = {day:'numeric',weekday:'long',year:'numeric',month:'long',hour:'numeric',minute:'numeric', second:'numeric'};
var button_operation_courante='<button class="btn btn-info gestion_fiche_oération_courante " disabled="disabled" name="operation_courante_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="opération courante sur la batterie"	value="operation_courante_fiche_batterie">	<span class="fa fa-wrench fa-1x "></span></button>';
var button_consulter='<button class="btn btn-primary gestion_fiche_consulter" name="consultation_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="consultation de la fiche de la batterie"	value="consulatation_fiche_batterie">	<span class="fa fa-eye fa-1x "></span></button>';
var button_modifier='<button class="btn btn-warning gestion_fiche_modifier" name="modifier_fiche_batterie" 	data-toogle="tooltip" data-placement="top" title="modification de la fiche de la batterie"	value="modification_fiche_batterie">	<span class="fa fa-edit fa-1x "></span></button>';
var button_supprimer='<button class="btn btn-danger gestion_fiche_supprimer" name="suppression_fiche_batterie" title="suppression de la fiche batterie" data-toggle="tooltip" data-placement="top"   value="suppression_fiche_batterie"><span class="fa fa-trash fa-1x "></span></button>';
/*$(function(){
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // newly activated tab
  e.relatedTarget // previous active tab
  //console.log(e.currentTarget.id);
})
})*/;
var tab_msg={};
$('[data-toggle="tooltip"]').tooltip();
$.getScript("./js/badbat/gestion_fiche_creation_modification_nav_1.js");
function liste_fiche(){
	if($('#recherche_fiche_tension_select').val())
	{
	var tension_recherche=$('#recherche_fiche_tension_select').val();
	}
	else
	{
	var tension_recherche = 01;
	}
	if($('#recherche_fiche_equipement_select').val())
	{
	var equipement_recherche=$('#recherche_fiche_equipement_select').val();
	}
	else
	{
	var equipement_recherche = 01;
	}
	var lieu_recherche = $('#recherche_fiche_lieux').val();
	//var tension_recherche=$('#recherche_fiche_tension_select').val();
	//var equipement_recherche = 01;//$('#recherche_fiche_equipement_select').val();
	//console.log("liste_fiche:tension: "+tension_recherche+" equipement: "+equipement_recherche+" lieu:"+lieu_recherche);
	$.ajax({
						url      	: "code/gestion_fiche_liste.php",
						type   		: "POST",	
						data     	: {lieu: lieu_recherche, equipement: equipement_recherche, tension:tension_recherche},		
						cache    	: false,
						async		: true,
						dataType 	: "json",
						error    	: function(request, error) { // Info Debuggage si erreur         
									   alert("Erreur gestion_fiche_liste.php: ReponseText: "+request.ReponseText);
									 },
						success  	:function(reponse) 
									{  	////console.log(reponse);
										$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
										$('#gestion_fiche_tableau tr').each(function(){
											$(this).remove();
										});
										var ligne_table='';
										$.each(reponse,function(i,item){
											if(item.resultat ==  tab_msg['code_ok']['id'])
											{
												ligne_table +='<tr class="consultation_fiche" id='+item.id+'><td>' + item.tension+ 'V</td><td>'+ item.equipements+ '</td><td>'+ item.lieux+ '</td><td>'+item.etat_actuel+'</td><td>'+ item.date_mes+ '</td><td>'+button_operation_courante+'</td><td>'+button_consulter+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
											}
											else
											{
											var res = item.resultat;
											var message = tab_msg[res]['texte'];
											$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
											$('#toast_enregistrement_echec').toast('show');
											}
										});
										$('#gestion_fiche_tableau').append(ligne_table);
									}						
							});
}
$(document).ready(function(){
			//$('#validation_fiche_spinner').hide();
			//on cache les toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			$('.gestion_fiche_consultation').hide();
			$('.gestion_fiche_modification').hide();
			$('#recherche_fiche_lieux').show();		
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				e.target // newly activated tab
				  e.relatedTarget // previous active tab 
				  });
			$.ajax({
				url			:"./constantes/code_message.json",
				type		:"get",
				cache		:"true",
				dataType	:"json",
				success		:function(data){
							$.each(data,function(index,d){			
								var tab_msg_tampon={};
								tab_msg_tampon['id']= d.id;
								tab_msg_tampon['nom']=d.nom;
								tab_msg_tampon['texte']=d.texte;
								tab_msg[index]=tab_msg_tampon;
				});
				}
			});
			//insertions de la liste des tenions diposnibles	
			//insertion de la liste des équipements disponibles
			$('#recherche_fiche_tension_select').empty();
			$.ajax({
				url			:"code/tension_reseau/admin_tension_reseau_liste_2.php", //on utilise la page admin_equipement
				type		:"POST",
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur Admin_tension_reseau_liste_2.php: ReponseText: "+request.ReponseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							//ligne_select+='<option  value="-1"> entrer l\'équipement </option>'
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if(item.id == 1)
									{
										//ligne_select +='<option value=1 selected="selected" >'+item.valeur+'</option>';
										ligne_select +='<option value=1 selected="selected" >'+'tous'+'</option>';
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.valeur+' V'+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
							//ajout de la valeur -1 pour les remises à zéro
							$('#recherche_fiche_tension_select').append(ligne_select);
							//mise_a_jour_liste();
			$.ajax({
				url			:"code/equipement/admin_equipement_liste.php", //on utilise la page admin_equipement
				type		:"POST",
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur 1 er admin_equipement_liste.php: ReponseText: "+request.ReponseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							//ligne_select+='<option  value="-1"> entrer l\'équipement </option>'
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if(item.id == 1)
									{
										ligne_select +='<option value=1 selected="selected" >'+'tous'+'</option>';
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
							//ajout de la valeur -1 pour les remises à zéro
							$('#recherche_fiche_equipement_select').append(ligne_select);
							liste_fiche();
							}
				});
			}
			});	
});


/*************************************************/
//recherche de la fiche
/************************************************/
/************************ recherche avec la tension ************************/

$('#recherche_fiche_tension_select').on('change',function(){
	liste_fiche();
});
/************************ recherche avec l'equipement ************************/
$('#recherche_fiche_equipement_select').on('change',function(){
	liste_fiche();
});
/************************ recherche avec le lieu ************************/
$('#recherche_fiche_lieux').on('change',function(){
	liste_fiche();
});
$('#recherche_fiche_lieux').autocomplete({
	minLength:		0,
	select:			function(event,ui){
					$('#recherche_fiche_lieux').val(ui.item.value);
					liste_fiche();
					//mise_a_jour_liste();
					}, 	//action de selection
	Reponse: 		function( event, ui ) {
					////console.log($(this).val());
					var lieu_recherche =$(this).val();
					var tension_recherche=$('#recherche_fiche_tension_select').val();
					var equipement_recherche = $('#recherche_fiche_equipement_select').val();
					//console.log("Reponse:"+lieu_recherche);
					$.ajax({
						url      	: "code/gestion_fiche_liste.php",
						type   		: "POST",	
						data     	: {lieu: lieu_recherche, equipement: equipement_recherche},		
						cache    	: false,
						async		: true,
						dataType 	: "json",
						error    	: function(request, error) { // Info Debuggage si erreur         
									   alert("Erreur gestion_fiche_liste.php: ReponseText: "+request.ReponseText);
									 },
						success  	:function(reponse) 
									{  	////console.log(reponse);
										$('#consultation_fiche_nombre').html("<b>"+reponse.length+"</b>");
										$('#gestion_fiche_tableau tr').each(function(){
											$(this).remove();
										});
										var ligne_table='';
										$.each(reponse,function(i,item){
											if(item.resultat ==  tab_msg['code_ok']['id'])
											{
												ligne_table +='<tr class="consultation_fiche" id='+item.id+'><td>'+ item.references + '</td><td>' + item.tension+ 'V</td><td>'+ item.equipements+ '</td><td>'+ item.lieux+ '</td><td>'+ item.date_dop+ '</td><td>'+button_consulter+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
											}
											else
											{
											var res = item.resultat;
											var message = tab_msg[res]['texte'];
											$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
											$('#toast_enregistrement_echec').toast('show');
											}
										});
										$('#gestion_fiche_tableau').append(ligne_table);
									}						
							});	
					//mise_a_jour_liste();
					liste_fiche();
					},
	source:			function(requete,reponse){
						//récupération de la liste des lieux
					$.ajax({
								url			:"code/recherche_lieux_liste.php", 
								type		:"POST",
								cache		:false,
								data		:{lieu: requete.term},
								dataType	:"json",
								error		: function(request,error){
											alert("Erreur recherche_lieux_liste: ReponseText: "+request.ReponseText);
											},
								success		: function(donne){
													reponse(donne);
													////console.log(donne);
											}
							});
					}
});
/*********************************************************************************************************************************************************************/
// 
//							bandeau principal
//
//
//boutons généraux - validation, raz et annulation , création d'une nouvelle fiche
/*********************************************************************************************************************************************************************/
/************************************************/
// bouton: validation
/************************************************/
// bouton: effacer la recherche
$('#raz_recherche_fiche').on('click',function(e){
	$('#recherche_fiche_lieux').val("");
	$('#recherche_fiche_equipement_select').val(1);
	$('#recherche_fiche_tension_select').val(1);
	//$('#gestion_fiche_tableau tr').each(function(){	$(this).remove();});
	liste_fiche();
	$(this).blur();
});
/************************************************/
// bouton: annulation recherche
$('#annulation_recherche_fiche').on('click',function(e){
	$('.gestion_fiche_liste').hide();
	$('.gestion_fiche_consultation').hide();
	$('.gestion_fiche_modification').hide();
	//$('#gestion_fiche_retour_liste').hide();
	$(this).blur();
	$(location).attr('href','index.php');
});
	//bouton retour à la liste
	$('#gestion_fiche_retour_liste').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabeld',false);
		$('.gestion_fiche_liste').show();
		$('#recherche_fiche_lieux').show();
		$('#gestion_fiche_titre').text("Liste des fiches");
		liste_fiche();
		$(this).blur();
	});	
	//bouton création d'une nouvelle fiche
	$('#gestion_fiche_creation_fiche').on('click',function(){
		$('.gestion_fiche_consultation').hide();
		$('.gestion_fiche_modification').hide();
		//$('#gestion_fiche_retour_liste').hide();
		$('.gestion_fiche_liste').hide();
		$('#gestion_fiche_titre').text("Liste des fiches");
		$(location).attr('href','./index.php?page=creation_fiche.php');
	});	
/*********************************************************************************************************************************************************************/
// 
//							tableau
//
//boutons du tableau - consultation, modification et suppression
//
/*********************************************************************************************************************************************************************/
//
//
//
//
//
/*
*/
/*****************************************************************/
//
//
// //consultation
//
//
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_consulter',function(e){
		$(this).blur();		
		$('.gestion_fiche_liste').hide();
		$('.gestion_fiche_modification').hide();
		$('#recherche_fiche_lieux').hide();
		$('.gestion_fiche_consultation').show();
		$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_reference').show();
		$('#gestion_fiche_titre').text("Consultation d'une fiche");
		
		 //mise à jour des valeurs
		 var id_recup  = 	$(e.currentTarget).parent().parent().attr("id");
		 ////console.log("valeur id envoyé:"+id_recup);
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur gestion_fiche_consultation_valeurs : ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec_texte').html("Erreur consulatation valeur: ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							//console.log(retour_json);
							$('#gestion_fiche_reference').text(retour_json['valeur_tension_reseau']+'/'+retour_json['nom_equipement']+'/'+retour_json['nom_lieux']);
							//
							//tension
							$('#gestion_fiche_tension_reseau_nom').text(retour_json['nom_tension_reseau']);
							$('#gestion_fiche_tension_reseau_valeur').text(retour_json['valeur_tension_reseau']);
							$('#gestion_fiche_tension_reseau_divers').html((retour_json['divers_tension_reseau']).replace(/\n/g,'<br/>'));
							//equipement
							$('#gestion_fiche_equipement_nom').text(retour_json['nom_equipement']);
							$('#gestion_fiche_equipement_divers').html((retour_json['divers_equipement']).replace(/\n/g,'<br/>'));
							
							//lieux
							$('#gestion_fiche_lieux_nom').text(retour_json['nom_lieux']);
							$('#gestion_fiche_lieux_divers').html((retour_json['divers_lieux']).replace(/\n/g,'<br/>'));
							//
							//inserion données états et événement
							$('#gestion_fiche_consultation_etat_nom').text(retour_json['nom_etat'].toLowerCase());
							$('#gestion_fiche_consultation_etat_divers').text(retour_json['divers_etat'].toLowerCase());
							$('#gestion_fiche_consultation_nom_evenement').text(retour_json['nom_evenement'].toLowerCase());
							$('#gestion_fiche_consultation_etat_date_dernier_evenement').text(retour_json['date_dernier_evenement']);
							var date_dernier_evenement = new Date(retour_json['date_dernier_evenement']*1000);
							$('#gestion_fiche_consultation_etat_date_dernier_evenement').text(date_dernier_evenement.toLocaleDateString('fr-FR', options_date));
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							mise_a_jour_liste();
							}
						}													
				});	
});
//
//
//
//
/*****************************************************************/
//
//
//
//
 //modification
//
//
//
//
/*****************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_modifier',function(e){
		$('.gestion_fiche_liste').hide();
		$('#recherche_fiche_lieux').hide();	
		$('#gestion_fiche_reference').show();
		$('.gestion_fiche_modification').show();
		//$('#gestion_fiche_retour_liste').show();
		$('#gestion_fiche_titre').text("Modification d'une fiche");
		//$('#gestion_fiche_retour_liste').text("quitter sans enregistrer");
		//$.each(".my_onglets",function(){removeClass('active');});
		//$('#nav-1').addClass('active');
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_validation_fiche').attr('disabled',false);		
		$(this).blur();
		//
		//
		//mise en place des valeurs
		//
		//
		var id_recup = 	$(e.currentTarget).parent().parent().attr("id");
		tab_provisoire['id_batteries']= id_recup;
		////console.log("valeur id envoyé:"+id_recup);
		$('#creation_fiche_lieux_liste_select').empty();
		$('#validation_fiche_spinner').hide();
			$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
		//création du tab_provisoire et du tab_provisoire_initiale (pour le retour arrière)
		 $.ajax({
			url      	: "code/gestion_fiche_consultation_valeurs.php",
			type   		: "POST",
			data     	: {id: id_recup},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur gestion_fiche_consultation_valeurs.php: ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec_texte').html("Erreur toast enregistrement valeurs: ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						if(retour_json['resultat']==  tab_msg['code_ok']['id'])
						{
						//chargement des données de la base dans le tableau provisoire
						$.each(retour_json,function(i,item){
						////console.log("retour json: "+i+"/"+item);
						tab_provisoire[i] = retour_json[i];
						});
							////console.log("tab: "+tab_provisoire);
							$('#gestion_fiche_reference').text(retour_json['valeur_tension_reseau']+'/'+retour_json['nom_equipement']+'/'+retour_json['nom_lieux']);
							//insertion de la liste des lieux après le success précédent pour être sur d'avoir les bonnes valeurs dans le tab_provisoire
							$.ajax({
									url			:"code/admin_lieux_liste.php", //on utilise la page admin_lieux
									type		:"POST",
									cache		:false,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur admin_lieux_liste.php: ReponseText: "+request.ReponseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer le lieu </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	////console.log("item.id:"+item.id+" tab_provisoire:"+tab_provisoire['id_lieux']);
															if((item.id)==(tab_provisoire['id_lieux']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_lieux_liste_select').append(ligne_select);
												}
									});
							//creation de la liste des tensions
							$('#creation_fiche_tension_liste_select').empty();
							$.ajax({
									url			:"code/tension_reseau/admin_tension_reseau_liste_2.php", //on utilise le code de listing des tensions réseaux
									type		:"POST",
									cache		:false,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur admin_tension_reseau_liste_2.php: ReponseText: "+request.ReponseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer la tension </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
														if(item.id == 1)
															{
															ligne_select +='<option selected value='+item.id+'>'+item.nom+' </option>';
															}
															else
															{
																if((item.id)==(tab_provisoire['id_tension']))
																{
																	ligne_select +='<option selected value='+item.id+'>'+item.valeur+' V </option>';
																}
																else
																{
																	ligne_select +='<option value='+item.id+'>'+item.valeur+' V </option>';
																}
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_tension_liste_select').append(ligne_select);
												}
									});
							//creation de la liste des équipements
							$('#creation_fiche_equipement_liste_select').empty();
							$.ajax({
									url			:"code/equipement/admin_equipement_liste.php", //on utilise le code de listing des tensions réseaux
									type		:"POST",
									cache		:false,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur admin_equipement_liste.php: ReponseText: "+request.ReponseText);
												},
									success		: function(reponse){
													var ligne_select='';
													//ligne_select+='<option value="-1"> entrer l\'état </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
															if((item.id)==(tab_provisoire['id_equipement']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_equipement_liste_select').append(ligne_select);
												}
									});
							//chargement des états possibles
							$('#creation_fiche_etat_liste_select').empty();
							$.ajax({
									url			:"code/etat/admin_etat_liste.php", //on utilise la page admin_lieux
									type		:"POST",
									cache		:false,
									dataType	:"json",
									error		: function(request,error){
												alert("Erreur admin_etat_liste.php: ReponseText: "+request.ReponseText);
												},
									success		: function(reponse){
													var ligne_select='';
													ligne_select+='<option value="-1"> entrer l\'état </option>'
													$.each(reponse,function(i,item){
														if(item.resultat ==  tab_msg['code_ok']['id'])
														{	
															if((item.id)==(tab_provisoire['id_etat']))
															{
																ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
															}
															else
															{
																ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
															}
														}
														else
														{
														var res = item.resultat;
														var message = tab_msg[res]['texte'];
														$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
														$('#toast_enregistrement_echec').toast('show');
														}
													});
													$('#creation_fiche_etat_liste_select').append(ligne_select);
												}
									});
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}													
				});			
			});
		}); //fin de modifier
	//
	/*************************************************/
// boutons généraux - validation, raz et annulation
/************************************************/
/************************************************/
// bouton: validation
/************************************************/
$('#gestion_fiche_validation_fiche').on('click',function(){
	var json_tab = JSON.stringify(tab_provisoire);
	//
	//insertion des éléments dans la base à partir des index
	$.ajax({
				url			:"code/gestion_fiche_validation_fiche.php", 
				type		:"POST",
				data		:{tab: json_tab},
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur gestion_fiche_validation_fiche.php: ReponseText: "+request.ReponseText);
							},
				success		: function(retour_json){
							$('#validation_fiche_spinner').hide();
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$(this).blur();
							$(this).addClass('disabled');
							$(this).attr('disabled',true);
							$('#gestion_fiche_raz_fiche').attr('disabled',true);
							$('#gestion_fiche_raz_fiche').addClass('disabled');
							$('.gestion_fiche_consultation').hide();
							$('.gestion_fiche_modification').hide();
							$('#gestion_fiche_raz_fiche').removeClass('disabled');
							$('#gestion_fiche_raz_fiche').attr('disabled',false);
							$('#gestion_fiche_validation_fiche').removeClass('disabled');
							$('#gestion_fiche_validation_fiche').attr('disabeld',false);
							$('.gestion_fiche_liste').show();
							$('#recherche_fiche_lieux').show();
							$('#gestion_fiche_titre').text("Liste des fiches");
							liste_fiche();
							$(this).blur();							
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}	
	});
});
	//************************************************************/
	//RAZ - retour des valeurs initiales
	/******************************************************************/
	$('#gestion_fiche_raz_fiche').on('click',function(){
		/*$.each(tab_provisoire,function(index,d){
			tab_provisoire[index]="";
		});	*/
		//console.log(tab_provisoire);
		//retour aux valeurs initiales de toutes les valeurs
		tab_provisoire['id_lieux'] = 01;//tab_provisoire_initiale['id_lieux'];
		tab_provisoire['id_equipement'] =  01;//tab_provisoire_initiale['id_equipement'];
		tab_provisoire['id_tension'] =  01;//tab_provisoire_initiale['id_tension'];
		tab_provisoire['id_etat'] = 01;
		//tab_provisoire['id_batteries']= 01;//tab_provisoire_initiale['id_batteries'];
		//tab_provisoire['lieux']=tab_provisoire_initiale['lieux'];
		//tab_provisoire['equipement']=tab_provisoire_initiale['equipements'];
		//tab_provisoire['tension']=tab_provisoire_initiale['tension'];
		$('#creation_fiche_tension_liste_select').val(tab_provisoire['id_tension']);
		$('#creation_fiche_equipement_liste_select').val(tab_provisoire['id_equipement']);
		$('#creation_fiche_lieux_liste_select').val(tab_provisoire['id_lieux']);
		$('#creation_fiche_etat_liste_select').val(tab_provisoire['id_etat']);
		mise_a_jour_reference();
	$(this).blur();
	});
	//************************************************************/
	//suppression
	/******************************************************************/
	$('#gestion_fiche_tableau').on('click','.gestion_fiche_supprimer',function(e){
	//récupération des informations
		var id_recup = 			$(e.currentTarget).parent().parent().attr("id");
		var reference_recup = 	$(e.currentTarget).parent().parent().find("td").eq(0).html();
		var tension_recup = 	$(e.currentTarget).parent().parent().find("td").eq(01).html();
		var equipement_recup = 	$(e.currentTarget).parent().parent().find("td").eq(02).html();
		var lieu_recup = 		$(e.currentTarget).parent().parent().find("td").eq(03).html();
		var date_recup = 		$(e.currentTarget).parent().parent().find("td").eq(04).html();
		$('#gestion_fiche_titre').text("Suppression d'une fiche");
		//initialisation du modal
		$('#modal_suppression_gestion_fiche_id').attr("value",id_recup);
		$('#modal_suppression_gestion_fiche_reference').text(reference_recup);
		$('#modal_suppression_gestion_fiche_tension_reseau').text(tension_recup);
		$('#modal_suppression_gestion_fiche_equipement').text(equipement_recup);
		$('#modal_suppression_gestion_fiche_lieu').text(lieu_recup);
		$('#modal_suppression_gestion_fiche_date_mes').text(date_recup);
	//lancement de la modal	
		$('#modal_suppression_gestion_fiche').modal('show');
		$(this).blur();
	});
			$('#modal_suppression_gestion_fiche').on('hide.bs.modal',function(){
				$('#gestion_fiche_titre').text("Liste des fiches");
				liste_fiche();
			});
	// suppression de la fiche
		$('#modal_suppression_gestion_fiche_validation_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var id_supprime = $('#modal_suppression_gestion_fiche_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/gestion_fiche_suppression.php",
			type   		: "POST",
			data     	: {id: id_supprime},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur gestion_fiche_suppression.php: ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec_texte').html("Erreur suppresion: ReponseText: "+request.ReponseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							liste_fiche();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_gestion_fiche').modal('toggle');
							liste_fiche();
							}
						}												
				});		
	});
/************************************************/
// bouton: interdiction pendant le chargement
/************************************************/
	$('#toast_enregistrement_ok').on('hidden.bs.toast',function(){
		/*
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');*/
		//$(location).attr('href','index.php?page=gestion_fiche.php');
	});
	$('#toast_enregistrement_echec').on('hidden.bs.toast',function(){
		/*
		$('#gestion_fiche_validation_fiche').attr('disabled',false);
		$('#gestion_fiche_validation_fiche').removeClass('disabled');
		$('#gestion_fiche_raz_fiche').attr('disabled',false);
		$('#gestion_fiche_raz_fiche').removeClass('disabled');
		//$(location).attr('href','index.php?page=gestion_fiche.php');*/
	});
	