
var ligne_changement = "";
var ligne_prevenance = "";




function maj_echeance()
{/*
		$.ajax({
					url      	: "code/banniere_echeance/banniere_echeance_4.php",		//envoi du mail
					type   		: "POST",	
					cache    	: false,
					async		: true,
					dataType 	: "json",
					/*error    	: function(request, error) { // Info Debuggage si erreur         
								   alert("Erreur : responseText: "+request.responseText);
								 },*/
					/*success  	:function(reponse) 
								{  
								console.log(reponse);
								ligne_changement="";
								ligne_prevenance="";
								//traitement des batteries en mode prevenance
								if(reponse.nbre_batt_prev!=0)
								{
									//partie prevenance
									$('#banniere_echeances_prevenance_button').prop('enabled', true);
									$('#banniere_echeances_prevenance_texte').empty();
									$('#banniere_echeances_prevenance_badge').text((reponse.nbre_batt_prev)-(reponse.nbre_batt_chgt));
									$('#banniere_echeances_prevenance_badge').removeClass("badge-info").addClass("badge-warning");
									$('#banniere_echeances_prevenance_button').removeClass("element_cache_debut");
									$.each(reponse,function(i,item)
									{		
										if((item.temps_prevenance=="en_cours")&&(item.changement_etat!="fait"))
										{
											ligne_prevenance+="<div class = \"card my-2 \">\
											<div class=\"card-header\">\
											"+item.valeur_tension_reseau+"V - "+item.nom_equipement+" - "+item.nom_lieux+" \
											</div>\
											<div class=\"card-body\">\
											date de la dernière opération:<br>"+item.date_derniere_operation_string+" <br>\
											date du dernier événement:<br>"+item.date_derniere_evenement_string+" <br>\
											temps avant changement:<br>"+item.temps_prevenance_restant_string+"<br>\
											</div>\
											</div>";
										}
									});
								}
								else
								{
									$('#banniere_echeances_prevenance_badge').text("0");
									$('#banniere_echeances_prevenance_badge').addClass("badge-info").removeClass("badge-danger");
									$('#banniere_echeances_prevenance_button').addClass("element_cache_debut");
									$('#banniere_echeances_prevenance_button').prop('disabled', true);
								}
								if(reponse.nbre_batt_chgt!=0)
								{
									//partie changement
									$('#banniere_echeances_changement_button').prop('enabled', true);
									$('#banniere_echeances_changement_texte').empty();
									$.each(reponse,function(i,item)
									{
										if(item.changement_etat=="fait")
										{
											//collecte des informations pour l'affichage et l'envoi du mail
											//affichage
											ligne_changement+="<div class = \"card my-2 \">\
											<div class=\"card-header\">\
											"+item.valeur_tension_reseau+"V - "+item.nom_equipement+" - "+item.nom_lieux+" \
											</div>\
											<div class=\"card-body\">\
											"+item.nom_etat_prec+" <span class=\"fa fa-arrow-right fa-1x\" ></span> "+item.nom_etat_suiv+"<br>\
											</div>\
											</div>";
											$('#banniere_echeances_changement_badge').text(reponse.nbre_batt_chgt);
											$('#banniere_echeances_changement_badge').removeClass("badge-info").addClass("badge-danger");
											$('#banniere_echeances_changement_button').removeClass("element_cache_debut");
										}
									});
								}
								else
								{
									$('#banniere_echeances_changement_badge').text("0");
									$('#banniere_echeances_changement_badge').addClass("badge-info").removeClass("badge-danger");
									$('#banniere_echeances_changement_button').addClass("element_cache_debut");
									$('#banniere_echeances_changement_button').prop('disabled', true);
								}
							},
				});*/
}				
//appui du bouton
//
//	modal d'information des changements
//
//
$('#banniere_echeances_changement_button').on('click',function(){
	$('#modal_echeances_changement_texte').html(ligne_changement);
	$('#modal_echeances_changement').modal('show');
});
//
//
//acquitement des changements
$('#modal_echeances_changement_acq_button').click(function() {
	$('#banniere_echeances_changement_badge').text("0");
	$('#banniere_echeances_changement_badge').addClass("badge-info").removeClass("badge-danger");
	$('#banniere_echeances_changement_button').addClass("element_cache_debut");
	$('#banniere_echeances_changement_button').prop('disabled', true);
	$('#modal_echeances_changement').modal('hide');
});
//	
//
//modal d'information des prevenances
//
//
//
$('#banniere_echeances_prevenance_button').on('click',function(){
	$('#modal_echeances_prevenance_texte').html(ligne_prevenance);
	$('#modal_echeances_prevenance').modal('show');
});
//
//
//
//acquitement des prévenances
$('#modal_echeances_prevenance_acq_button').click(function() {
	$('#banniere_echeances_prevenance_badge').text("0");
	$('#banniere_echeances_prevenance_badge').addClass("badge-info").removeClass("badge-danger");
	$('#banniere_echeances_prevenance_button').addClass("element_cache_debut");
	$('#banniere_echeances_prevenance_button').prop('disabled', true);
	$('#modal_echeances_prevenance').modal('hide');
});

