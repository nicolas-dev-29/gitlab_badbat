$(document).ready(function(){ 	
	
	function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
	}	

var pattern_utilisateur_nom = /.{1,}/i; 	//tous les caractères nbre:3->20 sans saut ligne
var pattern_utilisateur_prenom = /.{1,}/i; //tous les caractères nbre:1 sans saut ligne
var pattern_utilisateur_mail = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i; //adresse mail valide
var pattern_utilisateur_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_prenom = 20;
var longueur_max_divers = 255;
var tab_msg={};

/********************************************************/
// fonctions admin utilisateur
/********************************************************/
/**variagles globales    
	                             **/
function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des utilisateur
			$.ajax({
				url      	: "code/utilisateur/admin_utilisateur_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								////console.log(reponse);
								$('#nombre_utilisateurs').html("<b>"+reponse.length+"</b>");
								$('#table_utilisateur tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_utilisateur" name="modification_utilisateur" 	data-toogle="tooltip" data-placement="top" title="modification du utilisateur"	value="modification_utilisateur">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_utilisateur" name="suppression_utilisateur" title="suppression du utilisateur" data-toggle="tooltip" data-placement="top"   value="suppression_utilisateur"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								//console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défut
										{
										ligne_table +='<tr class="admin_utilisateur" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.prenom+'</td><td>'+ item.mail+'</td><td>'+item.divers+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_utilisateur').append(ligne_table);
							}						
					});	
			});
}

				//mise à jour de la banniere échéances
		$.getScript("./js/badbat/echeances_all_pages.js",function(){
		maj_echeance();
		});
		$('[data-toggle="tooltip"]').tooltip();
		
		
		
		
		//alert("mise à jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		$('#suppression_utilisateur_tous_spinner').hide();
		$('#ajout_utilisateur_spinner').hide();
		mise_a_jour_liste();  
		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
	});
	/*************************************************/
	// modal suppression de tous les utilisateur en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_utilisateur_tous').click(function(event){
		$('#modal_suppression_utilisateur_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_utilisateur_tous_button').click(function(event){
		$('#modal_suppression_utilisateur_tous').hide();
		$('#suppression_utilisateur_tous_spinner').show();
		//suppression en cours
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/utilisateur/admin_utilisateur_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_utilisateur_tous').modal('toggle');
							$('#suppression_utilisateur_tous_spinner').hide();
							mise_a_jour_liste();
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_utilisateur_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_utilisateur_tous').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_utilisateur_tous').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
	/*************************************************/
	// modal modification des utilisateurs
	/************************************************/
	//sélection des boutons "modifier" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_utilisateur').on("click",".modification_utilisateur",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var prenom_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var mail_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(03).html();
		////console.log("id: "+id_recup +" / nom: "+nom_recup+" / valeur divers:"+divers_recup );
		//ouverture modal de modification
		$('#modal_modification_utilisateur_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_utilisateur_prenom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_utilisateur_mail').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_utilisateur_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_utilisateur_prenom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_utilisateur_mail_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_utilisateur_nom_aide').show();
		$('#modal_modification_utilisateur_prenom_aide').show();
		$('#modal_modification_utilisateur_button').attr('disabled',false);
		$('#modal_modification_utilisateur_button').removeClass('disabled');
		$('#modal_modification_utilisateur_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_utilisateur_nom').val("");
		$('#modal_modification_utilisateur_divers').val("");
		$('#modal_modification_utilisateur_nom').focus();
		$('#modal_modification_utilisateur').modal('show');
		$('#modal_modification_utilisateur').on('shown.bs.modal', function() {
			$('#modal_modification_utilisateur_nom').focus();
			$('#modal_modification_utilisateur_nom').val(nom_recup);	
			$('#modal_modification_utilisateur_prenom').val(prenom_recup);	
			$('#modal_modification_utilisateur_mail').val(mail_recup);	
			$('#modal_modification_utilisateur_divers').val(divers_recup);
			$('#modal_modification_utilisateur_id').attr("value",id_recup);
			});	
		}); //fin du On click	
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_modification_utilisateur_nom" ).focus(function() {
					$('#modal_modification_utilisateur_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_utilisateur_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_utilisateur_nom_aide').show();
					$('#modal_modification_utilisateur_button').attr('disabled',false);
					$('#modal_modification_utilisateur_button').removeClass('disabled');
		});
		//test de la validité des données
		$( ".modal_modification_utilisateur_nom_prenom" ).blur(function() {
			//récupération de l'id de la ligne concernée
			var id_utilisateur = $('#modal_modification_utilisateur_id').attr("value");
			//recherche d'un élément déjà présent
			var nom_utilisateur_modifie = 		$('#modal_modification_utilisateur_nom').val();
			var prenom_utilisateur_modifie = 	$('#modal_modification_utilisateur_prenom').val();
			//console.log(nom_utilisateur_modifie+"/"+prenom_utilisateur_modifie+"/"+id_utilisateur);
			var longueur_nom_utilisateur_modifie = nom_utilisateur_modifie.length;
			var longueur_prenom_utilisateur_modifie = prenom_utilisateur_modifie.length;
			if(($('#modal_modification_utilisateur_nom').val()!="")&&($('#modal_modification_utilisateur_prenom').val()!=""))
				{
				if(pattern_utilisateur_nom.test(nom_utilisateur_modifie)&&(longueur_nom_utilisateur_modifie<longueur_max_nom)
					&&pattern_utilisateur_prenom.test(nom_utilisateur_modifie)&&(longueur_prenom_utilisateur_modifie<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				console.log(id_utilisateur +'/'+nom_utilisateur_modifie+' / '+prenom_utilisateur_modifie +' /');
				$.ajax({
					 url      	: "code/utilisateur/admin_utilisateur_modification_verifications.php",
					type   		: "POST",
					data     	: { id_user:id_utilisateur, nom_utilisateur: nom_utilisateur_modifie,prenom_utilisateur: prenom_utilisateur_modifie},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_utilisateur').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas présent ->ok
								//			- le nom est présent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, échec
									console.log("retour:  "+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										$('#modal_modification_utilisateur_nom').addClass('is-valid');
										$('#modal_modification_utilisateur_nom').removeClass('is-invalid');
										$('#modal_modification_utilisateur_nom_label').addClass('text-success').removeClass('text-danger');
										$('#modal_modification_utilisateur_nom_aide').hide();
										$('#modal_modification_utilisateur_prenom').addClass('is-valid');
										$('#modal_modification_utilisateur_prenom').removeClass('is-invalid');
										$('#modal_modification_utilisateur_prenom_label').addClass('text-success').removeClass('text-danger');
										//$('#modal_modification_utilisateur_erreur').text("les données sont valides");
										$('#modal_modification_utilisateur_prenom_aide').hide();
										$('#modal_modification_utilisateur_button').attr('disabled',false);
										$('#modal_modification_utilisateur_button').removeClass('disabled');
									}
									else
									{
										$('#modal_modification_utilisateur_nom').addClass('is-invalid');
										$('#modal_modification_utilisateur_nom').removeClass('is-valid');
										$('#modal_modification_utilisateur_nom_label').addClass('text-danger');
										$('#modal_modification_utilisateur_nom_aide').hide();
										$('#modal_modification_nom_utilisateur_erreur').text("l'utilisateur existe déjà");					
										$('#modal_modification_utilisateur_button').attr('disabled',true);
										$('#modal_modification_utilisateur_button').addClass('disabled');										
										$('#modal_modification_utilisateur_prenom').addClass('is-invalid');
										$('#modal_modification_utilisateur_prenom').removeClass('is-valid');
										$('#modal_modification_utilisateur_prenom_label').addClass('text-danger');
										$('#modal_modification_utilisateur_prenom_aide').hide();
										$('#modal_modification_prenom_utilisateur_erreur').text("l'utilisateur existe déjà");					
										$('#modal_modification_utilisateur_button').attr('disabled',true);
										$('#modal_modification_utilisateur_button').addClass('disabled');
									}
								}
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_utilisateur_nom').addClass('is-invalid');
				$('#modal_modification_utilisateur_nom_label').addClass('text-danger');
				$('#modal_modification_utilisateur_nom_aide').hide();
				$('#modal_modification_utilisateur_erreur').text("le nom doit comporter entre 4 et 20 caractères");					
				$('#modal_modification_utilisateur_button').attr('disabled',true);
				$('#modal_modification_utilisateur_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_utilisateur_nom').addClass('is-invalid');
		$('#modal_modification_utilisateur_nom_label').addClass('text-danger');
		$('#modal_modification_utilisateur_nom_aide').hide();
		$('#modal_modification_utilisateur_erreur').text("le  champs est vide");					
		$('#modal_modification_utilisateur_button').attr('disabled',true);
		$('#modal_modification_utilisateur_button').addClass('disabled');
		}
	});
	$('#modal_modification_utilisateur_divers').blur(function() {
		var divers_modifie = $('#modal_modification_utilisateur_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_utilisateur_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_utilisateur_divers').removeClass('is-invalid');
			$('#modal_modification_utilisateur_divers').addClass('is-valid');
			$('#modal_modification_utilisateur_divers_label').removeClass('text-danger');
			$('#modal_modification_utilisateur_divers_label').addClass('text-success');
			$('#modal_modification_utilisateur_divers_erreur').text("le champs est valide!");
			$('#modal_modification_utilisateur_button').attr('disabled',false);
			$('#modal_modification_utilisateur_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_utilisateur_divers').removeClass('is-valid');
			$('#modal_modification_utilisateur_divers').addClass('is-invalid');
			$('#modal_modification_utilisateur_divers_label').addClass('text-danger');
			$('#modal_modification_utilisateur_divers_label').removeClass('text-success');
			$('#modal_modification_utilisateur_nom_aide').hide();
			$('#modal_modification_utilisateur_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_utilisateur_button').attr('disabled',true);
			$('#modal_modification_utilisateur_button').addClass('disabled');
		}
	});
	//mise à jour des données depuis la fenêtre modal
	$('#modal_modification_utilisateur_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var nom_utilisateur_modifie = $('#modal_modification_utilisateur_nom').val();
		var prenom_utilisateur_modifie = $('#modal_modification_utilisateur_prenom').val();
		var mail_utilisateur_modifie = $('#modal_modification_utilisateur_mail').val();
		var divers_modifie = $('#modal_modification_utilisateur_divers').val();
		var id_modifie = $('#modal_modification_utilisateur_id').attr("value");
			//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/utilisateur/admin_utilisateur_modification.php",
			type   		: "POST",
			data     	: {mail_utilisateur: mail_utilisateur_modifie, prenom_utilisateur: prenom_utilisateur_modifie,nom_utilisateur: nom_utilisateur_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_utilisateur').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						console.log(retour_json);
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un utilisateur
	/************************************************/
	function initialisation_modal_suppresion_utilisateur(fonction_callback)
	{
	$('#modal_suppression_utilisateur_nom').val("");
	$('#modal_suppression_utilisateur_prenom').val("");
	$('#modal_suppression_utilisateur_mail').val("");
	$('#modal_suppression_utilisateur_divers').val("");
	$('#modal_suppression_utilisateur_button').attr('disabled',false);
	$('#modal_suppression_utilisateur_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	//sélection des boutons "supprimer" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_utilisateur').on("click",".suppression_utilisateur",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var prenom_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var mail_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(03).html();
		console.log(id_recup+nom_recup+divers_recup);
		$('#modal_suppression_utilisateur').modal('show');
		//ouverture modal de modification			
		initialisation_modal_ajout_utilisateur(function(){
			$('#modal_suppression_utilisateur_id').attr("value",id_recup);
			$('#modal_suppression_utilisateur_nom ').text(nom_recup);	
			$('#modal_suppression_utilisateur_prenom ').text(prenom_recup);	
			$('#modal_suppression_utilisateur_mail ').text(mail_recup);	
			$('#modal_suppression_utilisateur_divers').text(divers_recup);
			 
			});
			
		}); //fin du On click	
		
		
	$('#modal_suppression_utilisateur').on('shown.bs.modal', function() {		
			});			
			
	//mise à jour des données depuis la fenêtre modal
	$('#modal_suppression_utilisateur_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var utilisateur_modifie = $('#modal_suppression_utilisateur_nom').text();
		var divers_modifie = $('#modal_suppression_utilisateur_divers').text();
		var id_modifie = $('#modal_suppression_utilisateur_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/utilisateur/admin_utilisateur_suppression.php",
			type   		: "POST",
			data     	: {utilisateur: utilisateur_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_utilisateur').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	/*************************************************/
	// modal ajout d'un utilisateur
	/************************************************/
	function initialisation_modal_ajout_utilisateur(fonction_callback)
	{
	//$('#modal_ajout_utilisateur_nom').val("");
	//$('#modal_ajout_utilisateur_prenom').val("");
	//$('#modal_ajout_utilisateur_mail').val("");
	$('#modal_ajout_utilisateur_divers').val("");
	$('#modal_ajout_utilisateur_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_utilisateur_prenom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_utilisateur_mail').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_utilisateur_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_utilisateur_prenom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_utilisateur_mail_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_utilisateur_nom_aide').show();
	$('#modal_ajout_utilisateur_prenom_aide').show();
	$('#modal_ajout_utilisateur_button').attr('disabled',false);
	$('#modal_ajout_utilisateur_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	$('#ajout_utilisateur').click(function() {
		initialisation_modal_ajout_utilisateur(function(){
			$('#modal_ajout_utilisateur').modal('show');
			});
		});
		$('#modal_ajout_utilisateur').on('shown.bs.modal', function() {
			$('#modal_ajout_utilisateur_nom').focus();
			//$('#modal_ajout_utilisateur_nom').prop('required',true);
						});
		$('#modal_ajout_utilisateur').on('hide.bs.modal', function (e) {
				$('#modal_ajout_utilisateur_nom').prop('required',false);
		});
		$('#modal_ajout_utilisateur').on('hidden.bs.modal', function (e) {
			//remise à zéro de l'ensemble des champs
			$('#modal_ajout_utilisateur_nom').prop('required',false);
			$(this).find("input").val("");
			$('#modal_ajout_utilisateur_nom').removeClass('is-invalid').removeClass('is-valid');
					$('#modal_ajout_utilisateur_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_utilisateur_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_utilisateur_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_utilisateur_nom_aide').show();
					$('#modal_ajout_utilisateur_button').attr('disabled',false);
					$('#modal_ajout_utilisateur_button').removeClass('disabled');
		});
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_ajout_utilisateur_nom" ).focus(function() {
					$('#modal_ajout_utilisateur_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_utilisateur_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_utilisateur_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_utilisateur_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_utilisateur_nom_aide').show();
					$('#modal_ajout_utilisateur_button').attr('disabled',false);
					$('#modal_ajout_utilisateur_button').removeClass('disabled');
			});
			$( "#modal_ajout_utilisateur_prenom" ).focus(function() {
				$('#modal_ajout_utilisateur_prenom').removeClass('is-valid').removeClass('is-invalid');
				$('#modal_ajout_utilisateur_prenom_label').removeClass('text-success').removeClass('text-danger');
				$('#modal_ajout_utilisateur_prenom_aide').show();
		});	
		//test de la validité des données sur le nom et le prénom
		$( ".modal_ajout_utilisateur_nom_prenom" ).blur(function() {
			//recherche d'un élément déjà présent
			var utilisateur_nom_ajoute = $('#modal_ajout_utilisateur_nom').val();
			var utilisateur_prenom_ajoute = $('#modal_ajout_utilisateur_prenom').val();
			//
			var longueur_utilisateur_nom_ajoute = utilisateur_nom_ajoute.length;
			var longueur_utilisateur_prenom_ajoute = utilisateur_prenom_ajoute.length;
			//
			if(($('#modal_ajout_utilisateur_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_utilisateur_nom.test(utilisateur_nom_ajoute)&&(longueur_utilisateur_nom_ajoute<longueur_max_nom))
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_utilisateur_nom').removeClass('is-invalid');
				$('#modal_ajout_utilisateur_nom_label').removeClass('text-danger');
				//
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/utilisateur/admin_utilisateur_verifications_nom_prenom.php",
					type   		: "POST",
					data     	: {nom_utilisateur: utilisateur_nom_ajoute , prenom_utilisateur: utilisateur_prenom_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_utilisateur').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  //console.log(retour_json);
									if(retour_json['resultat'] == tab_msg['code_ok']['id'])
									{
										//la requete est valide
										if(retour_json['nombre'] == 0)
										{	//il n' y a pas d'homonymes
											texte_nom = "ok";
											texte_prenom = "ok";
											$('#modal_ajout_utilisateur_button').attr('disabled',false);
											$('#modal_ajout_utilisateur_button').removeClass('disabled');
											$('#modal_ajout_utilisateur_nom').removeClass('is-invalid');
											$('#modal_ajout_utilisateur_nom').addClass('is-valid');
											$('#modal_ajout_utilisateur_nom_label').addClass('text-success');
											$('#modal_ajout_utilisateur_nom_label').removeClass('text-danger');
											$('#modal_ajout_utilisateur_nom_aide').hide();
											$('#modal_ajout_utilisateur_nom_ok').text(texte_nom);
											if(($('#modal_ajout_utilisateur_prenom').val()!=""))
											{											
												$('#modal_ajout_utilisateur_prenom').removeClass('is-invalid');
												$('#modal_ajout_utilisateur_prenom').addClass('is-valid');
												$('#modal_ajout_utilisateur_prenom_label').addClass('text-success');
												$('#modal_ajout_utilisateur_prenom_label').removeClass('text-danger');
												$('#modal_ajout_utilisateur_prenom_aide').hide();
												$('#modal_ajout_utilisateur_prenom_ok').text(texte_prenom);
											}
										}
										else
										{	//il y a des homonymes
											$('#modal_ajout_utilisateur_button').attr('disabled',true);
											$('#modal_ajout_utilisateur_button').addClass('disabled');
											if(retour_json['nombre'] == 01)
											{
											//l'utilisateur est déjà présent dans la base
											texte_nom = "il y a un homonyme";
											$('#modal_ajout_utilisateur_nom').addClass('is-invalid');
											$('#modal_ajout_utilisateur_nom').removeClass('is-valid');
											$('#modal_ajout_utilisateur_nom_label').addClass('text-danger');
											$('#modal_ajout_utilisateur_nom_label').removeClass('text-success');
											$('#modal_ajout_utilisateur_nom_aide').hide();
											$('#modal_ajout_utilisateur_nom_erreur').text("L'utilisateur existe déjà");
											$('#modal_ajout_utilisateur_prenom').addClass('is-invalid');
											$('#modal_ajout_utilisateur_prenom').removeClass('is-valid');
											$('#modal_ajout_utilisateur_prenom_label').addClass('text-danger');
											$('#modal_ajout_utilisateur_prenom_label').removeClass('text-success');
											$('#modal_ajout_utilisateur_prenom_aide').hide();
											$('#modal_ajout_utilisateur_prenom_erreur').text("l'utilisateur existe déjà");
											$('#modal_ajout_utilisateur_button').attr('disabled',true);
											$('#modal_ajout_utilisateur_button').addClass('disabled');
											}
											else
											{
											// des utilisateurs ont déjà le même nom
											texte_nom="il y a "+retour_json['nombre']+" homonymes";
											$('#modal_ajout_utilisateur_nom').addClass('is-valid');
											$('#modal_ajout_utilisateur_nom').removeClass('is-invalid');
											$('#modal_ajout_utilisateur_nom_label').removeClass('text-danger');
											$('#modal_ajout_utilisateur_nom_label').addClass('text-success');
											$('#modal_ajout_utilisateur_nom_aide').hide();
											$('#modal_ajout_utilisateur_nom_ok').text("ok, mais il y a "+texte_nom);
											if(($('#modal_ajout_utilisateur_prenom').val()==""))
											{											
												$('#modal_ajout_utilisateur_prenom').removeClass('is-invalid');
												$('#modal_ajout_utilisateur_prenom').removeClass('is-valid');
												$('#modal_ajout_utilisateur_prenom_label').removeClass('text-success');
												$('#modal_ajout_utilisateur_prenom_label').removeClass('text-danger');
												$('#modal_ajout_utilisateur_prenom_aide').show();
												//$('#modal_ajout_utilisateur_prenom_ok').text(texte_prenom);
											}
											}
										}
									}
									else	
									{
									var res = retour_json['resultat'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_suppression_utilisateur').modal('toggle');
									}
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_utilisateur_nom').addClass('is-invalid');
				$('#modal_ajout_utilisateur_nom_label').addClass('text-danger');
				$('#modal_ajout_utilisateur_nom_aide').hide();
				$('#modal_ajout_utilisateur_nom_erreur').text("le nom doit comporter entre 1 et 20 lettres");					
				$('#modal_ajout_utilisateur_button').attr('disabled',true);
				$('#modal_ajout_utilisateur_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_utilisateur_nom').addClass('is-invalid');
				$('#modal_ajout_utilisateur_nom_label').addClass('text-danger');
				$('#modal_ajout_utilisateur_nom_aide').hide();
				$('#modal_ajout_utilisateur_nom_erreur').text("le  champs est vide");					
				$('#modal_ajout_utilisateur_button').attr('disabled',true);
				$('#modal_ajout_utilisateur_button').addClass('disabled');
			}				
		});
	/**** - vérification de l'email - ****/
	$('#modal_ajout_utilisateur_mail').focus(function(){
			$('#modal_ajout_utilisateur_mail').removeClass('is-valid').removeClass('is-invalid');
	});
	$('#modal_ajout_utilisateur_mail').blur(function(){
		mail_ajoute = $('#modal_ajout_utilisateur_mail').val();
		if(isEmail(mail_ajoute))
		{
		$('#modal_ajout_utilisateur_button').attr('disabled',false);
		$('#modal_ajout_utilisateur_button').removeClass('disabled');
		$('#modal_ajout_utilisateur_mail').removeClass('is-invalid');
		$('#modal_ajout_utilisateur_mail').addClass('is-valid');
		$('#modal_ajout_utilisateur_mail_label').addClass('text-success');
		$('#modal_ajout_utilisateur_mail_label').removeClass('text-danger');
		$('#modal_ajout_utilisateur_mail_aide').hide();
		$('#modal_ajout_utilisateur_mail_ok').text("ok");
		}
		else
		{
		$('#modal_ajout_utilisateur_button').attr('disabled',true);
		$('#modal_ajout_utilisateur_button').addClass('disabled');
		$('#modal_ajout_utilisateur_mail').addClass('is-invalid');
		$('#modal_ajout_utilisateur_mail_label').addClass('text-danger');
		$('#modal_ajout_utilisateur_mail_aide').hide();
		$('#modal_ajout_utilisateur_mail_erreur').text("le mail doit comporter entre 1 et 20 lettres");					
		$('#modal_ajout_utilisateur_button').attr('disabled',true);
		$('#modal_ajout_utilisateur_button').addClass('disabled');
		}
	});		
	$('#modal_ajout_utilisateur_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var utilisateur_nom_ajoute = $('#modal_ajout_utilisateur_nom').val();
		var utilisateur_prenom_ajoute = $('#modal_ajout_utilisateur_prenom').val();
		var utilisateur_mail_ajoute = $('#modal_ajout_utilisateur_mail').val();
		var divers_ajoute = $('#modal_ajout_utilisateur_divers').val();
		$('#ajout_utilisateur_spinner').show();
		console.log(utilisateur_mail_ajoute);
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/utilisateur/admin_utilisateur_ajout.php",
			type   		: "POST",
			data     	: {utilisateur_nom: utilisateur_nom_ajoute,utilisateur_prenom: utilisateur_prenom_ajoute,utilisateur_mail: utilisateur_mail_ajoute,divers:divers_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_utilisateur').modal('toggle');
							$('#ajout_utilisateur_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_utilisateur_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_utilisateur').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
