

/********************************************************/
// fonctions admin déclencheurs
/********************************************************/
/**variagles globales                                  **/

function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des evenement
			$.ajax({
				url      	: "code/evenement/admin_evenement_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								console.log(reponse);
								$('#nombre_evenement').html("<b>"+((reponse.length)-1)+"</b>");
								$('#table_evenement tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_evenement" name="modification_evenement" 	data-toogle="tooltip" data-placement="top" title="modification du evenement"	value="modification_evenement">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_evenement" name="suppression_evenement" title="suppression du evenement" data-toggle="tooltip" data-placement="top"   value="suppression_evenement"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
									moment.locale('fr');
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défaut
										{
										var unite = "";
										var unit='';
										var duree_maintien_formatee='';
										var duree_prevenance_formatee='';
										var unite_annee='';
										var unite_jour='';
										var utilisateur_maintien_nom_prenom='';
										var utilisateur_prevenance_nom_prenom='';
										
										if(item.temps_maintien_etat !=0)
										{
											//formatage temps de maintien
											var nbre_annees_maintien_etat = Math.floor(item.temps_maintien_etat/31536000);
											var nbre_mois_maintien_etat= Math.floor((item.temps_maintien_etat - nbre_annees_maintien_etat	*31536000) / 2678400);
											var nbre_jours_maintien_etat = Math.floor((item.temps_maintien_etat - nbre_annees_maintien_etat*31536000 - nbre_mois_maintien_etat*2678400)/86400);
											console.log("Années "+nbre_annees_maintien_etat+" mois: "+nbre_mois_maintien_etat	+"jours: "+nbre_jours_maintien_etat);
											var affichage_annee='';
											var affichage_mois='';
											var affichage_jours='';
											switch(nbre_annees_maintien_etat)
											{
											case 0: 
													affichage_annee='';
												break;
											case 1:
													affichage_annee='1 an ';
												break;
											default:
													affichage_annee=nbre_annees_maintien_etat+' années ';
											}
											switch(nbre_mois_maintien_etat		)
											{
											case 0: 
													affichage_mois='';
												break;
											case 1:
													affichage_mois='1 mois ';
												break;
											default:
													affichage_mois=nbre_mois_maintien_etat	+' mois ';
											}
											switch(nbre_jours_maintien_etat)
											{
											case 0: 
													affichage_jours='';
												break;
											case 1:
													affichage_jours='1 jour ';
												break;
											default:
													affichage_jours=nbre_jours_maintien_etat+' jours ';
											}
											duree_maintien_formatee = affichage_annee+affichage_mois+affichage_jours;
											utilisateur_maintien_nom_prenom =  item.utilisateur_fin_maintien_nom+' '+ item.utilisateur_fin_maintien_prenom;
										}
										else
										{
											duree_maintien_formatee='';
											utilisateur_maintien_nom_prenom =  '';
											
										}
										if(item.temps_prevenance_changement_etat !=0)
										{
											//formatage temps de maintien
											var nbre_annees_prevenance = Math.floor(item.temps_prevenance_changement_etat/31536000);
											var nbre_mois_prevenance= Math.floor((item.temps_prevenance_changement_etat - nbre_annees_prevenance	*31536000) / 2678400);
											var nbre_jours_prevenance = Math.floor((item.temps_prevenance_changement_etat - nbre_annees_prevenance*31536000 - nbre_mois_prevenance*2678400)/86400);
											//console.log("années "+nbre_annees_prevenance+" mois: "+nbre_mois_prevenance	+"jours: "+nbre_jours_prevenance);
											var affichage_annee='';
											var affichage_mois='';
											var affichage_jours='';
											switch(nbre_annees_prevenance)
											{
											case 0: 
													affichage_annee='';
												break;
											case 1:
													affichage_annee='1 an ';
												break;
											default:
													affichage_annee=nbre_annees_prevenance+' années ';
											}
											switch(nbre_mois_prevenance		)
											{
											case 0: 
													affichage_mois='';
												break;
											case 1:
													affichage_mois='1 mois ';
												break;
											default:
													affichage_mois=nbre_mois_prevenance	+' mois ';
											}
											switch(nbre_jours_prevenance)
											{
											case 0: 
													affichage_jours='';
												break;
											case 1:
													affichage_jours='1 jour ';
												break;
											default:
													affichage_jours=nbre_jours_prevenance+' jours ';
											}
											duree_prevenance_formatee = affichage_annee+affichage_mois+affichage_jours; 
											utilisateur_prevenance_nom_prenom = item.utilisateur_fin_prevenance_nom+' '+ item.utilisateur_fin_prevenance_prenom;
										}
										else
										{
											duree_prevenance_formatee = '';
											utilisateur_prevenance_nom_prenom='';
										}
										//var duree = moment.duration(item.temps_maintien_etat);
										//var essai = moment.duration(item.temps_maintien_etat, "seconds").asMonths();
										
										ligne_table +='<tr class="admin_evenement" id='+item.id+'>\
										<td>'+item.id+'</td>\
										<td>'+ item.nom +'</td>\
										<td>'+ item.etat_precedent+'</td>\
										<td>'+ item.etat_suivant+'</td>\
										<td>'+ duree_maintien_formatee+'</td>\
										<td>'+ utilisateur_maintien_nom_prenom+'</td>\
										<td>'+ duree_prevenance_formatee+'</td>\
										<td>'+ utilisateur_prevenance_nom_prenom+'</td>\
										<td>'+ item.divers+ '</td>\
										<td>'+ button_modifier+'</td>\
										<td>'+ button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_evenement').append(ligne_table);
							}						
					});	
			});
}

$(document).ready(function()
{
const options_date = {day:'numeric',weekday:'long',year:'numeric',month:'long',hour:'numeric',minute:'numeric', second:'numeric'};
var pattern_evenement_nom = /.{3,}/i; 	//tous les caractères nbre:3->20 sans saut ligne
var pattern_evenement_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_divers = 255;

var tab_msg={};
		
		//mise à jour de la banniere échéances
		$.getScript("./js/badbat/echeances_all_pages.js",function(){
		maj_echeance();});
		$('[data-toggle="tooltip"]').tooltip();
		
		
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		$('#suppression_evenement_tous_spinner').hide();
		$('#ajout_evenement_spinner').hide();
		mise_a_jour_liste();  
		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				//////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				//////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
	});
	/*************************************************/
	// modal suppression de tous les evenement en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_evenement_tous').click(function(event){
		$('#modal_suppression_evenement_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_evenement_tous_button').click(function(event){
		$('#modal_suppression_evenement_tous').hide();
		$('#suppression_evenement_tous_spinner').show();
		//suppression en cours
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/evenement/admin_evenement_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_evenement_tous').modal('toggle');
							$('#suppression_evenement_tous_spinner').hide();
							mise_a_jour_liste();
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_evenement_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_evenement_tous').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_evenement_tous').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
	/******************************************************************************/
	//
	//
	//
	//modal modification évenements       
	//
	//
	/******************************************************************************/
	//sélection des boutons "modifier" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_evenement').on("click",".modification_evenement",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		$('#modal_modification_evenement_id').attr("value",id_recup);
		//initialisation des champs
		$('#modal_modification_evenement_nom').val("");
		$('#modal_modification_evenement_divers').val("");
		$('#modal_modification_evenement_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_evenement_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_evenement_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_evenement_nom_aide').show();
		$('#modal_modification_evenement_button').attr('disabled',false);
		$('#modal_modification_evenement_button').removeClass('disabled');
		//etats
		$('#modal_modification_evenement_etat_initial').empty();
		$('#modal_modification_evenement_etat_final').empty();
		//maintien
		$('#modal_modification_evenement_temps_maintien').prop('checked',false);
		$('#modal_modification_evenement_temps_prevenance').prop('checked',false);
		$('#modal_modification_evenement_temps_maintien_choix').hide();
		$('#modal_modification_evenement_temps_maintien_annee').val(0);
		$('#modal_modification_evenement_temps_maintien_mois').val(0);
		$('#modal_modification_evenement_temps_maintien_jours').val(0);
		$('#modal_modification_evenement_temps_maintien_utilisateur').prop('checked',false);
		$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').val(1);
		//prevenance
		$('#modal_modification_evenement_temps_prevenance').prop('checked',false);
		$('#modal_modification_evenement_temps_prevenance').prop('checked',false);
		$('#modal_modification_evenement_temps_prevenance_choix').hide();
		$('#modal_modification_evenement_temps_prevenance_annee').val(0);
		$('#modal_modification_evenement_temps_prevenance_mois').val(0);
		$('#modal_modification_evenement_temps_prevenance_jours').val(0);
		$('#modal_modification_evenement_temps_prevenance_utilisateur').prop('checked',false);
		$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').val(1);
		$('#modal_modification_evenement_nom').focus();
		$('#modal_modification_evenement').modal('show');
		//
		//mise en place des renseignements dans le modal
		//
			//récupération des données depuis la base de données
			$.ajax({
					 url      	: "code/evenement/admin_evenement_valeurs.php",
					type   		: "POST",
					data     	: {id : id_recup},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_modification_evenement').modal('toggle');
								 },
					success  	: function(reponse) 
									{
									console.log(reponse);
										if(reponse['resultat'] ==  tab_msg['code_ok']['id']) 
										{
										$('#modal_modification_evenement_nom').val(reponse.nom);
										//$('#modal_modification_evenement_etat_initial').attr("value",);
										$('#modal_modification_evenement_divers').val(reponse.divers);
										//mise à jour des états
										$.ajax({
											url      	: "code/etat/admin_etat_liste.php",
											type   		: "POST",		
											cache    	: false,
											async		: true,		
											dataType 	: "json",
											error    	: function(request, error) { // Info Debuggage si erreur         
															alert("Erreur : responseText: "+request.responseText);
															$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
															$('#toast_enregistrement_echec').toast('show');
															$('#modal_ajout_evenement').modal('toggle');
														 },
											success  	: function(reponse_select_etat) 
															{
																$('#modal_modification_evenement_etat_initial').empty();
																$('#modal_modification_evenement_etat_final').empty();
																var ligne_select_etat_initial='';
																var ligne_select_etat_final='';
																$.each(reponse_select_etat,function(i,item){
																	if(item.resultat ==  tab_msg['code_ok']['id'])
																	{
																		
																		if((item.id)==(reponse.id_etat_precedent))
																		{console.log(reponse.id);
																			ligne_select_etat_initial +='<option selected value='+item.id+'>'+item.nom+'</option>';
																		}
																		else
																		{
																			ligne_select_etat_initial +='<option value='+item.id+'>'+item.nom+'</option>';
																		}
																		if((item.id)==(reponse.id_etat_suivant))
																		{
																		ligne_select_etat_final +='<option selected value='+item.id+'>'+item.nom+'</option>';
																		}
																		else
																		{
																		ligne_select_etat_final +='<option value='+item.id+'>'+item.nom+'</option>';
																		}
																	}
																	else
																	{
																	var res = reponse['resultat'];
																	$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
																	$('#toast_enregistrement_echec').toast('show');
																	$('#modal_modification_evenement').modal('toggle');
																	}
																});
																$('#modal_modification_evenement_etat_initial').append(ligne_select_etat_initial);
																$('#modal_modification_evenement_etat_final').append(ligne_select_etat_final);
															}
													});
										$.ajax({
												url      	: "code/utilisateur/admin_utilisateur_liste.php",
												type   		: "POST",		
												cache    	: false,
												async		: true,		
												dataType 	: "json",
												error    	: function(request, error) { // Info Debuggage si erreur         
																alert("Erreur : responseText: "+request.responseText);
																$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
																$('#toast_enregistrement_echec').toast('show');
																$('#modal_ajout_evenement').modal('toggle');
															 },
												success  	: function(reponse_select_utilisateur) 
																{
																$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').empty();
																$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').empty();
																var ligne_select_maintien_nom_prenom='';
																var ligne_select_prevenance_nom_prenom='';
																$.each(reponse_select_utilisateur,function(i,item_utilisateur){
																	if(item_utilisateur.resultat ==  tab_msg['code_ok']['id'])
																	{
																		if(item_utilisateur.id !=1)
																		{
																			if((item_utilisateur.id)==(reponse.id_utilisateur_fin_maintien))
																			{
																			ligne_select_maintien_nom_prenom +='<option selected value='+item_utilisateur.id+'>'+item_utilisateur.nom+' '+item_utilisateur.prenom+'</option>';
																			}
																			else
																			{
																			ligne_select_maintien_nom_prenom +='<option value='+item_utilisateur.id+'>'+item_utilisateur.nom+' '+item_utilisateur.prenom+'</option>';
																			}
																			if((item_utilisateur.id)==(reponse.id_utilisateur_fin_prevenance))
																			{
																			ligne_select_prevenance_nom_prenom +='<option selected value='+item_utilisateur.id+'>'+item_utilisateur.nom+' '+item_utilisateur.prenom+'</option>';
																			}
																			else
																			{
																			ligne_select_prevenance_nom_prenom +='<option value='+item_utilisateur.id+'>'+item_utilisateur.nom+' '+item_utilisateur.prenom+'</option>';
																			}
																		}
																	}
																	else
																	{
																	}
																});
																$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').append(ligne_select_maintien_nom_prenom);
																$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').append(ligne_select_prevenance_nom_prenom);
																}
												});
											if(reponse.presence_utilisateur_fin_maintien!=0) //il existe un utilisateur à prevenir
											{
												$('#modal_modification_evenement_temps_maintien_utilisateur').prop("checked",true);
												$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').show();
											}
											else
											{
												$('#modal_modification_evenement_temps_maintien_utilisateur').prop("checked",false);
												$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').hide();
											}
											if(reponse.presence_utilisateur_fin_prevenance!=0) //il existe un utilisateur à prevenir
											{
												$('#modal_modification_evenement_temps_prevenance_utilisateur').prop("checked",true);
												$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').show();
											}
											else
											{
												$('#modal_modification_evenement_temps_prevenance_utilisateur').prop("checked",false);
												$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').hide();
											}	
											//mise à jour des temps de maintien et de prévenance
											if(reponse.temps_maintien_etat!=0) //il existe un temps de maintien
											{
												$('#modal_modification_evenement_temps_maintien').prop("checked",true);
												$('#modal_modification_evenement_temps_maintien_choix').show();
												var annee_maintien=0 ;
												var mois_maintien=0 ;
												var jour_maintien=0 ;
												//formatage du temps (timestamp->année/mois/jour
												annee_maintien = Math.floor(reponse.temps_maintien_etat/31536000);
												mois_maintien= Math.floor((reponse.temps_maintien_etat - annee_maintien*31536000)/2678400);
												jour_maintien = Math.floor(((reponse.temps_maintien_etat - annee_maintien*31536000) - mois_maintien*2678400)/86400);
												console.log("année"+annee_maintien+" mois: "+mois_maintien+" jours"+jour_maintien);
												$('#modal_modification_evenement_temps_maintien_annee option:eq('+annee_maintien+')').prop('selected',true);
												$('#modal_modification_evenement_temps_maintien_mois option:eq('+mois_maintien+')').prop('selected',true);
												$('#modal_modification_evenement_temps_maintien_jours option:eq('+jour_maintien+')').prop('selected',true);
											}
											else
											{
												$('#modal_modification_evenement_temps_maintien').prop("checked",false);
												$('#modal_modification_evenement_temps_maintien_choix').hide();
											}
											//temps de prevenance
											if(reponse.temps_prevenance_changement_etat!=0) //il existe un temps de maintien
											{
												$('#modal_modification_evenement_temps_prevenance').prop("checked",true);
												$('#modal_modification_evenement_temps_prevenance_choix').show();
												var annee_prevenance=0 ;
												var mois_prevenance=0 ;
												var jour_prevenance=0 ;
												annee_prevenance = Math.floor(reponse.temps_prevenance_changement_etat/31536000);
												mois_prevenance= Math.floor((reponse.temps_prevenance_changement_etat - annee_prevenance*31536000) / 2678400);
												jour_prevenance = Math.floor(((reponse.temps_prevenance_changement_etat - annee_prevenance*31536000) - mois_prevenance*2678400)/86400);
												console.log("année"+annee_prevenance+" mois: "+mois_prevenance+" jours"+jour_prevenance);
												$('#modal_modification_evenement_temps_prevenance_annee option:eq('+annee_prevenance+')').prop('selected',true);
												$('#modal_modification_evenement_temps_prevenance_mois option:eq('+mois_prevenance+')').prop('selected',true);
												$('#modal_modification_evenement_temps_prevenance_jours option:eq('+jour_prevenance+')').prop('selected',true);
											}
											else
											{
												$('#modal_modification_evenement_temps_prevenance').prop("checked",false);
												$('#modal_modification_evenement_temps_prevenance_choix').hide();
											}
										}
										else
										{
										var res = reponse['resultat'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
										$('#modal_modification_evenement').modal('toggle');
										}
									}
							});
			$('#modal_modification_evenement_nom').focus();
		}); //fin du On click	
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_modification_evenement_nom" ).focus(function() {
					$('#modal_modification_evenement_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_evenement_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_evenement_nom_aide').show();
					$('#modal_modification_evenement_button').attr('disabled',false);
					$('#modal_modification_evenement_button').removeClass('disabled');
		});
		//
		//
		//
		//test de la validité des données
		$( "#modal_modification_evenement_nom" ).blur(function() {
			//récupération de l'id de la ligne concernée
			var id_evenement = $('#modal_modification_evenement_id').attr("value");
			//recherche d'un élément déjà présent
			var evenement_modifie = $('#modal_modification_evenement_nom').val();
			var longueur_evenement_modifie = evenement_modifie.length;
			if($('#modal_modification_evenement_nom').val()!="")
				{
				if(pattern_evenement_nom.test(evenement_modifie)&&(longueur_evenement_modifie<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/evenement/admin_evenement_modification_verifications.php",
					type   		: "POST",
					data     	: {evenement: evenement_modifie, id:id_evenement},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_evenement').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas présent ->ok
								//			- le nom est présent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, échec
									//////console.log("retour"+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										$('#modal_modification_evenement_nom').addClass('is-valid');
										$('#modal_modification_evenement_nom').removeClass('is-invalid');
										$('#modal_modification_evenement_nom_label').addClass('text-success');
										$('#modal_modification_evenement_erreur').text("l'état est valide");
										$('#modal_modification_evenement_button').attr('disabled',false);
										$('#modal_modification_evenement_button').removeClass('disabled');
										$('#modal_modification_evenement_nom_aide').hide();
									}
									else
									{
										$('#modal_modification_evenement_nom').addClass('is-invalid');
										$('#modal_modification_evenement_nom').removeClass('is-valid');
										$('#modal_modification_evenement_nom_label').addClass('text-danger');
										$('#modal_modification_evenement_nom_aide').hide();
										$('#modal_modification_evenement_erreur').text("échec");					
										$('#modal_modification_evenement_button').attr('disabled',true);
										$('#modal_modification_evenement_button').addClass('disabled');
									}
								}
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_evenement_nom').addClass('is-invalid');
				$('#modal_modification_evenement_nom_label').addClass('text-danger');
				$('#modal_modification_evenement_nom_aide').hide();
				$('#modal_modification_evenement_erreur').text("le nom doit comporter entre 4 et 20 caractères");					
				$('#modal_modification_evenement_button').attr('disabled',true);
				$('#modal_modification_evenement_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_evenement_nom').addClass('is-invalid');
		$('#modal_modification_evenement_nom_label').addClass('text-danger');
		$('#modal_modification_evenement_nom_aide').hide();
		$('#modal_modification_evenement_erreur').text("le  champs est vide");					
		$('#modal_modification_evenement_button').attr('disabled',true);
		$('#modal_modification_evenement_button').addClass('disabled');
		}
	});
	$('#modal_modification_evenement_divers').blur(function() {
		var divers_modifie = $('#modal_modification_evenement_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_evenement_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_evenement_divers').removeClass('is-invalid');
			$('#modal_modification_evenement_divers').addClass('is-valid');
			$('#modal_modification_evenement_divers_label').removeClass('text-danger');
			$('#modal_modification_evenement_divers_label').addClass('text-success');
			$('#modal_modification_evenement_divers_erreur').text("le champs est valide!");
			$('#modal_modification_evenement_button').attr('disabled',false);
			$('#modal_modification_evenement_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_evenement_divers').removeClass('is-valid');
			$('#modal_modification_evenement_divers').addClass('is-invalid');
			$('#modal_modification_evenement_divers_label').addClass('text-danger');
			$('#modal_modification_evenement_divers_label').removeClass('text-success');
			$('#modal_modification_evenement_nom_aide').hide();
			$('#modal_modification_evenement_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_evenement_button').attr('disabled',true);
			$('#modal_modification_evenement_button').addClass('disabled');
		}
	});
//	
//	
//	
//	
/***********************************************************************/
// gestion des temps de maintien et des temps de prevenance
/***********************************************************************/
$('#modal_modification_evenement_temps_maintien').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_modification_evenement_temps_maintien_choix').is(":visible"))
	{
	$('#modal_modification_evenement_temps_maintien_choix').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_modification_evenement_temps_maintien_choix').show();
	}
});
$('#modal_modification_evenement_temps_prevenance').on("change",function(e,data){
	////console.log(e.target.checked);
	if($('#modal_modification_evenement_temps_prevenance_choix').is(":visible"))
	{
		$('#modal_modification_evenement_temps_prevenance_choix').hide();
	}
	else
	{
			// on montre le choix des valeurs
		$('#modal_modification_evenement_temps_prevenance_choix').show();
	}
});
//	
//
/***********************************************************************/
// gestion des utilisateurs
/***********************************************************************/
$('#modal_modification_evenement_temps_maintien_utilisateur').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').is(":visible"))
	{
	$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').show();
	}
});
$('#modal_modification_evenement_temps_prevenance_utilisateur').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').is(":visible"))
	{
	$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').show();
	}
});	
//	
	//mise à jour des données depuis la fenêtre modal
//	
	$('#modal_modification_evenement_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var id_evenement = $('#modal_modification_evenement_id').attr("value");
		var evenement_modifie = $('#modal_modification_evenement_nom').val();
		var divers_modifie = $('#modal_modification_evenement_divers').val();
		//etat initial / etat final
		var etat_init = $('#modal_modification_evenement_etat_initial').val();
		var etat_fin = $('#modal_modification_evenement_etat_final').val();
		//récupération des informations de maintien
		var pres_temps_maintien =0;
		var duree_timestamp_maintien = 0;
		var pres_user_fin_maintien = 0;
		var user_fin_maintien=0;		
		var pres_temps_prevenance =0;
		var duree_timestamp_prevenance = 0;
		var pres_user_fin_prevenance = 0;
		var user_fin_prevenance=0;	
		if($('#modal_modification_evenement_temps_maintien').prop('checked'))
		{
			pres_temps_maintien = 1;
			//conversion des durées en entiers
			duree_timestamp_maintien = 	($('#modal_modification_evenement_temps_maintien_annee').val()*31536000+
									($('#modal_modification_evenement_temps_maintien_mois').val())*2678400+
									$('#modal_modification_evenement_temps_maintien_jours').val()*86400);
		}
		else
		{
			pres_temps_maintien = 0;
			
		}
		if($('#modal_modification_evenement_temps_prevenance').prop('checked'))
		{
			pres_temps_prevenance = 1;
			duree_timestamp_prevenance = 	$('#modal_modification_evenement_temps_prevenance_annee').val()*31536000+
											$('#modal_modification_evenement_temps_prevenance_mois').val()*2678400+
											$('#modal_modification_evenement_temps_prevenance_jours').val()*86400;
		}
		else
		{
			pres_temps_prevenance = 0;
		}


		
		
		//présence utilisateurs à prévenir
		//maintien
		if($('#modal_modification_evenement_temps_maintien_utilisateur').prop('checked'))
		{
			pres_user_fin_maintien = 1;
			user_fin_maintien = $('#modal_modification_evenenement_temps_maintien_utilisateur_nom_prenom').val();			
		}
		else
		{
			pres_user_fin_maintien = 0;
		}
		if($('#modal_modification_evenement_temps_prevenance_utilisateur').prop('checked'))
		{
			pres_user_fin_prevenance =1;
			user_fin_prevenance = $('#modal_modification_evenenement_temps_prevenance_utilisateur_nom_prenom').val();			
		}
		else
		{
			pres_user_fin_prevenance = 0;
		}

		
		
		
		$('#modification_evenement_spinner').show();
		//
		//
		//console.log(id_evenement+"/"+evenement_modifie+" / "+divers_modifie+" / "+etat_init+" / "+etat_fin+" / "+duree_timestamp_maintien+" / "+duree_timestamp_prevenance);
		//
		console.log("duréeé: "+($('#modal_modification_evenement_temps_maintien_mois').val()));
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/evenement/admin_evenement_modifications.php",
			type   		: "POST",
			data     	: {id:id_evenement, nom: evenement_modifie, divers: divers_modifie, 
							etat_initial: etat_init, etat_final: etat_fin, 
							presence_temps_maintien: pres_temps_maintien, duree_maintien: duree_timestamp_maintien, presence_utilisateur_fin_maintien : pres_user_fin_maintien,  utilisateur_fin_maintien: user_fin_maintien,
							presence_temps_prevenance: pres_temps_prevenance,duree_prevenance: duree_timestamp_prevenance, presence_utilisteur_fin_prevenance : pres_user_fin_prevenance, utilisateur_fin_prevenance: user_fin_prevenance},
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_evenement').modal('toggle');
							$('#modification_evenement_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  //console.log(retour_json);
						$('#modification_evenement_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_evenement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_evenement').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});
	});	
//	
//	
//	
//	
//	
//	
//	
//	
	/*************************************************/
	// modal suppression d'un événement
	/************************************************/
//
//
//
	function initialisation_modal_suppresion_evenement(fonction_callback)
	{
	$('#modal_suppression_evenement_nom').val("");
	//$('#modal_suppression_evenement_divers').val("");
	$('#modal_suppression_evenement_button').attr('disabled',false);
	$('#modal_suppression_evenement_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	//sélection des boutons "supprimer" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_evenement').on("click",".suppression_evenement",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(06).html();
		//ouverture modal de modification			
		initialisation_modal_ajout_evenement(function(){
			$('#modal_suppression_evenement_id').attr("value",id_recup);
			$('#modal_suppression_evenement_nom ').text(nom_recup);	
			//$('#modal_suppression_evenement_divers').text(divers_recup);
			$('#modal_suppression_evenement').modal('show'); 
			});
		}); //fin du On click	
	$('#modal_suppression_evenement').on('shown.bs.modal', function() {		
			});	
	//mise à jour des données depuis la fenêtre modal
	$('#modal_suppression_evenement_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var nom_evenement_modifie = $('#modal_suppression_evenement_nom').text();
		//var divers_modifie = $('#modal_suppression_evenement_divers').text();
		var id_modifie = $('#modal_suppression_evenement_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/evenement/admin_evenement_suppression.php",
			type   		: "POST",
			data     	: {evenement: nom_evenement_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_evenement').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_evenement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_evenement').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	/*************************************************/
	// modal ajout des evenement
	/************************************************/
	function initialisation_modal_ajout_evenement(fonction_callback)
	{
	$('#modal_ajout_evenement_nom').val("");
	$('#modal_ajout_evenement_divers').val("");
	$('#modal_ajout_evenement_divers').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_evenement_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_evenement_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_evenement_nom_aide').show();
	$('#modal_ajout_evenement_button').attr('disabled',false);
	$('#modal_ajout_evenement_button').removeClass('disabled');
	//etats
	$('#modal_ajout_evenement_etat_initial').empty();
	$('#modal_ajout_evenement_etat_final').empty();
	//maintien
	$('#modal_ajout_evenement_temps_maintien').prop('checked',false);
	$('#modal_ajout_evenement_temps_prevenance').prop('checked',false);
	$('#modal_ajout_evenement_temps_maintien_choix').hide();
	$('#modal_ajout_evenement_temps_maintien_annee').val(0);
	$('#modal_ajout_evenement_temps_maintien_mois').val(0);
	$('#modal_ajout_evenement_temps_maintien_jours').val(0);
	$('#modal_ajout_evenement_temps_maintien_utilisateur').prop('checked',false);
	$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').hide();
	$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').val(0);
	//prevenance
	$('#modal_ajout_evenement_temps_prevenance').prop('checked',false);
	$('#modal_ajout_evenement_temps_prevenance').prop('checked',false);
	$('#modal_ajout_evenement_temps_prevenance_choix').hide();
	$('#modal_ajout_evenement_temps_prevenance_annee').val(0);
	$('#modal_ajout_evenement_temps_prevenance_mois').val(0);
	$('#modal_ajout_evenement_temps_prevenance_jours').val(0);
	$('#modal_ajout_evenement_temps_prevenance_utilisateur').prop('checked',false);
	$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').hide();
	$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').val(0);
	//prevenance
	if(fonction_callback)
		{fonction_callback();}
	}
	$('#ajout_evenement').click(function() {
		initialisation_modal_ajout_evenement(function(){
			$('#modal_ajout_evenement').modal('show');
			$('#modal_ajout_evenement_nom').focus()	;
			//création des listes d'états: état initial et état final
			valeur_ligne ='';
			$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				})
			$('#modal_ajout_evenement_etat_initial').empty();
			$('#modal_ajout_evenement_etat_final').empty();
			$.ajax({
					url      	: "code/etat/admin_etat_liste.php",
					type   		: "POST",		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
									alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_evenement').modal('toggle');
								 },
					success  	: function(reponse) 
									{
										var ligne_select='';
										$.each(reponse,function(i,item){
											if(item.resultat ==  tab_msg['code_ok']['id'])
											{
												ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
											}
											else
											{
											}
										});
										$('#modal_ajout_evenement_etat_initial').append(ligne_select);
										$('#modal_ajout_evenement_etat_final').append(ligne_select);
									}
					});
			$.ajax({
					url      	: "code/utilisateur/admin_utilisateur_liste.php",
					type   		: "POST",		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
									alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_evenement').modal('toggle');
								 },
					success  	: function(reponse) 
									{
									$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').empty();
									$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').empty();
									var ligne_select='';
									$.each(reponse,function(i,item){
										if(item.resultat ==  tab_msg['code_ok']['id'])
										{
											/*if(item.id !=1)
											{*/
											ligne_select +='<option value='+item.id+'>'+item.nom+' '+item.prenom+'</option>';
											//}
										}
										else
										{
										}
									});
									$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').append(ligne_select);
									$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').append(ligne_select);
									}
					});
				});
			});
		});
		$('#modal_ajout_evenement').on('shown.bs.modal', function() {
			$('#modal_ajout_evenement_nom').focus();
						});
		$('#modal_ajout_evenement').on('hidden.bs.modal', function (e) {
//en réserve		
		});
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_ajout_evenement_nom" ).focus(function() {
					$('#modal_ajout_evenement_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_evenement_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_evenement_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_evenement_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_evenement_nom_aide').show();
					$('#modal_ajout_evenement_button').attr('disabled',false);
					$('#modal_ajout_evenement_button').removeClass('disabled');
			});
		//test de la validité des données
		$( "#modal_ajout_evenement_nom" ).blur(function() {
			//recherche d'un élément déjà présent
			var evenement_ajoute = $('#modal_ajout_evenement_nom').val();
			var longueur_evenement_ajoute = evenement_ajoute.length;
			if(($('#modal_ajout_evenement_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_evenement_nom.test(evenement_ajoute)&&(longueur_evenement_ajoute<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/evenement/admin_evenement_verifications.php",
					type   		: "POST",
					data     	: {evenement: evenement_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_evenement').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  
									if(retour_json['resultat'] == tab_msg['code_ok']['id']) 
									{
									//champs valide
									$('#modal_ajout_evenement_nom').removeClass('is-invalid');
									$('#modal_ajout_evenement_nom').addClass('is-valid');
									$('#modal_ajout_evenement_nom_label').addClass('text-success');
									$('#modal_ajout_evenement_nom_aide').hide();
									$('#modal_ajout_evenement_erreur').text("l'état est valide");
									$('#modal_ajout_evenement_button').attr('disabled',false);
									$('#modal_ajout_evenement_button').removeClass('disabled');
									}
									else	
									{
										//le champs est déjà utilisé
									$('#modal_ajout_evenement_nom').addClass('is-invalid');
									$('#modal_ajout_evenement_nom').removeClass('is-valid');
									$('#modal_ajout_evenement_nom_label').addClass('text-danger');
									$('#modal_ajout_evenement_nom_aide').hide();
									$('#modal_ajout_evenement_erreur').text("l'état est déjà présent");
									$('#modal_ajout_evenement_button').attr('disabled',true);
									$('#modal_ajout_evenement_button').addClass('disabled');
									}
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_evenement_nom').addClass('is-invalid');
				$('#modal_ajout_evenement_nom_label').addClass('text-danger');
				$('#modal_ajout_evenement_nom_aide').hide();
				$('#modal_ajout_evenement_erreur').text("le nom doit comporter entre 4 et 20 lettres");					
				$('#modal_ajout_evenement_button').attr('disabled',true);
				$('#modal_ajout_evenement_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_evenement_nom').addClass('is-invalid');
				$('#modal_ajout_evenement_nom_label').addClass('text-danger');
				$('#modal_ajout_evenement_nom_aide').hide();
				$('#modal_ajout_evenement_erreur').text("le  champs est vide");					
				$('#modal_ajout_evenement_button').attr('disabled',true);
				$('#modal_ajout_evenement_button').addClass('disabled');
			}					
		});
	$('#modal_ajout_evenement_divers').blur(function() {
		var divers_ajoute = $('#modal_ajout_evenement_divers').val();
		var longueur_divers_ajoute = divers_ajoute.length;
		//test du pattern
		if(pattern_evenement_divers.test(divers_ajoute)&&(longueur_divers_ajoute<longueur_max_divers))
		{
		//champs valide
			$('#modal_ajout_evenement_divers').removeClass('is-invalid');
			$('#modal_ajout_evenement_divers').addClass('is-valid');
			$('#modal_ajout_evenement_divers_label').removeClass('text-danger');
			$('#modal_ajout_evenement_divers_label').addClass('text-success');
			$('#modal_ajout_evenement_divers_erreur').text("le champs est valide!");
			$('#modal_ajout_evenement_button').attr('disabled',false);
			$('#modal_ajout_evenement_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_evenement_divers').removeClass('is-valid');
			$('#modal_ajout_evenement_divers').addClass('is-invalid');
			$('#modal_ajout_evenement_divers_label').addClass('text-danger');
			$('#modal_ajout_evenement_divers_label').removeClass('text-success');
			$('#modal_ajout_evenement_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_ajout_evenement_button').attr('disabled',true);
			$('#modal_ajout_evenement_button').addClass('disabled');
		}
	});
/***********************************************************************/
// gestion des temps de maintien et des temps de prevenance
/***********************************************************************/
$('#modal_ajout_evenement_temps_maintien').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_ajout_evenement_temps_maintien_choix').is(":visible"))
	{
	$('#modal_ajout_evenement_temps_maintien_choix').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_ajout_evenement_temps_maintien_choix').show();
	}
});
$('#modal_ajout_evenement_temps_prevenance').on("change",function(e,data){
	////console.log(e.target.checked);
	if($('#modal_ajout_evenement_temps_prevenance_choix').is(":visible"))
	{
		$('#modal_ajout_evenement_temps_prevenance_choix').hide();
	}
	else
	{
			// on montre le choix des valeurs
		$('#modal_ajout_evenement_temps_prevenance_choix').show();
	}
});
/***********************************************************************/
// gestion des utilisateurs
/***********************************************************************/
$('#modal_ajout_evenement_temps_maintien_utilisateur').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').is(":visible"))
	{
	$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').show();
	}
});
$('#modal_ajout_evenement_temps_prevenance_utilisateur').on("change",function(e,data){
	////console.log(e.value);
	if($('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').is(":visible"))
	{
	$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').hide();
	}
	else
	{
		// on montre le choix des valeurs
	$('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').show();
	}
});
	$('#modal_ajout_evenement_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var evenement_ajoute = $('#modal_ajout_evenement_nom').val();
		var divers_ajoute = $('#modal_ajout_evenement_divers').val();
		//etat initial / etat final
		var etat_init = $('#modal_ajout_evenement_etat_initial').val();
		var etat_fin = $('#modal_ajout_evenement_etat_final').val();
		//conversion des durées en entiers
		//maintien
		var pres_temps_maintien = 0;
		if($('#modal_ajout_evenement_temps_maintien').val())
		{
			pres_temps_maintien = 1;
		}
		var duree_timestamp_maintien = $('#modal_ajout_evenement_temps_maintien_annee').val()*31536000+
								$('#modal_ajout_evenement_temps_maintien_mois').val()*2678400+
								$('#modal_ajout_evenement_temps_maintien_jours').val()*86400;
		var pres_user_fin_maintien = 0;
		//utilisateur maintien
		if($('#modal_ajout_evenement_temps_maintien_utilisateur').val())
		{
			pres_user_fin_maintien = 1;
		}
		var user_fin_maintien= $('#modal_ajout_evenenement_temps_maintien_utilisateur_nom_prenom').val();
		//prevenance
		var pres_temps_prevenance = 0;
		if($('#modal_ajout_evenement_temps_prevenance').val())
		{
			pres_temps_prevenance = 1;
		}
		var duree_timestamp_prevenance = $('#modal_ajout_evenement_temps_prevenance_annee').val()*31536000+
						$('#modal_ajout_evenement_temps_prevenance_mois').val()*2678400+
						$('#modal_ajout_evenement_temps_prevenance_jours').val()*86400;		
		var pres_user_fin_prevenance = 0;
		if($('#modal_ajout_evenement_temps_prevenance_utilisateur').val())
		{
			pres_user_fin_prevenance = 1;
		}
		var user_fin_prevenance= $('#modal_ajout_evenenement_temps_prevenance_utilisateur_nom_prenom').val();
		console.log(pres_temps_maintien+'/'+duree_timestamp_maintien+'/'+pres_user_fin_maintien+'/'+user_fin_maintien);
		console.log(pres_temps_prevenance+'/'+duree_timestamp_prevenance+'/'+pres_user_fin_prevenance+'/'+user_fin_prevenance);
		$('#ajout_evenement_spinner').show();
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/evenement/admin_evenement_ajout.php",
			type   		: "POST",
			data     	: {nom: evenement_ajoute, etat_initial: etat_init, etat_final: etat_fin,
							presence_temps_maintien: pres_temps_maintien, duree_maintien: duree_timestamp_maintien, presence_utilisateur_fin_maintien : pres_user_fin_maintien,  utilisateur_fin_maintien: user_fin_maintien, 
							presence_temps_prevenance: pres_temps_prevenance,duree_prevenance: duree_timestamp_prevenance, presence_utilisteur_fin_prevenance : pres_user_fin_prevenance, utilisateur_fin_prevenance: user_fin_prevenance,	
							divers: divers_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_evenement').modal('toggle');
							$('#ajout_evenement_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  //console.log(retour_json);
						$('#ajout_evenement_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_evenement').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_evenement').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});	
