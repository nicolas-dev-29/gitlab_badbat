$(function(){
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // newly activated tab
  e.relatedTarget // previous active tab
  //console.log(e.currentTarget.id);
})
$(document).ready(function(){
$.getScript("./js/badbat/gestion_fiche_creation_modification_nav_1.js");
var tab_msg={};

/*************************************************/
// boutons généraux - validation, raz et annulation
/************************************************/
/************************************************/
// bouton: validation
$('#validation_fiche').on('click',function(){	
	
	var json_tab = JSON.stringify(tab_provisoire);
	console.log('tab_ avant: '+json_tab);
	alert("az");
	$('#validation_fiche').addClass('disabled');
	$('#validation_fiche').attr('disabled',true);
	$('#creation_fiche_raz').attr('disabled',true);
	$('#creation_fiche_raz').addClass('disabled');
	$('#creation_fiche_quitter').attr('disabled',true);
	$('#creation_fiche_quitter').addClass('disabled');
	console.log ("entrée des données:"+json_tab)
	//insertion des éléments dans la base à partir des index
	$.ajax({
				url			:"code/creation_fiche_validation.php",
				type		:"POST",
				data		:{tab: json_tab},
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(retour_json){
							console.log(retour_json.resultat);
							$('#validation_fiche_spinner').hide();
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							}
						}	
	});
});
//redirection après dispartion du message de validation
$('#toast_enregistrement_ok').on('hidden.bs.toast', function(){
$(location).attr('href','./index.php?page=gestion_fiche.php');
$('#validation_fiche').blur();

});
/************************************************/
// bouton: raz de toutes les données
/************************************************/
	$('#creation_fiche_raz').on('click',function(){
		$.each(tab_provisoire,function(index,d){
			tab_provisoire[index]="";
		});	
		mise_a_jour_reference();
		//raz de tous les sélecteurs
		$('.creation_fiche_select').val("1");
		$(this).blur();
	});
/************************************************/
// bouton: quitter
/************************************************/
	$('#creation_fiche_quitter').on('click',function(){
		$.each(tab_provisoire,function(index,d){
			tab_provisoire[index]="";
		});	
		
		
		mise_a_jour_reference();
		//raz de tous les sélecteurs
		$('.creation_fiche_select').val("-1");
		$(this).blur();
		$(location).attr('href','./index.php');
	});
/************************************************/
// bouton: interdiction pendant le chargement
/************************************************/
	$('#toast_enregistrement_ok').on('hidden.bs.toast',function(){
		//$(location).attr('href','./index.php?page=gestion_fiche.php');
		$('#validation_fiche').attr('disabled',false);
		$('#validation_fiche').removeClass('disabled');
		$('#creation_fiche_raz').attr('disabled',false);
		$('#creation_fiche_raz').removeClass('disabled');
		$('#creation_fiche_quitter').attr('disabled',false);
		$('#creation_fiche_quitter').removeClass('disabled');
	});
	$('#toast_enregistrement_echec').on('hidden.bs.toast',function(){
		$('#validation_fiche').attr('disabled',false);
		$('#validation_fiche').removeClass('disabled');
		$('#creation_fiche_raz').attr('disabled',false);
		$('#creation_fiche_raz').removeClass('disabled');
		$('#creation_fiche_quitter').attr('disabled',false);
		$('#creation_fiche_quitter').removeClass('disabled');
	});
/*************************************************/
// tab 1 - paramètres généraux
/************************************************/
/************************************************/
// champs: lieu
//chargement de la liste des lieux disponibles
	//$(document).ready(function(){
			//masque des toasts
			$('#toast_enregistrement_echec').addClass("hide");
			$('#toast_enregistrement_ok').addClass("hide");
			//masque des spinners
			$('#validation_fiche_spinner').hide();
			$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
		//récupération de la liste des tensions
		$.ajax({
					url			:"code/tension_reseau/admin_tension_reseau_liste_2.php", 
					type		:"POST",
					cache		:false,
					dataType	:"json",
					error		: function(request,error){
								//alert("Erreur : responseText: "+request.responseText);
								},
					success		: function(reponse)
								{
									console.log(reponse);
									var ligne_select='';
									$.each(reponse,function(i,item){
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id == "1")
										{
										ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
										}
										else
										{
										ligne_select +='<option  value='+item.id+'>'+item.valeur+' V</option>';
										}
									}
									else
									{
										var res = item.resultat;
										var message = tab_msg[res]['texte'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
									}
									});
									$('#creation_fiche_tension_liste_select').append(ligne_select);
								}
			});
			//récupération de la liste des équipements
		$.ajax({
					url			:"code/equipement/admin_equipement_liste.php", 
					type		:"POST",
					cache		:false,
					dataType	:"json",
					error		: function(request,error){
								//alert("Erreur : responseText: "+request.responseText);
								},
					success		: function(reponse)
								{
									var ligne_select='';
									$.each(reponse,function(i,item){
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.nom)
										{
										ligne_select +='<option value='+item.id+'>'+item.nom+' </option>';
										}
										else
										{
										ligne_select +='<option value='+item.id+'> entrer la valeur </option>';
										}
									}
									else
									{
										var res = item.resultat;
										var message = tab_msg[res]['texte'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
									}
									});
									$('#creation_fiche_equipement_liste_select').append(ligne_select);
								}
			});			
		//récupération de la liste des lieux
		$.ajax({
					url			:"code/admin_lieux_liste.php", 
					type		:"POST",
					cache		:false,
					dataType	:"json",
					error		: function(request,error){
								//alert("Erreur : responseText: "+request.responseText);
								},
					success		: function(reponse)
								{
									console.log(reponse)
									var ligne_select='';
									$.each(reponse,function(i,item){
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id == 1)
										{
										ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
										}
										else
										{
										ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
										}
										
										
									}
									else
									{
										var res = item.resultat;
										var message = tab_msg[res]['texte'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
									}
									});
									$('#creation_fiche_lieux_liste_select').append(ligne_select);
								}
			});
				//récupération de la liste des états
		$.ajax({
					url			:"code/etat/admin_etat_liste.php", 
					type		:"POST",
					cache		:false,
					dataType	:"json",
					error		: function(request,error){
								//alert("Erreur : responseText: "+request.responseText);
								},
					success		: function(reponse)
								{
									var ligne_select='';
									$.each(reponse,function(i,item){
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id == 1)
										{
										ligne_select +='<option selected="selected" value='+item.id+'>'+item.nom+'</option>';
										}
										else
										{
										ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
										}
									}
									else
									{
										var res = item.resultat;
										var message = tab_msg[res]['texte'];
										$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
										$('#toast_enregistrement_echec').toast('show');
									}
									});
									$('#creation_fiche_etat_liste_select').append(ligne_select);
								}
			});
		});
	});
});