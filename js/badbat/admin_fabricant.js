

$(document).ready(function(){ 	
	
	function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
	}	

var pattern_fabricant_nom = /.{1,}/i; 	//tous les caractères nbre:3->20 sans saut ligne	
var pattern_fabricant_adresse = /.{1,}/i; //tous les caractères nbre:1 sans saut ligne
var pattern_fabricant_mail = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i; //adresse mail valide
var pattern_telephone = /.{1,}/i;
var pattern_fabricant_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_adresse = 20;
var longueur_max_divers = 255;
var tab_msg={};

/********************************************************/
// fonctions admin fabricant
/********************************************************/
/**variagles globales    
	                             **/
function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des fabricant
			$.ajax({
				url      	: "code/fabricant/admin_fabricant_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								console.log(reponse);
								$('#nombre_fabricants').html("<b>"+reponse.length+"</b>");
								$('#table_fabricant tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_fabricant" name="modification_fabricant" 	data-toogle="tooltip" data-placement="top" title="modification du fabricant"	value="modification_fabricant">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_fabricant" name="suppression_fabricant" title="suppression du fabricant" data-toggle="tooltip" data-placement="top"   value="suppression_fabricant"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								//console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défut
										{
										ligne_table +='<tr class="admin_fabricant" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.adresse+'</td><td>'+ item.mail+'</td><td>'+item.telephone+'</td><td>'+item.divers+'</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_fabricant').append(ligne_table);
							}						
					});	
			});
}

				//mise à jour de la banniere échéances
		$.getScript("./js/badbat/echeances_all_pages.js",function(){
		maj_echeance();
		});
		$('[data-toggle="tooltip"]').tooltip();
		
		
		
		
		//alert("mise à jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		$('#suppression_fabricant_tous_spinner').hide();
		$('#ajout_fabricant_spinner').hide();
		mise_a_jour_liste();  
		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
	});
	/*************************************************/
	// modal suppression de tous les fabricant en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_fabricant_tous').click(function(event){
		$('#modal_suppression_fabricant_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_fabricant_tous_button').click(function(event){
		$('#modal_suppression_fabricant_tous').hide();
		$('#suppression_fabricant_tous_spinner').show();
		//suppression en cours
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/fabricant/admin_fabricant_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_fabricant_tous').modal('toggle');
							$('#suppression_fabricant_tous_spinner').hide();
							mise_a_jour_liste();
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_fabricant_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_fabricant_tous').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_fabricant_tous').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
	/*************************************************/
	// modal modification des fabricants
	/************************************************/
	//sélection des boutons "modifier" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_fabricant').on("click",".modification_fabricant",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var adresse_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var mail_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();		
		var telephone_recup = $(e.currentTarget).parent().parent().find("td").eq(03).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(04).html();
		console.log("id: "+id_recup +" / nom: "+nom_recup+" / valeur divers:"+divers_recup+" / mail:"+mail_recup+" / telephone: "+telephone_recup );
		//ouverture modal de modification
		$('#modal_modification_fabricant_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_fabricant_adresse').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_fabricant_mail').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_fabricant_adresse_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_fabricant_mail_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_modification_fabricant_nom_aide').show();
		$('#modal_modification_fabricant_adresse_aide').show();
		$('#modal_modification_fabricant_button').attr('disabled',false);
		$('#modal_modification_fabricant_button').removeClass('disabled');
		$('#modal_modification_fabricant_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_fabricant_nom').val("");
		$('#modal_modification_fabricant_divers').val("");
		$('#modal_modification_fabricant_nom').focus();
		$('#modal_modification_fabricant').modal('show');
		$('#modal_modification_fabricant').on('shown.bs.modal', function() {
			$('#modal_modification_fabricant_nom').focus();
			$('#modal_modification_fabricant_nom').val(nom_recup);	
			$('#modal_modification_fabricant_adresse').val(adresse_recup);	
			$('#modal_modification_fabricant_mail').val(mail_recup);	
			$('#modal_modification_fabricant_telephone').val(telephone_recup);
			$('#modal_modification_fabricant_divers').val(divers_recup);
			$('#modal_modification_fabricant_id').attr("value",id_recup);
			});	
		}); //fin du On click	
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_modification_fabricant_nom" ).focus(function() {
					$('#modal_modification_fabricant_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_fabricant_nom_aide').show();
					$('#modal_modification_fabricant_button').attr('disabled',false);
					$('#modal_modification_fabricant_button').removeClass('disabled');
		});
		//test de la validité des données
		$( ".modal_modification_fabricant_nom_adresse" ).blur(function() {
			//récupération de l'id de la ligne concernée
			var id_fabricant = $('#modal_modification_fabricant_id').attr("value");
			//recherche d'un élément déjà présent
			var nom_fabricant_modifie = 		$('#modal_modification_fabricant_nom').val();
			var adresse_fabricant_modifie = 	$('#modal_modification_fabricant_adresse').val();
			//console.log(nom_fabricant_modifie+"/"+adresse_fabricant_modifie+"/"+id_fabricant);
			var longueur_nom_fabricant_modifie = nom_fabricant_modifie.length;
			var longueur_adresse_fabricant_modifie = adresse_fabricant_modifie.length;
			if(($('#modal_modification_fabricant_nom').val()!="")&&($('#modal_modification_fabricant_adresse').val()!=""))
				{
				if(pattern_fabricant_nom.test(nom_fabricant_modifie)&&(longueur_nom_fabricant_modifie<longueur_max_nom)
					&&pattern_fabricant_adresse.test(nom_fabricant_modifie)&&(longueur_adresse_fabricant_modifie<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				console.log(id_fabricant +'/'+nom_fabricant_modifie+' / '+adresse_fabricant_modifie +' /');
				$.ajax({
					 url      	: "code/fabricant/admin_fabricant_modification_verifications.php",
					type   		: "POST",
					data     	: { id_user:id_fabricant, nom_fabricant: nom_fabricant_modifie,adresse_fabricant: adresse_fabricant_modifie},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_fabricant').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas présent ->ok
								//			- le nom est présent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, échec
									console.log("retour:  "+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										$('#modal_modification_fabricant_nom').addClass('is-valid');
										$('#modal_modification_fabricant_nom').removeClass('is-invalid');
										$('#modal_modification_fabricant_nom_label').addClass('text-success').removeClass('text-danger');
										$('#modal_modification_fabricant_nom_aide').hide();
										$('#modal_modification_fabricant_adresse').addClass('is-valid');
										$('#modal_modification_fabricant_adresse').removeClass('is-invalid');
										$('#modal_modification_fabricant_adresse_label').addClass('text-success').removeClass('text-danger');
										//$('#modal_modification_fabricant_erreur').text("les données sont valides");
										$('#modal_modification_fabricant_adresse_aide').hide();
										$('#modal_modification_fabricant_button').attr('disabled',false);
										$('#modal_modification_fabricant_button').removeClass('disabled');
									}
									else
									{
										$('#modal_modification_fabricant_nom').addClass('is-invalid');
										$('#modal_modification_fabricant_nom').removeClass('is-valid');
										$('#modal_modification_fabricant_nom_label').addClass('text-danger');
										$('#modal_modification_fabricant_nom_aide').hide();
										$('#modal_modification_nom_fabricant_erreur').text("l'fabricant existe déjà");					
										$('#modal_modification_fabricant_button').attr('disabled',true);
										$('#modal_modification_fabricant_button').addClass('disabled');										
										$('#modal_modification_fabricant_adresse').addClass('is-invalid');
										$('#modal_modification_fabricant_adresse').removeClass('is-valid');
										$('#modal_modification_fabricant_adresse_label').addClass('text-danger');
										$('#modal_modification_fabricant_adresse_aide').hide();
										$('#modal_modification_adresse_fabricant_erreur').text("l'fabricant existe déjà");					
										$('#modal_modification_fabricant_button').attr('disabled',true);
										$('#modal_modification_fabricant_button').addClass('disabled');
									}
								}
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_fabricant_nom').addClass('is-invalid');
				$('#modal_modification_fabricant_nom_label').addClass('text-danger');
				$('#modal_modification_fabricant_nom_aide').hide();
				$('#modal_modification_fabricant_erreur').text("le nom doit comporter entre 4 et 20 caractères");					
				$('#modal_modification_fabricant_button').attr('disabled',true);
				$('#modal_modification_fabricant_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_fabricant_nom').addClass('is-invalid');
		$('#modal_modification_fabricant_nom_label').addClass('text-danger');
		$('#modal_modification_fabricant_nom_aide').hide();
		$('#modal_modification_fabricant_erreur').text("le  champs est vide");					
		$('#modal_modification_fabricant_button').attr('disabled',true);
		$('#modal_modification_fabricant_button').addClass('disabled');
		}
	});
	$('#modal_modification_fabricant_divers').blur(function() {
		var divers_modifie = $('#modal_modification_fabricant_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_fabricant_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_fabricant_divers').removeClass('is-invalid');
			$('#modal_modification_fabricant_divers').addClass('is-valid');
			$('#modal_modification_fabricant_divers_label').removeClass('text-danger');
			$('#modal_modification_fabricant_divers_label').addClass('text-success');
			$('#modal_modification_fabricant_divers_erreur').text("le champs est valide!");
			$('#modal_modification_fabricant_button').attr('disabled',false);
			$('#modal_modification_fabricant_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_fabricant_divers').removeClass('is-valid');
			$('#modal_modification_fabricant_divers').addClass('is-invalid');
			$('#modal_modification_fabricant_divers_label').addClass('text-danger');
			$('#modal_modification_fabricant_divers_label').removeClass('text-success');
			$('#modal_modification_fabricant_nom_aide').hide();
			$('#modal_modification_fabricant_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_fabricant_button').attr('disabled',true);
			$('#modal_modification_fabricant_button').addClass('disabled');
		}
	});
	//mise à jour des données depuis la fenêtre modal
	$('#modal_modification_fabricant_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var nom_fabricant_modifie = $('#modal_modification_fabricant_nom').val();
		var adresse_fabricant_modifie = $('#modal_modification_fabricant_adresse').val();
		var mail_fabricant_modifie = $('#modal_modification_fabricant_mail').val();
		var telephone_fabricant_modifie = $('#modal_modification_fabricant_telephone').val();
		var divers_modifie = $('#modal_modification_fabricant_divers').val();
		var id_modifie = $('#modal_modification_fabricant_id').attr("value");
		console.log('mail'+mail_fabricant_modifie+'elephone')	
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/fabricant/admin_fabricant_modification.php",
			type   		: "POST",
			data     	: { mail_fabricant: mail_fabricant_modifie, 
							adresse_fabricant: adresse_fabricant_modifie,
							nom_fabricant: nom_fabricant_modifie,
							telephone_fabricant:telephone_fabricant_modifie ,
							divers:divers_modifie, 
							id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_fabricant').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						console.log(retour_json);
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un fabricant
	/************************************************/
	function initialisation_modal_suppresion_fabricant(fonction_callback)
	{
	$('#modal_suppression_fabricant_nom').val("");
	$('#modal_suppression_fabricant_adresse').val("");
	$('#modal_suppression_fabricant_mail').val("");
	$('#modal_suppression_fabricant_divers').val("");
	$('#modal_suppression_fabricant_button').attr('disabled',false);
	$('#modal_suppression_fabricant_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	//sélection des boutons "supprimer" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_fabricant').on("click",".suppression_fabricant",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var adresse_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		var mail_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();
		var telephone_recup = $(e.currentTarget).parent().parent().find("td").eq(03).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(04).html();
		console.log(id_recup+nom_recup+divers_recup+telephone_recup);
		$('#modal_suppression_fabricant').modal('show');
		//ouverture modal de modification			
		initialisation_modal_ajout_fabricant(function(){
			$('#modal_suppression_fabricant_id').attr("value",id_recup);
			$('#modal_suppression_fabricant_nom ').text(nom_recup);	
			$('#modal_suppression_fabricant_adresse ').text(adresse_recup);	
			$('#modal_suppression_fabricant_mail ').text(mail_recup);
			$('#modal_suppression_fabricant_telephone ').text(telephone_recup);	
			$('#modal_suppression_fabricant_divers').text(divers_recup);
			 
			});
			
		}); //fin du On click	
		
		
	$('#modal_suppression_fabricant').on('shown.bs.modal', function() {		
			});			
			
	//mise à jour des données depuis la fenêtre modal
	$('#modal_suppression_fabricant_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var fabricant_modifie = $('#modal_suppression_fabricant_nom').text();
		var divers_modifie = $('#modal_suppression_fabricant_divers').text();
		var id_modifie = $('#modal_suppression_fabricant_id').attr("value");
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/fabricant/admin_fabricant_suppression.php",
			type   		: "POST",
			data     	: {fabricant: fabricant_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_fabricant').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	/*************************************************/
	// modal ajout d'un fabricant
	/************************************************/
	function initialisation_modal_ajout_fabricant(fonction_callback)
	{
	//$('#modal_ajout_fabricant_nom').val("");
	//$('#modal_ajout_fabricant_adresse').val("");
	//$('#modal_ajout_fabricant_mail').val("");
	$('#modal_ajout_fabricant_divers').val("");
	$('#modal_ajout_fabricant_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_fabricant_adresse').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_fabricant_mail').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_fabricant_telephone').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_fabricant_adresse_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_fabricant_mail_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_fabricant_telephone_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_fabricant_nom_aide').show();
	$('#modal_ajout_fabricant_adresse_aide').show();
	$('#modal_ajout_fabricant_button').attr('disabled',false);
	$('#modal_ajout_fabricant_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	$('#ajout_fabricant').click(function() {
		initialisation_modal_ajout_fabricant(function(){
			$('#modal_ajout_fabricant').modal('show');
			});
		});
		$('#modal_ajout_fabricant').on('shown.bs.modal', function() {
			$('#modal_ajout_fabricant_nom').focus();
			//$('#modal_ajout_fabricant_nom').prop('required',true);
						});
		$('#modal_ajout_fabricant').on('hide.bs.modal', function (e) {
				$('#modal_ajout_fabricant_nom').prop('required',false);
		});
		$('#modal_ajout_fabricant').on('hidden.bs.modal', function (e) {
			//remise à zéro de l'ensemble des champs
			$('#modal_ajout_fabricant_nom').prop('required',false);
			$(this).find("input").val("");
			$('#modal_ajout_fabricant_nom').removeClass('is-invalid').removeClass('is-valid');
					$('#modal_ajout_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_fabricant_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_fabricant_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_fabricant_nom_aide').show();
					$('#modal_ajout_fabricant_button').attr('disabled',false);
					$('#modal_ajout_fabricant_button').removeClass('disabled');
		});
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_ajout_fabricant_nom" ).focus(function() {
					$('#modal_ajout_fabricant_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_fabricant_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_fabricant_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_fabricant_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_fabricant_nom_aide').show();
					$('#modal_ajout_fabricant_button').attr('disabled',false);
					$('#modal_ajout_fabricant_button').removeClass('disabled');
			});
			$( "#modal_ajout_fabricant_adresse" ).focus(function() {
				$('#modal_ajout_fabricant_adresse').removeClass('is-valid').removeClass('is-invalid');
				$('#modal_ajout_fabricant_adresse_label').removeClass('text-success').removeClass('text-danger');
				$('#modal_ajout_fabricant_adresse_aide').show();
		});	
		//test de la validité des données sur le nom et l'adresse
		$( ".modal_ajout_fabricant_nom_adresse" ).blur(function() {
			//recherche d'un élément déjà présent
			var fabricant_nom_ajoute = $('#modal_ajout_fabricant_nom').val();
			var fabricant_adresse_ajoute = $('#modal_ajout_fabricant_adresse').val();
			//
			var longueur_fabricant_nom_ajoute = fabricant_nom_ajoute.length;
			var longueur_fabricant_adresse_ajoute = fabricant_adresse_ajoute.length;
			//
			if(($('#modal_ajout_fabricant_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_fabricant_nom.test(fabricant_nom_ajoute)&&(longueur_fabricant_nom_ajoute<longueur_max_nom))
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_fabricant_nom').removeClass('is-invalid');
				$('#modal_ajout_fabricant_nom_label').removeClass('text-danger');
				//
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/fabricant/admin_fabricant_verifications_nom_adresse.php",
					type   		: "POST",
					data     	: {nom_fabricant: fabricant_nom_ajoute , adresse_fabricant: fabricant_adresse_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_fabricant').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  //console.log(retour_json);
									if(retour_json['resultat'] == tab_msg['code_ok']['id'])
									{
										//la requete est valide
										if(retour_json['nombre'] == 0)
										{	//il n' y a pas d'homonymes
											texte_nom = "ok";
											texte_adresse = "ok";
											$('#modal_ajout_fabricant_button').attr('disabled',false);
											$('#modal_ajout_fabricant_button').removeClass('disabled');
											$('#modal_ajout_fabricant_nom').removeClass('is-invalid');
											$('#modal_ajout_fabricant_nom').addClass('is-valid');
											$('#modal_ajout_fabricant_nom_label').addClass('text-success');
											$('#modal_ajout_fabricant_nom_label').removeClass('text-danger');
											$('#modal_ajout_fabricant_nom_aide').hide();
											$('#modal_ajout_fabricant_nom_ok').text(texte_nom);
											if(($('#modal_ajout_fabricant_adresse').val()!=""))
											{											
												$('#modal_ajout_fabricant_adresse').removeClass('is-invalid');
												$('#modal_ajout_fabricant_adresse').addClass('is-valid');
												$('#modal_ajout_fabricant_adresse_label').addClass('text-success');
												$('#modal_ajout_fabricant_adresse_label').removeClass('text-danger');
												$('#modal_ajout_fabricant_adresse_aide').hide();
												$('#modal_ajout_fabricant_adresse_ok').text(texte_adresse);
											}
										}
										else
										{	//il y a des homonymes
											$('#modal_ajout_fabricant_button').attr('disabled',true);
											$('#modal_ajout_fabricant_button').addClass('disabled');
											if(retour_json['nombre'] == 01)
											{
											//l'fabricant est déjà présent dans la base
											texte_nom = "il y a un homonyme";
											$('#modal_ajout_fabricant_nom').addClass('is-invalid');
											$('#modal_ajout_fabricant_nom').removeClass('is-valid');
											$('#modal_ajout_fabricant_nom_label').addClass('text-danger');
											$('#modal_ajout_fabricant_nom_label').removeClass('text-success');
											$('#modal_ajout_fabricant_nom_aide').hide();
											$('#modal_ajout_fabricant_nom_erreur').text("L'fabricant existe déjà");
											$('#modal_ajout_fabricant_adresse').addClass('is-invalid');
											$('#modal_ajout_fabricant_adresse').removeClass('is-valid');
											$('#modal_ajout_fabricant_adresse_label').addClass('text-danger');
											$('#modal_ajout_fabricant_adresse_label').removeClass('text-success');
											$('#modal_ajout_fabricant_adresse_aide').hide();
											$('#modal_ajout_fabricant_adresse_erreur').text("l'fabricant existe déjà");
											$('#modal_ajout_fabricant_button').attr('disabled',true);
											$('#modal_ajout_fabricant_button').addClass('disabled');
											}
											else
											{
											// des fabricants ont déjà le même nom
											texte_nom="il y a "+retour_json['nombre']+" homonymes";
											$('#modal_ajout_fabricant_nom').addClass('is-valid');
											$('#modal_ajout_fabricant_nom').removeClass('is-invalid');
											$('#modal_ajout_fabricant_nom_label').removeClass('text-danger');
											$('#modal_ajout_fabricant_nom_label').addClass('text-success');
											$('#modal_ajout_fabricant_nom_aide').hide();
											$('#modal_ajout_fabricant_nom_ok').text("ok, mais il y a "+texte_nom);
											if(($('#modal_ajout_fabricant_adresse').val()==""))
											{											
												$('#modal_ajout_fabricant_adresse').removeClass('is-invalid');
												$('#modal_ajout_fabricant_adresse').removeClass('is-valid');
												$('#modal_ajout_fabricant_adresse_label').removeClass('text-success');
												$('#modal_ajout_fabricant_adresse_label').removeClass('text-danger');
												$('#modal_ajout_fabricant_adresse_aide').show();
												//$('#modal_ajout_fabricant_adresse_ok').text(texte_adresse);
											}
											}
										}
									}
									else	
									{
									var res = retour_json['resultat'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_suppression_fabricant').modal('toggle');
									}
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_fabricant_nom').addClass('is-invalid');
				$('#modal_ajout_fabricant_nom_label').addClass('text-danger');
				$('#modal_ajout_fabricant_nom_aide').hide();
				$('#modal_ajout_fabricant_nom_erreur').text("le nom doit comporter entre 1 et 20 lettres");					
				$('#modal_ajout_fabricant_button').attr('disabled',true);
				$('#modal_ajout_fabricant_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_fabricant_nom').addClass('is-invalid');
				$('#modal_ajout_fabricant_nom_label').addClass('text-danger');
				$('#modal_ajout_fabricant_nom_aide').hide();
				$('#modal_ajout_fabricant_nom_erreur').text("le  champs est vide");					
				$('#modal_ajout_fabricant_button').attr('disabled',true);
				$('#modal_ajout_fabricant_button').addClass('disabled');
			}				
		});
	/**** - vérification de l'email - ****/
	$('#modal_ajout_fabricant_mail').focus(function(){
			$('#modal_ajout_fabricant_mail').removeClass('is-valid').removeClass('is-invalid');
	});
	$('#modal_ajout_fabricant_mail').blur(function(){
		mail_ajoute = $('#modal_ajout_fabricant_mail').val();
		if(isEmail(mail_ajoute))
		{
		$('#modal_ajout_fabricant_button').attr('disabled',false);
		$('#modal_ajout_fabricant_button').removeClass('disabled');
		$('#modal_ajout_fabricant_mail').removeClass('is-invalid');
		$('#modal_ajout_fabricant_mail').addClass('is-valid');
		$('#modal_ajout_fabricant_mail_label').addClass('text-success');
		$('#modal_ajout_fabricant_mail_label').removeClass('text-danger');
		$('#modal_ajout_fabricant_mail_aide').hide();
		$('#modal_ajout_fabricant_mail_ok').text("ok");
		}
		else
		{
		$('#modal_ajout_fabricant_button').attr('disabled',true);
		$('#modal_ajout_fabricant_button').addClass('disabled');
		$('#modal_ajout_fabricant_mail').addClass('is-invalid');
		$('#modal_ajout_fabricant_mail_label').addClass('text-danger');
		$('#modal_ajout_fabricant_mail_aide').hide();
		$('#modal_ajout_fabricant_mail_erreur').text("le mail doit comporter entre 1 et 20 lettres");					
		$('#modal_ajout_fabricant_button').attr('disabled',true);
		$('#modal_ajout_fabricant_button').addClass('disabled');
		}
	});		
	$('#modal_ajout_fabricant_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var fabricant_nom_ajoute = $('#modal_ajout_fabricant_nom').val();
		var fabricant_adresse_ajoute = $('#modal_ajout_fabricant_adresse').val();
		var fabricant_mail_ajoute = $('#modal_ajout_fabricant_mail').val();
		var fabricant_telephone_ajoute = $('#modal_ajout_fabricant_telephone').val();
		var divers_ajoute = $('#modal_ajout_fabricant_divers').val();
		$('#ajout_fabricant_spinner').show();
		console.log(fabricant_mail_ajoute);
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/fabricant/admin_fabricant_ajout.php",
			type   		: "POST",
			data     	: {	fabricant_nom: fabricant_nom_ajoute,
							fabricant_adresse: fabricant_adresse_ajoute,
							fabricant_mail: fabricant_mail_ajoute,
							divers:divers_ajoute,
							fabricant_telephone:fabricant_telephone_ajoute},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   //alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_fabricant').modal('toggle');
							$('#ajout_fabricant_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_fabricant_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_fabricant').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
