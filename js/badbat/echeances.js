var tab_msg={};
var ligne_changement = "";
var ligne_prevenance = "";

$('#modal_changement').modal('hide');
//$('#banniere_echeances_changement_button').prop('disabled', true);
$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});	
				});
	$('#maj_echeances').on('click',function(){
		$.ajax({
					url      	: "code/banniere_echeance/banniere_echeance_3.php",		//envoi du mail
					type   		: "POST",	
					cache    	: false,
					async		: true,
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								   alert("Erreur : responseText: "+request.responseText);
								 },
					success  	:function(reponse) 
								{  
								console.log(reponse);
								ligne_changement="";
								ligne_prevenance="";
								//traitement des batteries en mode prevenance
								if(reponse.nbre_batt_prev!=0)
								{
									//partie prevenance
									$('#banniere_echeances_prevenance_button').prop('enabled', true);
									$('#banniere_echeances_prevenance_texte').empty();
									$('#banniere_echeances_prevenance_badge').text((reponse.nbre_batt_prev)-(reponse.nbre_batt_chgt));
									$('#banniere_echeances_prevenance_badge').removeClass("badge-info").addClass("badge-warning");
									$('#banniere_echeances_prevenance_button').removeClass("element_cache_debut");
									$.each(reponse,function(i,item)
									{		
										if((item.temps_prevenance=="en_cours")&&(item.changement_etat!="fait"))
										{
											ligne_prevenance+="<div class = \"card my-2 \">\
											<div class=\"card-header\">\
											"+item.valeur_tension_reseau+"V - "+item.nom_equipement+" - "+item.nom_lieux+" \
											</div>\
											<div class=\"card-body\">\
											date de la dernière opération:<br>"+item.date_derniere_operation_string+" <br>\
											temps avant changemet:<br>"+item.temps_prevenance_restant_string+"<br>\
											</div>\
											</div>";
										}
									});
								}
								else
								{
									$('#banniere_echeances_prevenance_badge').text("0");
									$('#banniere_echeances_prevenance_badge').addClass("badge-info").removeClass("badge-danger");
									$('#banniere_echeances_prevenance_button').addClass("element_cache_debut");
									$('#banniere_echeances_prevenance_button').prop('disabled', true);
								}
								if(reponse.nbre_batt_chgt!=0)
								{
									//partie changement
									$('#banniere_echeances_changement_button').prop('enabled', true);
									$('#banniere_echeances_changement_texte').empty();
									$.each(reponse,function(i,item)
									{
										if(item.changement_etat=="fait")
										{
											//collecte des informations pour l'affichage et l'envoi du mail
											//affichage
											ligne_changement+="<div class = \"card my-2 \">\
											<div class=\"card-header\">\
											"+item.valeur_tension_reseau+"V - "+item.nom_equipement+" - "+item.nom_lieux+" \
											</div>\
											<div class=\"card-body\">\
											"+item.nom_etat_prec+" <span class=\"fa fa-arrow-right fa-1x\" ></span> "+item.nom_etat_suiv+"<br>\
											</div>\
											</div>";
											$('#banniere_echeances_changement_badge').text(reponse.nbre_batt_chgt);
											$('#banniere_echeances_changement_badge').removeClass("badge-info").addClass("badge-danger");
											$('#banniere_echeances_changement_button').removeClass("element_cache_debut");
										}
									});
								}
								else
								{
									$('#banniere_echeances_changement_badge').text("0");
									$('#banniere_echeances_changement_badge').addClass("badge-info").removeClass("badge-danger");
									$('#banniere_echeances_changement_button').addClass("element_cache_debut");
									$('#banniere_echeances_changement_button').prop('disabled', true);
								}
							},
				});
});				
//appui du bouton
$('#banniere_echeances_changement_button').on('click',function(){
	$('#modal_echeances_changement_texte').html(ligne_changement);
	$('#modal_echeances_changement').modal('show');
});
$('#banniere_echeances_prevenance_button').on('click',function(){
	$('#modal_echeances_prevenance_texte').html(ligne_prevenance);
	$('#modal_echeances_prevenance').modal('show');
});
		/*$.ajax({
					url      	: "code/mail/generation_mail.php",		//envoi du mail
					type   		: "POST",	
					cache    	: false,
					async		: true,
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								   alert("Erreur : responseText: "+request.responseText);
								 },
					success  	:function(reponse) 
								{  
								console.log(reponse);
								/*var ligne_changement = "";
								var ligne_prevenance = ""
									if(reponse.nbre !=0)
									{
										$('#banniere_echeances_changement_texte').empty();
										$('#banniere_echeances_prevenance_texte').empty();
											$.each(reponse,function(i,item){
											if(item.temps_prevenance == "en cours") //  nous prévenons l'utilisateur que la batterie va bientôt chnager d'état
											{
												ligne_prevenance+=" la batterie ("+item.id+") va changer bientôt d'état <br>" 
												$('#banniere_echeances_prevenance_texte').html("zerty");
												$('#banniere_echeances_prevenance').removeClass("element_cache_debut");
												}
											else	//pas d'informations sur la prévenance
											{
											}
											if(item.changement_etat == "fait") //la batterie a changé d'état
											{
											 //collecte des informations pour l'affichage et l'envoi du mail
												//affichage
													ligne_changement+="La batterie "+item.valeur_tension_reseau+"V ("+item.nom_tension_reseau+") de "+
													item.nom_equipement+" de "+item.nom_lieux+"  est passée de l'état "+item.nom_etat_prec+" à l'état "+item.nom_etat_suiv+"<br>";
													$('#banniere_echeances_changement_texte').html(ligne_changement);
													$('#banniere_echeances_changement_texte').parent().removeClass("element_cache_debut");
													$('#banniere_echeances_changement_texte').parent().parent().addClass("banniere_echeances_on").removeClass("banniere_echeances_off");
													$('#banniere_echeances_changement_badge').text(item.nbre);
													$('#banniere_echeances_changement_badge').removeClass("element_cache_debut");
													$('#banniere_echeances_changement_badge').removeClass("badge-info").addClass("badge-danger");
													$('#banniere_echeances_changement_fleches').removeClass("element_cache_debut");
													}
											else // pas d'information sur le changement d'état
											{
											}
									});
										//ligne = ligne_prevenance + ligne_changement;
										//console.log("ligne"+ligne);
									}
									else
									{
										$('#banniere_echeances_changement_texte').parent().addClass("element_cache_debut");
										$('#banniere_echeances_changement_badge ').addClass("element_cache_debut");
										$('#banniere_echeances_changement_texte').parent().parent().removeClass("banniere_echeances_on").addClass("banniere_echeances_off");
										//pas d'affichage
										$('#banniere_echeances_changement_badge').text("0");
										$('#banniere_echeances_changement_badge').addClass("element_cache_debut");
										$('#banniere_echeances_changement_badge').addClass("badge-info").removeClass("badge-danger");
									}
								//appui sur le bouton
								},
				});*/
		
