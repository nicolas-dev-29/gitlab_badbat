	$(document).ready(function(){ 
let pattern_tension_reseau_nom = /.{3,}/i; 	//tous les caractères nbre:3->20 sans saut ligne
var pattern_tension_reseau_divers = /.{0,255}/i;
var longueur_max_nom = 20;
var longueur_max_divers = 255;
	
var tab_msg={};



/********************************************************/
// fonctions admin tension_reseau
/********************************************************/
/**variagles globales       **/
                          
function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des tension_reseau
			$.ajax({
				url      	: "code/tension_reseau/admin_tension_reseau_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   //alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								$('#nombre_tension_reseau').html("<b>"+reponse.length+"</b>");
								$('#table_tension_reseau tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_tension_reseau" name="modification_tension_reseau" 	data-toogle="tooltip" data-placement="top" title="modification du tension_reseau"	value="modification_tension_reseau">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_tension_reseau" name="suppression_tension_reseau" title="suppression du tension_reseau" data-toggle="tooltip" data-placement="top"   value="suppression_tension_reseau"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								//console.log(reponse);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défut
										{
										ligne_table +='<tr class="admin_tension_reseau" id='+item.id+'><td>'+ item.nom + '</td><td>' + item.valeur + '</td><td>' + item.divers+ '</td><td>'+button_modifier+'</td><td>'+button_supprimer+'</td></tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_tension_reseau').append(ligne_table);
							}						
					});	
			});
};
	

		
				//mise à jour de la banniere échéances
		//$.getScript("./js/badbat/echeances_all_pages.js",function(){
		//maj_echeance();
		$('[data-toggle="tooltip"]').tooltip();
		
		
		
		////alert("mise à jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		
		$('#suppression_tension_reseau_tous_spinner').hide();
		$('#ajout_tension_reseau_spinner').hide();
		
		mise_a_jour_liste();  


		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		
		
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
		
	});
	
	/*************************************************/
	// modal suppression de tous les tension_reseau en une fois
	/************************************************/	
	//affichage du modal
	$('#suppression_tension_reseau_tous').click(function(event){
		$('#modal_suppression_tension_reseau_tous').modal('show');
	});
	//appui sur le bouton de suppression
	$('#modal_suppression_tension_reseau_tous_button').click(function(event){
		$('#modal_suppression_tension_reseau_tous').hide();
		$('#suppression_tension_reseau_tous_spinner').show();
		
		//suppression en cours
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/tension_reseau/admin_tension_reseau_suppression_tous.php",
			type   		: "POST",		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   ////alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_tension_reseau_tous').modal('toggle');
							$('#suppression_tension_reseau_tous_spinner').hide();
							mise_a_jour_liste();
							
						 },
			success  	: function(retour_json) 
						{  
						$('#suppression_tension_reseau_tous_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']==tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_tension_reseau_tous').modal('toggle');
							mise_a_jour_liste();
							
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_tension_reseau_tous').modal('toggle');
							
							mise_a_jour_liste();
							}
						}													
				});		
		
		
	});
	
	
	/*************************************************/
	// modal modification des tension_reseau
	/************************************************/
	
	//sélection des boutons "modifier" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_tension_reseau').on("click",".modification_tension_reseau",function(e){
		
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(02).html();
		var valeur_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		//console.log("id: "+id_recup +" / nom: "+nom_recup+"valeur: "+valeur_recup+" / valeur divers:"+divers_recup );
		
		//ouverture modal de modification
		$('#modal_modification_tension_reseau_nom').val("");
		$('#modal_modification_tension_reseau_divers').val("");
		$('#modal_modification_tension_reseau_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_tension_reseau_divers').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_modification_tension_reseau_nom').focus();
		
		$('#modal_modification_tension_reseau').modal('show');
		$('#modal_modification_tension_reseau').on('shown.bs.modal', function() {
			$('#modal_modification_tension_reseau_nom').focus();
			$('#modal_modification_tension_reseau_nom').val(nom_recup);	
			$('#modal_modification_tension_reseau_valeur').val(valeur_recup);	
			$('#modal_modification_tension_reseau_divers').val(divers_recup);
			$('#modal_modification_tension_reseau_id').attr("value",id_recup);
			
			});	
		}); //fin du On click	
			
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_modification_tension_reseau_nom" ).focus(function() {
					$('#modal_modification_tension_reseau_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_modification_tension_reseau_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_modification_tension_reseau_nom_aide').show();
					
					$('#modal_modification_tension_reseau_button').attr('disabled',false);
					$('#modal_modification_tension_reseau_button').removeClass('disabled');
		});
		
		//test de la validité des données
		$( "#modal_modification_tension_reseau_nom" ).blur(function() {
			
			//récupération de l'id de la ligne concernée
			var id_tension_reseau = $('#modal_modification_tension_reseau_id').attr("value");
			//console.log("id: "+id_tension_reseau);
		
			//recherche d'un élément déjà présent
			var nom_tension_reseau_modifie = $('#modal_modification_tension_reseau_nom').val();
			var longueur_tension_reseau_modifie = nom_tension_reseau_modifie.length;
			if($('#modal_modification_tension_reseau_nom').val()!="")
				{
				if(pattern_tension_reseau_nom.test(nom_tension_reseau_modifie)&&(longueur_tension_reseau_modifie<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/tension_reseau/admin_tension_reseau_modification_verifications.php",
					type   		: "POST",
					data     	: {tension_reseau: nom_tension_reseau_modifie, id:id_tension_reseau},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    //alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_tension_reseau').modal('toggle');
								 },
					success  	: function(retour_json) {
								//3 cas: 	- le nom n'est pas présent ->ok
								//			- le nom est présent mais c'est celui du modal (on modifie le champs divers) ->ok
								//			- sinon, échec
									//console.log("retour"+retour_json['resultat']);
									if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
									{
										
										$('#modal_modification_tension_reseau_nom').addClass('is-valid');
										$('#modal_modification_tension_reseau_nom').removeClass('is-invalid');
										$('#modal_modification_tension_reseau_nom_label').addClass('text-success');
										$('#modal_modification_tension_reseau_erreur').text("l'état est valide");
										$('#modal_modification_tension_reseau_button').attr('disabled',false);
										$('#modal_modification_tension_reseau_button').removeClass('disabled');
										$('#modal_modification_tension_reseau_nom_aide').hide();
									}
									else
									{
										$('#modal_modification_tension_reseau_nom').addClass('is-invalid');
										$('#modal_modification_tension_reseau_nom').removeClass('is-valid');
										$('#modal_modification_tension_reseau_nom_label').addClass('text-danger');
										$('#modal_modification_tension_reseau_nom_aide').hide();
										
										$('#modal_modification_tension_reseau_erreur').text("échec");					
										$('#modal_modification_tension_reseau_button').attr('disabled',true);
										$('#modal_modification_tension_reseau_button').addClass('disabled');
									}
								}
					
					});
				}
				else
				{
				// le nom ne passe pas le pattern
				$('#modal_modification_tension_reseau_nom').addClass('is-invalid');
				$('#modal_modification_tension_reseau_nom_label').addClass('text-danger');
				$('#modal_modification_tension_reseau_nom_aide').hide();
				
				$('#modal_modification_tension_reseau_erreur').text("le nom doit comporter entre 4 et 20 caractères");					
				$('#modal_modification_tension_reseau_button').attr('disabled',true);
				$('#modal_modification_tension_reseau_button').addClass('disabled');
				}
			}
		else
		{
		$('#modal_modification_tension_reseau_nom').addClass('is-invalid');
		$('#modal_modification_tension_reseau_nom_label').addClass('text-danger');
		$('#modal_modification_tension_reseau_nom_aide').hide();
		
		$('#modal_modification_tension_reseau_erreur').text("le  champs est vide");					
		$('#modal_modification_tension_reseau_button').attr('disabled',true);
		$('#modal_modification_tension_reseau_button').addClass('disabled');
		}
	});
	
	$('#modal_modification_tension_reseau_valeur').blur(function(){
		var tension_reseau_valeur = $('#modal_modification_tension_reseau_valeur').val();
		if($.isNumeric($('#modal_modification_tension_reseau_valeur').val()))
		{	//champs valide
		
			$('#modal_modification_tension_reseau_valeur').removeClass('is-invalid');
			$('#modal_modification_tension_reseau_valeur').addClass('is-valid');
			$('#modal_modification_tension_reseau_valeur_label').removeClass('text-danger');
			$('#modal_modification_tension_reseau_valeur_label').addClass('text-success');
			
			$('#modal_modification_tension_reseau_valeur_erreur').text("le champs est valide!");
			$('#modal_modification_tension_reseau_button').attr('disabled',false);
			$('#modal_modification_tension_reseau_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_tension_reseau_valeur').removeClass('is-valid');
			$('#modal_modification_tension_reseau_valeur').addClass('is-invalid');
			$('#modal_modification_tension_reseau_valeur_label').addClass('text-danger');
			$('#modal_modification_tension_reseau_valeur_label').removeClass('text-success');
			
			$('#modal_modification_tension_reseau_valeur_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_tension_reseau_button').attr('disabled',true);
			$('#modal_modification_tension_reseau_button').addClass('disabled');
		}
		
	
		
	});	

	
	$('#modal_modification_tension_reseau_divers').blur(function() {
		
		var divers_modifie = $('#modal_modification_tension_reseau_divers').val();
		var longueur_divers_modifie = divers_modifie.length;
		//test du pattern
		if(pattern_tension_reseau_divers.test(divers_modifie)&&(longueur_divers_modifie<longueur_max_divers))
		{
		//champs valide
			$('#modal_modification_tension_reseau_divers').removeClass('is-invalid');
			$('#modal_modification_tension_reseau_divers').addClass('is-valid');
			$('#modal_modification_tension_reseau_divers_label').removeClass('text-danger');
			$('#modal_modification_tension_reseau_divers_label').addClass('text-success');
			
			$('#modal_modification_tension_reseau_divers_erreur').text("le champs est valide!");
			$('#modal_modification_tension_reseau_button').attr('disabled',false);
			$('#modal_modification_tension_reseau_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_modification_tension_reseau_divers').removeClass('is-valid');
			$('#modal_modification_tension_reseau_divers').addClass('is-invalid');
			$('#modal_modification_tension_reseau_divers_label').addClass('text-danger');
			$('#modal_modification_tension_reseau_divers_label').removeClass('text-success');
		
			$('#modal_modification_tension_reseau_nom_aide').hide();
			$('#modal_modification_tension_reseau_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_modification_tension_reseau_button').attr('disabled',true);
			$('#modal_modification_tension_reseau_button').addClass('disabled');
		}
	
	});
	
	//mise à jour des données depuis la fenêtre modal
	$('#modal_modification_tension_reseau_button').click(function(event){
		
		//récupération des informations de la fenêtre moddal
		var nom_tension_reseau_modifie = $('#modal_modification_tension_reseau_nom').val();
		var valeur_tension_reseau_modifie = $('#modal_modification_tension_reseau_valeur').val();
		var divers_modifie = $('#modal_modification_tension_reseau_divers').val();
		var id_modifie = $('#modal_modification_tension_reseau_id').attr("value");
		//console.log("id: "+id_modifie);	
			//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/tension_reseau/admin_tension_reseau_modification.php",
			type   		: "POST",
			data     	: {nom_tension: nom_tension_reseau_modifie, divers:divers_modifie, id:id_modifie , valeur_tension: valeur_tension_reseau_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   ////alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_tension_reseau').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat'] ==  tab_msg['code_ok']['id']) 
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_modification_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_modification_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							
							}
						}													
				});
	});			
	/*************************************************/
	// modal suppression d'un état
	/************************************************/
	function initialisation_modal_suppresion_tension_reseau(fonction_callback)
	{
	$('#modal_suppression_tension_reseau_nom').val("");
	$('#modal_suppression_tension_reseau_divers').val("");
	$('#modal_suppression_tension_reseau_button').attr('disabled',false);
	$('#modal_suppression_tension_reseau_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	
	
	
	//sélection des boutons "supprimer" par la méthode de délégation car ils sont ajoutés dynamiquement
	$('#table_tension_reseau').on("click",".suppression_tension_reseau",function(e){
		var id_recup = $(e.currentTarget).parent().parent().attr("id");
		var nom_recup = $(e.currentTarget).parent().parent().find("td").eq(0).html();
		var divers_recup = $(e.currentTarget).parent().parent().find("td").eq(01).html();
		
	
		//ouverture modal de modification			
		initialisation_modal_ajout_tension_reseau(function(){
			$('#modal_suppression_tension_reseau_id').attr("value",id_recup);
			$('#modal_suppression_tension_reseau_nom ').text(nom_recup);	
			$('#modal_suppression_tension_reseau_divers').text(divers_recup);
			
			$('#modal_suppression_tension_reseau').modal('show'); 
			
			});
		}); //fin du On click	
		
		
			$('#modal_suppression_tension_reseau').on('shown.bs.modal', function() {		
			
			});			
		
		
	//mise à jour des données depuis la fenêtre modal
	$('#modal_suppression_tension_reseau_button').click(function(event){
		
		//récupération des informations de la fenêtre moddal
		var tension_reseau_modifie = $('#modal_suppression_tension_reseau_nom').text();
		var divers_modifie = $('#modal_suppression_tension_reseau_divers').text();
		var id_modifie = $('#modal_suppression_tension_reseau_id').attr("value");
		
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			url      	: "code/tension_reseau/admin_tension_reseau_suppression.php",
			type   		: "POST",
			data     	: {tension_reseau: tension_reseau_modifie, divers:divers_modifie, id:id_modifie},		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   ////alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_tension_reseau').modal('toggle');
						 },
			success  	: function(retour_json) 
						{  
						
						//fermeture de la fenetre modal
							if(retour_json['resultat']==  tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_suppression_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_suppression_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});			
	
	/*************************************************/
	// modal ajout des tension_reseau
	/************************************************/
/*	
	function initialisation_modal_ajout_tension_reseau(fonction_callback)
	{
	$('#modal_ajout_tension_reseau_nom').val("");
	$('#modal_ajout_tension_reseau_divers').val("");
	$('#modal_ajout_tension_reseau_nom').removeClass('is-valid').removeClass('is-invalid');
	$('#modal_ajout_tension_reseau_nom_label').removeClass('text-success').removeClass('text-danger');
	$('#modal_ajout_tension_reseau_nom_aide').show();
	$('#modal_ajout_tension_reseau_button').attr('disabled',false);
	$('#modal_ajout_tension_reseau_button').removeClass('disabled');
	if(fonction_callback)
		{fonction_callback();}
	}
	 
*/	
	
	$('#ajout_tension_reseau').click(function() {
		initialisation_modal_ajout_tension_reseau(function(){
			$('#modal_ajout_tension_reseau').modal('show');
			$('#modal_ajout_tension_reseau_nom').focus()	
			});
		});
		
		$('#modal_ajout_tension_reseau').on('shown.bs.modal', function() {
			$('#modal_ajout_tension_reseau_nom').focus();
						});
		
		$('#modal_ajout_tension_reseau').on('hidden.bs.modal', function (e) {
//en réserve		
		});
			
		
		//remise à zéro des couleurs avant l'entrée des informations	
		$( "#modal_ajout_tension_reseau_nom" ).focus(function() {
					$('#modal_ajout_tension_reseau_nom').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_tension_reseau_nom_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_tension_reseau_divers').removeClass('is-valid').removeClass('is-invalid');
					$('#modal_ajout_tension_reseau_divers_label').removeClass('text-success').removeClass('text-danger');
					$('#modal_ajout_tension_reseau_nom_aide').show();
					$('#modal_ajout_tension_reseau_button').attr('disabled',false);
					$('#modal_ajout_tension_reseau_button').removeClass('disabled');
				
			});
		
		//test de la validité des données
		$( "#modal_ajout_tension_reseau_nom" ).blur(function() {
			
			//recherche d'un élément déjà présent
			var tension_reseau_ajoute = $('#modal_ajout_tension_reseau_nom').val();
			var longueur_tension_reseau_ajoute = tension_reseau_ajoute.length;
			
			
			if(($('#modal_ajout_tension_reseau_nom').val()!=""))
			{	
				//test du pattern
				if(pattern_tension_reseau_nom.test(tension_reseau_ajoute)&&(longueur_tension_reseau_ajoute<longueur_max_nom))
				{
				//envoi des données vers le fichier PHP de traitement en AJAX
				$.ajax({
					 url      	: "code/tension_reseau/admin_tension_reseau_verifications.php",
					type   		: "POST",
					data     	: {tension_reseau: tension_reseau_ajoute},		
					cache    	: false,
					async		: true,		
					dataType 	: "json",
					error    	: function(request, error) { // Info Debuggage si erreur         
								    //alert("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
									$('#toast_enregistrement_echec').toast('show');
									$('#modal_ajout_tension_reseau').modal('toggle');
								 },
					success  	: function(retour_json) 
								{  
									
									if(retour_json['resultat'] == tab_msg['code_ok']['id']) 
									{
									//champs valide
									$('#modal_ajout_tension_reseau_nom').removeClass('is-invalid');
									$('#modal_ajout_tension_reseau_nom').addClass('is-valid');
									$('#modal_ajout_tension_reseau_nom_label').addClass('text-success');
									$('#modal_ajout_tension_reseau_nom_aide').hide();
									
									$('#modal_ajout_tension_reseau_erreur').text("l'état est valide");
									$('#modal_ajout_tension_reseau_button').attr('disabled',false);
									$('#modal_ajout_tension_reseau_button').removeClass('disabled');
									}
									else	
									{
										//le champs est déjà utilisé
									$('#modal_ajout_tension_reseau_nom').addClass('is-invalid');
									$('#modal_ajout_tension_reseau_nom').removeClass('is-valid');
									$('#modal_ajout_tension_reseau_nom_label').addClass('text-danger');
									$('#modal_ajout_tension_reseau_nom_aide').hide();
									
									$('#modal_ajout_tension_reseau_erreur').text("l'état est déjà présent");
									$('#modal_ajout_tension_reseau_button').attr('disabled',true);
									$('#modal_ajout_tension_reseau_button').addClass('disabled');
									}
									
								}							
						});
				}
				else
				{
				// le champs ne respecte pas le pattern
				$('#modal_ajout_tension_reseau_nom').addClass('is-invalid');
				$('#modal_ajout_tension_reseau_nom_label').addClass('text-danger');
				$('#modal_ajout_tension_reseau_nom_aide').hide();
				
				$('#modal_ajout_tension_reseau_erreur').text("le nom doit comporter entre 4 et 20 lettres");					
				$('#modal_ajout_tension_reseau_button').attr('disabled',true);
				$('#modal_ajout_tension_reseau_button').addClass('disabled');
				}
			}	
			else		//le champs est vide
			{	
				// le champs est vide
				$('#modal_ajout_tension_reseau_nom').addClass('is-invalid');
				$('#modal_ajout_tension_reseau_nom_label').addClass('text-danger');
				$('#modal_ajout_tension_reseau_nom_aide').hide();
				
				$('#modal_ajout_tension_reseau_erreur').text("le  champs est vide");					
				$('#modal_ajout_tension_reseau_button').attr('disabled',true);
				$('#modal_ajout_tension_reseau_button').addClass('disabled');
			}					
		});
					
	
	$('#modal_ajout_tension_reseau_divers').blur(function() {
		
		var divers_ajoute = $('#modal_ajout_tension_reseau_divers').val();
		var longueur_divers_ajoute = divers_ajoute.length;
		//test du pattern
		if(pattern_tension_reseau_divers.test(divers_ajoute)&&(longueur_divers_ajoute<longueur_max_divers))
		{
		//champs valide
			$('#modal_ajout_tension_reseau_divers').removeClass('is-invalid');
			$('#modal_ajout_tension_reseau_divers').addClass('is-valid');
			$('#modal_ajout_tension_reseau_divers_label').removeClass('text-danger');
			$('#modal_ajout_tension_reseau_divers_label').addClass('text-success');
			
			$('#modal_ajout_tension_reseau_divers_erreur').text("le champs est valide!");
			$('#modal_ajout_tension_reseau_button').attr('disabled',false);
			$('#modal_ajout_tension_reseau_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_tension_reseau_divers').removeClass('is-valid');
			$('#modal_ajout_tension_reseau_divers').addClass('is-invalid');
			$('#modal_ajout_tension_reseau_divers_label').addClass('text-danger');
			$('#modal_ajout_tension_reseau_divers_label').removeClass('text-success');
			
			$('#modal_ajout_tension_reseau_divers_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_ajout_tension_reseau_button').attr('disabled',true);
			$('#modal_ajout_tension_reseau_button').addClass('disabled');
		}
	
	
	});
	$('#modal_ajout_tension_reseau_valeur').blur(function(){
		var tension_reseau_valeur = $('#modal_ajout_tension_reseau_valeur').val();
		if($.isNumeric($('#modal_ajout_tension_reseau_valeur').val()))
		{	//champs valide
		
			$('#modal_ajout_tension_reseau_valeur').removeClass('is-invalid');
			$('#modal_ajout_tension_reseau_valeur').addClass('is-valid');
			$('#modal_ajout_tension_reseau_valeur_label').removeClass('text-danger');
			$('#modal_ajout_tension_reseau_valeur_label').addClass('text-success');
			
			$('#modal_ajout_tension_reseau_valeur_erreur').text("le champs est valide!");
			$('#modal_ajout_tension_reseau_button').attr('disabled',false);
			$('#modal_ajout_tension_reseau_button').removeClass('disabled');			
		}
		else
		{
			// le champs ne respecte pas le pattern
			$('#modal_ajout_tension_reseau_valeur').removeClass('is-valid');
			$('#modal_ajout_tension_reseau_valeur').addClass('is-invalid');
			$('#modal_ajout_tension_reseau_valeur_label').addClass('text-danger');
			$('#modal_ajout_tension_reseau_valeur_label').removeClass('text-success');
			
			$('#modal_ajout_tension_reseau_valeur_erreur').text("le champs doit comporter 255 caractères maximum");					
			$('#modal_ajout_tension_reseau_button').attr('disabled',true);
			$('#modal_ajout_tension_reseau_button').addClass('disabled');
		}
		
	
		
	});	

	
	
	$('#modal_ajout_tension_reseau_button').click(function(event){
		//récupération des informations de la fenêtre moddal
		var tension_reseau_ajoute = $('#modal_ajout_tension_reseau_nom').val();
		var tension_reseau_valeur_ajoute = $('#modal_ajout_tension_reseau_valeur').val();
		var divers_ajoute = $('#modal_ajout_tension_reseau_divers').val();
		////console.log('tension_reseau '+tension_reseau_ajoute+'val: '+tension_reseau_valeur_ajoute+' divers'+divers_ajoute );
		$('#ajout_tension_reseau_spinner').show();
		//envoi des données vers le fichier PHP de traitement en AJAX
		$.ajax({
			 url      	: "code/tension_reseau/admin_tension_reseau_ajout.php",
			type   		: "POST",
			data     	: {tension_reseau: tension_reseau_ajoute,divers:divers_ajoute, tension_reseau_valeur:tension_reseau_valeur_ajoute },		
			cache    	: false,
			dataType 	: "json",
			error    	: function(request, error) { // Info Debuggage si erreur         
						   ////alert("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec_texte').html("Erreur : responseText: "+request.responseText);
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_tension_reseau').modal('toggle');
							$('#ajout_tension_reseau_spinner').hide();
						 },
			success  	: function(retour_json) 
						{  
						$('#ajout_tension_reseau_spinner').hide();
						//fermeture de la fenetre modal
							if(retour_json['resultat']== tab_msg['code_ok']['id'])
							{
							$('#toast_enregistrement_ok_texte').text(tab_msg['code_ok']['texte']);	
							$('#toast_enregistrement_ok').toast('show');
							$('#modal_ajout_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							}
							else
							{
							var res = retour_json['resultat'];
							$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
							$('#toast_enregistrement_echec').toast('show');
							$('#modal_ajout_tension_reseau').modal('toggle');
							mise_a_jour_liste();
							}
						}													
				});		
	});
