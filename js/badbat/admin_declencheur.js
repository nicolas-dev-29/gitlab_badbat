
	$(document).ready(function(){
	
var tab_msg={};

$('[data-toggle="tooltip"]').tooltip();

/********************************************************/
// fonctions admin déclencheurs
/********************************************************/
/**variagles globales                                  **/

function mise_a_jour_liste()
{
		$.getJSON('./constantes/code_message.json',function(data)
			{
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
			//mise à jour du tableau des etat
			$.ajax({
				url      	: "code/declencheur/admin_declencheur_liste.php",
				type   		: "POST",	
				cache    	: false,
				async		: true,
				dataType 	: "json",
				error    	: function(request, error) { // Info Debuggage si erreur         
							   alert("Erreur : responseText: "+request.responseText);
							 },
				success  	:function(reponse) 
							{  
								//console.log(reponse);
								//$('#nombre_etat').html("<b>"+reponse.length+"</b>");
								$('#table_declencheur_temporel tr').each(function(){
									$(this).remove();
								});
								var button_modifier='<button class="btn btn-warning modification_etat" name="modification_etat" 	data-toogle="tooltip" data-placement="top" title="modification du etat"	value="modification_etat">	<span class="fa fa-edit fa-1x "></span></button>';
								var button_supprimer='<button class="btn btn-danger suppression_etat" name="suppression_etat" title="suppression du etat" data-toggle="tooltip" data-placement="top"   value="suppression_etat"><span class="fa fa-trash fa-1x "></span></button>';
								var ligne_table='';
								$.each(reponse,function(i,item){
								//console.log(item.resultat);
									if(item.resultat ==  tab_msg['code_ok']['id'])
									{
										if(item.id != 1) // on n'affiche pas la valeur par défut
										{
										ligne_table +='<tr class="admin_etat" id='+item.id+'>\
										<td>'+item.id+'</td>\
										<td>'+ item.nom +'</td>\
										<td>'+ item.delai+'</td>\
										<td>'+ item.activation+ '</td><td>' + item.divers+ '</td>\
										<td>'+button_modifier+'</td>\
										<td>'+button_supprimer+'</td>\
										</tr>';
										}
									}
									else
									{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
									}
								});
								$('#table_declencheur_temporel').append(ligne_table);
							}						
					});	
			});
	}



				//mise à jour de la banniere échéances
		$.getScript("./js/badbat/echeances_all_pages.js",function(){
		maj_echeance();
		});
		//alert("mise à jour des variables de la page");
		$('#toast_enregistrement_echec').addClass("hide");
		$('#toast_enregistrement_ok').addClass("hide");		
		
		$('#suppression_etat_tous_spinner').hide();
		$('#ajout_etat_spinner').hide();
		
		mise_a_jour_liste();  


		//récupération des informations d'échanges du fichier JSON (code erreur,..)
		
		
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				
				////console.log("tb:"+tab_msg[index].id+"/"+tab_msg[index].nom+" / "+tab_msg[index].texte);				
				////console.log("BASE -index:"+index+" nom:"+d.nom+" texte:"+d.texte);
			});
		});
		
	});
























































