$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	e.target // newly activated tab
	e.relatedTarget // previous active tab
	console.log(e.currentTarget.id);
  })
//mise à jour de la banniere échéances
$.getScript("./js/badbat/echeances_all_pages.js",function(){
maj_echeance();});

var tab_msg={};
function test_validation_creation_fiche()
{
	if(($('#creation_fiche_tension_liste_select option:selected').val()=="1") 
		||($('#creation_fiche_equipement_liste_select option:selected').val()=="1")
		||($('#creation_fiche_lieux_liste_select option:selected').val() == "1")
		||($('#creation_fiche_etat_liste_select option:selected').val()=="1"))
	{
			$('#validation_fiche').addClass('disabled');
			$('#validation_fiche').attr('disabled',true);			
	}
	else
	{
			$('#validation_fiche').removeClass('disabled');
			$('#validation_fiche').attr('disabled',false);
	}
}
function acqusition_echange()
{
		$.getJSON('./constantes/code_message.json',function(data){
			$.each(data,function(index,d){			
				var tab_msg_tampon={};
				tab_msg_tampon['id']= d.id;
				tab_msg_tampon['nom']=d.nom;
				tab_msg_tampon['texte']=d.texte;
				tab_msg[index]=tab_msg_tampon;
				});
				});
}
var tab_provisoire={};
tab_provisoire['id_batteries']="";
tab_provisoire['id_lieux']="";
tab_provisoire['id_equipement']="";
tab_provisoire['id_tension']="";
tab_provisoire['lieux']="";
tab_provisoire['equipement']="XXXX";
tab_provisoire['tension']="XXXX";
tab_provisoire['id_etat']="";
tab_provisoire['etat']="XXXX";
tab_provisoire['autonomie_requise']="";
tab_provisoire['reference_industrielle']="123456789";
//dimensions
tab_provisoire['dimension_longueur']="";
tab_provisoire['dimension_largeur']="";
tab_provisoire['dimension_hauteur']="";
//fabricant
tab_provisoire['id_fabricant']="";
tab_provisoire['fabricant_nom']="";
tab_provisoire['fabricant_adresse']="";
tab_provisoire['fabricant_mail']="";
tab_provisoire['fabricant_telephone']="";
tab_provisoire['fabricant_divers']="";
var tab_provisoire_initiale={};
tab_provisoire_initiale['id_batteries']="";
tab_provisoire_initiale['id_lieux']="";
tab_provisoire_initiale['id_equipement']="";
tab_provisoire_initiale['id_tension']="";
tab_provisoire_initiale['lieux']="XXXX";
tab_provisoire_initiale['equipement']="XXXX";
tab_provisoire_initiale['tension']="XXXX";
tab_provisoire_initiale['id_etat']="";
tab_provisoire_initiale['etat']="XXXX";
tab_provisoire_initiale['autonomie_requise']="";
tab_provisoire_initiale['reference_industrielle']="";
//dimensions
tab_provisoire_initiale['dimension_longueur']="";
tab_provisoire_initiale['dimension_largeur']="";
tab_provisoire_initiale['dimension_hauteur']="";
//pour la modification
function mise_a_jour_reference()
{
	//acqusition_echange();
	//mise à jour de la référence
	var ligne_reference={};
	ligne_reference= tab_provisoire['tension']+'-'+tab_provisoire['equipement']+'-'+tab_provisoire['lieux'];
	$('#gestion_fiche_reference').text(ligne_reference);
	//$('#recherche_fiche_lieux').val(tab_provisoire['lieux']);
}


/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des paramêtres généraux
//
/*****************************************************************************************************************************************************************************************/
//
/*****************************************************************************************************************************************************************************************/
//			gestion des tensions
/*****************************************************************************************************************************************************************************************/
//
//mise à jour après un changement de Select de  tension
	$('#creation_fiche_tension_liste_select').on('change',function(){
	var id_tension_selectionne = $('#creation_fiche_tension_liste_select').val();
	tab_provisoire['id_tension'] = id_tension_selectionne;
	tab_provisoire['tension'] = $('#creation_fiche_tension_liste_select option:selected').text();
	//vérification des conditions nécessaire à l'activiaton du bouton de validation.
	//console.log($('#creation_fiche_equipement_liste_select option:selected').val());
	test_validation_creation_fiche();
	mise_a_jour_reference();
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_tension').on('click',function(){
		tab_provisoire['id_tension']="";
		tab_provisoire['tension']="XXXX";
		$('#creation_fiche_tension_liste_select').val("1");
		mise_a_jour_reference();
		$(this).blur();
	});
	//ajout de la modal d'ajout des tensions (inclusion)
	$('#creation_fiche_ajout_tension').on('click',function(){
		$('#modal_ajout_lieux_nom').val("");
		$('#modal_ajout_lieux_divers').val("");
		$('#modal_ajout_lieux_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_ajout_lieux_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_ajout_lieux_nom_aide').show();
		$('#modal_ajout_lieux_button').attr('disabled',false);
		$('#modal_ajout_lieux_button').removeClass('disabled');
		$('#modal_ajout_tension_reseau').modal('show');
		$('#modal_ajout_tension_reseau_nom').focus();	
			
	});
	//fermeture du modal ajout tension-> insertion du nouveau lieu dans le select et selected
$('#modal_ajout_tension').on('hide.bs.modal',function(){
	acqusition_echange();
	$('#creation_fiche_tension_liste_select').empty();
	$.ajax({
				url			:"code/tension/admin_tension_liste.php", //on utilise la page admin_tension
				type		:"POST",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							//alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if((item.nom)==($('#modal_ajout_tension_nom').val().toUpperCase()))
									{
									ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
									tab_provisoire['tension'] = item.nom;
									tab_provisoire['id_tension'] = item.id;
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
						
						$('#creation_fiche_tension_liste_select').append(ligne_select);
						}
		})
});
//
/*****************************************************************************************************************************************************************************************/
//			gestion des équipements
/*****************************************************************************************************************************************************************************************/
//mise à jour après un changement de Select de  equipement
	$('#creation_fiche_equipement_liste_select').on('change',function(){
	var id_equipement_selectionne = $('#creation_fiche_equipement_liste_select').val();
	tab_provisoire['id_equipement'] = id_equipement_selectionne;
	tab_provisoire['equipement'] = $('#creation_fiche_equipement_liste_select option:selected').text();
	
	test_validation_creation_fiche();
	mise_a_jour_reference();
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_equipement').on('click',function(){
		tab_provisoire['id_equipement']="";
		tab_provisoire['equipement']="XXXX";
		$('#creation_fiche_equipement_liste_select').val("1");
		mise_a_jour_reference();
		$(this).blur();
	});
	//ajout de la modal d'ajout des equipement (inclusion)
	$('#creation_fiche_ajout_equipement').on('click',function(){
		$('#modal_ajout_equipement_nom').val("");
		$('#modal_ajout_equipement_divers').val("");
		$('#modal_ajout_equipement_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_ajout_equipement_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_ajout_equipement_nom_aide').show();
		$('#modal_ajout_equipement_button').attr('disabled',false);
		$('#modal_ajout_equipement_button').removeClass('disabled');
			$('#modal_ajout_equipement').modal('show');
			$('#modal_ajout_equipement_nom').focus();
		
	});
	//fermeture du modal ajout equipement-> insertion du nouveau lieu dans le select et selected
$('#modal_ajout_equipement').on('hide.bs.modal',function(){
	acqusition_echange();
	$('#creation_fiche_equipement_liste_select').empty();
	$.ajax({
				url			:"code/equipement/admin_equipement_liste.php", //on utilise la page admin_equipement
				type		:"POST",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							//alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if((item.nom)==($('#modal_ajout_equipement_nom').val().toUpperCase()))
									{
									ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
									tab_provisoire['equipement'] = item.nom;
									tab_provisoire['id_equipement'] = item.id;
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
						//ajout de la valeur -1 pour les remises à zéro
						
						$('#creation_fiche_equipement_liste_select').append(ligne_select);
						}
		})
});
//
/*****************************************************************************************************************************************************************************************/
//			gestion des lieux
/*****************************************************************************************************************************************************************************************/
//mise à jour après un changement de Select de  lieux
	$('#creation_fiche_lieux_liste_select').on('change',function(){
	var id_lieux_selectionne = $('#creation_fiche_lieux_liste_select').val();
	tab_provisoire['id_lieux'] = id_lieux_selectionne;
	tab_provisoire['lieux'] = $('#creation_fiche_lieux_liste_select option:selected').text();
	test_validation_creation_fiche();
	mise_a_jour_reference();
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_lieux').on('click',function(){
		tab_provisoire['id_lieux']="";
		tab_provisoire['lieux']="XXXX";
		$('#creation_fiche_lieux_liste_select').val("1");
		mise_a_jour_reference();
		$(this).blur();
	});
	//ajout de la modal d'ajout des lieux (inclusion)
	$('#creation_fiche_ajout_lieux').on('click',function(){
		$('#modal_ajout_lieux_nom').val("");
		$('#modal_ajout_lieux_divers').val("");
		$('#modal_ajout_lieux_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_ajout_lieux_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_ajout_lieux_nom_aide').show();
		$('#modal_ajout_lieux_button').attr('disabled',false);
		$('#modal_ajout_lieux_button').removeClass('disabled');
			$('#modal_ajout_lieux').modal('show');
			$('#modal_ajout_lieux_nom').focus();
			
	});
	//fermeture du modal ajout lieux-> insertion du nouveau lieu dans le select et slected
$('#modal_ajout_lieux').on('hide.bs.modal',function(){
	acqusition_echange();
	
	$('#creation_fiche_lieux_liste_select').empty();
	$.ajax({
				url			:"code/lieux/admin_lieux_liste_2.php", //on utilise la page admin_lieux
				type		:"POST",
				cache		:false,
				dataType	:"json",
				error		: function(request,error){
							//alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if((item.nom)==($('#modal_ajout_lieux_nom').val().toUpperCase()))
									{
									ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
									tab_provisoire['lieux'] = item.nom;
									tab_provisoire['id_lieux'] = item.id;
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
						//ajout de la valeur -1 pour les remises à zéro
						console.log(ligne_select);
						$('#creation_fiche_lieux_liste_select').append(ligne_select);
						}
		})
});
/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des états
//
/*****************************************************************************************************************************************************************************************/
//mise à jour après un changement de Select de de etat
	$('#creation_fiche_etat_liste_select').on('change',function(){
	var id_etat_selectionne = $('#creation_fiche_etat_liste_select').val();
	tab_provisoire['id_etat'] = id_etat_selectionne;
	tab_provisoire['etat'] = $('#creation_fiche_etat_liste_select option:selected').text();
	test_validation_creation_fiche();
	//mise_a_jour_reference();
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_etat').on('click',function(){
		tab_provisoire['id_etat']="";
		tab_provisoire['etat']="XXXX";
		$('#creation_fiche_etat_liste_select').val("1");
		//mise_a_jour_reference();
		$(this).blur();
	});
	//ajout de la modal d'ajout des etat (inclusion)
	$('#creation_fiche_ajout_etat').on('click',function(){
		$('#modal_ajout_etat_nom').val("");
		$('#modal_ajout_etat_divers').val("");
		$('#modal_ajout_etat_nom').removeClass('is-valid').removeClass('is-invalid');
		$('#modal_ajout_etat_nom_label').removeClass('text-success').removeClass('text-danger');
		$('#modal_ajout_etat_nom_aide').show();
		$('#modal_ajout_etat_button').attr('disabled',false);
		$('#modal_ajout_etat_button').removeClass('disabled');
			$('#modal_ajout_etat').modal('show');
			$('#modal_ajout_etat_nom').focus();
			
	});
	//fermeture du modal ajout etat-> insertion du nouveau lieu dans le select et slected
$('#modal_ajout_etat').on('hide.bs.modal',function(){
	acqusition_echange();
	$('#creation_fiche_etat_liste_select').empty();
	$.ajax({
				url			:"code/etat/admin_etat_liste.php", //on utilise la page admin_etat
				type		:"POST",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							//alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if((item.nom)==($('#modal_ajout_etat_nom').val().toUpperCase()))
									{
									ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
									tab_provisoire['etat'] = item.nom;
									tab_provisoire['id_etat'] = item.id;
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
						//ajout de la valeur -1 pour les remises à zéro
						
						$('#creation_fiche_etat_liste_select').append(ligne_select);
						}
		})
});

/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card de la référence
//
/*****************************************************************************************************************************************************************************************/
	$('#creation_fiche_reference').on('change',function(){
	
	var reference_selectionne = $('#creation_fiche_reference').val();
	tab_provisoire['reference_industrielle'] = reference_selectionne;
	//alert("changement");
	//console.log(reference_selectionne);
	
	//test_validation_creation_fiche();
	//mise_a_jour_reference();
	});



$('#raz_fiche_reference').on('click',function(){
		tab_provisoire['reference_industrielle']="";
		$('#creation_fiche_reference').val('');
		$(this).blur();
	});






/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card de l'autonomie requise
//
/*****************************************************************************************************************************************************************************************/
//
/*****************************************************************/
// animation des labels en fonction du nombre heure et minuetes
//
/*****************************************************************/	
//
//	gestion de la partie heure
//
	$('#creation_fiche_autonomie_requise_heure').on("change",function(e,data){
		if($('#creation_fiche_autonomie_requise_heure').val()>1)
		{
			$('#creation_fiche_autonomie_requise_heure_label').text("heures");
		}
		else
		{
			$('#creation_fiche_autonomie_requise_heure').text("heure");
		}
	});
	$('#creation_fiche_autonomie_requise_heure').on("blur",function(e,data){
	//interdiction de sortir  si valeur incorrect
		if(($('#creation_fiche_autonomie_requise_heure').val())!='')
		{
			if(($('#creation_fiche_autonomie_requise_heure').val())>23)
				{
					//$('#gestion_fiche_validation_fiche').attr('disabled',true);
					//$('#gestion_fiche_validation_fiche').addClass('disabled');
					$('#creation_fiche_autonomie_requise_heure_erreur').text("erreur! (Max:23)");
					$('#creation_fiche_autonomie_requise_heure_erreur').show();
					$('#creation_fiche_autonomie_requise_heure').val("23");
				}
				else //données valides --> on l'insére dans le tab_provisoire
				{
					var  autonomie_requise = Number((60*$('#creation_fiche_autonomie_requise_heure').val()))
												+Number(($('#creation_fiche_autonomie_requise_minute').val()));
					tab_provisoire['autonomie_requise'] = autonomie_requise;	
					$('#gestion_fiche_validation_fiche').attr('disabled',false);
					$('#gestion_fiche_validation_fiche').removeClass('disabled');
					$('#creation_fiche_autonomie_requise_minute_erreur').text("");
					$('#creation_fiche_autonomie_requise_minute_erreur').hide();
				}
		}
		else //données incorrectes
		{
			$('#gestion_fiche_validation_fiche').attr('disabled',true);
			$('#gestion_fiche_validation_fiche').addClass('disabled');
			$('#creation_fiche_autonomie_requise_heure_erreur').text("erreur!");
			$('#creation_fiche_autonomie_requise_heure_erreur').show();
		}
	});
	$('#modal_operation_courante_mesure_autonomie_minute').on("change",function(e,data){
		if($('#modal_operation_courante_mesure_autonomie_minute').val()>1)
		{
			$('#modal_operation_courante_mesure_autonomie_minute_label').text("minutes");
		}
		else
		{
			$('#modal_operation_courante_mesure_autonomie_minute_label').text("minute");
		}
	});
$('#creation_fiche_autonomie_requise_minute').on("change",function(e,data){
		if($('#creation_fiche_autonomie_requise_minute').val()>1)
		{
			$('#creation_fiche_autonomie_requise_minute_label').text("minutes");
		}
		else
		{
			$('#creation_fiche_autonomie_requise_minute').text("minute");
		}
	});
	$('#creation_fiche_autonomie_requise_minute').on("blur",function(e,data){
	//interdiction de sortir  si valeur incorrect
		if(($('#creation_fiche_autonomie_requise_minute').val())!='')
		{
			if(($('#creation_fiche_autonomie_requise_minute').val())>59)
				{
					$('#gestion_fiche_validation_fiche').attr('disabled',true);
					$('#gestion_fiche_validation_fiche').addClass('disabled');
					$('#creation_fiche_autonomie_requise_minute_erreur').text("erreur! (Max:59)");
					$('#creation_fiche_autonomie_requise_minute_erreur').show();
					$('#creation_fiche_autonomie_requise_minute').val("59");
				}
				else //données correctes --> on les insere dans le tab_provisoire
				{
					var  autonomie_requise = Number((60*$('#creation_fiche_autonomie_requise_heure').val()))
												+Number(($('#creation_fiche_autonomie_requise_minute').val()));
					tab_provisoire['autonomie_requise'] = autonomie_requise;
					$('#gestion_fiche_validation_fiche').attr('disabled',false);
					$('#gestion_fiche_validation_fiche').removeClass('disabled');
					$('#creation_fiche_autonomie_requise_minute_erreur').text("");
					$('#creation_fiche_autonomie_requise_minute_erreur').hide();
				}
		}
		else //données incorrectes
		{
			$('#gestion_fiche_validation_fiche').attr('disabled',true);
			$('#gestion_fiche_validation_fiche').addClass('disabled');
			$('#creation_fiche_autonomie_requise_minute_erreur').text("erreur!");
			$('#creation_fiche_autonomie_requise_minute_erreur').show();
		}
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_autonomie_requise').on('click',function(){
		tab_provisoire['autonomie_requise']="";
		$('#creation_fiche_autonomie_requise_heure').val("0");
		$('#creation_fiche_autonomie_requise_minute').val("0");
		//mise_a_jour_reference();
		$(this).blur();
	});

/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des paramêtres mécaniques
//
/*****************************************************************************************************************************************************************************************/
//



/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des dimensions
//
/*****************************************************************************************************************************************************************************************/
$('#creation_fiche_dimension_longueur').on('change',function(){
	
	var longueur_selectionne = $('#creation_fiche_dimension_longueur').val();
	tab_provisoire['dimension_longueur'] = longueur_selectionne;
	//alert("changement");
	//console.log(reference_selectionne);
	
	//test_validation_creation_fiche();
	//mise_a_jour_reference();
	});
$('#creation_fiche_dimension_largeur').on('change',function(){

	var largeur_selectionne = $('#creation_fiche_dimension_largeur').val();
	tab_provisoire['dimension_largeur'] = largeur_selectionne;
	});

$('#creation_fiche_dimension_hauteur').on('change',function(){

	var hauteur_selectionne = $('#creation_fiche_dimension_hauteur').val();
	tab_provisoire['dimension_hauteur'] = hauteur_selectionne;

	});
$('#raz_fiche_dimensions').on('click',function(){
		tab_provisoire['dimension_longueur']="";
		tab_provisoire['dimension_largeur']="";
		tab_provisoire['dimension_hauteur']="";
		$('#creation_fiche_dimension_longueur').val('');
		$('#creation_fiche_dimension_largeur').val('');
		$('#creation_fiche_dimension_hauteur').val('');
		$(this).blur();
	});

/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des dimensions du chantier
//
/*****************************************************************************************************************************************************************************************/
$('#creation_fiche_dimension_longueur_chantier').on('change',function(){
	
	var longueur_chantier_selectionne = $('#creation_fiche_dimension_longueur_chantier').val();
	tab_provisoire['dimension_longueur_chantier'] = longueur_chantier_selectionne;
	//alert("changement");
	//console.log(reference_selectionne);
	
	//test_validation_creation_fiche();
	//mise_a_jour_reference();
	});
$('#creation_fiche_dimension_largeur_chantier').on('change',function(){

	var largeur_chantier_selectionne = $('#creation_fiche_dimension_largeur_chantier').val();
	tab_provisoire['dimension_largeur_chantier'] = largeur_chantier_selectionne;
	});

$('#creation_fiche_dimension_hauteur_chantier').on('change',function(){

	var hauteur_chantier_selectionne = $('#creation_fiche_dimension_hauteur_chantier').val();
	tab_provisoire['dimension_hauteur_chantier'] = hauteur_chantier_selectionne;

	});
$('#raz_fiche_dimensions').on('click',function(){
		tab_provisoire['dimension_longueur_chantier']="";
		tab_provisoire['dimension_largeur_chantier']="";
		tab_provisoire['dimension_hauteur_chantier']="";
		$('#creation_fiche_dimension_longueur_chantier').val('');
		$('#creation_fiche_dimension_largeur_chantier').val('');
		$('#creation_fiche_dimension_hauteur_chantier').val('');
		$(this).blur();
	});


/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card des paramêtres d'achats
//
/*****************************************************************************************************************************************************************************************/
//

/*****************************************************************************************************************************************************************************************/
//
//			gestion de la card du fabricant
//
/*****************************************************************************************************************************************************************************************/
//mise à jour après un changement de Select de de fabricant
$('#creation_fiche_fabricant_liste_select').on('change',function(){
	var id_fabricant_selectionne = $('#creation_fiche_fabricant_liste_select').val();
	tab_provisoire['id_fabricant'] = id_fabricant_selectionne;
	tab_provisoire['fabricant_nom'] = $('#creation_fiche_fabricant_liste_select option:selected').text();
	test_validation_creation_fiche();
	//mise_a_jour_reference();
	});
	//RAZ avec le bouton de la CARD
	$('#raz_fiche_fabricant').on('click',function(){
		tab_provisoire['id_fabricant']="";
		tab_provisoire['fabricant_nom']="XXXX";
		$('#creation_fiche_fabricant_liste_select').val("1");
		//mise_a_jour_reference();
		$(this).blur();
	});
	//ajout de la modal d'ajout des fabricant (inclusion)
	$('#creation_fiche_ajout_fabricant').on('click',function(){
			//initialisation_modal_ajout_fabricant(function(){
			$('#modal_ajout_fabricant').modal('show');
			$('#modal_ajout_fabricant_nom').focus();
			//});
	});
	//fermeture du modal ajout fabricant-> insertion du nouveau lieu dans le select et slected
$('#modal_ajout_fabricant').on('hide.bs.modal',function(){
	acqusition_echange();
	$('#creation_fiche_fabricant_liste_select').empty();
	$.ajax({
				url			:"code/fabricant/admin_fabricant_liste.php", //on utilise la page admin_fabricant
				type		:"POST",
				cache		:true,
				dataType	:"json",
				error		: function(request,error){
							//alert("Erreur : responseText: "+request.responseText);
							},
				success		: function(reponse){
							//on place les éléments dans la liste déroulante
							var ligne_select='';
							$.each(reponse,function(i,item){
								if(item.resultat ==  tab_msg['code_ok']['id'])
								{
									if((item.nom)==($('#modal_ajout_fabricant_nom').val().toUpperCase()))
									{
									ligne_select +='<option selected value='+item.id+'>'+item.nom+'</option>';
									tab_provisoire['fabricant'] = item.nom;
									tab_provisoire['id_fabricant'] = item.id;
									}
									else
									{
									ligne_select +='<option value='+item.id+'>'+item.nom+'</option>';
									}
								}
								else
								{
									var res = item.resultat;
									var message = tab_msg[res]['texte'];
									$('#toast_enregistrement_echec_texte').text(tab_msg[res]['texte']);	
									$('#toast_enregistrement_echec').toast('show');
								}
							});
						//ajout de la valeur -1 pour les remises à zéro
						
						$('#creation_fiche_fabricant_liste_select').append(ligne_select);
						}
		})
});