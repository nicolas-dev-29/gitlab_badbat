<!--		consultation d'un fiche batterie		
				date:11/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/gesteq_constante.inc");

?>
 
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1> Consultation d'un nouvelle fiche batterie </h1>
		</div>
	</div>
	<div  class="row align-item-center"> <!--champs de la recherche -->
		<div class="col-lg-8 ">
			référence de la fiche:			
			<span  class="reference_batterie_css" id="creation_fiche_reference">XX-XX-</span>
				<input type="text" id="recherche_fiche_lieux" data-placement="top" title="entrez le lieux recherché"
				placeholder="entrez le lieu recherché" >
		</div>	
		<div class="  col-lg-4 text-right my-3 ">
			<button class="btn btn-primary"  id="validation_recherche_fiche" name="validation_recherche_fichefiche" data-toggle="tooltip" data-placement="top"
				title="validation de la recherche de la fiche" 	value="validation_recherche_fiche">	
				<!--<span id="validation_fiche_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				valider la recherche
			</button>
			<button class="btn btn-warning"  id="raz_recherche_fiche" name="raz_fiche" data-toggle="tooltip" data-placement="top"
                title="remise à zéro des champs"  	value="raz_recherche_fiche">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				raz
			</button>
			<button class="btn btn-danger"  id="annulation_recherche_fiche" name="annulation_recherche_fiche" data-toggle="tooltip" data-placement="top"
                title="quitter"  	value="annulation_recherche_fiche">	
				<!--span id="suppression_lieux_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
				quitter
			</button>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			<h3> Liste des fiches disponibles: <span id="consultation_fiche_nombre">X</span> </h3>
		</div>
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table align-middle text-center table-condensed table-stripped ">
					<thead>
						<tr>
							<!--<th scope="col">	Référence 				</th>-->
							<th scope="col">	tension					</th>
							<th scope="col">	équipement				</th>
							<th scope="col">	lieux					</th>
							<th scope="col">	mise en service 		</th>
							<th scope="col">	consulter				</th>
							<th scope="col">	modifier				</th>
							<th scope="col">	supprimer				</th>
						</tr>
					</thead>
					<tbody id="consultation_fiche_tableau">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
	include ("./code/modal_lieux.php");
	include ("./code/toast_perso.php");
?>
<script src="js/badbat/common_admin_lieux.js"></script> 
<script src="js/badbat/badbat_admin_lieux.js"></script> 
<script src="js/badbat/badbat_consultation_fiche.js"></script>