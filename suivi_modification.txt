listes des modifications
- vérifier la compliance W3C
index
menu nav bar toujours présent en haut ->scrollspy
toujours présent en haut->scrollspy
- ajouter une colone état: en préparation, active, retirée, obsolète

/******************************************************/
BO - admin utilisateurs
/******************************************************/
- créer une base pour l'administration des utilisateurs


/******************************************************/
BO - admin événements
/******************************************************/
- cocher les cases que l'on peut modifier dans les opérations courantes
- cocher les cases que l'on veut voir dans les opérations courantes



/******************************************************/
BO - base de données
/******************************************************/
passer en postgesql


/******************************************************/
BO - base de données
/******************************************************/
partie ADMIN - réaliser l'insertion du type de cosse (VUE?)


/******************************************************/
FE- capacitées
/******************************************************/
insérer la capacitées


/******************************************************/
FE - stockage de photos
/******************************************************/
ajouter l'insertion de photos


//tache cron
php -q  /home/grpnecjy/public_html/code/test/admin_test_cron_temp.php >/dev/null 2>&1

//
/******************************************************/
FE - opérations courantes
/******************************************************/
- insérer un destinataire dans l'opération courantes
- liste des paramatres modifiables pour les opérations courantes
	- tension d'arrêt

	- tension d'arrêt
	- 
modal operation courantes autonomie:
-remplacer l'alerte par un toast

- revoir la gestion de la conversion de la date en timestamp

- envoyer un mail à chaque modification de l'opération courante
//

/******************************************************/
FE - historique
/******************************************************/
-faire une liste + un graph des autonomie mesurées
-faire une table d'histirsation des opérations

/******************************************************/
FE - bannière échéances
/******************************************************/



- ajouter la class alert pour la gestion des couleurs,
- faire varier le fond suivant les échéances (vert, ok rouge ko)
/*********************fait*****************************/
changer le fond de échanéance (code CSS)
//
/******************************************************/
FE -  page d'acceuil
/******************************************************/
- nav bar - drop: la police n'est pas identiique dans le sous menu celle de la nav bar
- faitre l'icone de l'application
-regarder pour insérer les fichiers scripts (toast et modal en fin de fichier.

/******************************************************/
FE -  page d'acceuil
/******************************************************/
- coder les boutons modifier et supprimer

/******************************************************/
BO - admin état batteries
/******************************************************/




/******************************************************/
BO - admin utilisateurs	
/******************************************************/
- ajouter des utilisateurs
- LORS DE LA SUPPRESSION D'UN OU PLUSIEURS UTILISATEURS, IL FAUT METTRE À JOUR LA TABLE D'ÉVÉNEMENT
- ajouter des fonctions pour chaque utilisateurs

/******************************************************/
FE - fiche batterie - gestion fiche
/******************************************************/
- gérer le suat de ligne dans le mode de consultation des divers
- initialisation de la recherche (pas de résultats ) pour le moment.

- changer le format de la date
- ajouter "tous" dans le choix des tensions 
- ajout du bouton pour les actions courantes
- retirer les seconde de la date de mise en service

- la date dans la card états est celle du jour

- bouton ajpout etat inopérant

- mettre le bandeua de recherhche en scrollspy
- créer des liens vers les parties admin (ajout lieux),
- pouvoir dupliquer une fiche


- possibiliter d'ajouter des photos
- créer un lien vers les courbes des batteries,
- prévoir une case à cocher pour ne voir que les battteries en service (filtre sur état)

- faire une colonne à gauche avec informations remplies en précisant les indispensables

- vérifier le POST dans les fichies php




/*********************fait*****************************/
- créer une sauvegarde brouillon,
- reprendre la gestion de la référence: la fonction est commune avec la crattion - il faut tenir commpte de l'autocompletion;
- empécher l'effacement de non défini
- afficher la liste de toutes les fiches lors de la premiére venue la page
- faire une fiche avec des tabs
- utiliser fieldset
- faire un tabspar catégorie (mécanique/électrique/..)
- mettre un champs RAZ sur chaque cards
- création d'un identifiant unique pour la batterie (différent de Id de la base). construction par concaténation: lieu_equipement_tension??	
- gérer le focis avec le bouton de retour liste
- suppression d'un lieu -> mettre à non défini / ne pas pouvoir l'effacer / ne pas l'afficher

/******************************************************/
FE - modification fiche
/******************************************************/

- retour liste de modification: il n'affiche que les éléments ayant la valeur que l'on vient de modifier

/*********************fait*****************************/
-htmlenitities: passage des accents
- empecher l'iinteraction avec les boutons pendant le chargement (comme pour la création)
- validation fiche : identique que pour la création
- modification de la fiche: le select s'ajoute: il faut purger la liste avant de l'écrire de nouveau
- le select est dupliquer
- interdire l'appui multiple sur modification comme la création fiche

/******************************************************/
FE - consultation fiche
/******************************************************/

- reprendre la gestion des erreurs des requetes AJAX
- gérer et vérifier les sipnners

/*********************fait*****************************/
- reprendre la gestion de la référence
- remplir les champs avec les valeurs (pas de valeurs dans le card!)
- dans l'autocmplete, ne mettre que de manière unique les noms (requte SQL dans PHP avec GROUP BY)
- gérer et vérifier les toasts
- ajouter un bouton pour ajouter une fiche
- autocmplete: si aucune fiche ne possède le lieux demandé, ne pas autorisé la poursuite ou mettre en grisé
- après un retour liste relancer une liste
- autocmplete: ne proposer que les nom de lieux possédant des fiches
- bouton effacer la recherche doit aussi supprimer le tableau


/******************************************************/
FE - creation fiche - gestion fiche
/******************************************************/
-inihibtion la validation de la création si les valuers de tension/lieux et équipements sont absents
- card deck
- à la création, la gestion de l'état ne s'affiche pas.


- gérer le tooltips de quitter
- impémenter le bouton quiter
- changer le'id des boutons du haut.
- vérifier l'unicité de la fiche (en tout cas, prévenir du doublon possible).

/*********************fait*****************************/
-éviter les sscintillements des éléemnts (buttons) au chargement (chargement du script dédié en haut de page + css (.element {display: none;}) +js normal)

- reprendre la gestion de la référence
- une fois le bouton valider la fiche appuyer, empécher de l'appuyer de nouveau tant que la page est ouverte
- appui sur bouton valider -> on ouvre automatiquement la page de gestion des fiches avec le tableau mis à jour
- lors de la premir affichage de la page, le select est vide.
- mettre les toast à hide pour gérer l'affichage
- traiter le bouton effacer recherche
- après RAZ de la boite, le place holder ne revient pas 
- (non retenu ou en mode admin) boutons pour effacer toutes les fiches
- il y a des soucis à l'initaitalisation de l'affichage de la page
- la référence de ne s'affiche plus
-le select ne fonctionne pas correctment après l'ajout d'un nouveau champs lieu - on perd le select
- ajouter un bouton "+" pour aller vers la création d'une fiche.
- créer un  bouton pour lister toutes les fiches des batteries




/*******************************************************/
horodatage
/*******************************************************/
- poubvoir prolonger la vie d'une batterie
-gestion des dates: 
	- date de mise en service
	- date de décharge: dépend de la technologie
	- date de retrait
cela permettra l'archivage.s
- prévoir un champs état de la batterie et associer un timestamp
- prévoir un champs date heure pour horodater la dernière modification
- prévoirunchamps pour connaitrre la dernière opération sur la batterie (consulataion, modification,....)



/*******************************************************/
FE - listes
/*******************************************************/
- prévoir un menu de listes déjà pré-établies:
	- liste des batteries actives
	- liste des batteries retirées,
	- liste des batteries périmé mais encore en service
- prévoir un champs recherche + vaste que celui de la référence pour aller cher des listes de batteries plsu fine.




/******************************************************/
FE - mise à jour des décharges
/******************************************************/	
- prévoir la sauvegarde des fichiers de courbes de déccharges



/******************************************************/
page admin de l'application
/******************************************************/
créer une page admin de l'appliaction:
-inséere le temps d'alerte avant l'échéance (stockage table?)
page de suivi des modifications
- créer une base de suivi des modifications
- crézer une interface de suivid es modifications




/******************************************************/
page login du site
/******************************************************/
- créer une page de login
- créer une page de mot de passe perdu -> envoi vers une page de récupération du mots de passe
- réaliser trois profils:
	- un profil admin pour gérer la base
	- un profil utilisateur surper simple
	- un profil expert pour dépouiller les résultats et valider les remplacements

	
	
	/******************************************************/
page administration du site - base
/******************************************************/
- créer une page d'adminsitration de la base: truncate des tables lieux, équipements,....
- créer un bouton pour vider toutes les tables,
- créer un bouton pour récuprer les id déjà utilisés,
- créer un bouton pour vider la base table_batteries (truncate),
- créer une interface pour entrer le délai avant préremtion de la batterie



/******************************************************/
BO / FE - gestion de l'état de la batterie
/******************************************************/
- ajouter une colone pour sépcifié l'état de la batterie (uutilisé, archéivé,..)
- modifier le centrage dans les le tableau de liste



/******************************************************/
SQL
/******************************************************/
- ajouter une colone pour état batteries
- ajouter une colone pour l'autonomie mesurée
- ajouter une colonne pôur l'autonomie attendue (modal 2 champs)
- faire une focntion de récupération des id déjà utilisés.
/******************************************************/
modal 
/******************************************************/
/*********************fait*****************************/
- ajuster la larguer des modals avec le contenu
/******************************************************/
admin lieux
/******************************************************/
-
- ajouter dans le client l'interdiction demettre des guillements simple ou doubleS
- changer les noms des fichiers (retrer badbat)
- mettre un repere pour montrer les lieux non utilisés par des fiches
- dans le tableau, mettre un lieu vers la gestion des fiches pour avoir toutes les fiches du lieux
- modal suppression remplcer les val par des texcts
- tooltips à revoir
-
-
/*********************fait*****************************/
- revoir la mise à jour lors de l'affcihage du premier écran (pas de tableau visible)
- largeur max colonne dans table responsive
- metre le curseur / spinner  en atnete durenat les requetes AJAX
- gere le focus du moadl' ajout lieu)
- appui sur la touche entrée avec le modal ouverture
- vérifeier le container fluid dans le modal
- placement des toast sur mobile
- probleme avec google chrome-> target->currentTarget
- pas de mise à jour de la luste lors de la première arrivée sur la paage
- vérifier la longeur des mots avec legnth
- placement des toasts
- vérifier le alert
- mettre des limtes sur les valeurs numériques,
- mettre des limites sur les inputs en accord avac la valeur mise dans la table
- ajouter les opposées dans les tests des champs
- mettre un pattren si ncésaire sur les inputs,
- réaliser requete préparées,
- modal modifications: s'assurer que le nom entrée est différent de celui qui existe
- passage en majuscule de tous les noms de lieux
- faire le tri avec data et datindex
- remplacer tous les mnémoniques pour être cohérents avec la regle de nommage
- modal modifications : pas de focus
-passer modification au signulieur/ vérifier suppression
- verification du pattern du coté php
- regarder pour inclure un fichier de constante dans JS
- suppresion des variables en dur
- fonctions de callback pour nettoyer avant l'ouverture modal
- mettre un test sur la longueur du texte coté serveur
/******************************************************/