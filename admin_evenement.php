<!--		administration de la table evenement des batteries		
				date:26/05/2020
-->
<?php 
	//chargement des constantes 
	include ("./constantes/badbat_constante.inc");
?>
<div class="container-fluid" >
	<div  class="row" >
		<div class="col-lg-12">
			<h1> Administration des événements </h1>
		</div>
	</div>
	
	<div  class="row align-item-center">
		<div class="col-lg-2">nombre d'événements définis:</div>
		<div class="col-lg-1"><span id="nombre_evenement">0</span></div>
		<div  class="offset-lg-5 col-lg-4">
			<button class="btn btn-primary"  id="ajout_evenement" name="ajout_evenement" data-toggle="tooltip" data-placement="top"
                title="ajout d'un évenement" 	value="ajout_evenement">	
				<span id="ajout_evenement_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				ajouter un évenement
			</button>
			<button class="btn btn-danger"  id="suppression_evenement_tous" name="suppression_evenement_tous" data-toggle="tooltip" data-placement="top"
                title="suppression de tous les evenements"  	value="suppression_evenement_tous">	
				<span id="suppression_evenement_tous_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				supprimer tous les évenements
			</button>
		</div>
	</div>	
		<div  class="row align-items-center my-3" >
		<div class=" col-lg-12">
			<h3> liste des evenements présents dans la base </h3>
			<div class="table-responsive ">
				<table class="table  text-center align-middle text-left table-condensed table-stripped">
					<thead>
						<tr>
							<th scope="col">	référence						</th>
							<th scope="col">	nom de l'évenement				</th>
							<th scope="col">	état précédent					</th>
							<th scope="col">	état suivant					</th>
							<th scope="col">	temps de maintien				</th>
							<th scope="col">	utilisateur prévenu				</th>
							<th scope="col">	temps de prevenance				</th>
							<th scope="col">	utilisateur prévenu				</th>
							<th scope="col">	divers							</th>
							<th scope="col">	modifier						</th>
							<th scope="col">	supprimer						</th>
						</tr>
					</thead>
					<tbody id="table_evenement">
						<!-- insertion des données par jquery depuis une requête AJAX -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



	<div class="modal fade" id="modal_suppression_evenement_tous" tabindex="-1" role="dialog" aria-labelledby="modal_suppression_evenement_tous" aria-hidden="true">
		<div class="modal-dialog  " role="document">
			<div class="modal-content ">
				<div class="modal-header my_modal_header_suppression">
					<h5 class="modal-title">suppression de tous les evenements</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> 
					<div class="container-fluid">
						<span>êtes vous sur de vouloir supprimer tous les evenements de la liste?</span>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button aria-disabled="true" type="submit" class="btn btn-danger" id="modal_suppression_evenement_tous_button">
						 supprimer
						 </button>
				</div>
			</div>
		</div>
	</div>

	<?php
	//chargement des toasts de validation/echec enregistrements
		include ("./code/evenement/modal_evenement.php");
	?>	
	<?php
//chargement des toasts de validation/echec enregistrements
	include ("./code/toast_perso.php");
?>	
<!--<script src="js/badbat/common_admin_evenement.js"></script> -->
<script src="js/moment/moment-with-locales-2.26.0.js"></script>
<script src="js/badbat/admin_evenement.js"></script> 