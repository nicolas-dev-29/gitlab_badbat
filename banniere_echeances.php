		<div id= "banniere_echeances" class="container-fluid">
				<div  class="row">
				<div class="col-lg-10">
						<h3> echéances à venir </h3>
					</div>
					<div class="col-lg-02">
						<button class="btn btn-primary"  id="maj_echeances" name="maj_echeance" data-toggle="tooltip" data-placement="top"
							title="mise à jour des échéances" 	value="maj_echeance">	
							vérifications échéances
						</button> 
					</div>
				</div>
				<div class="row d-flex align-items-stretch    justify-content-center my-2" id="index_echeances_affichage">
						<div class=" col-auto   d-flex justify-content-center align-items-center banniere_echeances_off ">
							<span class="mx-2 fa fa-bell fa-1x"> </span>
							<span id="banniere_echeances_changement_badge" class="badge-pill badge-info">0</span>
							<button id="banniere_echeances_changement_button" class=" btn   btn-danger text-right element_cache_debut" type="button"  >
									<span class="fa fa-angle-double-down fa-1x "> </span>
							</button>
						</div>
						<div class=" col-auto mx-auto banniere_echeances_off ">
							<span class="fa fa-exclamation-triangle fa-1x"> </span>
							<span id="banniere_echeances_prevenance_badge" class="badge-pill badge-info">0</span>
							<button id="banniere_echeances_prevenance_button" class=" btn   btn-warning text-right element_cache_debut" type="button"  >
									<span class="fa fa-angle-double-down fa-1x "> </span>
							</button>
						</div>
						<div class="col-auto  d-flex justify-content-center align-items-center  banniere_echeances_off mx-2">
							<div class="row">
								<div class="col-1">
									<span class="fa fa-wrench fa-1x"> </span>
								</div>
								<div id="banniere_echeances_derniere_operation" class="col-11 element_cache_debut">
									<span id="banniere_echeances_derniere_operation_texte">zzz</span>
								</div>
							</div>
						</div>
				</div>
		</div>
